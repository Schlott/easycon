# coding: utf8

# choice groups
ATTRIBUTE_CHOICES = [ 'required','optional','do_not_ask']
ATTENDEE_ITEM_ACTIONS = ['view', 'booked', 'declined', 'delete', 'waitlist', 'moveup_waitlist','moveup_notify']
AUTO_GROUPS = [ 'registered','attendee','needs_to_pay','waitlist', 'deregistered', 'con_orga', 'con_admin', 'easycon_admin', ]
BOOL_ATTRIBUTE_CHOICES = [ 'optional','do_not_ask']
BUYITEM_STATE_CHOICES = [ 'booked','declined','waitlist']
CONVENTION_STATE_CHOICES = [  'requested','to_be_configured','planned', 'open', 'over', 'declined','to_be_deleted', 'active'] # planned and over is deprecated
DATATYPE_CHOICES = [ 'string', 'double', 'integer', 'numeric', 'date', 'datetime', 'boolean']
DATA_EXPORT_CHOICE = [ 'event',  'empty_event', 'attendee', 'host', 'boughtitem', 'service', 'bedroom', 'table', 'convention_config'] #, 'special_needs_per_item']
DATA_EXPORT_FORMAT_CHOICE = ['htmltable', 'tsv', 'csv']
EMAIL_ADDRESSEE_ROLES = [ 'to', 'cc', 'reply to']
EMAIL_FUNCTIONS = [ 'generic','attendee_registered','got_money','registered_for_waitlist','account_created','account_deleted','event_registered','new_attendee_event','new_attendee_convention']
ENTITY_CHOICES = [ 'attendance']#, 'game', 'event','service',]
EVENT_TYPE_CHOICES = ['rpggame','workshop','boardgame','tabletop','misc']
EVENT_FILTERS = EVENT_TYPE_CHOICES + ['free_online',]
FILE_EXTENSIONS = [ 'pdf','zip']
FONTS = ['Courier','Arial','Times','Symbol','Zapfdingbats']
IMAGE_EXTENSIONS = [ 'png','jpg','gif','bmp'] # as given by web2py
INVITATION_CHOICES = [ 'convention',]# 'event']
INVITATION_STATE_CHOICES = [ 'invited', 'accepted but not registered', 'accepted', 'declined', 'expired']
MENU_PAGES = ['convention', 'register', 'participants', 'manage_services', 'events', 'organize', 'configure_main', 'contact']
NEWSLETTER_CHOICES = ['daily', 'immediate',  'weekly', 'never']
NEWS_STATE_CHOICES = [ 'draft', 'requested', 'approved', 'published', 'declined']
PAPER_SIZES = ['A4','legal','letter']
PROGRAM_FILTER_CHOICE = ['events', 'personal_events', 'boughtitems', 'services']
REGISTRATION_STATE_CHOICES = [ 'registered', 'attendee', 'waitlist', 'deregistered']
REQUIRED_PERSON_FIELDS = ['ec_firstname','ec_nickname','ec_lastname','ec_email'] # these fields are always part of registration
RULE_TYPE_CHOICES = [ 'one_of_these_items' ,'combined_capacity','min_items','count_items']#, 'combine_these_items', 'max_items','requires','all_or_none']
SYSTEM_PAGES = [ 'No', 'convention_page', 'event_statistics', 'Registration', 'Participants', 'manage_services', 'Events', 'Organize', 'Contact', 'newevent','newgame','registration_summary']
TASK_PRIORITY_CHOICES = ['2', '1', '3']
TASK_PRIORITY_REPRESENT = {'1' : 'critical', '2': 'normal', '3': 'nice to have'}
TASK_STATE_CHOICES = ['2', '1', '3']
TASK_STATE_REPRESENT = { '1' : 'started', '2': 'to do', '3' : 'done'}
TEMPLATE_TYPES = ['rpgevent','event','generic']

RULE_EXPLANATIONS=dict(
one_of_these_items=T('These items are combined so that only one item can be chosen. All items will be grouped at the location of the first item.'),
combine_these_items=T('These items are aggregate and disappear from the list. Instead, the name of the rule will be shown, and the sum of all preselected items. The number of this rule is a fixed factor that will be added to the sum.'),
min_items=T('Of all of these items, a minimum number has to be bought for a valid registration'),
max_items=T('Of all of these items, a maximum number has to be bought for a valid registration'),
requires=T('The first item in the list requires that you buy all other items in the list'),
combined_capacity=T('These items have a combined maximum capacity; each booked item of this rule reduces the combined availability'),
count_items=T('Create a counter for these items; substracts the number of bought items from the rule number'),
)

# Which tables belong to a convention configuration in addition to ec_configuration?
# Code assumes that they all have a n:1 relationship with ec_convention
# and refer with ec_id_convention to it
CONFIG_TABLES = [	'ec_buyitem', 'ec_buyitemrule', 'ec_timeslot',
                    'ec_service', 'ec_bringthing', 'ec_extrafield',
                    'ec_bedroom', 'ec_page', 'ec_task',
                    'ec_in_series', 'ec_table']

# Date and Time strings
dateformatstring = T('%Y-%m-%d', lazy=False)
timeformatstring = str(T('%H:%M'))
datetimeformatstring = str(T('%Y-%m-%d %H:%M'))
monthyearformatstring = str(T('%B %Y'))

# map value: technical terms to printable text (and translate)
def MV(value, convention=None, lazy=True):
    mv = {
    'No': T('No', lazy=lazy),
    'admin': T('Admin group', lazy=lazy),
    'already_registered': T('You are already registered', lazy=lazy),
    'at_the_end': T('At the end', lazy=lazy),
    'attendance': T('Attendance', lazy=lazy),
    'attendees': T('Attendees', lazy=lazy),
    'basic_config': T('Basic configuration', lazy=lazy),
    'bedrooms': T('Bed rooms', lazy=lazy),
    'boardgame': T('Board game event', lazy=lazy),
    'boolean': T('True or false', lazy=lazy),
    'boughtitems': T('Bought items', lazy=lazy),
    'bringthings': T('Needed things', lazy=lazy),
    'buyitemrules': T('Rules for registration', lazy=lazy),
    'buyitems': T('Buyable items', lazy=lazy),
    'click_to_events_at_table': T('Click to assign events to table', lazy=lazy),
    'con_invitations': T('Convention invitations', lazy=lazy),
    'configure_emails': T('Configure system emails', lazy=lazy),
    'configure_main': T('Configure', lazy=lazy),
    'contact': T('Contact', lazy=lazy),
    'convention_config': T('Convention configuration', lazy=lazy),
    'convention_configs': T('Convention configuration', lazy=lazy),
    'convention_page': T('Convention main page', lazy=lazy),
    'csv': T('CSV (Plain text)', lazy=lazy),
    'data': T('Data export', lazy=lazy),
    'date': T('A calendar date', lazy=lazy),
    'datetime': T('A date with time', lazy=lazy),
    'double': T('Fraction numbers', lazy=lazy),
    'ec_attendee_count': T('Attendee count', lazy=lazy),
    'ec_attendee_names': T('Attendee names', lazy=lazy),
    'ec_end' : T('End', lazy=lazy),
    'ec_id_rpggame' : T('Role play game', lazy=lazy),
    'ec_id_timeslot' : T('Time slot', lazy=lazy),
    'ec_introduction' : T('Open for people new to the game?', lazy=lazy),
    'ec_max_attendees': T('Maximum number of attendees', lazy=lazy),
    'ec_min_attendees': T('Minimum number of attendees', lazy=lazy),
    'ec_organized_by' : T('Organized by', lazy=lazy),
    'ec_prepared_chars': T('Prepared character sheets?', lazy=lazy),
    'ec_registration_period_stop': T('End of registration period', lazy=lazy),
    'ec_rpg_newbees': T('Open for people new to RPG?', lazy=lazy),
    'ec_special_needs': T('Special needs', lazy=lazy),
    'ec_start': T('Begin', lazy=lazy),
    'ec_vegetarians': T('Vegetarians', lazy=lazy),
    'emails': T('Emails', lazy=lazy),
    'empty_events': T('Empty programme sheets', lazy=lazy),
    'empty_page': T('Empty page', lazy=lazy),
    'event': T('Event', lazy=lazy),
    'event_statistics': T('Event statistics', lazy=lazy),
    'events': T('Events', lazy=lazy),
    'events_at_table': T('Assign events to table', lazy=lazy),
    'export_config': T('Export configuration', lazy=lazy),
    'extra_text_pdf_only': T('Extra text (PDF only)', lazy=lazy),
    'extradata': T('Additional fields for registration', lazy=lazy),
    'extradata': T('Additional fields for registration', lazy=lazy),
    'extrafields' : T('Additional fields for registration', lazy=lazy),
    'files': T('Files', lazy=lazy),
    'free_online': T('Vacancies (online)', lazy=lazy),
    'game': T('Game', lazy=lazy),
    'games': T('Games',lazy=lazy),
    'grant_conventions': T('Grant installation', lazy=lazy),
    'host': T('Host', lazy=lazy),
    'htmltable': T('HTML table', lazy=lazy),
    'in_seriess': T('Participation in convention series', lazy=lazy),
    'in_series': T('Participation in convention series', lazy=lazy),
    'include_registered': T('Include attendees of state "%s"', lazy=lazy) % T('Data registered'),
    'integer': T('Whole numbers', lazy=lazy),
    'invite_nok': T('Although you do have an invitation, there is no capacity left', lazy=lazy),
    'invite_ok': T('You can use an invitation', lazy=lazy),
    'items': T('Buyable items', lazy=lazy),
    'item_booked_attendee_count': T('How many did buy this item? (Status: Attendee)', lazy=lazy),
    'item_booked_registered_count': T('How many did buy this item? (Status: Data registered)', lazy=lazy),
    'item_waitlist_attendee_count': T('How many wait for this item? (Status: Attendee)', lazy=lazy),
    'item_waitlist_registered_count': T('How many wait for this item? (Status: Data registered)', lazy=lazy),
    'item_waitlist_waitlist_count': T('How many on the waitlist would buy this item?', lazy=lazy),
    'item_not_buy_count': T('How many did not buy or book this item? (All states)', lazy=lazy),
    'item_booked_attendee_names': T('Who bought this item? (Status: Attendee)', lazy=lazy),
    'item_booked_registered_names': T('Who bought this item? (Status: Data registered)', lazy=lazy),
    'item_waitlist_attendee_names': T('Who waits for this item? (Status: Attendee)', lazy=lazy),
    'item_waitlist_registered_names': T('Who waits for this item? (Status: Data registered)', lazy=lazy),
    'item_waitlist_waitlist_names': T('Who on the waitlist would buy this item?', lazy=lazy),
    'item_not_buy_names': T('Who did not buy this item? (All states)', lazy=lazy),
    'location': T('Location', lazy=lazy),
    'manage_accounts': T('Manage accounts', lazy=lazy),
    'manage_con_invitations': T('Manage convention invitations', lazy=lazy),
    'manage_conventions': T('Manage conventions', lazy=lazy),
    'manage_item_waitlist': T('Waitlist for %s') % T('buyable items', lazy=lazy),
    'manage_news': T('Manage news entries', lazy=lazy),
    'manage_people': T('Manage people', lazy=lazy),
    'manage_series': T('Manage convention series', lazy=lazy),
    'manage_services': T('Services', lazy=lazy),
    'manage_waitlist': T('Waitlist for %s') % T('participants', lazy=lazy),
    'misc': T('Misc event', lazy=lazy),
    'misc-short': T('Misc.', lazy=lazy),
    'myevents': T('events', lazy=lazy),
    'needs_to_pay': T('Needs to pay', lazy=lazy),
    'new_boardgame': T('New board game event', lazy=lazy),
    'new_misc': T('New misc event', lazy=lazy),
    'new_rpggame': T('New role play event', lazy=lazy),
    'new_tabletop': T('New table top event', lazy=lazy),
    'new_workshop': T('New workshop', lazy=lazy),
    'newevent': T('New event', lazy=lazy),
    'newgame': T('New game', lazy=lazy),
    'no_invite': T('This registration is only by invitation', lazy=lazy),
    'occupied_from': T('Occupied from', lazy=lazy),
    'occupied_until': T('Occupied until', lazy=lazy),
    'orga': T('Orga group', lazy=lazy),
    'orga_standard_tasks': T('Create standard tasks', lazy=lazy),
    'organize': T('Organize', lazy=lazy),
    'orgatasks': T('Orga tasks', lazy=lazy),
    'pages': T('Convention pages', lazy=lazy),
    'pdf': 'PDF',
    'people_in_room': T('Manage people in room', lazy=lazy),
    'person': T('Person', lazy=lazy),
    'personal_events': T('Personal events', lazy=lazy),
    'persons': T('Persons', lazy=lazy),
    'preview_registration': T('Preview registration', lazy=lazy),
    'register': T('Registration', lazy=lazy),
    'register_nok': T('You cannot register', lazy=lazy),
    'register_ok': T('You can register', lazy=lazy),
    'registered': T('Data registered', lazy=lazy),
    'registration_summary': T('Registration summary', lazy=lazy),
    'rpggame': T('Role play event', lazy=lazy),
    'rules': T('Rules for registration', lazy=lazy),
    'schedule_system_tasks': T('Schedule system tasks', lazy=lazy),
    'selectevent': T('Event selection', lazy=lazy),
    'series': T('Convention series', lazy=lazy),
    'service': T('Service', lazy=lazy),
    'services': T('Services', lazy=lazy),
    'special_needs_per_items': T('Special needs per item', lazy=lazy),
    'string': T('Text values', lazy=lazy),
    'tables': T('Tables', lazy=lazy),
    'tabletop': T('Table top event', lazy=lazy),
    'tasks': T('Tasks', lazy=lazy),
    'timeslots': T('Time slots for events', lazy=lazy),
    'tsv': T('TSV (Excel compatible)', lazy=lazy),
    'workshop': T('Workshop', lazy=lazy),
    }
    if value==True:
        return T('Yes')
    elif value==False:
        return T('No')
    elif value is None:
        return ''
    try:
        return mv[value]
    except KeyError:
        return T(value)


# explain something
def EX(value, lazy=True):
    explanation = {
    'action_paid_all' : T('Changes "Payment so far" to the value of "Needs to pay", changes "State of registration" to "attendee", and sends an automated e-mail to the attendee', lazy=lazy),
    'activate' : T('Changes "State of registration" to "attendee" and sends an automated message with the amount that was already paid to the attendee', lazy=lazy),
    'admin_group' : T('Accounts in the admin group may change the configuration of the convention. They should know what they are doing', lazy=lazy),
    'attendees': T('Manage attendees and their registrations', lazy=lazy),
    'bedrooms': T('Create bedrooms for your attendees and assign attendees to bedrooms', lazy=lazy),
    'configure_basic_config' : T('Visibility, convention series and if the convention needs registration, services, etc'),
    'configure_design' : T('Some adjustments in the design, like background color or a logo', lazy=lazy),
    'configure_emails' : T('The text and addressees of automated e-mails used by the system', lazy=lazy),
    'configure_emails': T('See and change the e-mails that are sent out by the system', lazy=lazy),
    'configure_events' : T('If your convention has pre-registered events, you can configure visibility, dates, event types, time slots or the pre-registration for events', lazy=lazy),
    'configure_extradata' : T('If your registration form requires additional fields, you can create them here', lazy=lazy),
    'configure_items' : T('Often, people need to buy for things to register for a convention, like the registration fee, meals or the convention mug', lazy=lazy),
	'configure_location' : T('Information about the location, directions, and contact phone numbers', lazy=lazy),
	'configure_main' : T('Basic information for your convention, like dates, organizer, status, or description on homepage', lazy=lazy),
    'configure_registration' : T('If your convention requires pre-registration, you can configure it here: like the registration form, deadlines, visibility of attendee list, and your account information', lazy=lazy),
    'configure_rules' : T('There might be rules for buyable items in the registration form, like "choose only one of these two items". A counter of available items ("free capacity") is such a rule, too', lazy=lazy),
    'con_invitations': T('Create and send invitations for your convention', lazy=lazy),
    'daily' : T('You will get a daily email with all entries (if there are some)'),
    'data': T('Export data of your convention in various formats', lazy=lazy),
    'decline_registration' : T('De-register attendee', lazy=lazy),
    'delete_registration': T('This will delete all data for this user which is associated with that convention. You will not be able to restore it. If you just want to de-register the user, use deregistration.'),
    'deregistration': T('Deregistration: Changes "State of registration" to "declined",  sets status of all bought items to "declined", deregisters attendee from events, services, and bedrooms, and sends an e-mail to the attendee that she has been de-registered from the convention'),
    'deregistration_no_auto_deletion_of_events': T('Registered events will not be automatically deleted, but the registration status of the organizer is shown'),
    'edit_personal_data_by_orga' : T('You can edit the personal data of this user. You should do this only if the user asks you to do so, since this data might be used by other conventions, too', lazy=lazy),
    'edit_registration' : T('Change the registration data', lazy=lazy),
    'emails': T('Write e-mails to attendees', lazy=lazy),
    'emails_escape' : T('You can use variables in the %(variable)s syntax. Example: "Hello %(firstname)s" becomes "Hello Ruth" if "Ruth" is the firstname of the attendee you address. Note: If you want to use a % without a variable (e. g., within an URL or in your normal email text), you have to escape it by writing it twice. Example: "You get a 10%% discount" becomes "You get a 10% discount". Unfortunately, this feature is quite error-prone, so if you get internal errors, try to remove variables until it works.', lazy=lazy),
    'events': T('See, create and change events', lazy=lazy),
    'event_statistics': T('Do we need more events?'),
    'export_config': T('You can export a full convention configuration as JSON file'),
    'family_programme': T('A combined programme for all attendees who are accessible from your account'),
    'files': T('Upload and manage images and downloadable files for your con pages', lazy=lazy),
    'immediate': T('If a new entry is published, you will get an immediate email'),
    'manage_attendee' : T('Manage all registration data, like data from the registration form and additional fields, bought items, and personal data', lazy=lazy),
    'manage_news': T('In Easy-Con, every convention orga member can suggest news entries. The Easy-Con-Admins decide whether they will be included in the newsletter'),
    'manage_waitlist': T('See and manage the waitlist for your convention and for certain buyable items', lazy=lazy),
    'move_up' : T('Register attendee and buy all waitlist items that are available; you can adjust the bought items before email is sent', lazy=lazy),
    'myevents': T('Search and export your events', lazy=lazy),
    'never': T('You will not get any newsletter emails'),
    'news_for_conventions': T('If a news entry is assigned to a convention, every orga group member of this can convention can manage it'),
    'news_status_draft': T('The entry will be only saved and can be further edited'),
    'news_status_requested': T('The Easy-Con-Admins get a message and can approve the entry'),
    'no_attendees': T('This convention is not configured to accept registrations', lazy=lazy),
    'no_change_for_published_news': T('After a news entry is published, it cannot be changed or deleted'),
    'no_config_state_requested' : T('Your convention installation has not been approved yet', lazy=lazy),
    'no_config_state_to_be_configured' : T('You have to copy or create a configuration for your convention', lazy=lazy),
    'no_con_invitations': T('This convention is not configured to accept convention invitations', lazy=lazy),
    'no_events': T('This convention is not configured to have events', lazy=lazy),
    'no_other_registration': T('At this convention, you can only register yourself', lazy=lazy),
    'no_preview_registration':  T('This convention is not configured to accept registrations', lazy=lazy),
    'no_services':  T('This convention is not configured to have services', lazy=lazy),
    'no_tables': T('This convention is not configured to manage tables', lazy=lazy),
    'no_timeslots' : T('This convention is not configured to have timeslots', lazy=lazy),
    'no_waitlist': T('This convention is not configured to have a waitlist', lazy=lazy),
    'no_waitlist_items': T('There are no buyable items configured to have a waitlist', lazy=lazy),
    'only_german': T('We are sorry that this information is only available in German'),
    'orgatasks': T('Create a to-do list for your convention and assign members of the orga team to it', lazy=lazy),
    'orga_group' : T('Accounts in the orga group may do all the things under "organize"', lazy=lazy),
    'orga_standard_tasks' : T('Choose and create typical tasks needed for organizing conventions'),
    'pages': T('Create convention specific pages or additional content for Easy-Con system pages', lazy=lazy),
    'paid_something' : T('Enter an amount that this person has already paid. Does not send any e-mail (use "activate" for this)', lazy=lazy),
    'personal_event': T('A personal event is just for you - no attendees, not visible to others'),
    'personal_event': T('A personal event is just for you - no attendees, not visible to others'),
    'personal_programme': T('Your personal overview', lazy=lazy),
    'preview_registration' : T('Show how the registration form would look like', lazy=lazy),
    'preview_registration': T('preview_registration', lazy=lazy),
    'preview_registration': T('This is how the registration form looks like', lazy=lazy),
    'publish_date_news': T('As soon as it is approved, the news will appear at the newsletter page (but not before that date). Users with policy "immediate" will receive it, and it be part of the next newsletter'),
    'remind_one_orgatask': T('Send reminder email for this task, either to responsible orga member (if assigned) or to all orga members (if not)'),
    'remind_orgatask' : T('Send reminder email with an overview of open task to all orga members'),
    'selectevent': T('You can select one of these events by clicking on the buttom in the left column'),
    'send_email_to_groups' : T('You can send emails to groups of attendees with a common status', lazy=lazy),
    'services': T('Create helper services for your convention', lazy=lazy),
    'state_active': T('This convention is active; depending on its configuration and the current date, it is open, closed or over', lazy=lazy),
    'state_declined' : T('The request for your convention installation has been declined', lazy=lazy),
    'state_open' : T('This convention is open for registrations', lazy=lazy),
    'state_over' : T('This convention is over', lazy=lazy),
    'state_planned' : T('This convention is planned (this state will disappear soon)', lazy=lazy),
    'state_to_be_configured' : T('You can configure your convention. When your configuration is finished, change state to "planned"', lazy=lazy),
    'state_to_be_deleted' : T('Your convention is about to be deleted', lazy=lazy),
    'table_validation_no_end': T('These events have not end time and therefor could not be checked'),
    'table_validation_no_max_attendees': T('These events have no maximum attendees specified and therefore could not be checked'),
    'table_validation_no_start': T('These events have not start time and therefor could not be checked'),
    'table_validation_overlap': T('These events overlap in time'),
    'table_validation_too_early': T('These events begin before the table is available'),
    'table_validation_too_late': T('These events end when the table is not available anymore'),
    'table_validation_too_many_attendees': T('These events have a maximum attendees bigger than the capacity of this table'),
    'timeslots': T('Create time slots for your events', lazy=lazy),
    'waitlist_convention' : T('The general waitlists contains people who wait for an option to participate', lazy=lazy),
    'waitlist_items' : T('The item waitlist contains attendees who participate and wait for a certain item to become available', lazy=lazy),
    'waitlist_move_to_waitlist': T('Move attendee to waitlist'),
    'waitlist_only' : T('You can register only for the waitlist', lazy=lazy),
    'weekly': T('You will get a weekly email with all entries (if there are some)'),
    }
    try:
        return explanation[value]
    except KeyError:
        return T('There is no explanation for %s yet', lazy=False) % value

# give a shorter label for grids
def short_label(value):
    sl = {
    'attendees': T('Attendees'),
    'bedrooms': T('Bed rooms'),
    'buy_attendees_count': T('# Buying Att.'),
    'buy_attendees_names': T('Buying Att.'),
    'buyitems': T('Bought items'),
    'days': T('Days at convention'),
    'ec_apply_online': T('Online attendees'),
    'ec_assigned_to' : T('Who?'),
    'ec_attendee_count': MV('ec_attendee_count'),
    'ec_attendee_names': MV('ec_attendee_names'),
    'ec_capacity': T('Capacity'),
    'ec_description': T('Description'),
    'ec_due_date': T('Due'),
    'ec_end': T('End time'),
    'ec_end_time': T('End time'),
    'ec_has_waitlist': T('Waitlist?'),
    'ec_hidden': T('Orga only?'),
    'ec_id_pdf': T('Event ID in PDF'),
    'ec_id_rpggame': T('Role play game'),
    'ec_id_timeslot': T('Time slot'),
    'ec_is_heading': T('Heading?'),
    'ec_is_image': T('Image?'),
    'ec_is_perday': T('Per day?'),
    'ec_is_preselected': T('Presel.?'),
    'ec_max_helpers': T('Max. helpers'),
    'ec_min': T('Min.'),
    'ec_min_helpers': T('Min. helpers'),
    'ec_name': T('Name'),
    'ec_newsletter_date' : T('In newsletter on'),
    'ec_order': T('#'),
    'ec_phonehome': T('Phone (home)'),
    'ec_phonemobil': T('Phone (mobile)'),
    'ec_phonework': T('Phone (work)'),
    'ec_potential_gamemaster': T('GM?'),
    'ec_price': T('Price'),
    'ec_public': T('Public?'),
    'ec_publish_date' : T('Publ. date'),
    'ec_registertime': T('Register date'),
    'ec_request_date': T('Requested on'),
    'ec_special_needs': MV('ec_special_needs'),
    'ec_special_needs': T('Needs?'),
    'ec_special_needs_all': MV('ec_special_needs'),
    'ec_special_needs_count': T('# attendees w. needs'),
    'ec_special_needs_count_all': T('# attendees w. needs'),
    'ec_special_needs_names_all': T('Attendees w. needs'),
    'ec_start': T('Start time'),
    'ec_start_date': T('Start'),
    'ec_start_time': T('Start time'),
    'ec_type': T('Type'),
    'ec_vegetarians': T('Vegis'),
    'ec_vegetarians_count': T('# vegis'),
    'ec_vegetarians_count_all': T('# vegis'),
    'ec_vegetarians_names': T('Vegis'),
    'ec_vegetarians_names_all': T('Vegis'),
    'events': T('Events'),
    'helpers': T('Helpers'),
    'hosts': T('Registered by'),
    'include_registered': T('With registered'),
    'item_booked_attendee_count': T('# Bought (Attendee)'),
    'item_booked_registered_count': T('# Bought (Data registered)'),
    'item_waitlist_attendee_count': T('# Waiting (Attendee)'),
    'item_waitlist_registered_count': T('# Waiting (Data registered)'),
    'item_waitlist_waitlist_count': T('# Would buy (Waitlist)'),
    'item_not_buy_count': T('# Not bought (All states)'),
    'item_booked_attendee_names': T('Bought (Attendee)'),
    'item_booked_registered_names': T('Bought (Data registered)'),
    'item_waitlist_attendee_names': T('Waiting (Attendee)'),
    'item_waitlist_registered_names': T('Waiting (Data registered)'),
    'item_waitlist_waitlist_names': T('Would buy (Waitlist)'),
    'item_not_buy_names': T('Not bought (All states)'),
    'needs_to_pay': T('Needs to pay'),
    'nosort_slot': T('Time slot'),
    'number_of_attendees': T('Number of attendees'),
    'occupied_from': T('Occupied from'),
    'occupied_until': T('Occupied until'),
    'organized_by': T('Organized by'),
    'registration_lines': T('Lines to register'),
    'services': T('Services'),
    'table': T('Table'),
    'tables': T('Tables'),
    'v_helper_count': T('Needed'),
    'v_helpers': T('Helpers'),
    'vacancies': T('Vacancies'),
    }
    try:
        return sl[value]
    except KeyError:
        if value.startswith('extra_'):
            extra = db.ec_extrafield(value[len('extra_'):])
            if extra:
                return extra.ec_name
        if value.endswith('_as_columns'):
            return "%s (%s)" % (sl[value[:-len('_as_columns')]], T('as columns'))
        return value

# custom validators
class IS_POSITIVE_INT_LIST(object):
    def __call__(self, value):
        mylist = value.split(",")
        for item in mylist:
            try:
                i = int(item)
            except:
                return item, T('All list items need to be numbers')
        return (value, None)

class IS_SLUG_FILENAME(object):
    def __call__(self, value):
        for ext in IMAGE_EXTENSIONS + FILE_EXTENSIONS:
            if value.endswith(ext):
                newvalue, error = IS_SLUG()(value[:-len(ext)])
                if error:
                    return (value, error)
                else:
                    return ('%s.%s' % (newvalue, ext), None)
        return (value, T('When you change the filename, do not change the extension'))

class IS_NAME_EMAIL(object):
    def __call__(self, value):
        if value.find('<') > -1:
            email=value.split('<')[1].rstrip()
            if email.endswith('>'):
                return IS_EMAIL()(email[:-1])
            else:
                return IS_EMAIL()('Gruetze')
        else:
            email=value
        if IS_EMAIL()(email)[1] != None:
            return IS_EMAIL()(email)
        else:
            return (value, None)

class IS_EMAIL_CSV_LIST(object):
    def __call__(self, value):
        value = value.lower()
        mylist = value.split(",")
        for email in mylist:
            email, error = IS_EMAIL()(email.strip())
            if error != None:
                return None, T('All list items need to be email addresses (even "%s")') % email
        return (value, None)

class IS_ISO_8859_1(object):
    def __call__(self, value):
        try:
            newvalue = str(value, 'utf-8').encode('iso-8859-1')
            return (value, None)
            # if newvalue != value:
                # return (value, T('Please do not use any special characters (like "–", a long dash) since they may hinder the PDF export'))
            # else:
                # return (value, None)
        except:
            return (value, T('Please do not use any special characters (like "–", a long dash) since they may hinder the PDF export'))

# tables
db.define_table('ec_configuration',
    SQLField('ec_name', type='string', default= 'Unnamed Configuration', label=T('Name of the configuration'), readable=True, writable=True,  ),
    SQLField('ec_convention_public', type='boolean', default=False, label=T('Should the convention be visible in public EasyCon lists?'), readable=True, writable=True,  ),
    SQLField('ec_convention_requires_registration', type='boolean', default=False, label=T('Does this convention have a registration?'), readable=True, writable=True,  ),
    SQLField('ec_convention_requires_services', type='boolean', default=False, label=T('Does this convention need services from attendees?'), readable=True, writable=True,  ),
    SQLField('ec_convention_show_bedrooms', type='boolean', default=False, label=T('Should the rooms be showed to the participants yet?'), readable=True, writable=True,  ),
#    SQLField('ec_convention_imprint', type='text', label=T('Imprint'), readable=True, writable=True, comment=T('Convention specific add-ons to the imprint'),represent=lambda x, row: MARKMIN(x) ),
#    SQLField('ec_convention_privacy', type='text', label=T('Privacy notitce'), readable=True, writable=True, comment=T('Convention specific add-ons to the privacy notice'),represent=lambda x, row: MARKMIN(x) ),
    SQLField('ec_design_logo', type='upload', label=T('A logo of the convention (will replace the Easy-Con logo)'), readable=True, writable=True,  ),
    SQLField('ec_design_logo_height', type='string', required=True, notnull=True, default= '150px', label=T('The height of the logo'), readable=True, writable=True, comment=T('Enter an HTML conform value, e.g., 150px'), ),
    SQLField('ec_design_backgroundcolor', type='string', default= '#3333CC', label=T('Which background color shall be used for this convention?'), readable=True, writable=True, comment=T('Enter an HTML color code or color name'), ),
    SQLField('ec_design_background', type='upload', label=T('Background image'), readable=True, writable=True,),
    SQLField('ec_design_pdf_font', type='string', required=True, notnull=True, default= 'Arial', label=T('Which font should be used in PDF exports?'), requires=IS_IN_SET(FONTS, zero=None), readable=True, writable=True,  ),
    SQLField('ec_design_pdf_logo', type='upload', label=T('Which logo should be used in PDF exports?'), comment=T('Not implemented yet'), readable=True, writable=True,  ),
    SQLField('ec_location_show_location', type='boolean', required=True, notnull=True, default=False, label=T('Show location information on convention start page'), readable=True, writable=True,  ),
    SQLField('ec_location_address', type='string', label=T('Address'), readable=True, writable=True,  ),
    SQLField('ec_location_zip', type='string', label=T('Zip code'), readable=True, writable=True,  ),
    SQLField('ec_location_city', type='string', label=T('City'), readable=True, writable=True,  ),
    SQLField('ec_location_country', type='string', label=T('Country'), readable=True, writable=True,  ),
    SQLField('ec_location_timezone', type='string', default='Europe/Berlin', label=T('Timezone'), readable=True, writable=True,  ),
    SQLField('ec_location_phone_before', type='string', label=T('Phone (before convention)'), readable=True, writable=True,  ),
    SQLField('ec_location_phone_at_convention', type='string', label=T('Phone (at convention)'), readable=True, writable=True, comment=T('This information is only shown to attendees'), ),
    SQLField('ec_location_description', type='text', label=T('Location'), readable=True, writable=True, comment=T('Description'),represent=lambda x, row: MARKMIN(x) ),
    SQLField('ec_location_directions', type='text', label=T('Directions'), readable=True, writable=True, represent=lambda x, row: MARKMIN(x) ),
    SQLField('ec_email_signature', type='text', default= '\n-- \nsent via Easy-Con (http://www.easy-con.org) for %(conname)s\n%(conwebsite)s', label=T('Which text shall be attached to every email?'), readable=True, writable=True,  ),
    SQLField('ec_email_tag', type='string', default= '[%(conname)s]', label=T('Which tag  shall be inserted to every email subject?'), readable=True, writable=True,  ),
    SQLField('ec_email_generic_bcc', type='string', default= '', label=T('Who else should get a blind carbon copy of this email?'), requires=IS_EMPTY_OR(IS_EMAIL_CSV_LIST()), readable=True, writable=True,  ),
    SQLField('ec_email_generic_orga', type='boolean', default= False, label=T('Shall the organisators get a blind carbon copy of this email?'), readable=True, writable=True,  ),
    SQLField('ec_email_generic_subject', type='string', default= '', label=T('Which subject shall be used for generic convention e-mail?'), readable=True, writable=True,  ),
    SQLField('ec_email_generic_text', type='text', default= '', label=T('Which text shall be used for generic convention e-mail?'), readable=True, writable=True,  ),
    SQLField('ec_email_registration_attendee_registered_subject', type='string', default= 'Registrierungsbestätigung. Bitte Con-Beitrag überweisen', label=T('Which subject shall be used if a new attendee registers?'), readable=True, writable=True,  ),
    SQLField('ec_email_registration_attendee_registered_text', type='text', default= 'Hallo %(firstname)s!\n\nWir haben kürzlich deine Anmeldung für %(conname)s erhalten. Du kannst deine Daten unter folgender URL ansehen:\n%(regurl)s\n\nDu hast Dir damit einen Platz auf der Teilnehmerliste reserviert. Um dir diesen Platz zu sichern, musst du den Unkostenbeitrag von %(fee)s auf folgendes Konto überweisen:\n%(account)s\n\nWenn der Zahlungseingang bei uns registriert wurde, schalten wir dich frei und du bekommst dann die Möglichkeit, Spielrunden oder andere Programmpunkte anzumelden. \n\nSolltest Du noch weitere Fragen haben, kannst du dich gerne jederzeit mit einer E-Mail an %(conemail)s an uns wenden.\n\nViele Grüße\nDie Orga', label=T('Which text shall be used if a new attendee registers?'), readable=True, writable=True,  ),
    SQLField('ec_email_registration_attendee_registered_orga', type='boolean', default=True, label=T('Shall the organisators get a blind carbon copy of this email?'), readable=True, writable=True,  ),
    SQLField('ec_email_registration_attendee_registered_bcc', type='string', default= '', label=T('Who else should get a blind carbon copy of this email?'), requires=IS_EMPTY_OR(IS_EMAIL_CSV_LIST()), readable=True, writable=True,  ),
    SQLField('ec_email_registration_got_money_subject', type='string', default= 'Geld erhalten', label=T('Which subject shall be used if we got the money from a attendee?'), readable=True, writable=True,  ),
    SQLField('ec_email_registration_got_money_text', type='text', default= 'Hallo %(firstname)s!\n\nVielen Dank für deine Anmeldung. Dein Con-Beitrag für den Con "%(conname)s ist %(fee)s. Wir haben von dir einen Beitrag in Höhe von %(payment)s erhalten und dein Status wurde auf "angemeldet" gesetzt.\nFalls du noch nicht alles bezahlt hast, denke bitte daran, den Restbeitrag rechtzeitig zu begleichen.\nSolltest du zu viel bezahlt haben, setze dich bitte mit der Con-Orga in Verbindung.\nSofern für diesen Con schon freigegeben, kannst du nun im System nun Spielrunden und andere Programmpunkte anmelden.\n\nViele Grüße\nDie Orga', label=T('Which text shall be used if we got the money from a attendee?'), readable=True, writable=True,  ),
    SQLField('ec_email_registration_got_money_orga', type='boolean', default=True, label=T('Shall the organisators get a blind carbon copy of this email?'), readable=True, writable=True,  ),
    SQLField('ec_email_registration_got_money_bcc', type='string', default= '', label=T('Who else should get a blind carbon copy of this email?'), requires=IS_EMPTY_OR(IS_EMAIL_CSV_LIST()), readable=True, writable=True,  ),
    SQLField('ec_email_registration_waitlist_subject', type='string', default= 'Platz auf der Warteliste', label=T('Which subject shall be used if someone registers and gets only the waitlist?'), readable=True, writable=True,  ),
    SQLField('ec_email_registration_waitlist_text', type='text', default= 'Hallo %(firstname)s!\n\nVielen Dank für deine Anmeldung für die Warteliste!\n\nDu hast den %(waitlist)s. Platz auf der Warteliste erhalten. Sollte ein Platz frei werden, senden wir dir eine E-Mail, wenn vor dir niemand mehr auf der Warteliste steht. Du hast dann drei Tage Zeit, auf diese E-Mail zu reagieren, ansonsten verfällt Dein Platz und wir schreiben die nächste Person auf der Liste an.\n\nViele Grüße\nDie Orga', label=T('Which text shall be used if someone registers and gets only the waitlist?'), readable=True, writable=True,  ),
    SQLField('ec_email_registration_waitlist_orga', type='boolean', default=True, label=T('Shall the organisators get a blind carbon copy of this email?'), readable=True, writable=True,  ),
    SQLField('ec_email_registration_waitlist_bcc', type='string', default= '', label=T('Who else should get a blind carbon copy of this email?'), requires=IS_EMPTY_OR(IS_EMAIL_CSV_LIST()), readable=True, writable=True,  ),
    SQLField('ec_email_registration_waitlist_moved2waitlist_subject', type='string', default='Platz auf der Warteliste', label=T('Which subject shall be used if someone was moved to the waitlist by the convention orga?'), readable=True, writable=True,  ),
    SQLField('ec_email_registration_waitlist_moved2waitlist_text', type='text', default='Hallo %(firstname)s!\n\nWir haben dich auf die Warteliste gesetzt.\n\nViele Grüße\nDie Orga', label=T('Which text shall be used if someone was moved to the waitlist?'), readable=True, writable=True,  ),
    SQLField('ec_email_registration_waitlist_moved2waitlist_orga', type='boolean', default=True, label=T('Shall the organisators get a blind carbon copy of this email?'), readable=True, writable=True,  ),
    SQLField('ec_email_registration_waitlist_moved2waitlist_bcc', type='string', default= '', label=T('Who else should get a blind carbon copy of this email?'), requires=IS_EMPTY_OR(IS_EMAIL_CSV_LIST()), readable=True, writable=True,  ),
    SQLField('ec_email_registration_waitlist_movedup_waitlist_subject', type='string', default='Nachrücken von der Warteliste', label=T('Which subject shall be used if someone was moved up from the waitlist?'), readable=True, writable=True,  ),
    SQLField('ec_email_registration_waitlist_movedup_waitlist_text', type='text', default='Hallo %(firstname)s!\n\nDu bist von der Warteliste nachgerückt und dein Status ist nun "Daten registriert". Bitte besuche folgende URL um deine Anmeldedaten zu sehen:\n%(regurl)s\n\nWir haben Dir damit einen Platz auf der Teilnehmerliste reserviert. Um dir diesen Platz zu sichern, musst du den Unkostenbeitrag von %(fee)s auf folgendes Konto überweisen:\n%(account)s\n\nWenn der Zahlungseingang bei uns registriert wurde, schalten wir dich frei und du bekommst dann die Möglichkeit, Spielrunden oder andere Programmpunkte anzumelden. \n\nSolltest Du noch weitere Fragen haben, kannst du dich gerne jederzeit mit einer E-Mail an %(conemail)s an uns wenden.\n\nViele Grüße\nDie Orga', label=T('Which text shall be used if someone was moved up from the waitlist?'), readable=True, writable=True,  ),
    SQLField('ec_email_registration_waitlist_movedup_waitlist_orga', type='boolean', default=True, label=T('Shall the organisators get a blind carbon copy of this email?'), readable=True, writable=True,  ),
    SQLField('ec_email_registration_waitlist_movedup_waitlist_bcc', type='string', default= '', label=T('Who else should get a blind carbon copy of this email?'), requires=IS_EMPTY_OR(IS_EMAIL_CSV_LIST()), readable=True, writable=True,  ),
    SQLField('ec_email_registration_decline_subject', type='string', default= 'Abmeldung', label=T('Which subject shall be used if someone de-registers from the convention?'), readable=True, writable=True,  ),
    SQLField('ec_email_registration_decline_text', type='text', default= 'Hallo %(firstname)s!\n\nDu wurdest von %(conname)s abgemeldet.\n\nWenn das nicht ist, was du wolltest, kontaktiere die Con-Orga.\n\nViele Grüße\nDie Orga', label=T('Which text shall be used if someone de-registers from the convention?'), readable=True, writable=True,  ),
    SQLField('ec_email_registration_decline_orga', type='boolean', default=True, label=T('Shall the organisators get a blind carbon copy of this email?'), readable=True, writable=True,  ),
    SQLField('ec_email_registration_decline_bcc', type='string', default= '', label=T('Who else should get a blind carbon copy of this email?'), requires=IS_EMPTY_OR(IS_EMAIL_CSV_LIST()), readable=True, writable=True,  ),
    SQLField('ec_email_event_registered_subject', type='string', default= 'Neuer Programmpunkt', label=T('Which subject shall be used if someone registers a new event?'), readable=True, writable=True,  ),
    SQLField('ec_email_event_registered_text', type='text', default= 'Hallo %(firstname)s!\n\nDu hast für "%(conname)s" folgenden Programmpunkt angemeldet:\n%(eventdata)s\n\nVielen Dank!\n\n Die Orga', label=T('Which text shall be used if someone registers a new event?'), readable=True, writable=True,  ),
    SQLField('ec_email_event_registered_orga', type='boolean', default=True, label=T('Shall the organisators get a blind carbon copy of this email?'), readable=True, writable=True,  ),
    SQLField('ec_email_event_registered_bcc', type='string', default= '', label=T('Who else should get a blind carbon copy of this email?'), requires=IS_EMPTY_OR(IS_EMAIL_CSV_LIST()), readable=True, writable=True,  ),
    SQLField('ec_email_event_new_attendee_subject', type='string', default= 'Neue Teilnehmerin bei Programmpunkt', label=T('Which subject shall be used if someone registers for an event?'), readable=True, writable=True,  ),
    SQLField('ec_email_event_new_attendee_text', type='text', default= 'Hallo,\n\n%(nickname)s hat sich für den Programmpunkt "%(eventname)" auf dem Con %(conname)s angemeldet.', label=T('Which text shall be used if someone registers for an event?'), readable=True, writable=True,  ),
    SQLField('ec_email_event_new_attendee_orga', type='boolean', default= False, label=T('Shall the organisators get a blind carbon copy of this email?'), readable=True, writable=True,  ),
    SQLField('ec_email_event_new_attendee_bcc', type='string', default= '', label=T('Who else should get a blind carbon copy of this email?'), requires=IS_EMPTY_OR(IS_EMAIL_CSV_LIST()), readable=True, writable=True,  ),
    SQLField('ec_email_promote_orga_group_subject', type='string', default= 'Du wurdest in die Orga-Gruppe aufgenommen', label=T('Which subject shall be used if someone gets promoted to orga group?'), readable=True, writable=True,  ),
    SQLField('ec_email_promote_orga_group_text', type='text', default= 'Hallo %(firstname)s,\n\ndu wurdest in die Orga-Gruppe des %(conname)s aufgenommen. Unter dem Menupunkt "Organisieren" kannst du nun unter anderem Teilnehmer verwalten, E-Mails an Teilnehmer schreiben und Daten exportieren.' , label=T('Which text shall be used if someone gets promoted to orga group?'), readable=True, writable=True,  ),
    SQLField('ec_email_promote_admin_group_subject', type='string', default= 'Du wurdest in die Admin-Gruppe aufgenommen', label=T('Which subject shall be used if someone gets promoted to admin group?'), readable=True, writable=True,  ),
    SQLField('ec_email_promote_admin_group_text', type='text', default= 'Hallo %(firstname)s,\n\ndu wurdest in die Admin-Gruppe des %(conname)s aufgenommen. Unter dem Menupunkt "Einstellungen bearbeiten" kannst du nun den Con konfigurieren. Sei vorsichtig!', label=T('Which text shall be used if someone gets promoted to admin group?'), readable=True, writable=True,  ),
    SQLField('ec_registration_orga', type='boolean', default=False, label=T('Is the registration pre-open for members of the orga group?'), readable=True, writable=True,  ),
    SQLField('ec_registration_period_start', type='datetime', label=T('When will the registration for this convention start?'), readable=True, writable=True,  ),
    SQLField('ec_registration_period_stop', type='datetime', label=T('When will the registration for this convention stop?'), readable=True, writable=True,  ),
    SQLField('ec_registration_currency_symbol', type='string', required=True, notnull=True, default= '€', label=T('Which currency symbol will be used for prices?'), readable=True, writable=True,  ),
    SQLField('ec_registration_other', type='boolean', default=False, label=T('Is it allowed to register other people for the convention (e.g., friends or family)?'), readable=True, writable=True,  ),
    SQLField('ec_registration_capacity', type='integer', required=True, notnull=True, default=-1, label=T('Capacity: how many (un-invited) people can register for that convention?'), readable=True, writable=True, comment=T('-1 : unlimited in general, will be determined by the buyable items; 0: only invited attendees'), ),
    SQLField('ec_registration_waitlist', type='boolean', required=True, notnull=True, default=False, label=T('Should this convention have a waitlist?'), readable=True, writable=True,  ),
    SQLField('ec_registration_invite_capacity', type='integer', required=True, notnull=True, default=0, label=T('How many invited people can register for that convention?'), readable=True, writable=True, comment=T('-1: unlimited; 0: invitations disabled'), ),
    SQLField('ec_registration_invite_text', type='text', default= 'Du wurdest mit dieser E-Mail zu %(conname)s eingeladen. Auf der Seite %(invite_url)s kannst du die Einladung annehmen und dich anmelden. Falls du sie nicht annehmen kannst oder möchstest, würde es uns helfen, wenn du sie dort ablehnen würdest.\n\nVielen Dank!\n\nDie Con-Orga', label=T('Which standard text shall be used for invitations?'), readable=True, writable=True, comment=T('Can be edited for each new invitation'), ),
    SQLField('ec_registration_show_realnames', type='boolean', required=True, notnull=True, default=False, label=T('Should firstname and lastname in partipants list  be shown to other participants?'), readable=True, writable=True,  ),
    SQLField('ec_registration_perday', type='boolean', default=False, label=T('Does the registration of the convention count some items per day?'), readable=True, writable=True,  ),
    SQLField('ec_registration_perday_arrival_days', type='string', required=True, notnull=True, default= '1', label=T('A list of valid arrival days'), requires=IS_POSITIVE_INT_LIST(), readable=True, writable=True, comment=T('e.g., "1,2": first and second day'), ),
    SQLField('ec_registration_perday_arrival_factor', type='double', default=0.5, label=T('A factor: how much should the arrival day be counted?'), readable=True, writable=True, comment=T('1 means full, 0.5 means half, …'), ),
    SQLField('ec_registration_perday_departure_days', type='string', required=True, notnull=True, default= '3', label=T('A list of valid departure days'), requires=IS_POSITIVE_INT_LIST(), readable=True, writable=True, comment=T('e.g., "3": third day'), ),
    SQLField('ec_registration_perday_departure_factor', type='double', default=0.5, label=T('A factor: how much should the departure day be counted?'), readable=True, writable=True, comment=T('1 means full, 0.5 means half, …'), ),
    SQLField('ec_registration_account', type='text', default='Kontoinhaber: ...\nIBAN: ...\nBIC: ...\nBank: ...\n\nVerwendungszweck: Unkostenbeitrag %(conname)s für: %(firstname)s %(lastname)s\n', label=T('Account information (e.g., for registration fees)'), readable=True, writable=True,  ),
    SQLField('ec_registration_show_rules', type='boolean', default=True, label=T('Show rules for buyable items in registration'), readable=True, writable=True,  ),
    SQLField('ec_registration_data_person_address', type='string', required=True, notnull=True, default= 'optional', label=T('Registration data: Address'), requires=IS_IN_SET(ATTRIBUTE_CHOICES), readable=True, writable=True,  ),
    SQLField('ec_registration_data_person_zip', type='string', required=True, notnull=True, default= 'optional', label=T('Registration data: Zip code'), requires=IS_IN_SET(ATTRIBUTE_CHOICES), readable=True, writable=True,  ),
    SQLField('ec_registration_data_person_city', type='string', required=True, notnull=True, default= 'optional', label=T('Registration data: City'), requires=IS_IN_SET(ATTRIBUTE_CHOICES), readable=True, writable=True,  ),
    SQLField('ec_registration_data_person_country', type='string', required=True, notnull=True, default= 'required', label=T('Registration data: Country'), requires=IS_IN_SET(ATTRIBUTE_CHOICES), readable=True, writable=True,  ),
    SQLField('ec_registration_data_person_phonehome', type='string', required=True, notnull=True, default= 'optional', label=T('Registration data: Phone number (at home)'), requires=IS_IN_SET(ATTRIBUTE_CHOICES), readable=True, writable=True,  ),
    SQLField('ec_registration_data_person_phonemobil', type='string', required=True, notnull=True, default= 'optional', label=T('Registration data: Phone number (mobile)'), requires=IS_IN_SET(ATTRIBUTE_CHOICES), readable=True, writable=True,  ),
    SQLField('ec_registration_data_person_phonework', type='string', required=True, notnull=True, default= 'optional', label=T('Registration data: Phone number (at work)'), requires=IS_IN_SET(ATTRIBUTE_CHOICES), readable=True, writable=True,  ),
    SQLField('ec_registration_data_person_birthday', type='string', required=True, notnull=True, default= 'optional', label=T('Registration data: Birthday'), requires=IS_IN_SET(ATTRIBUTE_CHOICES), readable=True, writable=True,  ),
    SQLField('ec_registration_data_person_potential_gamemaster', type='string', required=True, notnull=True, default= 'optional', label=T('Registration data: Potential game master'), requires=IS_IN_SET(BOOL_ATTRIBUTE_CHOICES), readable=True, writable=True,  ),
    SQLField('ec_registration_data_person_special_needs', type='string', required=True, notnull=True, default= 'optional', label=T('Registration data: Any special needs (allergies, etc)'), requires=IS_IN_SET(ATTRIBUTE_CHOICES), readable=True, writable=True,  ),
    SQLField('ec_registration_data_person_vegeterian', type='string', required=True, notnull=True, default= 'optional', label=T('Registration data: Vegeterian'), requires=IS_IN_SET(BOOL_ATTRIBUTE_CHOICES), readable=True, writable=True,  ),
    SQLField('ec_registration_data_attendance_arrival', type='string', required=True, notnull=True, default= 'required', label=T('Registration data: Day of arrival'), requires=IS_IN_SET(ATTRIBUTE_CHOICES), readable=True, writable=True, comment=T('If convention is to be configured to count some items per day, this field is always required'), ),
    SQLField('ec_registration_data_attendance_departure', type='string', required=True, notnull=True, default= 'required', label=T('Registration data: Day of departure'), requires=IS_IN_SET(ATTRIBUTE_CHOICES), readable=True, writable=True, comment=T('If convention is to be configured to count some items per day, this field is always required'), ),
    SQLField('ec_events', type='boolean', default=True, label=T('Will there be pre-registered events for this convention?'), readable=True, writable=True,  ),
    SQLField('ec_event_public', type='boolean', default=True, label=T('Should events be visible to the public?'), readable=True, writable=True,  ),
    SQLField('ec_event_show_statistics', type='boolean', required=True, notnull=True, default=False, label=T('Should we show statistics on registered events?'), readable=True, writable=True,  ),
    SQLField('ec_event_type_rpggame', type='boolean', required=True, notnull=True, default=True, label=T('Can events of type "RPG games" be registered?'), readable=True, writable=True,  ),
    SQLField('ec_event_type_tabletop', type='boolean', required=True, notnull=True, default=True, label=T('Can events of type "tabletop" be registered?'), readable=True, writable=True,  ),
    SQLField('ec_event_type_boardgame', type='boolean', required=True, notnull=True, default=True, label=T('Can events of type "board game" be registered?'), readable=True, writable=True,  ),
    SQLField('ec_event_type_workshop', type='boolean', required=True, notnull=True, default=True, label=T('Can events of type "workshop" be registered?'), readable=True, writable=True,  ),
    SQLField('ec_event_requires_registration', type='boolean', default=True, label=T('To register events, do people have to be registered for the convention?'), readable=True, writable=True,  ),
    SQLField('ec_event_registration_period_start', type='datetime', label=T('When will the event registration start?'), readable=True, writable=True,  ),
    SQLField('ec_event_registration_period_stop', type='datetime', label=T('When will the event registration stop?'), readable=True, writable=True,  ),
    SQLField('ec_event_registration_limit_eventorganizer', type='integer', default=-1, label=T('For how many events can someone register who organizes at least one event? (-1: unlimited)'), readable=True, writable=True,  ),
    SQLField('ec_event_registration_limit_normalattendee', type='integer', default=-1, label=T('For how many events can a normal attendee register? (-1: unlimited)'), readable=True, writable=True,  ),
    SQLField('ec_event_time_slots', type='boolean', default=True, label=T('Will there be time slots for events?'), readable=True, writable=True,  ),
    SQLField('ec_event_double_registration_timeslot', type='boolean', default=True, label=T('Can attendees register and/or organize more than one event in the same time slot?'), readable=True, writable=True,  ),
    SQLField('ec_event_free_time', type='boolean', default=False, label=T('Can events be registered for arbitrary start / end times?'), readable=True, writable=True,),
    SQLField('ec_event_free_time_outofcon', type='boolean', default=False, label=T('If events can be registered for arbitrary times, could their start / end times be outside the duration of the convention?'), readable=True, writable=True, ),
    SQLField('ec_event_attendance', type='boolean', default=False, label=T('Is it possible for attendees to register for events?'), readable=True, writable=True,  ),
    SQLField('ec_event_attendance_requires_registration', type='boolean', default=True, label=T('To register for events, do people have to be registered for the convention?'), readable=True, writable=True,  ),
    SQLField('ec_event_attendee_registration_period_start', type='datetime', label=T('When will the attendee registration start?'), readable=True, writable=True,  ),
    SQLField('ec_event_attendee_registration_period_stop', type='datetime', label=T('When will the attendee registration stop?'), readable=True, writable=True,  ),
    SQLField('ec_event_tables', type='boolean', required=True, notnull=True, default=False, label=T('Do you want to create tables for events?'), readable=True, writable=True,  ),
    SQLField('ec_event_tables_assignment_orga', type='boolean', required=True, notnull=True, default=True, label=T('Only the convention organizers can assign tables to events'), readable=True, writable=False, comment=T('Assignment by event organizers not implemented yet'), ),
    SQLField('ec_orgatask_reminder', type='boolean', required=True, notnull=True, default=False, label=T('Should there be automatic notifications of open tasks for the orga?'), readable=True, writable=True,  ),
    SQLField('ec_orgatask_reminder_start', type='date', default=request.now, label=T('When should notification of open tasks start?'), readable=True, writable=True,  ),
    SQLField('ec_orgatask_reminder_days', type='integer', default=7, label=T('Wie oft sollen die Erinnerungen versandt werden (in Tagen)?'), readable=True, writable=True,  ),
)
db.define_table('ec_convention',
    SQLField('ec_name', type='string', required=True, notnull=True, default= 'SomeCon', label=T('Convention name'), readable=True, writable=True,  ),
    SQLField('ec_short_name', type='string', required=True, notnull=True, label=T('Convention short name/acronym'), requires=[IS_NOT_EMPTY(), IS_ALPHANUMERIC(), IS_NOT_IN_DB(db, 'ec_convention.ec_short_name')], readable=True, writable=True, comment= '%s. %s. %s.' % (T('Will be used for URLs'),T('Must not contain spaces'),T('Example: BC15')), ),
    SQLField('ec_organizer', type='string', required=True, notnull=False, default=T('Not Easy-Con'), comment= T('Please enter the organizer'), label=T('Legal organizer (person or club)'), readable=True, writable=True,  ),
    SQLField('ec_description', type='text', label=T('Description'), readable=True, writable=True, comment=T('This description will be shown to Easy-Con users'),represent=lambda x, row: MARKMIN(x) ),
    SQLField('ec_start_date', type='date', label=T('Start date'), readable=True, writable=True,  ),
    SQLField('ec_end_date', type='date', label=T('End date'), readable=True, writable=True,  ),
    SQLField('ec_website', type='string', label=T('Web site'), requires=IS_EMPTY_OR(IS_URL()), readable=True, writable=True, comment=T('A link to an external website'),represent=lambda x, row: A(x, _href=x) if x else '' ),
    SQLField('ec_convention_state', type='string', required=True, notnull=True, default=  'requested', label=T('Convention state'), requires=IS_IN_SET(CONVENTION_STATE_CHOICES,zero=None), readable=True, writable=True,  ),
    SQLField('ec_contact_email', type='string', label=T('Contact e-mail (public)'), requires=IS_EMPTY_OR(IS_EMAIL()), readable=True, writable=True, comment=T('If set, this e-mail address will be official public contact e-mail'), ),
    SQLField('ec_hidden_email', type='string', label=T('Contact e-mail (hidden)'), requires=IS_EMPTY_OR(IS_EMAIL()), readable=True, writable=True, comment=T('If no public contact e-mail address is set, this address will be used for contact form e-mails from users'), ),
    SQLField('ec_admin_comment', type='text', label=T('Comment (for admins)'), readable=False, writable=False,  ),
    SQLField('ec_request_comment', type='text', label=T('Comment'), readable=False, writable=False, comment=T('Any comment for your request. If this is the first time you use Easy-Con please let us know what you want to do with it.'), ),
    SQLField('ec_request_date', type='date', default=request.now, label=T('Request date'), readable=True, writable=False,  ),
    SQLField('ec_id_configuration', db.ec_configuration, label=T('Configuration'), readable=False, writable=False,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_configuration.id', '%(ec_name)s')), represent=lambda id, row: '%(ec_name)s' % db.ec_configuration[id] if id else ''),
)
db.define_table('ec_person',
    SQLField('ec_firstname', type='string', required=True, notnull=True, default= '', label=T('First name'), readable=True, writable=True, comment=T('Will be shown only to other participants and to the orga'), ),
    SQLField('ec_lastname', type='string', default= '', label=T('Last name(s)'), readable=True, writable=True, comment=T('Will be shown only to other participants and to the orga'), ),
    SQLField('ec_nickname', type='string', required=True, notnull=True, default= '', label=T('Nickname'), readable=True, writable=True, comment=T('Your view name will be visible to other people, e.g., when you register an event. When in doubt, use your first name'), ),
    SQLField('ec_email', type='string', default= '', label=T('Email'), requires=IS_EMPTY_OR([IS_LOWER(),IS_EMAIL()]), readable=True, writable=True,  ),
    SQLField('ec_address', type='string', default= '', label=T('Address'), readable=True, writable=True,  ),
    SQLField('ec_zip', type='string', default= '', label=T('Zip code'), readable=True, writable=True,  ),
    SQLField('ec_city', type='string', default= '', label=T('City'), readable=True, writable=True,  ),
    SQLField('ec_country', type='string', default= '', label=T('Country'), readable=True, writable=True,  ),
    SQLField('ec_phonehome', type='string', default= '', label=T('Phone number (at home)'), readable=True, writable=True,  ),
    SQLField('ec_phonemobil', type='string', default= '', label=T('Phone number (mobile)'), readable=True, writable=True,  ),
    SQLField('ec_phonework', type='string', default= '', label=T('Phone number (at work)'), readable=True, writable=True,  ),
    SQLField('ec_birthday', type='date', label=T('Birthday'), readable=True, writable=True,  ),
    SQLField('ec_admin_comment', type='text', default= '', label=T('Comment (for admins)'), readable=False, writable=False,  ),
    SQLField('ec_vegeterian', type='boolean', default=False, label=T('Vegeterian'), readable=True, writable=True,  ),
    SQLField('ec_special_needs', type='string', default= '', label=T('Any special needs (allergies, etc)'), readable=True, writable=True,  ),
    SQLField('ec_potential_gamemaster', type='boolean', default=False, label=T('Potential game master'), readable=True, writable=True,  ),
    SQLField('ec_access_token', type='string', default='', label=T('Access token'), readable=True, writable=True, ),
)
db.define_table('ec_extrafield',
    SQLField('ec_entity', type='string', default= 'attendance', label=T('Where does the field belong to?'), requires=IS_EMPTY_OR(IS_IN_SET(ENTITY_CHOICES)), readable=True, writable=True, comment= ', '.join(['%s: %s' %(a,MV(a)) for a in ENTITY_CHOICES]), ),
    SQLField('ec_name', type='string', required=True, notnull=True, label=T('Field name'), readable=True, writable=True,  ),
    SQLField('ec_comment', type='string', label=T('Comment or explanation'), readable=True, writable=True,  ),
    SQLField('ec_datatype', type='string', default= 'string', label=T('Which type of data should be entered in this field?'), requires=IS_EMPTY_OR(IS_IN_SET(DATATYPE_CHOICES)), readable=True, writable=True, comment= ', '.join(['%s: %s' %(a,MV(a)) for a in DATATYPE_CHOICES]), ),
    SQLField('ec_order', type='integer', required=True, notnull=True, default=1, label=T('Order'), readable=True, writable=True,  ),
    SQLField('ec_is_optional', type='boolean', required=True, notnull=True, default=True, label=T('Is the field optional?'), readable=True, writable=True,  ),
    SQLField('ec_id_convention', db.ec_convention, label=T('Convention'), readable=True, writable=False,  requires=IS_IN_DB(db, 'ec_convention.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_convention[id] if id else ''),
)
db.define_table('ec_attendance',
    SQLField('ec_id_person', db.ec_person, label=T('Person'), readable=True, writable=False,  requires=IS_IN_DB(db, 'ec_person.id', '%(ec_nickname)s'), represent=lambda id, row: '%(ec_nickname)s' % db.ec_person[id] if id else ''),
    SQLField('ec_registration_state', type='string', required=True, notnull=True, default= 'registered', label=T('State of registration'), requires=IS_IN_SET(REGISTRATION_STATE_CHOICES), readable=True, writable=True, represent=lambda value, row: MV(value)),
    SQLField('ec_payment', type='double', required=True, notnull=True, default=0, label=T('Payment so far'), readable=True, writable=True,  ),
    SQLField('ec_registertime', type='datetime', required=True, notnull=True, label=T('Date of registration'), readable=True, writable=False,  ),
    SQLField('ec_arrival', type='date', label=T('Day of arrival'), readable=True, writable=True, represent = lambda value, row: value.strftime(dateformatstring) if value else ''),
    SQLField('ec_departure', type='date', label=T('Day of departure'), readable=True, writable=True, represent = lambda value, row: value.strftime(dateformatstring) if value else '' ),
    SQLField('ec_attendance_comment', type='text', label=T('Comment'), readable=True, writable=True, represent=lambda x, row: (PRE(x) if x else '')),
    SQLField('ec_id_convention', db.ec_convention, label=T('Convention'), readable=True, writable=False,  requires=IS_IN_DB(db, 'ec_convention.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_convention[id] if id else ''),
)
db.define_table('ec_bringthing',
    SQLField('ec_name', type='string', required=True, notnull=True, label=T('Thing name'), readable=True, writable=True,  ),
    SQLField('ec_description', type='text', label=T('Description of the thing'), readable=True, writable=True, represent=lambda x, row: MARKMIN(x) ),
    SQLField('ec_number', type='integer', label=T('How many things do we need? (-1: unbounded)'), readable=True, writable=True,  ),
    SQLField('ec_id_convention', db.ec_convention, label=T('Needed thing'), readable=True, writable=False,  requires=IS_IN_DB(db, 'ec_convention.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_convention[id] if id else ''),
)
db.define_table('ec_is_member',
    SQLField('ec_member_since', type='datetime', required=True, notnull=True, default=request.now, label=T('Member since'), readable=False, writable=False,  ),
    SQLField('ec_id_person', db.ec_person, label=T('Member'), readable=True, writable=True,  requires=IS_IN_DB(db, 'ec_person.id', '%(ec_nickname)s'), represent=lambda id, row: '%(ec_nickname)s' % db.ec_person[id] if id else ''),
)
db.define_table('ec_profile',
    SQLField('ec_name', type='string', required=True, notnull=True, label=T('Profile name'), readable=True, writable=True,  ),
    SQLField('ec_nickname', type='string', required=True, notnull=True, default= 'Anonymus', label=T('Your view name in the profile'), readable=True, writable=True,  ),
    SQLField('ec_image', type='blob', label=T('An image'), readable=True, writable=True,  ),
    SQLField('ec_url', type='string', label=T('Your homepage'), requires=IS_EMPTY_OR(IS_URL()), readable=True, writable=True, represent=lambda x, row: A(x, _href=x) if x else '' ),
    SQLField('ec_id_person', db.ec_person, label=T('Person'), readable=True, writable=True,  requires=IS_IN_DB(db, 'ec_person.id', '%(ec_nickname)s'), represent=lambda id, row: '%(ec_nickname)s' % db.ec_person[id] if id else ''),
)
db.define_table('ec_rpggame',
    SQLField('ec_name', type='string', label=T('Name of the game'), readable=True, writable=True, comment=T('e.g., "Dogs in the Vineyard" or "Earthdawn"'), ),
    SQLField('ec_system', type='string', label=T('Game system'), readable=True, writable=True, comment=T('e.g., "Fudge" or "D10"'), ),
    SQLField('ec_setting', type='string', label=T('Setting or Genre'), readable=True, writable=True, comment=T('e.g., "High-Fantasy Ninja Nazi Nuns" or "SciFi"'), ),
)

db.define_table('ec_timeslot',
    SQLField('ec_name', type='string', required=True, notnull=True, label=T('Name of the time slot'), readable=True, writable=True,  ),
    SQLField('ec_start_time', type='datetime', label=T('Start time'), readable=True, writable=True,  ),
    SQLField('ec_end_time', type='datetime', label=T('End time (undefined for open end)'), readable=True, writable=True,  ),
    SQLField('ec_description', type='text', label=T('Description'), readable=True, writable=True,  ),
    SQLField('ec_id_convention', db.ec_convention, label=T('Convention'), readable=True, writable=False,  requires=IS_IN_DB(db, 'ec_convention.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_convention[id] if id else ''),
)

db.define_table('ec_service',
    SQLField('ec_order', type='integer', default=10, label=T('Order of service in list'), readable=True, writable=True,  ),
    SQLField('ec_name', type='string', required=True, notnull=True, label=T('Service name'), readable=True, writable=True,  ),
    SQLField('ec_description', type='text', label=T('Description of the service'), readable=True, writable=True, represent=lambda x, row: MARKMIN(x) ),
    SQLField('ec_min_helpers', type='integer', required=True, default=0, label=T('Minimum number of helpers '), readable=True, writable=True,  ),
    SQLField('ec_max_helpers', type='integer', required=True, default=-1, label=T('Maximum # of helpers (-1: unlimited)'), readable=True, writable=True,  ),
    SQLField('ec_start_time', type='datetime', label=T('Start time'), readable=True, writable=True,  ),
    SQLField('ec_end_time', type='datetime', label=T('End time (undefined for open end)'), readable=True, writable=True,  ),
    SQLField('ec_id_timeslot', db.ec_timeslot, label=T('Time slot'), readable=True, writable=True,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_timeslot.id', '%(ec_name)s')), represent=lambda id, row: '%(ec_name)s' % db.ec_timeslot[id] if id else '-', ondelete='SET NULL'),
    SQLField('ec_id_convention', db.ec_convention, label=T('Convention'), readable=True, writable=True,  requires=IS_IN_DB(db, 'ec_convention.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_convention[id] if id else ''),
)

db.define_table('ec_use_profile',
    SQLField('ec_id_convention', db.ec_convention, label=T('Convention'), readable=True, writable=True,  requires=IS_IN_DB(db, 'ec_convention.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_convention[id] if id else ''),
    SQLField('ec_id_profile', db.ec_profile, label=T('Profile'), readable=True, writable=True,  requires=IS_IN_DB(db, 'ec_profile.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_profile[id] if id else ''),
)

def represent_event_time_start(value, row, table='ec_event'):
    return represent_event_time(value, row, 'start', table)

def represent_event_time_end(value, row, table='ec_event'):
    return represent_event_time(value, row, 'end', table)

def represent_event_time(value, row, what='start', table='ec_event'):
    if value: return value.strftime(datetimeformatstring)
    event = db[table](row['id']) if row else None
    if event:
        timeslot = db.ec_timeslot[event.ec_id_timeslot]
        time = timeslot['ec_%s_time' % what] if timeslot else None
        if time: # hack because compute does not work
            return time.strftime(datetimeformatstring)
        if timeslot:
            return timeslot['ec_%s_time' % what].strftime(datetimeformatstring) if timeslot['ec_%s_time' % what] else '-'
        else:
            return '-'
    return ''

db.define_table('ec_event',
    SQLField('ec_name', type='string', required=True, notnull=True, label=T('Event name'), readable=True, writable=True,  ),
    SQLField('ec_type', type='string', default= 'misc', label=T('What kind of event is this?'), requires=IS_EMPTY_OR(IS_IN_SET(EVENT_TYPE_CHOICES)), readable=True, writable=False,  ),
    SQLField('ec_start', type='datetime', label=T('When will it start?'), readable=True, writable=True, represent = represent_event_time_start),
    SQLField('ec_end', type='datetime', label=T('When will it end?'), readable=True, writable=True,  represent = represent_event_time_end),
    SQLField('ec_min_attendees', type='integer', default=2, label=T('Minimum number of attendees'), readable=True, writable=True,  ),
    SQLField('ec_max_attendees', type='integer', default=5, label=T('Maximum number of attendees'), readable=True, writable=True,  comment=T('0: event without registration')),
    SQLField('ec_apply_online', type='integer', default=3, label=T('How many attendees can apply online?'), readable=True, writable=True, requires=IS_NOT_EMPTY() ),
    SQLField('ec_prepared_chars', type='boolean', required=True, notnull=True, default=False, label=T('Prepared character sheets?'), readable=True, writable=True,  ),
    SQLField('ec_introduction', type='boolean', required=True, notnull=True, default=True, label=T('Open for people new to the game?'), readable=True, writable=True,  ),
    SQLField('ec_rpg_newbees', type='boolean', required=True, notnull=True, default=True, label=T('Open for people new to RPG?'), readable=True, writable=True,  ),
    SQLField('ec_registertime', type='datetime', required=True, notnull=True, default=request.now, label=T('When was the event registered?'), readable=True, writable=False,  ),
    SQLField('ec_url', type='string', label=T('More information (URL)'), requires=IS_EMPTY_OR(IS_URL()), readable=True, writable=True, represent=lambda x, row: A(x, _href=x) if x else '' ),
    SQLField('ec_description', type='text', label=T('A description of the event'), readable=True, writable=True, represent=lambda x, row: MARKMIN(x) ),
    SQLField('ec_id_convention', db.ec_convention, label=T('Convention'), readable=True, writable=False,  requires=IS_IN_DB(db, 'ec_convention.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_convention[id] if id else ''),
    SQLField('ec_id_rpggame', db.ec_rpggame, label=T('Role play game'), readable=True, writable=True,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_rpggame.id', '%(ec_name)s (%(ec_system)s): %(ec_setting)s')), represent=lambda id, row: '%(ec_name)s (%(ec_system)s): %(ec_setting)s' % db.ec_rpggame[id] if id else '', ondelete='NO ACTION'),
    SQLField('ec_id_timeslot', db.ec_timeslot, label=T('Time slot'), readable=True, writable=True,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_timeslot.id', '%(ec_name)s')), represent=lambda id, row: '%(ec_name)s' % db.ec_timeslot[id] if id else '-', ondelete='SET NULL'),
)
db.define_table('ec_attends_event',
    SQLField('ec_id_event', db.ec_event, label=T('Event'), readable=True, writable=True,  requires=IS_IN_DB(db, 'ec_event.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_event[id] if id else ''),
    SQLField('ec_id_person', db.ec_person, label=T('Attendee'), readable=True, writable=True,  requires=IS_IN_DB(db, 'ec_person.id', '%(ec_nickname)s'), represent=lambda id, row: '%(ec_nickname)s' % db.ec_person[id] if id else ''),
)
db.define_table('ec_buyitem',
    SQLField('ec_name', type='string', required=True, notnull=True, label=T('Item name'), readable=True, writable=True,  ),
    SQLField('ec_is_heading', type='boolean', default=False, label=T('Is this not an item, but a heading?'), readable=True, writable=True,  ),
    SQLField('ec_is_perday', type='boolean', default=False, label=T('Should the item price be calculated per day?'), readable=True, writable=True, comment=T('Requires a convention which is configured to count some items per day'), ),
    SQLField('ec_is_preselected', type='boolean', default=False, label=T('Should this item be pre-selected?'), readable=True, writable=True,  ),
    SQLField('ec_at_time', type='datetime', label=T('Should this item be displayable at a certain time in the personal programme?'), readable=True, writable=True, comment=T('Leave empty if valid for whole convention'), ),
    SQLField('ec_id_timeslot', db.ec_timeslot, label=T('Time slot'), readable=True, writable=True,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_timeslot.id', '%(ec_name)s')), represent=lambda id, row: '%(ec_name)s' % db.ec_timeslot[id] if id else '-', ondelete='SET NULL'),
    SQLField('ec_price', type='double', default=0, label=T('Price per item'), readable=True, writable=True, requires=IS_NOT_EMPTY() ),
    SQLField('ec_capacity', type='integer', default=-1, label=T('How many items can be sold for this convention? (-1: unlimited)'), readable=True, writable=True,  ),
    SQLField('ec_order', type='integer', default=10, label=T('Order of item in list'), readable=True, writable=True, comment=T('In which order this item will appear in comparison to other items'), ),
    SQLField('ec_has_waitlist', type='boolean', label=T('Does the item have a wait list?'), readable=True, writable=True,  ),
    SQLField('ec_max', type='integer', default=1, label=T('How often can this item be bought per attendee? (Maximum)'), readable=True, writable=True,  ),
    SQLField('ec_min', type='integer', default=0, label=T('How often can this item be bought per attendee? (Minimum)'), readable=True, writable=True, comment=T('Currently: only 0 or 1 make sense'), ),
    SQLField('ec_from_date', type='date', label=T('Start date of availability'), readable=True, writable=True, comment=T('Leave empty for whole registration period'), ),
    SQLField('ec_to_date', type='date', label=T('End date of availability'), readable=True, writable=True, comment=T('Leave empty for whole registration period'), ),
    SQLField('ec_id_convention', db.ec_convention, label=T('Convention'), readable=True, writable=False,  requires=IS_IN_DB(db, 'ec_convention.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_convention[id] if id else ''),
)
db.define_table('ec_buys_item',
    SQLField('ec_buy_state', type='string', required=True, notnull=True, default=  'booked', label=T('State of buy'), requires=IS_IN_SET(BUYITEM_STATE_CHOICES), readable=True, writable=True,  ),
    SQLField('ec_quantity', type='integer', required=True, notnull=True, default=1, label=T('Quantity'), readable=True, writable=True,  ),
    SQLField('ec_bookingdate', type='datetime', required=True, notnull=True, default=request.now, label=T('Booking date'), readable=True, writable=False,  ),
    SQLField('ec_id_buyitem', db.ec_buyitem, label=T('Bought item'), readable=True, writable=False,  requires=IS_IN_DB(db, 'ec_buyitem.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_buyitem[id] if id else ''),
    SQLField('ec_id_person', db.ec_person, label=T('Buyer'), readable=True, writable=False,  requires=IS_IN_DB(db, 'ec_person.id', '%(ec_nickname)s'), represent=lambda id, row: '%(ec_nickname)s' % db.ec_person[id] if id else ''),
)
db.define_table('ec_helps',
    SQLField('ec_id_person', db.ec_person, label=T('Helper'), readable=True, writable=True,  requires=IS_IN_DB(db, 'ec_person.id', '%(ec_nickname)s'), represent=lambda id, row: '%(ec_nickname)s' % db.ec_person[id] if id else ''),
    SQLField('ec_id_service', db.ec_service, label=T('Service'), readable=True, writable=True,  requires=IS_IN_DB(db, 'ec_service.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_service[id] if id else ''),
)
db.define_table('ec_organizes_event',
    SQLField('ec_id_event', db.ec_event, label=T('Event'), readable=True, writable=True,  requires=IS_IN_DB(db, 'ec_event.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_event[id] if id else ''),
    SQLField('ec_id_person', db.ec_person, label=T('Organizer'), readable=True, writable=True,  requires=IS_IN_DB(db, 'ec_person.id', '%(ec_nickname)s'), represent=lambda id, row: '%(ec_nickname)s' % db.ec_person[id] if id else ''),
)
db.define_table('ec_buyitemrule',
    SQLField('ec_name', type='string', label=T('Rule name'), readable=True, writable=True, comment=T('A name for the rule'), ),
    SQLField('ec_number', type='double', default=1, label=T('Number'), readable=True, writable=True, comment=T('If applicable, a number that this rule needs'), ),
    SQLField('ec_type', type='string', default= 'one_of_these_items' , label=T('What type of rule is it?'), requires=IS_EMPTY_OR(IS_IN_SET(RULE_TYPE_CHOICES)), readable=True, writable=True, comment=T('The rule type'), ),
    SQLField('ec_message', type='string', label=T('Rule information for attendee'), readable=True, writable=True, comment=T('can be shown to user, e. g., if rule is violated'), ),
    SQLField('ec_id_convention', db.ec_convention, label=T('Convention'), readable=True, writable=False,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_convention.id', '%(ec_name)s')), represent=lambda id, row: '%(ec_name)s' % db.ec_convention[id] if id else ''),
)
db.define_table('ec_has_rule',
    SQLField('ec_list_order', type='integer', default=35, label=T('List order'), readable=True, writable=True,  ),
    SQLField('ec_id_buyitem', db.ec_buyitem, label=T('Buyable Item'), readable=True, writable=True,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_buyitem.id', '%(ec_name)s')), represent=lambda id, row: '%(ec_name)s' % db.ec_buyitem[id] if id else '', ondelete='CASCADE'),
    SQLField('ec_id_rule', db.ec_buyitemrule, label=T('Rule'), readable=True, writable=True,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_buyitemrule.id', '%(ec_name)s')), represent=lambda id, row: '%(ec_name)s' % db.ec_buyitemrule[id] if id else '', ondelete='CASCADE'),
)
db.define_table('ec_email',
    SQLField('ec_from', type='text', label=T('From'), requires=IS_EMPTY_OR(IS_EMAIL_CSV_LIST()), readable=True, writable=False, widget=SQLFORM.widgets.string.widget ),
    SQLField('ec_to', type='text', label=T('To'), requires=IS_EMAIL_CSV_LIST(), readable=True, writable=True, widget=SQLFORM.widgets.string.widget ),
    SQLField('ec_cc', type='text', label=T('CC'), requires=IS_EMPTY_OR(IS_EMAIL_CSV_LIST()), readable=True, writable=True, widget=SQLFORM.widgets.string.widget ),
    SQLField('ec_bcc', type='text', label=T('Bcc'), requires=IS_EMPTY_OR(IS_EMAIL_CSV_LIST()), readable=True, writable=True, widget=SQLFORM.widgets.string.widget ),
    SQLField('ec_replyto', type='text', label=T('Reply to'), requires=IS_EMPTY_OR(IS_EMAIL_CSV_LIST()), readable=True, writable=True, widget=SQLFORM.widgets.string.widget ),
    SQLField('ec_subject', type='string', label=T('Subject'), readable=True, writable=True,  ),
    SQLField('ec_text', type='text', label=T('Text'), readable=True, writable=True, widget = lambda field,value: SQLFORM.widgets.text.widget(field,value,_cols='80') ),
    SQLField('ec_showonly', type='boolean', required=True, notnull=True, default=False, label=T('Just show'), readable=False, writable=True, comment=T('Do not send, just show e-mail'), ),
    SQLField('ec_date', type='datetime', default=request.now, label=T('Date'), readable=False, writable=False,  ),
    SQLField('ec_email_type', type='string', default= 'generic', label=T('Email type'), readable=False, writable=False,  ),
    SQLField('ec_id_convention', db.ec_convention, label=T('Convention'), readable=False, writable=False,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_convention.id', '%(ec_name)s')), represent=lambda id, row: '%(ec_name)s' % db.ec_convention[id] if id else ''),
    SQLField('ec_id_person', db.ec_person, label=T('Author'), readable=False, writable=False,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_person.id', '%(ec_nickname)s')), represent=lambda id, row: '%(ec_nickname)s' % db.ec_person[id] if id else ''),
)
db.define_table('ec_has_value',
    SQLField('ec_integervalue', type='integer', label=T('Value (whole numbers)'), readable=True, writable=True,  ),
    SQLField('ec_doublevalue', type='double', label=T('Value (fraction numbers)'), readable=True, writable=True,  ),
    SQLField('ec_numericvalue', type='double', label=T('Value (fraction numbers)'), readable=True, writable=True,  ),
    SQLField('ec_datevalue', type='date', label=T('Value (date)'), readable=True, writable=True,  ),
    SQLField('ec_datetimevalue', type='datetime', label=T('Value (datetime)'), readable=True, writable=True,  ),
    SQLField('ec_stringvalue', type='string', label=T('Value (string)'), readable=True, writable=True,  ),
    SQLField('ec_booleanvalue', type='boolean', label=T('Value (True or False)'), readable=True, writable=True,  ),
    SQLField('ec_id_buyitem', db.ec_buyitem, label=T('Buyable Item'), readable=True, writable=True,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_buyitem.id', '%(ec_name)s')), represent=lambda id, row: '%(ec_name)s' % db.ec_buyitem[id] if id else ''),
    SQLField('ec_id_convention', db.ec_convention, label=T('Convention'), readable=False, writable=False,  requires=IS_IN_DB(db, 'ec_convention.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_convention[id] if id else ''),
    SQLField('ec_id_event', db.ec_event, label=T('Event'), readable=True, writable=True,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_event.id', '%(ec_name)s')), represent=lambda id, row: '%(ec_name)s' % db.ec_event[id] if id else ''),
    SQLField('ec_id_attendance', db.ec_attendance, label=T('Attendance'), readable=True, writable=True,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_attendance.id', '%(id)s')), represent=lambda id, row: '%(id)s' % db.ec_attendance[id] if id else ''),
    SQLField('ec_id_person', db.ec_person, label=T('Person'), readable=True, writable=True,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_person.id', '%(ec_nickname)s')), represent=lambda id, row: '%(ec_nickname)s' % db.ec_person[id] if id else ''),
    SQLField('ec_id_profile', db.ec_profile, label=T('Profile'), readable=True, writable=True,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_profile.id', '%(ec_name)s')), represent=lambda id, row: '%(ec_name)s' % db.ec_profile[id] if id else ''),
    SQLField('ec_id_service', db.ec_service, label=T('Service'), readable=True, writable=True,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_service.id', '%(ec_name)s')), represent=lambda id, row: '%(ec_name)s' % db.ec_service[id] if id else ''),
    SQLField('ec_id_extrafield', db.ec_extrafield, label=T('Extra Field'), readable=False, writable=False,  requires=IS_IN_DB(db, 'ec_extrafield.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_extrafield[id] if id else ''),
)
MENU_SET = [('at_the_end', MV('at_the_end'))] + [(page, '%s "%s"' % (T('before'), MV(page))) for page in MENU_PAGES]
db.define_table('ec_page',
    SQLField('ec_order', type='integer', default=10, label=T('Order of page in list'), readable=True, writable=True, comment=T('In which order this page will appear in comparison to other pages'), ),
    SQLField('ec_name', type='string', required=True, notnull=True, label=T('Page name'), requires=IS_SLUG(), readable=True, writable=True, comment= '%s; %s' % (T('Will appear in URL'),T('Will be ignored on system pages')), ),
    SQLField('ec_title', type='string', label=T('Page title'), readable=True, writable=True, comment=T('Will be ignored on system pages'), ),
    SQLField('ec_body', type='text', label=T('Page content'), readable=True, writable=True, represent=lambda x, row: MARKMIN(x) ),
    SQLField('ec_systempage', type='string', required=True, notnull=True, default= 'No', label=T('Should this page be part of a system page?'), requires=IS_IN_SET([(x, MV(x)) for x in SYSTEM_PAGES], zero=None), readable=True, writable=True, comment=T('Will appear before the system content'),represent=lambda x, row:  '%s: %s' % (T('Yes'), MV(x).capitalize()) if x!='No' else MV(x) ),
    SQLField('ec_menu', type='boolean', required=True, notnull=True, default=False, label=T('Should this page appear in the menu?'), readable=True, writable=True,  ),
    SQLField('ec_before_menu_entry', type='string', required=True, notnull=True, default='at_the_end', label=T('Where should this page appear in the menu?'), comment=T('Will appear only if the corresponding system page is displayed'), readable=True, writable=True, requires=IS_IN_SET(MENU_SET, zero=None), represent = lambda value, row: '%s "%s"' % (T('before'), MV(value)) if value != 'at_the_end' else MV(value)),
    SQLField('ec_hidden', type='boolean', required=True, notnull=True, default=True, label=T('Should this page be only for organizers?'), readable=True, writable=True, comment=T('Will override the public setting'), ),
    SQLField('ec_public', type='boolean', required=True, notnull=True, default=False, label=T('Should this page be public?'), readable=True, writable=True, comment=T('i.e., without being logged-in'), ),
    SQLField('ec_created_on', type='datetime', required=True, notnull=True, default=request.now, label=T('Created on'), readable=True, writable=False,  ),
    SQLField('ec_created_by', db.ec_person, label=T('Created by'), readable=True, writable=False,  requires=IS_IN_DB(db, 'ec_person.id', '%(ec_nickname)s'), represent=lambda id, row: '%(ec_nickname)s' % db.ec_person[id] if id else ''),
    SQLField('ec_id_convention', db.ec_convention, label=T('Convention'), readable=False, writable=False,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_convention.id', '%(ec_name)s')), represent=lambda id, row: '%(ec_name)s' % db.ec_convention[id] if id else ''),
)
db.define_table('ec_invitation',
    SQLField('ec_for', type='string', required=True, notnull=True, default= 'convention', label=T('For what is the invitation?'), requires=IS_IN_SET([(x, MV(x)) for x in INVITATION_CHOICES], zero=None), readable=True, writable=True,  ),
    SQLField('ec_email', type='string', required=True, notnull=True, label=T('For whom is the invitation? (e-mail)'), requires=[IS_LOWER(), IS_EMAIL()], readable=True, writable=True,  ),
    SQLField('ec_text', type='text', required=True, notnull=True, label=T('Text for the invitation'), readable=True, writable=True,  ),
    SQLField('ec_status', type='string', required=True, notnull=True, default= 'invited', label=T('State of the invitation'), requires=IS_IN_SET([(x, MV(x)) for x in INVITATION_STATE_CHOICES], zero=None), readable=True, writable=False,  ),
    SQLField('ec_notify_date', type='datetime', label=T('When was the invitation sent out?'), readable=True, writable=True,  ),
    SQLField('ec_invite_date', type='datetime', required=True, notnull=True, default=request.now, label=T('When was the invitation created?'), readable=True, writable=False,  ),
    SQLField('ec_answer_date', type='datetime', label=T('When was the invitation answered?'), readable=True, writable=False,  ),
    SQLField('ec_expiry_date', type='datetime', label=T('Optionally: when will the invitation expire?'), readable=True, writable=True,  ),
    SQLField('ec_created_by', db.ec_person, label=T('Created by'), readable=True, writable=False,  requires=IS_IN_DB(db, 'ec_person.id', '%(ec_nickname)s'), represent=lambda id, row: '%(ec_nickname)s' % db.ec_person[id] if id else ''),
    SQLField('ec_id_person', db.ec_person, label=T('Invited person'), readable=False, writable=False,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_person.id', '%(ec_nickname)s')), represent=lambda id, row: '%(ec_nickname)s' % db.ec_person[id] if id else ''),
    SQLField('ec_id_event', db.ec_event, label=T('Event'), readable=False, writable=False,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_event.id', '%(ec_name)s')), represent=lambda id, row: '%(ec_name)s' % db.ec_event[id] if id else ''),
    SQLField('ec_id_attendance', db.ec_attendance, label=T('Registration'), readable=False, writable=False,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_attendance.id', '%(id)s')), represent=lambda id, row: '%(id)s' % db.ec_attendance[id] if id else ''),
    SQLField('ec_id_convention', db.ec_convention, label=T('Convention'), readable=False, writable=False,  requires=IS_IN_DB(db, 'ec_convention.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_convention[id] if id else ''),
)
db.define_table('ec_bedroom',
    SQLField('ec_name', type='string', required=True, notnull=True, label=T('Room name'), readable=True, writable=True,  ),
    SQLField('ec_capacity', type='integer', default=2, label=T('Room capacity'), readable=True, writable=True, required=True, notnull=True, comment=T('Number of beds')),
    SQLField('ec_category', type='string',  label=T('Room category'), readable=True, writable=True,  comment=T('(e.g., "Non smoking double")')),
    SQLField('ec_comment', type='string', label=T('Comment'), readable=True, writable=True, represent=lambda value, row: value if value else '' ),
    SQLField('ec_id_convention', db.ec_convention, label=T('Convention'), readable=True, writable=False,  requires=IS_IN_DB(db, 'ec_convention.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_convention[id] if id else ''),
)
db.define_table('ec_stays_in_room',
    SQLField('ec_begindate', type='date', label=T('Start date'), readable=True, writable=True,  ),
    SQLField('ec_enddate', type='date', label=T('End date'), readable=True, writable=True,  ),
    SQLField('ec_id_room', db.ec_bedroom, label=T('Room'), readable=True, writable=True,  requires=IS_IN_DB(db, 'ec_bedroom.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_bedroom[id] if id else ''),
    SQLField('ec_id_person', db.ec_person, label=T('Attendee'), readable=True, writable=True,  requires=IS_IN_DB(db, 'ec_person.id', '%(ec_nickname)s'), represent=lambda id, row: '%(ec_nickname)s' % db.ec_person[id] if id else ''),
)
db.define_table('ec_series',
    SQLField('ec_name', type='string', required=True, notnull=True, requires = IS_NOT_IN_DB(db, 'ec_series.ec_name'), default= 'SomeCon', label=T('Convention series name'), readable=True, writable=True,  ),
    SQLField('ec_short_name', type='string', required=True, notnull=True, label=T('Convention short name/acronym'), requires=[IS_NOT_EMPTY(), IS_ALPHANUMERIC(), IS_NOT_IN_DB(db, 'ec_series.ec_short_name')], readable=True, writable=True, comment= '%s. %s. %s.' % (T('Will be used for URLs'),T('Must not contain spaces'),T('Example: burgcon')), ),
    SQLField('ec_description', type='text', label=T('Description'), readable=True, writable=True, comment=T('This description will be shown to Easy-Con users'),represent=lambda x, row: MARKMIN(x) ),
    SQLField('ec_website', type='string', label=T('Web site'), requires=IS_EMPTY_OR(IS_URL()), readable=True, writable=True, comment=T('A link to an external website'),represent=lambda x, row: A(x, _href=x) if x else '' ),
    SQLField('ec_contact_email', type='string', label=T('Contact e-mail (public)'), requires=IS_EMPTY_OR(IS_EMAIL()), readable=True, writable=True, comment=T('If set, this e-mail address will be official public contact e-mail'), ),
    SQLField('ec_hidden_email', type='string', label=T('Contact e-mail (hidden)'), requires=IS_EMPTY_OR(IS_EMAIL()), readable=True, writable=True, comment=T('If no public contact e-mail address is set, this address will be used for contact form e-mails from users'), ),
    SQLField('ec_request_date', type='date', default=request.now, label=T('Series request date'), readable=True, writable=False,  ),
    SQLField('ec_series_public', type='boolean', default=False, label=T('Should the convention series be visible in public EasyCon lists?'), readable=True, writable=True,  ),
    SQLField('ec_design_backgroundcolor', type='string', default= '#3333CC', label=T('Which background color shall be used for this convention series?'), readable=True, writable=True, comment=T('Enter an HTML color code or color name'), ),
    SQLField('ec_design_logo', type='upload', label=T('A logo of the convention series (will replace the Easy-Con logo)'), readable=True, writable=True,  ),
    SQLField('ec_design_logo_height', type='string', required=True, notnull=True, default= '150px', label=T('The height of the logo'), readable=True, writable=True, comment=T('Enter an HTML conform value, e.g., 150px'), ),
    SQLField('ec_design_background', type='upload', label=T('Background image'), readable=True, writable=True,),
)
db.define_table('ec_in_series',
    SQLField('ec_id_convention', db.ec_convention, label=T('Convention'), readable=True, writable=False,  requires=IS_IN_DB(db, 'ec_convention.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_convention[id] if id else ''),
    SQLField('ec_id_series', db.ec_series, label=T('Convention series'), readable=True, writable=False,  requires=IS_IN_DB(db, 'ec_series.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_series[id] if id else ''),
)
db.define_table('ec_personal_event',
    SQLField('ec_name', type='string', required=True, notnull=True, label=T('Event name'), readable=True, writable=True,  ),
    SQLField('ec_type', type='string', default= 'personal', label=T('What kind of event is this?'), notnull=True, readable=False, writable=False,  ),
    SQLField('ec_id_timeslot', db.ec_timeslot, label=T('Time slot'), readable=True, writable=True,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_timeslot.id', '%(ec_name)s')), represent=lambda id, row: '%(ec_name)s' % db.ec_timeslot[id] if id else '-', ondelete='SET NULL'),
    SQLField('ec_start', type='datetime', label=T('When will it start?'), readable=True, writable=True, represent = lambda value, row: represent_event_time_start(value, row, table='ec_personal_event')),
    SQLField('ec_end', type='datetime', label=T('When will it end?'), readable=True, writable=True,  represent = lambda value, row: represent_event_time_end(value, row, table='ec_personal_event')),
    SQLField('ec_registertime', type='datetime', required=True, notnull=True, default=request.now, label=T('When was the event registered?'), readable=True, writable=False,  ),
#    SQLField('ec_description', type='text', label=T('A description of the event'), readable=True, writable=True, represent=lambda x, row: MARKMIN(x) ),
    SQLField('ec_id_convention', db.ec_convention, label=T('Convention'), readable=True, writable=False,  requires=IS_IN_DB(db, 'ec_convention.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_convention[id] if id else ''),
    SQLField('ec_id_person', db.ec_person, label=T('Organizer'), readable=True, writable=True,  requires=IS_IN_DB(db, 'ec_person.id', '%(ec_nickname)s'), represent=lambda id, row: '%(ec_nickname)s' % db.ec_person[id] if id else ''),
)
db.define_table('ec_table',
    SQLField('ec_name', type='string', required=True, notnull=True, label=T('Table name'), readable=True, writable=True,  ),
    SQLField('ec_location', type='string', label=T('Location'), readable=True, writable=True, comment=T('e.g., name of room'), ),
    SQLField('ec_order', type='integer', default=10, label=T('Order (in lists)'), readable=True, writable=True,  ),
    SQLField('ec_min_attendees', type='integer', required=True, notnull=True, default=0, label=T('Minimum capacity'), readable=True, writable=True, comment=T('Including GM'), ),
    SQLField('ec_max_attendees', type='integer', required=True, notnull=True, default=6, label=T('Maximum capacity'), readable=True, writable=True, comment=T('Including GM'), ),
    SQLField('ec_available_begin', type='datetime', required=True, notnull=True, label=T('Available from'), readable=True, writable=True,  ),
    SQLField('ec_available_end', type='datetime', required=True, notnull=True, label=T('Available to'), readable=True, writable=True,  ),
    SQLField('ec_setup', type='text', label=T('Room setup'), readable=True, writable=True, comment=T('e.g., projector, sofa, ...'), ),
    SQLField('ec_comment', type='text', label=T('Comment'), readable=True, writable=True,  ),
    SQLField('ec_picture', type='upload', label=T('A picture of the table'), readable=True, writable=True,  ),
    SQLField('ec_id_convention', db.ec_convention, label=T('Convention'), readable=True, writable=False,  requires=IS_IN_DB(db, 'ec_convention.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_convention[id] if id else ''),
)
db.define_table('ec_event_at_table',
    SQLField('ec_id_event', db.ec_event, label=T('Event'), readable=True, writable=True,  requires=IS_IN_DB(db, 'ec_event.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_event[id] if id else ''),
    SQLField('ec_id_table', db.ec_table, label=T('Table'), readable=True, writable=True,  requires=IS_IN_DB(db, 'ec_table.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_table[id] if id else ''),
    SQLField('ec_begindate', type='datetime', label=T('Start date'), readable=True, writable=True,  ),
    SQLField('ec_enddate', type='datetime', label=T('End date'), readable=True, writable=True,  ),
)


## Add virtual fields
db.ec_bedroom.v_free_capacity = Field.Virtual('v_free_capacity', lambda row: row.ec_bedroom.ec_capacity-db(db.ec_stays_in_room.ec_id_room==row.ec_bedroom.id).count(), label=T('Free capacity'))
db.ec_bedroom.v_people = Field.Virtual('v_people', lambda row: _get_people_in_room(row.ec_bedroom.id), label=T('People'))

db.ec_table.v_events = Field.Virtual('v_events', lambda row: _get_events_at_table(row.ec_table.id), label=T('Events'))
db.ec_table.v_events_with_time = Field.Virtual('v_events_with_time', lambda row: _get_events_at_table_with_time(row.ec_table.id), label=T('Events with time'))

db.ec_service.v_helpers = Field.Virtual('v_helpers', lambda row: _get_helpers(row.ec_service.id), label=T('Helpers'))
db.ec_service.v_helper_count = Field.Virtual('v_helper_count', lambda row: row.ec_service.ec_max_helpers - db(db.ec_helps.ec_id_service==row.ec_service.id).count() if row.ec_service.ec_max_helpers!=-1 else T('unlimited'), label=T('Needed'))

db.ec_attendance.v_waitlist_position = Field.Virtual('v_waitlist_position', lambda row: db((db.ec_attendance.ec_id_convention==row.ec_attendance.ec_id_convention)&\
            (db.ec_attendance.ec_registration_state=='waitlist')&\
            (db.ec_attendance.ec_registertime < row.ec_attendance.ec_registertime)).count()+1 if row.ec_attendance.ec_registertime else 0, label='#')

db.ec_buyitem.v_sold_items = Field.Virtual('v_sold_items', lambda row: db((db.ec_buys_item.ec_id_buyitem==row.ec_buyitem.id)&(db.ec_buys_item.ec_buy_state=='booked')).count(), label=T('Sold items'))
db.ec_buyitem.v_waitlist_items = Field.Virtual('v_waitlist_items', lambda row: \
    db((db.ec_buys_item.ec_id_buyitem==row.ec_buyitem.id)&\
                (db.ec_buys_item.ec_buy_state=='waitlist')&\
                (db.ec_buys_item.ec_id_person==db.ec_attendance.ec_id_person)&\
                (db.ec_attendance.ec_registration_state.belongs(['registered','attendee']))&\
                (db.ec_attendance.ec_id_convention==row.ec_buyitem.ec_id_convention)).count(), label=T('Waiting attendees'))

db.ec_buys_item.v_waitlist_position = Field.Virtual('v_waitlist_position', lambda row: db((db.ec_buys_item.ec_id_buyitem == row.ec_buys_item.ec_id_buyitem) &\
            (db.ec_buys_item.ec_buy_state=='waitlist')&
            (db.ec_buys_item.ec_bookingdate < row.ec_buys_item.ec_bookingdate)).count()+1, label='#')

db.ec_event.ec_host = Field.Virtual('ec_host', lambda row: _get_hosts(row.ec_event.id))
db.ec_event.ec_hostname = Field.Virtual('ec_hostname', lambda row: T('Game master') if row.ec_event.ec_type=='rpggame' else T('Registered by'))
db.ec_event.ec_attendees = Field.Virtual('ec_attendees', lambda row: _get_attendees(row.ec_event.id))

db.ec_event.nosort_slot = Field.Virtual('nosort_slot', lambda row: db.ec_event.ec_id_timeslot.represent(row.ec_event.ec_id_timeslot, row.ec_event)) # only to prevent sorting

db.ec_rpggame.event_count = Field.Virtual('event_count', lambda row: db(db.ec_event.ec_id_rpggame==row.ec_rpggame.id).count(), label=T('# of events'))

db.ec_series.v_conventions = Field.Virtual('v_conventions', lambda row: _get_conventions_for_series(row.ec_series.id), label=T('Conventions'))
