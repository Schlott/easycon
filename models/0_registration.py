from datetime import datetime

class Registration(object):
    # represents a convention registration, either from DB or form
    # can be initialized either with (person and convention) or attendance

    def __init__(self, convention=None, person=None, attendance=None, use_invitation=None, oldperson={}, orga=False):
        self.person_items = {} # the items of the person and their status
        self.extrafields = {} # the contents of extra items
        if person and convention: # get data from  person parameter and optional attendancedata from database
            self.person=person
            self.persondata = person.as_dict()
            self.convention = convention # the convention (overrides attendance convention)
            attendance = db((db.ec_attendance.ec_id_person==person.id)&(db.ec_attendance.ec_id_convention==self.convention.id)).select().first()
            self.attendancedata = attendance.as_dict() if attendance else {}
        if attendance: # get data from database, including person and convention
            self.attendancedata = attendance.as_dict()
            self.attendance=attendance
            self.persondata = db.ec_person(attendance.ec_id_person).as_dict()
            self.person = db.ec_person(attendance.ec_id_person)
            person=self.person
            self.convention = db(db.ec_convention.id==attendance.ec_id_convention).select().first() # use convention from attendance
            convention=self.convention
            items = db((db.ec_buys_item.ec_id_person==attendance.ec_id_person)&(db.ec_buys_item.ec_id_buyitem==db.ec_buyitem.id)&(db.ec_buyitem.ec_id_convention==self.convention.id)).select()
            for item in items:
                self.person_items[item.ec_buyitem.id] = item.ec_buys_item.ec_buy_state
            extras = db((db.ec_extrafield.ec_id_convention==self.convention.id)&\
                (db.ec_has_value.ec_id_extrafield==db.ec_extrafield.id)&\
                ((db.ec_has_value.ec_id_attendance==attendance.id) or (db.ec_has_value.ec_id_person==attendance.ec_id_person))).select()
            for extra in extras:
                self.extrafields[extra.ec_extrafield.id] = extra.ec_has_value['ec_%svalue'%extra.ec_extrafield.ec_datatype]
        if not person: # new registration for new person
            if oldperson: del oldperson['ec_firstname']
            if oldperson: del oldperson['ec_nickname']
            self.persondata = oldperson # the data of the given person
            self.attendancedata = {} # the data of the attendance
            self.convention = convention # the convention (overrides attendance convention)
        # Initialize remaining fields
        self.con_items = db(db.ec_buyitem.ec_id_convention==self.convention.id).select(orderby=['ec_order']) # the buyable items of this convention
        self.use_invitation = use_invitation
        self.oldperson = oldperson
        self.orga = orga
        self.config = db.ec_configuration[self.convention.ec_id_configuration] # the configuration of the given convention
        self.person_fields = list(REQUIRED_PERSON_FIELDS)
        self.__get_person_fields() # extends the person_fields from configuration

    def get_regform(self,waitlist_only=False):
        # creates a regform from object data
        config = db.ec_configuration(self.convention.ec_id_configuration)
        fields = []
        fields.extend(self.__get_person_fields())
        fields.extend(self.__get_attendance_fields())
        fields.extend(self.__get_item_fields())
        if waitlist_only:
            self.regform = SQLFORM.factory(*fields, submit_button=T('Register for waitlist only'),hidden=dict(waitlist=True,conventionid=self.convention.id))
            return self.regform
        if config.ec_registration_waitlist:
            fields.append(Field('waitlist', 'boolean', label=T('Register for waitlist only')))
        self.regform = SQLFORM.factory(*fields, submit_button=T('Register'))
        return self.regform

    def update(self, regform, person=None, attendanceid=None, regstate='registered'):
        # updates the data of the object from a regform
        # update self.persondata
        self.persondata = dict(**db.ec_person._filter_fields(regform.vars))
        self.persondata['id'] = person.id if person else None
        # check for waitlist
        waitlist_only = regform.vars.waitlist
        # update items and extra fields
        bookitems = {}
        for field in regform.vars:
            # gather items
            item = self.__get_item(field)
            rule = self.__get_rule(field)
            extra = self.__get_extra(field)
            buystate='declined'
            if item:
                if not rule and getattr(regform.vars, field): # a checked single item
                    if (self.__free_items(item)==0 and item.ec_has_waitlist) or waitlist_only:
                        buystate = 'waitlist'
                    else:
                        buystate = 'booked'
            if rule: # e.g., an item chosen from a list
                item = db.ec_buyitem[getattr(regform.vars,field)]
                if (self.__free_items(item)==0 and item.ec_has_waitlist) or waitlist_only:
                    buystate = 'waitlist'
                else:
                    buystate = 'booked'
            if item:
                bookitems[item.id] = dict(ec_buy_state=buystate)
            elif extra: # update extrafields
                self.extrafields[extra.id] = dict(value=getattr(regform.vars,field))

        # update items and check if it is a valid combination. If not --> waitlist
        regstate = self.__update_items(bookitems, waitlist_only=waitlist_only)

        # update self.attendancedata
        self.attendancedata = dict(**db.ec_attendance._filter_fields(regform.vars))
        if waitlist_only:
            self.attendancedata['ec_registration_state'] = 'waitlist'
            self.attendancedata['ec_attendance_comment'] = T('Self-registered for waitlist') + '\n' + (self.attendancedata['ec_attendance_comment'] or '')
        if attendanceid:
            self.persondata['id'] = db.ec_attendance(attendanceid).ec_id_person
            self.attendancedata['id'] = attendanceid
        self.attendancedata.update(dict(ec_registration_state=regstate,ec_registertime=request.now))
        return dict(attendancedata=self.attendancedata, persondata=self.persondata, items=self.person_items, extras=self.extrafields)

    def save(self):
        # saves object data from registration to db and sends e-mail
        errormessage = self.__save_person()
        if errormessage: return errormessage
        can_attend = self.__save_items()
        if self.attendancedata['ec_registration_state']=='waitlist':
            can_attend = False
        self.__save_attendance(can_attend)
        self.__save_extrafields()
        attid = self.attendancedata['id']
        if can_attend:
            _send_auto_email('registration_attendee_registered',convention=self.convention,person=db.ec_person(self.persondata['id']),attendance=db.ec_attendance(attid))
            db.ec_systemevent.insert(ec_when=request.now, ec_what=T('%(ec_firstname)s %(ec_lastname)s registered') % self.persondata, ec_type='registration', ec_id_convention=self.convention.id, ec_id_person=self.persondata['id'], ec_id_auth_user=auth.user.id)
            attendmessage = T('%(ec_firstname)s %(ec_lastname)s registered') % self.persondata
            if self.use_invitation:
                self.use_invitation.update_record(ec_id_attendance=attid, ec_status='accepted', ec_answer_date=request.now)
                db.ec_systemevent.insert(ec_when=request.now, ec_what='invitation no. %s for %s was declined' % (self.use_invitation.id, self.use_invitation.ec_email), ec_type='invitation_accept', ec_id_convention=self.convention.id, ec_id_person=self.persondata['id'], ec_id_auth_user=auth.user.id)
                _send_invitation_answer(self.convention,self.use_invitation)
        else:
            db.ec_systemevent.insert(ec_when=request.now, ec_what=T('%(ec_firstname)s %(ec_lastname)s registered for waitlist') % self.persondata, ec_type='registration_waitlist', ec_id_convention=self.convention.id, ec_id_person=self.persondata['id'], ec_id_auth_user=auth.user.id)
            _send_auto_email('registration_waitlist',convention=self.convention,person=db.ec_person(self.persondata['id']),attendance=db.ec_attendance(attid))
            attendmessage = T('%(ec_firstname)s %(ec_lastname)s registered for waitlist') % self.persondata
        return attendmessage

    def get_attendance_form(self, readonly=True):
        db.ec_attendance.ec_id_person.represent = lambda id, row: '%(ec_firstname)s %(ec_lastname)s' % db.ec_person[id] if id else ''
        db.ec_attendance.ec_registration_state.represent = lambda value, row: MV(value)
        db.ec_attendance.id.readable,db.ec_attendance.id.writable=False,False
        db.ec_attendance.ec_id_convention.readable,db.ec_attendance.ec_id_convention.writable=False,False
        db.ec_attendance.ec_id_convention.default=self.convention.id
        db.ec_attendance.ec_payment.represent = lambda value, row: '%.2f %s' % (value, self.config.ec_registration_currency_symbol)
        fields = [   'ec_id_person',
                        'ec_registration_state',
                        'ec_payment',
                        'ec_registertime',
                        'ec_arrival',
                        'ec_departure',
                        'ec_attendance_comment',
        ]
        if self.config.ec_registration_data_attendance_arrival == 'do_not_ask':
            del fields[fields.index('ec_arrival')]
        if self.config.ec_registration_data_attendance_departure == 'do_not_ask':
            del fields[fields.index('ec_departure')]
        dbfields = [db.ec_attendance[field] for field in fields]
        return SQLFORM(db.ec_attendance, self.attendancedata['id'], fields=fields, readonly=readonly, comments=False)

    def get_person_form(self, readonly=True):
        db.ec_person.id.readable,db.ec_person.id.writable=False,False
        return SQLFORM(db.ec_person, self.persondata['id'], fields=self.person_fields, readonly=readonly, comments=False)

    def get_extras_form(self, readonly=True): #todo: return form
        fields = []
        a_extra = db((db.ec_extrafield.ec_id_convention==self.convention.id)&(db.ec_extrafield.ec_entity=='attendance')).select(orderby=['ec_order'])
        for a in a_extra:
            f = Field('extra_%s'%a.id, a.ec_datatype, required=(not a.ec_is_optional), comment=a.ec_comment,
                      label=a.ec_name if a.ec_is_optional else a.ec_name+'*')
            if a.id in self.extrafields:
                f.default = self.extrafields[a.id]
            fields.append(f)
        attform = SQLFORM.factory(*fields, readonly=readonly, submit_button=T('Save'))
        return attform # TABLE(*[TR(TD(db.ec_extrafield(extra)['ec_name']+':'),TD(MV(self.extrafields[extra]))) for extra in self.extrafields])

    def update_extras(self, extraform):
        for field in extraform.vars:
            # gather items
            extra = self.__get_extra(field)
            if extra: # update extrafields
                self.extrafields[extra.id] = dict(value=getattr(extraform.vars,field))

    def save_extras(self, extraform=None):
        if extraform:
            self.update_extras(extraform)
        self.__save_extrafields()

    def show_items(self):
        if self.person_items:
            return CAT(P(A(T('This attendee is booked for the following items')+':', _href=URL('attendee_items',args=self.convention.ec_short_name, vars=dict(attid=self.attendancedata['id']),user_signature=True))), UL(*[LI(db.ec_buyitem(item)['ec_name']+': '+MV(self.person_items[item])) for item in self.person_items]))
            #A(T('Unbuy all items'), _href=URL('delete_attendance', args=[self.convention.ec_short_name,self.attendancedata['id']], vars=dict(action='deregister_items')), _class="linkbutton"))
        else:
            return ''

    def show_services(self):
        helping=db((db.ec_helps.ec_id_person==self.persondata['id'])&(db.ec_helps.ec_id_service==db.ec_service.id)&(db.ec_service.ec_id_convention==self.convention.id)).select(db.ec_service.ALL)
        if helping:
            return CAT(P(A(T('This attendee is booked for the following services')+':', _href=URL('services', args=[self.convention.ec_short_name]))), UL(*[LI(service['ec_name']) for service in helping]))
        else:
            return ''

    def get_bedrooms(self, html=True):
        bedrooms=db(   (db.ec_stays_in_room.ec_id_person==self.person.id) & \
                                 (db.ec_stays_in_room.ec_id_room==db.ec_bedroom.id) & \
                                 (db.ec_bedroom.ec_id_convention==self.convention.id)).select(db.ec_bedroom.ALL)
        if bedrooms:
            if html:
                return CAT(P(T('This attendee is booked for the following bedrooms')+':', UL(*[A(bedroom['ec_name'],_href=URL('people_in_room', args=[self.convention.ec_short_name, bedroom.id]))  for bedroom in bedrooms])))
            else:
                return bedrooms
        else:
            return ''

    def get_events(self, what, html=True): #  returns all events that the person hosts or attends
        if not self.person : return
        if not self.convention : return
        if what not in ['organizes', 'attends']: return
        events = db( (db.ec_event.ec_id_convention==self.convention.id) &
                            (db['ec_%s_event' % what].ec_id_event==db.ec_event.id) &
                            (db['ec_%s_event' % what].ec_id_person==self.person.id)).select(db.ec_event.ALL)
        if events:
            if html:
                return CAT(P(T('This attendee %s the following events' % what)+':'), UL(*[LI(A(event['ec_name'] , _href=URL('event', args=[self.convention.ec_short_name, event.id]))) for event in events]))
            else:
                return events
        else:
            return ''

    def show_attendeedata(self):
        attendanceonly = self.get_attendance_form(readonly=True)
        items = self.show_items()
        extras = self.get_extras_form(readonly=True)
        services = self.show_services()
        bedrooms = self.get_bedrooms(html=True)
        event_host = self.get_events(what='organizes', html=True)
        event_attendee = self.get_events(what='attends', html=True)
        return CAT(attendanceonly, extras, items, services, event_host, event_attendee, bedrooms)

    def show_additionaldata(self):
        services = self.show_services()
        bedrooms = self.get_bedrooms(html=True)
        event_host = self.get_events(what='organizes', html=True)
        event_attendee = self.get_events(what='attends', html=True)
        return CAT(services, event_host, event_attendee, bedrooms)

    def moveup_from_waitlist(self):
        report = ''
        # Moves an attendee from waitlist to attendee list; books items with waitlist status if bookable; returns report in html
        if self.attendancedata.get('ec_registration_state') != 'waitlist':
            report = P(T('%s %s is not on the waitlist (any more)') % (self.person.ec_firstname, self.person.ec_lastname)+'.')
            return report
        # else:
        # update attendee status
        db(db.ec_attendance.id==self.attendancedata['id']).update(ec_registration_state='registered')
        self.attendancedata['ec_registration_state']='registered'
        # book items and generate report
        report = P(T('Registration state of %s %s updated to "%s"') % (self.person.ec_firstname, self.person.ec_lastname, T('registered'))+'.')
        booked_item_report = []
        waitlist_item_report = []
        for itemid in self.person_items:
            if self.person_items[itemid]=='waitlist':
                item = db.ec_buyitem(itemid)
                available = Registration.__free_items(item)
                if (available > 0) or (available == -1):
                    db.ec_buys_item.update_or_insert((db.ec_buys_item.ec_id_person==self.person.id) & (db.ec_buys_item.ec_id_buyitem==item.id), ec_id_person=self.person.id, ec_id_buyitem=item.id, ec_buy_state='booked')
                    self.person_items[item.id] = 'booked'
                    booked_item_report.append(item.ec_name)
                else:
                    waitlist_item_report.append(item.ec_name)
        if booked_item_report:
            report+=P('%s:' % T('The following items have been booked'))
            report+=UL([LI(i) for i in booked_item_report])
        if waitlist_item_report:
            report+=P('%s:' % T('The following items are still on waitlist since they are not bookable'))
            report+=UL([LI(i) for i in waitlist_item_report])
        return report

    def check_required_items(self):
        # checks if all required items are booked; if not, return a HTML report
        report = ''
        required_item_report = []
        for conitem in self.con_items:
            if conitem.ec_min and conitem.ec_min > 0 and self.person_items.get(conitem.id, 'not booked') != 'booked':
                required_item_report.append(conitem.ec_name)
        if required_item_report:
            report=CAT(B(P('%s:' % T('Warning! The following items are required but are not booked yet')),UL([LI(i) for i in required_item_report])))
        return report

    def decline(self, waitlist=False, waitlisttime=None, orga=None): # deregisteres an attendee or sends him*her to waitlist
        # set attendance status to 'declined'
        attendee_status = 'waitlist' if waitlist else 'deregistered'
        db(db.ec_attendance.id==self.attendancedata['id']).update(ec_registration_state=attendee_status)
        # change registration date and set comment
        if waitlist:
            regtime = self.attendancedata['ec_registertime']
            waitlisttime = waitlisttime or datetime.datetime.now()
            orga = orga or T('Organizer')
            db(db.ec_attendance.id==self.attendancedata['id']).update(ec_registertime=waitlisttime)
            waitlisttime = db.ec_attendance[self.attendancedata['id']]['ec_registertime']
            waitlist_comment = T('Moved to waitlist by %s at %s. Registration date was %s') % (orga, waitlisttime, regtime) + '\n' + (self.attendancedata['ec_attendance_comment'] or '')
            self.attendancedata['ec_attendance_comment'] = waitlist_comment
            self.attendancedata['ec_registertime'] = waitlisttime
            result=db(db.ec_attendance.id==self.attendancedata['id']).update(ec_attendance_comment=waitlist_comment)
        # change status of all booked items of that person
        buyitems=db((db.ec_buys_item.ec_id_person==self.person.id) &\
                    (db.ec_buys_item.ec_id_buyitem==db.ec_buyitem.id)&\
                    (db.ec_buyitem.ec_id_convention==self.convention.id)\
                    ).select(db.ec_buys_item.ALL)
        for bitem in buyitems:
            item_status = 'waitlist' if waitlist else 'declined'
            db(db.ec_buys_item.id==bitem).update(ec_buy_state=item_status)
        # delete help in services
        helping = self.deregister_from_services()
        # deregister from events
        events = self.deregister_from_events()
        # remove from bedrooms
        bedrooms = self.deregister_from_bedrooms()
        # send e-mail to attendee
        autoemail = 'registration_waitlist_moved2waitlist' if waitlist else 'registration_decline'
        mailresult = _send_auto_email(autoemail,convention=self.convention,person=self.person)
        # create feedback for orga
        messages=[]
        if waitlist:
            messages.append(str(T('%(ec_firstname)s %(ec_lastname)s moved to waitlist') % self.persondata))
        else:
            messages.append(str(T('%(ec_firstname)s %(ec_lastname)s declined') % self.persondata))
        if self.person_items:
            messages.append(str(T('All buyable items have been set to "%s"' % item_status)))
        if helping:
            messages.append(str(T('All help services of this person are removed')))
        if events:
            messages.append(str(T('Attendee was deregistered from all events')))
        if bedrooms:
            messages.append(str(T('All stays in bedrooms of this person are removed')))
        if self.attendancedata['ec_payment'] > 0:
            messages.append(str(T('You might want to refund up to %.2f %s') % (self.attendancedata['ec_payment'],self.config.ec_registration_currency_symbol)))
        messages.append('%s: %s' % (T('Email'), ','.join(mailresult)))
        return '; '.join(messages)

    def delete(self): # deletes registration data
        # delete attendancedata
        db(db.ec_attendance.id==self.attendancedata['id']).delete()
        # delete extra fields
        extras = db((db.ec_extrafield.ec_id_convention==self.convention.id)&\
                (db.ec_has_value.ec_id_extrafield==db.ec_extrafield.id)&\
                ((db.ec_has_value.ec_id_attendance==self.attendancedata['id']) or (db.ec_has_value.ec_id_person==self.persondata['id']))).select(db.ec_has_value.ALL)
        for extra in extras:
            db(db.ec_has_value.id==extra.ec_has_value.id).delete()
        # delete items
        bookings = db((db.ec_buys_item.ec_id_person==self.persondata['id'])&(db.ec_buys_item.ec_id_buyitem==db.ec_buyitem.id)&(db.ec_buyitem.ec_id_convention==self.convention.id)).select(db.ec_buys_item.ALL)
        for booking in bookings:
            db(db.ec_buys_item.id==booking.id).delete()
        # delete other connections
        self.deregister_from_services()
        self.deregister_from_events()
        self.deregister_from_bedrooms()
        self.delete_personal_events()

    def deregister_from_services(self):
        helping=db((db.ec_helps.ec_id_person==self.persondata['id'])&(db.ec_helps.ec_id_service==db.ec_service.id)&(db.ec_service.ec_id_convention==self.convention.id)).select(db.ec_helps.ALL)
        for help in helping:
            db(db.ec_helps.id==help.id).delete()
        return helping

    def deregister_from_events(self): # only for attendees of events
        eventresults = []
        events = db((db.ec_attends_event.ec_id_person==self.person.id) &\
                    (db.ec_attends_event.ec_id_event==db.ec_event.id) &\
                    (db.ec_event.ec_id_convention==self.convention.id)\
                    ).select(db.ec_event.ALL)
        for event in events:
            db( (db.ec_attends_event.ec_id_event==event.id) &\
                (db.ec_attends_event.ec_id_person==self.person.id)).delete()
        return events

    def deregister_from_bedrooms(self):
        bedrooms=db(   (db.ec_stays_in_room.ec_id_person==self.person.id) & \
                                 (db.ec_stays_in_room.ec_id_room==db.ec_bedroom.id) & \
                                 (db.ec_bedroom.ec_id_convention==self.convention.id)).select(db.ec_stays_in_room.ALL)
        for stay in bedrooms:
            db(db.ec_stays_in_room.id==stay.id).delete()
        return bedrooms

    def delete_personal_events(self):
        personal_events=db( (db.ec_personal_event.ec_id_person==self.person.id) & \
                                        (db.ec_personal_event.ec_id_convention==self.convention.id)).select()
        for pe in personal_events:
            db(db.ec_personal_event.id==pe.id).delete()
        return personal_events

    def __get_person_fields(self):
        # returns person fields from config
        # changes self.person_fields to cover the actual fields
        result = []
        for f in REQUIRED_PERSON_FIELDS:
            if not getattr(db.ec_person, f).label.endswith('*'):
                setattr(getattr(db.ec_person, f), 'label', '%s*'% getattr(db.ec_person, f).label)
        if self.config:
            for key in db.ec_configuration.fields:
                if key.startswith('ec_registration_data_person_'):
                    pkey='ec_'+key[len('ec_registration_data_person_'):]
                    if getattr(self.config, key) in ['required','optional']:
                        self.person_fields.append(pkey)
                    if getattr(self.config, key)=='required':
                        if not getattr(db.ec_person, pkey).label.endswith('*'):
                            setattr(getattr(db.ec_person, pkey), 'label', '%s*'% getattr(db.ec_person, pkey).label)
                    else:
                        if getattr(db.ec_person, pkey).label.endswith('*'):
                            setattr(getattr(db.ec_person, pkey), 'label', getattr(db.ec_person, pkey).label[:-1])
        for fieldstring in self.person_fields:
            f = getattr(db.ec_person,fieldstring)
            if fieldstring in self.persondata:
                f.set_attributes(default=self.persondata[fieldstring])
            result.append(f)
        if not self.persondata: # new person
            db.ec_person.ec_email.comment = T('If the person does not have an own email address, enter your address')
        return result

    def __get_attendance_fields(self):
        result = []
        field=db.ec_attendance.ec_attendance_comment
        if self.attendancedata:
            field.default=self.attendancedata.get('ec_attendance_comment')
        result.append(field)
        # append additional attendance fields from config
        a_extra = db((db.ec_extrafield.ec_id_convention==self.convention.id)&(db.ec_extrafield.ec_entity=='attendance')).select(orderby=['ec_order'])
        for a in a_extra:
            f = Field('extra_%s'%a.id, a.ec_datatype, required=(not a.ec_is_optional), comment=a.ec_comment,
                      label=a.ec_name if a.ec_is_optional else a.ec_name+'*')
            result.append(f)
        # create arrival and departure field for form based on config
        if self.config and self.convention.ec_start_date and self.convention.ec_end_date:
                from datetime import timedelta
                if self.config.ec_registration_perday or self.config.ec_registration_data_attendance_arrival in ['required','optional']:
                    arrivals=[]
                    for day in self.config.ec_registration_perday_arrival_days.split(','):
                        aday=self.convention.ec_start_date+timedelta(days=int(day)-1)
                        arrivals.append((aday,'%s, %s' % (T(aday.strftime('%A')),aday.strftime(dateformatstring))))
                    field=db.ec_attendance.ec_arrival
                    zero = T("Don't know yet") if (self.config.ec_registration_data_attendance_arrival=='optional' and not self.config.ec_registration_perday) else None
                    field.requires = IS_IN_SET(arrivals,zero=zero)
                    if self.attendancedata:
                        field.default=self.attendancedata.get('ec_arrival')
                    else:
                        field.default=arrivals[0]
                    result.append(field)

                if self.config.ec_registration_perday or self.config.ec_registration_data_attendance_departure in ['required','optional']:
                    departures=[]
                    for day in self.config.ec_registration_perday_departure_days.split(','):
                        dday=self.convention.ec_start_date+timedelta(days=int(day)-1)
                        departures.append((dday,'%s, %s' % (T(dday.strftime('%A')),dday.strftime(dateformatstring))))
                    field=db.ec_attendance.ec_departure
                    zero = T("Don't know yet") if (self.config.ec_registration_data_attendance_departure=='optional' and not self.config.ec_registration_perday) else None
                    field.requires = IS_IN_SET(departures,zero=zero)
                    if self.attendancedata:
                        field.default=self.attendancedata.get('ec_departure')
                    else:
                        field.default=departures[0]
                    result.append(field)
        return result

    def __get_item_fields(self):
        result = [Field('buyitems_heading', 'boolean'),] # not really rendered, just to display errors
        items = self.con_items
        for item in items:
            # check if there are rules for this item
            choicerule = self.__get_choice_rule(item.id)
            if choicerule:
                choicerulefield = self.__get_choice_rule_field(item.id, choicerule)
                if choicerulefield: result.append(choicerulefield) # will not contain sold out items if there is no waitlist
                if self.__free_items(item)==0 and not item.ec_has_waitlist:
                    #if item.ec_has_waitlist:
                    #    label = '%s %s %s: ........ %.2f %s' % (T('waitlist'), T('for'), item.ec_name, item.ec_price, self.config.ec_registration_currency_symbol)
                    #else:
                    label = '%s %s: ........ %.2f %s' % (item.ec_name,self.__availability_string(item),item.ec_price,self.config.ec_registration_currency_symbol)
                    result.append(Field('item_%s'%item.id, 'boolean', default=False, label=label, writable=False))
            elif item.ec_is_heading: # add a field, but with different name
                # check if item should be currently available
                if item.ec_from_date and item.ec_from_date > request.now.date(): continue # too early
                if item.ec_to_date and item.ec_to_date < request.now.date(): continue # too late
                f = Field('heading_item_%s'%item.id, 'boolean', label=item.ec_name)
                result.append(f)
            else: # no choice rules, just the item
                # check if item should be currently available
                if item.ec_from_date and item.ec_from_date > request.now.date(): continue # too early
                if item.ec_to_date and item.ec_to_date < request.now.date(): continue # too late
                if self.attendancedata:
                    default = db((db.ec_buys_item.ec_id_person==self.persondata.get('id'))&(db.ec_buys_item.ec_buy_state=='booked')&(db.ec_buys_item.ec_id_buyitem==item.id)).select()
                else:
                    default = item.ec_is_preselected
                if item.ec_has_waitlist and self.__free_items(item)==0:
                    label = '%s %s %s: ........ %.2f %s' % (T('waitlist'), T('for'), item.ec_name, item.ec_price, self.config.ec_registration_currency_symbol)
                else:
                    label = '%s %s: ........ %.2f %s' % (item.ec_name,self.__availability_string(item),item.ec_price,self.config.ec_registration_currency_symbol)
                result.append(Field('item_%s'%item.id, 'boolean', default=default, label=label, writable=self.__free_items(item)!=0 or item.ec_has_waitlist))
        return result

    def __get_choice_rule(self,itemid):
        return db((db.ec_buyitemrule.ec_type=='one_of_these_items') &
                  (db.ec_buyitemrule.id==db.ec_has_rule.ec_id_rule) &
                  (db.ec_has_rule.ec_id_buyitem==itemid)).select(db.ec_buyitemrule.ALL).first()

    def __get_choice_rule_field(self, itemid, item_choice_rule):
        cruleitems = db((db.ec_buyitemrule.id==item_choice_rule.id) &
                        (db.ec_buyitemrule.id==db.ec_has_rule.ec_id_rule) &
                        (db.ec_has_rule.ec_id_buyitem==db.ec_buyitem.id)).select(db.ec_buyitemrule.ALL, db.ec_has_rule.ALL, db.ec_buyitem.ALL, orderby=[db.ec_buyitem.ec_order])
        if cruleitems and cruleitems[0].ec_buyitem.id==itemid: # there is a choice rule for this item and we are the first, build rule field
            default = cruleitems[0].ec_buyitem.id
            iset = []
            for citem in cruleitems:
                # check if item should be currently available
                if citem.ec_buyitem.ec_from_date and citem.ec_buyitem.ec_from_date > request.now.date(): continue # too early
                if citem.ec_buyitem.ec_to_date and citem.ec_buyitem.ec_to_date < request.now.date(): continue # too late
                # check preselection
                if citem.ec_buyitem.ec_is_preselected or (self.attendancedata and db((db.ec_buys_item.ec_id_person==self.persondata.get('id'))&(db.ec_buys_item.ec_buy_state=='booked')&(db.ec_buys_item.ec_id_buyitem==citem.ec_buyitem.id)).select()):
                    default=citem.ec_buyitem.id
                # check capacity
                if self.__free_items(citem.ec_buyitem) == 0 and citem.ec_buyitem.ec_has_waitlist==True:
                    iset.append((citem.ec_buyitem.id, '%s %s %s: ........ %.2f %s' % (T('waitlist'), T('for'), citem.ec_buyitem.ec_name, citem.ec_buyitem.ec_price, self.config.ec_registration_currency_symbol)))
                elif self.__free_items(citem.ec_buyitem) != 0:
                    iset.append((citem.ec_buyitem.id,'%s %s: ........ %.2f %s'%(citem.ec_buyitem.ec_name, self.__availability_string(citem.ec_buyitem), citem.ec_buyitem.ec_price, self.config.ec_registration_currency_symbol)))
            itemlabel = '%(ec_name)s: %(ec_price)s '+self.config.ec_registration_currency_symbol
            if len(iset)==1: default=iset[0][0]
            f = Field('rule_%s'%item_choice_rule.id,
                        'integer',
                        default=default,
                        label=item_choice_rule.ec_name,
                        requires=IS_IN_SET(iset, zero=None),
                        widget=SQLFORM.widgets.radio.widget,
                        readable=False,)
            return f

    def __update_items(self, bookitems, waitlist_only):
        # updates self.person_items and returns if it is a valid combination
        regstate = ''
        for item in db(db.ec_buyitem.ec_id_convention==self.convention.id).select():
            if item.id in bookitems:
                self.person_items[item.id] = bookitems[item.id]
            elif item.id in self.person_items:
                self.person_items[item.id].update(dict(ec_buy_state='declined'))
        can_attend = self.__check_waitlist_items()
        if not can_attend or waitlist_only:
            for itemid in self.person_items.keys():
                if self.person_items[itemid]['ec_buy_state']=='booked':
                    self.person_items[itemid].update(dict(ec_buy_state='waitlist'))
            return 'waitlist'
        return 'registered'

    def __save_person(self):
        if self.persondata['id']:
            db(db.ec_person.id==(self.persondata['id'])).update(**self.persondata)
        elif self.config.ec_registration_other: # create new (another) person
            del self.persondata['id']
            person_id = db.ec_person.insert(**self.persondata)
            auth.add_permission(auth.user_group(), 'edit', db.ec_person, person_id)
            self.persondata['id'] = person_id
        else:
            return T('Error: no person specified')

    def __save_items(self):
        # creates or updates entries for items in database, returns True if it is a valid combination (else: book items with waitlist and returns False)
        can_attend = self.__check_waitlist_items()
        entries = db((db.ec_buys_item.ec_id_person==self.persondata['id']) &\
           (db.ec_buys_item.ec_id_buyitem==db.ec_buyitem.id) &\
           (db.ec_buyitem.ec_id_convention==self.convention.id)).select(db.ec_buys_item.ALL)
        for entry in entries:
            db(db.ec_buys_item.id==entry.id).delete()
        for itemid in self.person_items.keys():
            buystate = self.person_items[itemid]['ec_buy_state']
            if buystate=='booked' and not can_attend:
                buystate = 'waitlist'
                self.person_items[itemid]['ec_buy_state'] = 'waitlist'
            if buystate!='declined':
                db.ec_buys_item.insert(ec_id_person=self.persondata['id'], ec_id_buyitem=itemid, ec_buy_state=buystate)
        return can_attend

    def __save_attendance(self, can_attend):
        # creates or updates attendance object in database
        attid = self.attendancedata.get('id')
        if not can_attend:
            self.attendancedata['ec_registration_state']='waitlist'
        if attid:
            db.ec_attendance.update(db.ec_attendance.id==attid,**self.attendancedata)
        else:
            if 'id' in self.attendancedata:
                del self.attendancedata['id']
            db.ec_attendance.update_or_insert(((db.ec_attendance.ec_id_convention==self.convention.id)&(db.ec_attendance.ec_id_person==self.persondata['id'])),
                                              ec_id_convention=self.convention.id,
                                              ec_id_person=self.persondata['id'],
                                              **self.attendancedata)
            self.attendancedata['id'] = db((db.ec_attendance.ec_id_convention==self.convention.id)&(db.ec_attendance.ec_id_person==self.persondata['id'])).select().first()['id']

    def __check_waitlist_items(self):
        # checks if all required items are booked, returns boolean
        can_attend = True
        for item in db(db.ec_buyitem.ec_id_convention==self.convention.id).select():
            if self.person_items.get(item.id) and item.ec_min and item.ec_min > 0 and self.person_items[item.id]['ec_buy_state'] != 'booked': can_attend = False
        return can_attend

    def __save_extrafields(self):
        for key in self.extrafields.keys():
            extra = db.ec_extrafield(key)
            if extra.ec_entity=='attendance':
                self.__save_extra_value(key, self.extrafields[key]['value'], self.attendancedata['id'])
            elif extra.ec_entity=='person':
                self.__save_extra_value(key, self.extrafields[key]['value'], self.persondata['id'])

    @classmethod
    def __save_extra_value(self, extraid, value, entityid):
        # save an extra value to the database and returns the id of the database entry
        extra=db.ec_extrafield[extraid]
        if not extra:
            return None
        db((db.ec_has_value.ec_id_extrafield==extra.id)&(db.ec_has_value['ec_id_%s' % extra.ec_entity]==entityid)).delete()
        newdict = {'ec_id_%s'%extra.ec_entity: entityid,
                   'ec_%svalue'%extra.ec_datatype: value,
                   'ec_id_extrafield': extra.id}
        eid = db.ec_has_value.insert(**newdict)
        return eid

    @classmethod
    def __get_item(cls, field):
        # if field represents an item, returns an item (based on fieldname)
        if field.startswith('item_'):
            itemid=field[len('item_'):]
            return db.ec_buyitem(itemid)

    @classmethod
    def __get_rule(cls, field):
        # if field represents a rule, returns an item (based on fieldname)
        if field.startswith('rule_'):
            ruleid=field[len('rule_'):]
            return db.ec_buyitemrule(ruleid)

    @classmethod
    def __get_extra(cls, field):
        # if field represents an extra itmem, returns an item (based on fieldname)
        if field.startswith('extra_'):
            extraid=field[len('extra_'):]
            return db.ec_extrafield(extraid)

    @classmethod
    def __free_items(cls, item):
        # calculates the free capacity for an item
        query = (db.ec_buyitemrule.ec_type=='combined_capacity') &\
                (db.ec_buyitemrule.id==db.ec_has_rule.ec_id_rule) &\
                (db.ec_has_rule.ec_id_buyitem==item.id)
        combined_capacity_rules = db(query).select(db.ec_buyitemrule.ALL)
        available = None
        if combined_capacity_rules:
            for cr in combined_capacity_rules:
                bought = db((db.ec_has_rule.ec_id_rule==cr.id)&(db.ec_has_rule.ec_id_buyitem==db.ec_buys_item.ec_id_buyitem)&(db.ec_buys_item.ec_buy_state=='booked')).count()
                if available is None:
                    available = cr.ec_number - bought
                else:
                    available = min(available, cr.ec_number - bought)
            available = max(0, available)
        else:
            if not item.ec_capacity or item.ec_capacity==-1:
                available = -1
            else:
                bought = db((db.ec_buys_item.ec_id_buyitem==item.id)&(db.ec_buys_item.ec_buy_state=='booked')).count()
                available = max(0, item.ec_capacity - bought)
        return available

    @classmethod
    def __availability_string(cls, item):
        availability = cls.__free_items(item)
        if availability==-1:
            return ''  #T('unlimited', lazy=False)
        elif availability==0:
            return '(%s)' % T('currently sold out', lazy=False)
        else:
            return '(%s)' % T('%.0f left', lazy=False) % availability

    @classmethod
    def get_capacity(cls, convention):
        capacity = None
        config = db.ec_configuration(convention.ec_id_configuration)
        if not config: return
        items = db((db.ec_buyitem.ec_id_convention==convention.id)&(db.ec_buyitem.ec_min>0)).select()
        choicerules = db((db.ec_buyitemrule.ec_id_convention==convention.id)&(db.ec_buyitemrule.ec_type=='one_of_these_items')).select()
        if items:
            for item in items:
                if not capacity:
                    capacity = cls.__free_items(item)
                else:
                    capacity = min(capacity, cls.__free_items(item))
        if capacity and capacity!=-1: return capacity
        if choicerules:
            for rule in choicerules:
                ruleitems = db((db.ec_buyitem.id==db.ec_has_rule.ec_id_buyitem)&(db.ec_has_rule.ec_id_rule==rule.id)).select(db.ec_buyitem.ALL)
                for item in ruleitems:
                    if not capacity:
                        capacity = cls.__free_items(item)
                    else:
                        capacity = min(capacity, cls.__free_items(item))
        if config.ec_registration_capacity != -1:
            all_attendances = db((db.ec_attendance.ec_id_convention==convention.id)&(db.ec_attendance.ec_registration_state.belongs(['registered','attendee']))).count()
            if config.ec_registration_capacity - all_attendances < 1:
                capacity = 0
            elif capacity == -1 or not capacity:
                capacity = config.ec_registration_capacity - all_attendances
            else:
                capacity = min(capacity, config.ec_registration_capacity - all_attendances)
        return capacity

    @classmethod
    def get_item_counter(cls, convention):
    # Substracts the number of bought items from the number
        counter_rules = db((db.ec_buyitemrule.ec_id_convention==convention.id)&(db.ec_buyitemrule.ec_type=='count_items')).select(db.ec_buyitemrule.ALL, orderby=[db.ec_buyitemrule.ec_number])
        result = []
        for rule in counter_rules:
            booked_items = 0
            items = db( (db.ec_has_rule.ec_id_rule==rule.id)&\
                        (db.ec_has_rule.ec_id_buyitem==db.ec_buyitem.id)&\
                        (db.ec_buys_item.ec_buy_state=='booked')&\
                        (db.ec_buys_item.ec_id_buyitem==db.ec_buyitem.id)).select(db.ec_buys_item.ALL)
            for item in items:
                booked_items += item.ec_quantity
#                capacity = min([cls.__free_items(item) for item in items])
            capacity = rule.ec_number - booked_items
            result.append((rule.ec_name, int(capacity)))
        return result

    @classmethod
    def get_attendee_status(cls, conventionid, personid):
        att = db((db.ec_attendance.ec_id_person==personid)&(db.ec_attendance.ec_id_convention==conventionid)).select().first()
        if att:
            return att.ec_registration_state
        else:
            return 'no_status'

    @classmethod
    def get_nickname_with_status(cls, conventionid, personid):
        attendee_status=Registration.get_attendee_status(conventionid, personid)
        person=db.ec_person[personid]
        if attendee_status in ['attendee', 'registered', 'no_status']:
            return person['ec_nickname']
        else:
            return '%s (%s)' % (person['ec_nickname'], T(attendee_status))

    @classmethod
    def get_person_fields(cls, convention):
        registration = Registration(convention=convention)
        return registration.__get_person_fields()

    @classmethod
    def check_registration_capacity(cls, convention, email):
        # checks if people can register based on config capacity, registrations, and invitations
        # if convention has a waitlist, you can always register on the waitlist unless the registration period is over
        config=db.ec_configuration(convention.ec_id_configuration)
        if not config: return 'register_nok'
        # Check invitations first
        person_invites = _open_user_invitations(convention, email=email)
        invite_attendances = db((db.ec_invitation.ec_id_convention==convention.id)&(db.ec_invitation.ec_id_attendance!=None)).count()
        all_attendances = db((db.ec_attendance.ec_id_convention==convention.id)&(db.ec_attendance.ec_registration_state.belongs(['registered','attendee']))).count()
        open_attendances = all_attendances - invite_attendances
        # Can you use an invitation that is not expired? --> do it!
        if (config.ec_registration_invite_capacity==-1 or config.ec_registration_invite_capacity - invite_attendances > 0) and person_invites > 0:
            return 'invite_ok'
        # Are we in the registration period?
        if not (_check_valid_dates(config.ec_registration_period_start, config.ec_registration_period_stop)): return 'register_nok'
        # Are there open registrations left? --> use this!
        if config.ec_registration_capacity==-1 or config.ec_registration_capacity - open_attendances > 0:
            return 'register_ok'
        # Else: no invitation system?
        if config.ec_registration_invite_capacity==0:
            if config.ec_registration_waitlist:  return 'waitlist_only'
            return 'register_nok'
        # There are invitations, but you don't have an invitation?
        if person_invites <=0:
            if config.ec_registration_waitlist:  return 'waitlist_only'
            return 'no_invite'
        # No invitation capacity left
        if config.ec_registration_invite_capacity - invite_attendances <= 0:
            if config.ec_registration_waitlist:  return 'waitlist_only'
            return 'invite_nok'
        # Else
        if config.ec_registration_waitlist:  return 'waitlist_only'
        return 'register_nok'

    @classmethod
    def book_items(cls, attendanceid, itemlist):
        """Books buyable items if they belong to the user of the attendance"""
        return "I would book the following items: %s for attendanceid %s" % (itemlist, attendanceid)

    @classmethod
    def validate(cls, form):    
        convention = db.ec_convention(session.conventionid)
        config = db.ec_configuration(convention.ec_id_configuration)
        canregister = cls.check_registration_capacity(convention, auth.user.email) if not (_check_convention_permission('organize') and config.ec_registration_orga) else 'register_ok'
        if canregister not in ['invite_ok','register_ok','waitlist_only']:
            form.errors.ec_firstname = MV(canregister)
        booked_items=[]
        firstitem = None
        for field in form.fields:
            # check required fields
            if field in REQUIRED_PERSON_FIELDS:
                value, error = IS_NOT_EMPTY()(getattr(form.vars,field))
                if error:
                    setattr(form.errors, field, error)
            else:
                for thing in ['person', 'attendance']:
                    cfield = 'ec_registration_data_%s_%s' % (thing, field[3:])
                    if hasattr(config,cfield) and getattr(config,cfield)=='required':
                        value, error = IS_NOT_EMPTY()(getattr(form.vars,field))
                        if error:
                            setattr(form.errors, field, error)
            # check extra attributes
            if field.startswith('extra'):
                extrafield = cls.__get_extra(field)
                if extrafield and extrafield.ec_is_optional==False:
                    value, error = IS_NOT_EMPTY()(getattr(form.vars,field))
                    if error:
                        setattr(form.errors, field, error)
            item = cls.__get_item(field)
            if item:
                if not firstitem: firstitem = field
                if getattr(form.vars, field): booked_items.append(item.id)
                # check capacity
                if not form.vars.waitlist_only:
                    if getattr(form.vars, field):
                        availability = cls.__free_items(item)
                        if availability==0 and not item.ec_has_waitlist:
                            setattr(form.errors, field, '%s %s' % (item.ec_name, cls.__availability_string(item)))
                # check mininum values
                if (item.ec_min or -1) > 0 and not getattr(form.vars, field):
                    setattr(form.errors, field, T('You need to buy "%(ec_name)s"') % item.as_dict())
            rule = cls.__get_rule(field)
            if rule:
                item = db.ec_buyitem(getattr(form.vars,field))
                if item:
                    if not firstitem: firstitem = field
                    booked_items.append(item.id)
                    # check capacity
                    if not form.vars.waitlist_only:
                        availability = cls.__free_items(item)
                        if availability==0 and not item.ec_has_waitlist:
                            setattr(form.errors, field, '%s %s' % (item.ec_name, cls.__availability_string(item)))
                    # check mininum values
                    if item.ec_min > 0 and not getattr(form.vars, field):
                        setattr(form.errors, field, T('You need to buy "%(ec_name)s"') % item.as_dict())

        # check global conditions
        rules = db(db.ec_buyitemrule.ec_id_convention==convention.id).select()
        booked_items = set(booked_items)
        for rule in rules:
            if rule.ec_type=='min_items':
                min_items = db(db.ec_has_rule.ec_id_rule==rule.id).select(db.ec_has_rule.ec_id_buyitem).as_list()
                formitems = set(mi['ec_id_buyitem'] for mi in min_items)
                rulemessage = rule.ec_message if rule.ec_message else '%s: %s' % (RULE_EXPLANATIONS['min_items'], ', '.join([db.ec_buyitem(mi['ec_id_buyitem']).ec_name for mi in min_items]))
                if len(booked_items & formitems)==0:
                    setattr(form.errors, firstitem, rulemessage)

    @classmethod
    def get_manage_attendee_url(cls, linktext, attid):
        attendance = db.ec_attendance(attid)
        if not attendance:
            return "Not found."
        convention = db.ec_convention(attendance.ec_id_convention)
        return A(linktext, _href=URL('manage_attendee',args=[convention.ec_short_name,attid]))

def widget_submit_button(field, value):
    # widget for additional form button
    # currently not used
    item = INPUT(_type='submit', _name=field, _value=value, value=value)
    return (SPAN(item))

