from datetime import timedelta
from gluon.scheduler import Scheduler
scheduler = Scheduler(db,migrate=False)


def remind_orga(): # sends reminders for open tasks of conventions; should run (only!) once per day
    conventions = db((db.ec_convention.id == db.ec_task.ec_id_convention) &
                     (db.ec_task.ec_status.belongs(['1','2']))).select(db.ec_convention.ALL, distinct=True)
    email_results = []
    for convention in conventions:
        config=db.ec_configuration(convention.ec_id_configuration)
        if config and config.ec_orgatask_reminder and request.now.date() >= config.ec_orgatask_reminder_start and config.ec_orgatask_reminder_days > 0:
            delta = request.now.date() - config.ec_orgatask_reminder_start
            if delta.days % config.ec_orgatask_reminder_days == 0:
                email_results.append(_send_task_reminder(convention))
    db.commit()
    return email_results

def send_newsletter(): # sends newsletter to qualified users; should run (only!) once per day
    approved_news = db((db.ec_news.ec_status == 'approved') & (db.ec_news.ec_publish_date <= request.now.date()))
    approved_news.update(ec_status = 'published')
    # hack: only German Newsletter
    T.force('de')
    # send news for today
    news = db((db.ec_news.ec_status == 'published') & (db.ec_news.ec_publish_date == request.now.date())).select(orderby=~db.ec_news.ec_publish_date)
    users = db(db.auth_user.newsletter=='daily').select()
    if news and users:
        result = _send_newsletter(news, users, format='structure')
        successes = len([(r,s) for (r,s) in result if r])
        for entry in news:
            db.ec_systemevent.insert(ec_when=request.now, ec_what='News Entry %s (%s) sent to %s users' % (entry.id, entry.ec_subject, successes), ec_type='newsletter_daily', ec_id_convention=entry.ec_id_convention if entry.ec_id_convention else None)        
            entry.update_record(ec_email_count=entry.ec_email_count + successes)
    # send weekly news
    if request.now.date().weekday() == 4: # 0: Monday, 6: Sunday
        news = db((db.ec_news.ec_status == 'published') & (db.ec_news.ec_publish_date > request.now.date() - timedelta(days=8)) & (db.ec_news.ec_publish_date <= request.now.date())).select(orderby=~db.ec_news.ec_publish_date)
        users = db(db.auth_user.newsletter=='weekly').select()
        if news and users:
            db.ec_systemevent.insert(ec_when=request.now, ec_what='News Entry %s (%s) sent to %s users' % (entry.id, entry.ec_subject, successes), ec_type='newsletter_weekly', ec_id_convention=entry.ec_id_convention if entry.ec_id_convention else None)        
            result = _send_newsletter(news, users, format='structure')
            successes = len([(r,s) for (r,s) in result if r])
            for entry in news:
                entry.update_record(ec_email_count=entry.ec_email_count + successes)
    T.lazy=True
    db.commit()

def test_task(): # just for testing
    text = "%s:\n" % T('You can manage your tasks here')
    text += URL('convention', 'orgatasks', args=['DTC1'], scheme=True, host='www.easy-con.org')

    msgid = db.ec_email.insert(
    ec_from=mail.settings.sender,
    ec_to='dani@miracle-solutions.de',
    ec_subject='Easy-Con Test-Email',
    ec_text=text,
    ec_email_type='test e-mail',
    )
    _send_email(msgid)


VALID_SYSTEM_TASKS = {
    'remind_orga' : remind_orga,
    'test_task': test_task,
    'send_newsletter': send_newsletter,
}
