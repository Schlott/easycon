# all e-mail helpers

CHANGE_NEWSLETTER = T("""This email was sent by the Easycon newsletter service.
You can modify your newsletter settings in your profile:
%s
""") % URL('default', 'user', args='profile', scheme=True, host=EC_DOMAIN)

def validate_emails(emaillist=None, users=None): # returns a list of emails that are validated
    result = []
    if emaillist:
        for email in emaillist:
            user = db(db.auth_user.email == email).select().first()
            if user.registration_key == '':
                result.append(user.email)
    else:
        for user in users:
            if user.registration_key == '':
                result.append(user.email)
    return result

def _show_email(form,config=None,convention=None, todict={}, fakesignature=True, expandVariables=True):
    # Show one or multiple messages. If todict is given, exchange emails with that string (privacy feature)
    # expandVariables allows variables to be used in the e-mail
    ed = _get_email_dict(convention=convention)
    signature='\n'+config.ec_email_signature % ed if config and fakesignature else ''
    form.vars.ec_text = form.vars.ec_text +'\n'+signature if form.vars.ec_text else ''
    if expandVariables:
        try:
            form.vars.ec_text = form.vars.ec_text % ed
            form.vars.ec_subject = form.vars.ec_subject % ed
        except KeyError as e:
            return T('I cannot resolve this: %%(%s)s does not exist') % e.args
    result = '-' * 80
    result += '\n'
    # make sure that we have values
    for to in form.vars.ec_to.split(','):
        to = to.strip()
        # contruct the template
        msg=[]
        for key in ['ec_from','ec_to','ec_cc','ec_bcc','ec_replyto','ec_subject']:
            try:
                if getattr(form.vars, key):
                    msg.append('%s: %%(%s)s' % (key[3:].title(), key))
                else:
                    form.vars[key]=''
            except AttributeError:
                pass
        if form.vars.ec_text: msg.append('\nText:\n\n%(ec_text)s')
        msg.append('-' * 80)
        msgstr = '\n'.join(msg)

        # construct the result. If we give a todict, use nickname instead of email
        ed = dict(
            ec_from=form.vars.ec_from,
            ec_to=todict[to] if todict and todict.get(to) else to,
            ec_cc=form.vars.ec_cc,
            ec_bcc=form.vars.ec_bcc,
            ec_replyto=form.vars.ec_replyto,
            ec_subject=form.vars.ec_subject,
            ec_text=form.vars.ec_text,
            )
        result += msgstr % ed
        result += '\n'

    return result

def _save_email(emailform, convention=None, expandVariables=False):
# saves an email to database
    signature = ''
    if convention:
        ed = _get_email_dict(convention=convention)
        config = db.ec_configuration[convention.ec_id_configuration] if convention else None
        if config.ec_email_signature:
            signature = config.ec_email_signature
    emailform.vars.ec_text += '\n'+ signature
    if expandVariables:
        emailform.vars.ec_text = emailform.vars.ec_text % ed
        emailform.vars.ec_subject = emailform.vars.ec_subject % ed
    emailid = db.ec_email.insert(**db.ec_email._filter_fields(emailform.vars))
    auth.add_permission(0, 'edit', db.ec_email, emailid)
    return emailid

def _send_email(msgid,todict={},format='text'):
    # sends email from database
    # "format" for return value: 'text': just a list of text, 'structure': a list of ([True|False], msg)
    msg=db.ec_email[msgid].as_dict()
    result=[]
    for to in msg['ec_to'].split(','):
        to = to.strip()
        user = db(db.auth_user.email==to).select().first()
        if user:
            language = user.last_language if user.last_language else 'en'
            T.force(language)
        res = mail.send(to=to.strip(),
            cc=msg['ec_cc'],
            bcc=msg['ec_bcc'],
            reply_to=msg['ec_replyto'],
            subject=msg['ec_subject'],
            message=msg['ec_text'])
        if res:
            resstr = T('Send message to %s: %s') % (todict.get(to,to),T('ok'))
        else:
            resstr = T('Send message to %s: %s') % (todict.get(to,to),T('not ok'))
        if format == 'text':
            result.append(str(resstr))
        elif format == 'structure':
            result.append((res, str(resstr)))
    T.lazy=True
    return result

def _get_email_autogroup(convention, group='attendee'):
    if group in ['registered','attendee', 'waitlist', 'deregistered']:
        emails =  db((db.ec_attendance.ec_id_convention==convention.id)&\
            (db.ec_attendance.ec_registration_state==group)&\
            (db.ec_attendance.ec_id_person==db.ec_person.id)).select(db.ec_person.ec_email,distinct=True)
        emaillist = [e['ec_email'] for e in emails]
        return emaillist
    elif group=='needs_to_pay':
        people =  db((db.ec_attendance.ec_id_convention==convention.id)&\
            (db.ec_attendance.ec_id_person==db.ec_person.id)).select(db.ec_person.ALL,db.ec_attendance.ec_payment,distinct=True)
        emaillist = []
        for person in people:
            if person.ec_attendance.ec_payment - _get_item_sum(convention, person.ec_person.id) < 0:
                emaillist.append(person.ec_person.ec_email)
        return emaillist
    elif group in ['con_orga', 'con_admin']:
        emails = db((db.auth_group.role=='%s_%s' % (group, convention.id))&\
            (db.auth_group.id==db.auth_membership.group_id)&\
            (db.auth_membership.user_id==db.auth_user.id)).select(db.auth_user.email,distinct=True)
        emaillist = [e['email'] for e in emails]
        return emaillist
    elif group == 'easycon_admin':
        emails = db((db.auth_group.role==group)&\
            (db.auth_group.id==db.auth_membership.group_id)&\
            (db.auth_membership.user_id==db.auth_user.id)).select(db.auth_user.email,distinct=True)
        emaillist = [e['email'] for e in emails]
        return emaillist
    elif group.startswith('event_orga_'):
        try:
            eventid = int(group[len('event_orga_'):])
        except:
            return []
        emails = db((db.ec_event.id==db.ec_organizes_event.ec_id_event)&\
            (db.ec_organizes_event.ec_id_person==db.ec_person.id)).select(db.ec_person.ec_email,distinct=True)
        emaillist = [e['ec_email'] for e in emails]
        return emaillist

def _get_email_dict(person=None, convention=None, attendance=None, event=None, attendee=None, user=None):
    config = db.ec_configuration[convention.ec_id_configuration] if convention else None
    if not attendance and person and convention:
        attendance=_get_attendance(convention,person.id)
    feestr = None
    if (person and convention):
        fee = _get_item_sum(convention,person.id)
        if fee:
            feestr = '%.2f' % fee
            feestr += config.ec_registration_currency_symbol
        else:
            feestr = '0.0 ' + config.ec_registration_currency_symbol
    if (attendance and attendance.ec_payment and config and config.ec_registration_currency_symbol):
        payment = '%.2f %s' % (attendance.ec_payment or 0, config.ec_registration_currency_symbol)
    elif attendance and config and config.ec_registration_currency_symbol:
        payment = '0.0 ' + config.ec_registration_currency_symbol
    else:
        payment = None

    emaildict = {
        'firstname': person.ec_firstname if person and person.ec_firstname else user.first_name if user else None,
        'lastname': person.ec_lastname if person and person.ec_lastname else user.last_name if user else None,
        'nickname': person.ec_nickname if person and person.ec_nickname else user.nick_name if user else None,
        'att_nickname': attendee.ec_nickname if attendee and attendee.ec_nickname else None,
        'fee': feestr,
        'payment': payment,
        'account': config.ec_registration_account if config and config.ec_registration_account else None,
        'conname': convention.ec_name if convention and convention.ec_name else None,
        'conshortname': convention.ec_short_name if convention and convention.ec_short_name else None,
        'conemail': (convention.ec_contact_email or convention.ec_hidden_email) if convention else None,
        'waitlist': '%s' % attendance.v_waitlist_position if attendance and attendance.v_waitlist_position else None, #get_waitlist_position(person.id, convention.id) if (person and convention) else '',
        'attendance_comment': attendance.ec_attendance_comment if attendance and attendance.ec_attendance_comment else None,
        'regdata': _get_regdata_url(attendance) if attendance else None,
        'regurl': _get_regdata_url(attendance) if attendance else None,
        'eventdata': _get_eventdata(event) if event else None,
        'conwebsite': convention.ec_website if convention and convention.ec_website else None,
        'invite_url': URL('convention','my_invitations',args=convention.ec_short_name, scheme=True, host=True) if (convention and config and config.ec_registration_invite_capacity != 0) else None
    }
    emaildict['account'] = emaildict['account'] % emaildict if emaildict['account'] else ''
    return emaildict

def _get_valid_email_variables(person=None, convention=None, attendance=None, event=None, attendee=None, user=None):
    # returns only the valid information
    valid_expands = []
    ed = _get_email_dict(person=person,convention=convention,attendance=attendance,event=event,attendee=attendee)
    for e in ed:
        if ed[e]:
            valid_expands.append("%%(%s)s" % e)
    return valid_expands

def _send_auto_email(etype, convention=None, person=None, attendance=None, event=None):
    # returns a list of result messages (either: "ok" or error message)
    # Manual e-mails; TODO: Make configurable
    if etype=='deregistered_from_event':
        return _send_event_deregistration_notice(event,person)

    # Regular auto-emails
    config = db.ec_configuration[convention.ec_id_configuration] or None
    if not hasattr(config, 'ec_email_%s_text' % etype) or not getattr(config, 'ec_email_%s_text' % etype):
        return [str(T('No email template specified')),]
    if not attendance and person and convention:
        attendance=_get_attendance(convention,person.id)
    ed = _get_email_dict(convention=convention,person=person,attendance=attendance,event=event)
    bcc=''
    if etype in ['registration_attendee_registered','event_registered','registration_got_money','registration_waitlist','registration_decline','registration_waitlist_moved2waitlist', 'registration_waitlist_movedup_waitlist']:
        tos = set([person.ec_email,auth.user.email])
        if getattr(config,'ec_email_%s_orga' % etype):
            bcc += ','.join(_get_email_autogroup(convention,'con_orga'))
        if getattr(config,'ec_email_%s_bcc' % etype):
            bcc += getattr(config,'ec_email_%s_bcc' % etype)
    else:
        return ['%s: %s' % (T('Email not sent'), T('no to-addresses found')),]
    text,subject = '',''
    if hasattr(config, 'ec_email_%s_text' % etype) and getattr(config, 'ec_email_%s_text' % etype):
        text += getattr(config, 'ec_email_%s_text' % etype) % ed
    if config.ec_email_signature:
        text += '\n'+config.ec_email_signature % ed
    if hasattr(config, 'ec_email_%s_subject' % etype) and getattr(config, 'ec_email_%s_subject' % etype):
        subject += getattr(config, 'ec_email_%s_subject' % etype) % ed
    if config.ec_email_tag:
        subject = config.ec_email_tag % ed +' '+subject
    try:
        msgid = db.ec_email.insert(ec_to=','.join(tos),
            ec_from=mail.settings.sender,
            ec_replyto=convention.ec_contact_email if convention.ec_contact_email else convention.ec_hidden_email,
            ec_subject=subject,
            ec_bcc=bcc,
            ec_text=text,
            ec_email_type=etype,
            ec_id_convention=convention.id,
            ec_id_person=person.id
            )
    except:
        return [str(T('The message could not been sent')),]
    return _send_email(msgid)

def _show_auto_email_text(etype, convention=None, person=None, attendance=None, event=None):
    # Returns a string that contains the text that would be sent with this e-mail
    if etype=='deregistered_from_event':
        return '%s: %s' % (etype, T('Not implemented')) #_send_event_deregistration_notice(event,person)
    # Regular auto-emails
    config = db.ec_configuration[convention.ec_id_configuration] or None
    if not hasattr(config, 'ec_email_%s_text' % etype) or not getattr(config, 'ec_email_%s_text' % etype):
        #raise ValueError
        return str(T('No email template specified'))
    if not attendance and (person and convention):
        attendance=_get_attendance(convention,person.id)
    ed = _get_email_dict(convention=convention,person=person,attendance=attendance,event=event)
    text,subject = '',''
    if hasattr(config, 'ec_email_%s_text' % etype) and getattr(config, 'ec_email_%s_text' % etype):
        text += getattr(config, 'ec_email_%s_text' % etype) % ed
    if config.ec_email_signature:
        text += '\n'+config.ec_email_signature % ed
    if hasattr(config, 'ec_email_%s_subject' % etype) and getattr(config, 'ec_email_%s_subject' % etype):
        subject += getattr(config, 'ec_email_%s_subject' % etype) % ed
    if config.ec_email_tag:
        subject = config.ec_email_tag % ed +' '+subject
    emailtext = """%s: %s

%s"""  % (T('Subject'), subject, text)
    return emailtext


def _send_invitation(convention, tos, text, ec_for='convention', person=None, replyto=None):
    config=db.ec_configuration(convention.ec_id_configuration)
    ed = _get_email_dict(convention=convention)
    if config.ec_email_signature: text += '\n'+config.ec_email_signature % ed
    msgid = db.ec_email.insert(ec_to=','.join(tos),
        ec_from=mail.settings.sender,
        ec_replyto=replyto if replyto else convention.ec_contact_email if convention.ec_contact_email else convention.ec_hidden_email,
        ec_subject=T('You have an invitation for %s') % convention.ec_name,
        ec_text=text,
        ec_email_type='%s_invitation' % ec_for,
        ec_id_convention=convention.id,
        ec_id_person=person.id if person else None
        )
    return _send_email(msgid)

def _send_invitation_answer(convention, invite):
    inviter=db.ec_person(invite.ec_created_by)
    config=db.ec_configuration(convention.ec_id_configuration)
    text=None
    subject=None
    ed = _get_email_dict(convention=convention)
    if invite.ec_for=='convention':
        subject=T('%s Invitation #%s for %s has been answered') % (config.ec_email_tag % ed if config.ec_email_tag else convention.ec_name+':', invite.id, invite.ec_email)
        subject+=' (%s)' % MV(invite.ec_status)
    text=None
    if invite.ec_status=='accepted': text = '%s. %s.' % (subject, T('The invitation was accepted'))
    elif invite.ec_status=='accepted but not registered': text = '%s. %s.' % (subject, T('The invitation was accepted, but user did not register (yet)'))
    elif invite.ec_status=='declined': text = '%s. %s.' % (subject, T('The invitation was declined'))
    if not text: return T('Message not sent')
    if config.ec_email_signature: text += '\n'+config.ec_email_signature % ed
    msgid = db.ec_email.insert(
        ec_from=mail.settings.sender,
        ec_to=inviter.ec_email,
        ec_subject=subject,
        ec_text=text,
        ec_email_type='%s_invitation_answer' % invite.ec_for,
        ec_id_convention=convention.id,
        ec_id_person=inviter.id
        )
    return _send_email(msgid)

def _send_con_request_notification(convention):
    msgid = db.ec_email.insert(
        ec_from=mail.settings.sender,
        ec_to= ','.join(_get_email_autogroup(convention,'easycon_admin')),
        ec_subject='[Easy-Con] %s' % T('Convention requested'),
        ec_text="""%s: %s
%s.
%s
   """ % (T('Convention requested'),
       convention.ec_name,
       T('Go to %s to grant or reject this installation') % URL('default','grant_conventions', scheme=True, host=True),
       EC_SIGNATURE),
        ec_email_type='convention_request_notification',
        ec_id_convention=convention.id,
        )
    return _send_email(msgid)

def _send_news_request_notification(news):
    creator = db.auth_user(news.ec_created_by)
    msgid = db.ec_email.insert(
        ec_from=mail.settings.sender,
        ec_to= ','.join(_get_email_autogroup(None,'easycon_admin')),
        ec_subject='[Easy-Con] %s' % T('News entry requested'),
        ec_text="""
ID: %s
%s: %s
%s: %s %s
%s:
%s

%s.
%s
 """ % (news.id, T('Subject'), news.ec_subject, T('Created by'), creator.first_name, creator.last_name, T('Text'), news.ec_text,
    T('Go to %s to grant or reject news') % URL('default','manage_news', vars=dict(display='admin'), scheme=True, host=True),
    EC_SIGNATURE),
        ec_email_type='news_request_notification',
        )
    return _send_email(msgid)

def _send_single_news_email(news, users, format='text'):
    msgid = db.ec_email.insert(
        ec_from=mail.settings.sender,
        ec_to= ','.join(validate_emails(users=users)),
        ec_subject='[Easy-Con] %s: %s' % (T('New news entry'), news.ec_subject),
        ec_text= _get_news_text(news, False) + '\n\n--\n' + CHANGE_NEWSLETTER)
    return _send_email(msgid, format=format)

def _send_newsletter(news, users, format='text'):
    shortsep = '\n' + '-'*60 + '\n'
    sep = '\n' + shortsep + '\n'
    msgid = db.ec_email.insert(
        ec_from=mail.settings.sender,
        ec_to= ','.join(validate_emails(users=users)),
        ec_subject='[Easy-Con] %s' % T('Newsletter'),
        ec_text= sep + sep.join([_get_news_text(entry, False) for entry in news])+ shortsep + '--\n' + CHANGE_NEWSLETTER)
    return _send_email(msgid, format=format)

def _send_grant_mail(convention):
    tosdb = db((db.auth_group.role=='con_admin_%s' % convention.id) &\
        (db.auth_group.id==db.auth_membership.group_id) &\
        (db.auth_membership.user_id==db.auth_user.id)).select(db.auth_user.email)
    tos = set([to.email for to in tosdb])
    text = """Hallo,
wir haben deinen Installationswunsch bearbeitet und bei Easy-Con einen neuen Con angelegt.
Der Status deines Cons ist jetzt "to_be_configured"; bevor du loslegen kannst, musst du also noch ein paar Einstellungen zu deinen Wünschen vornehmen.
Solltest du schon einmal einen Con mit Easy-Con organisiert haben, kannst du auch frühere Einstellungen übernehmen.
Auf %(helppage)s findest du Informationen, wie es jetzt weiter geht.

Die URL zur Hauptseite zu deinem Con lautet: %(conpage)s

Viel Spaß damit!
   Daniela"""
    text_mapping = dict(helppage=URL('default','help', scheme=True, host=True), conpage=URL('convention','convention', args=convention.ec_short_name, scheme=True, host=True))
    msgid = db.ec_email.insert(ec_to=','.join(tos),
        ec_from=mail.settings.sender,
        ec_subject=T('Convention %s granted') % convention.ec_name,
        ec_bcc=auth.user.email,
        ec_text=text % text_mapping,
        ec_email_type='grant_convention',
        )
    return _send_email(msgid)

def _send_news_grant_mail(news):
    creator = db.auth_user(news.ec_created_by)
    text = T("""Hi %s,
we approved the following newsletter entry:

%s: %s
%s:
%s
%s""") % (creator.first_name, T('Subject'), news.ec_subject, T('Text'), news.ec_text, EC_SIGNATURE)
    if news.ec_publish_date == request.now.date():
        text += T('It appears today at the webpage and will be included in the next newsletter')
    else:
        text += T('It will appear at %s at the webpage and will be included in the next newsletter') % news.ec_publish_date.strftime(dateformatstring)
    text += """.

 Thank's a lot!"""
    language = creator.last_language if creator.last_language else 'en'
    T.force(language)
    msgid = db.ec_email.insert(ec_to=creator.email,
        ec_from=mail.settings.sender,
        ec_subject=T('News "%s" granted') % news.ec_subject,
        ec_text=text,
        ec_email_type='grant_news',
        )
    T.lazy=True
    return _send_email(msgid)

def _send_news_decline_mail(news):
    creator = db.auth_user(news.ec_created_by)
    text = T("""Hi %s,
we are sorry, but we cannot approve the following news entry:

%s: %s
%s:
%s

For the following reason:
%s

You may change and re-request your news entry.
%s""") % (creator.first_name, T('Subject'), news.ec_subject, T('Text'), news.ec_text, news.ec_comment, EC_SIGNATURE)
    msgid = db.ec_email.insert(ec_to=creator.email,
        ec_from=mail.settings.sender,
        ec_subject=T('News "%s" declined') % news.ec_subject,
        ec_text=text,
        ec_email_type='grant_news',
        )
    return _send_email(msgid)

def _send_task_reminder(convention, tasks=[]):
    config=db.ec_configuration(convention.ec_id_configuration)
    result = []
    emails = {}
    query = ((db.auth_group.role=='con_orga_%s' % convention.id) & (db.auth_group.id==db.auth_membership.group_id) & (db.auth_membership.user_id == db.auth_user.id))
    orgas = db(query).select(db.auth_user.ALL)
    if not tasks:
        tasks = db((db.ec_task.ec_id_convention==convention.id) &
                   (db.ec_task.ec_status.belongs(['1', '2']))).select(db.ec_task.ALL)
    allorga = False
    for task in tasks:
        text = _get_task_reminder(task)
        if task.ec_assigned_to:
            user = db.auth_user(task.ec_assigned_to)
            language = user.last_language if user.last_language else 'en'
            T.force(language)
            if user.email in emails:
                emails[user.email] += text
            else:
                emails[user.email] = text
        else:
            for orga in orgas:
                language = orga.last_language if orga.last_language else 'en'
                T.force(language)
                if orga.email in emails:
                    emails[orga.email] += text
                else:
                    emails[orga.email] = text
    for entry in emails:
        emails[entry] += "%s:\n" % T('You can manage your tasks here')
        emails[entry] += URL('convention', 'orgatasks', args=[convention.ec_short_name], scheme=True, host=True)
    subject = T('Friendly task reminder')
    sig = '\n\n'
    if config and config.ec_email_tag:
        ed = _get_email_dict(convention=convention)
        subject = config.ec_email_tag % ed + ' ' + subject
        sig += config.ec_email_signature % ed
    for entry in emails:
        T.force(emails[entry][0])
        msgid = db.ec_email.insert(ec_to=entry,
            ec_from=mail.settings.sender,
            ec_subject=subject,
            ec_text=emails[entry] + sig,
            ec_email_type='task_reminder',
            ec_id_convention=convention.id,
            )
        if auth.is_logged_in():
            T.force(auth.user.last_language)
        result.append(';'.join(_send_email(msgid)))
    T.lazy=True
    return '; '.join(result)

def _send_event_deregistration_notice(event, person):
    """Informs SL that someone was deregistered from their event. Todo: Replace with auto_email"""
    convention=db.ec_convention(event.ec_id_convention)
    msgid = db.ec_email.insert(
        ec_from=mail.settings.sender,
        ec_to= ','.join(_get_email_autogroup(None,'event_orga_%s' % event.id)),
        ec_subject='[Easy-Con for %s] %s' % (convention.ec_short_name, T('Attendee deregistered from event')),
        ec_text=T('%s deregistered from event "%s" at convention "%s"') % (person.ec_nickname, event.ec_name, convention.ec_name),
        ec_email_type='deregistered_from_event',
        )
    return _send_email(msgid)