# coding: utf8
from gluon.contrib.pyfpdf import FPDF, HTMLMixin

class Ec_pdftemplate(FPDF, HTMLMixin):
    def __init__(self, title=None, convention=None, page_numbers=True):
        FPDF.__init__(self)
        t = response.title if not title else title
        self.title=t
        self.config = db.ec_configuration(convention.ec_id_configuration) if convention else None
        self.font = self.config.ec_design_pdf_font if self.config else 'Arial'
        self.page_numbers = page_numbers
        
    def header(self):
        self.set_font(self.font, '', 12)
        self.cell(0,8,txt=self.title,border='B',align='C')
        self.ln(20)

    def footer(self):
        # "hook to draw custom page footer (printing page numbers)"
        if self.page_numbers:
            self.set_y(-15)
            self.set_font(self.font,'I',8)
            txt = '%s/%s' % (self.page_no(), self.alias_nb_pages())
            self.cell(0,10,txt,0,0,'C')
        
    def write_html(self, html):
        if not html.startswith('<font'):
            html = "<font face='%s'>%s</font>" % (self.font, html)
        return HTMLMixin.write_html(self, html)

