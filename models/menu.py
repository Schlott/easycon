### helpers
def _get_convention_conname(conname):
    conname = conname
    conventions = db(db.ec_convention.ec_short_name==conname).select()
    if conventions:
        return conventions[0]
    else:
        return None

### settings

response.title = settings.title
response.subtitle = settings.subtitle
response.meta.author = '%(author)s <%(author_email)s>' % settings
response.meta.keywords = settings.keywords
response.meta.description = settings.description

### default menu
response.menu = [
    (T('Welcome'),False,URL('default','index')),
    (T('Open Conventions'),False,URL('default','open_conventions')),
    (T('Convention series'),False,URL('default','series')),
    (T('My Conventions'),False,URL('default','my_conventions')),
]
if auth.is_logged_in():
    response.menu.append((T('My Data'),False,URL('mydata','index')))
    response.menu.append((T('New Installation'), False, URL('default', 'request_installation')))
if auth.is_logged_in() and auth.has_membership('easycon_admin'):
    response.menu.append((T('Admin Tasks'), False, URL('default', 'admin')))

response.menu.append((T('News'), False, URL('default','news')))
response.menu.append((T('Help'), False, URL('default','help')))

### convention specific menu
if request.controller == 'convention':
    if request.function=='viewevent':
        event=db.ec_event(request.args(0)) # only eventid
        if event: # first argument is eventid? --> redirect
            convention=db.ec_convention(event.ec_id_convention)
            if convention: redirect(URL('convention','viewevent',args=[convention.ec_short_name] + request.args))
    convention=_get_convention_conname(request.args(0))
    if not convention or (convention.ec_convention_state == 'to_be_configured' and not auth.has_permission('configure', 'ec_convention', convention.id)):
        if auth.is_logged_in() and not auth.user.privacy_consent:
            _redirect_message(T('Please accept first our privacy notice'),URL('default','user',args=['profile']))
        else:
            _redirect_message(T('This convention is not public (yet)'), URL('default','index'))
    _build_response_menu(convention)
