# coding: utf8

# table definitions that use auth_tables
db.define_table('ec_systemevent',
    SQLField('ec_when', type='datetime', required=True, notnull=True, default=request.now, label=T('When did it happen?'), readable=True, writable=False,  ),
    SQLField('ec_what', type='string', default= 'unknown', label=T('What did happen?'), readable=True, writable=False,  ),
    SQLField('ec_type', type='string', label=T('Type of the system event'), readable=True, writable=False,  ),
    SQLField('ec_id_convention', db.ec_convention, label=T('Convention'), readable=True, writable=True,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_convention.id', '%(ec_name)s')), represent=lambda id, row: '%(ec_name)s' % db.ec_convention[id] if id else ''),
    SQLField('ec_id_auth_user', db.auth_user, label=T('Caused by account'), readable=True, writable=True,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'auth_user.id', '%(email)s')), represent=lambda id, row: '%(email)s' % db.auth_user[id] if id else ''),
    SQLField('ec_id_person', db.ec_person, label=T('Caused by person'), readable=True, writable=True,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_person.id', '%(ec_nickname)s')), represent=lambda id, row: '%(ec_nickname)s' % db.ec_person[id] if id else ''),
)

db.define_table('ec_task',
    SQLField('ec_name', type='string', required=True, notnull=True, label=T('Name'), readable=True, writable=True,  ),
    SQLField('ec_status', type='string', required=True, notnull=True, default= '2', label=T('Status'), requires=IS_IN_SET([(x, T(TASK_STATE_REPRESENT[x])) for x in TASK_STATE_CHOICES], zero=None), readable=True, writable=True, represent=lambda value, row: T(TASK_STATE_REPRESENT[value])),
    SQLField('ec_priority', type='string', required=True, notnull=True, default= '2', label=T('Priority'), requires=IS_IN_SET([(x, T(TASK_PRIORITY_REPRESENT[x])) for x in TASK_PRIORITY_CHOICES], zero=None), readable=True, writable=True, represent=lambda value, row: T(TASK_PRIORITY_REPRESENT[value])),
    SQLField('ec_start_date', type='datetime', label=T('When should it start?'), readable=True, writable=True, represent = lambda value, row: value if value else '' ),
    SQLField('ec_due_date', type='datetime', label=T('When is it due?'), readable=True, writable=True, represent = lambda value, row: value if value else '' ),
    SQLField('ec_description', type='text', label=T('Beschreibung'), readable=True, writable=True,  ),
    SQLField('ec_assigned_to', db.auth_user, label=T('Assigned to'), readable=True, writable=True,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'auth_user.id', '%(first_name)s %(last_name)s')), represent=lambda id, row: '%(first_name)s %(last_name)s' % db.auth_user[id] if id else ''),
    SQLField('ec_created_by', db.auth_user, label=T('Created by'), readable=True, writable=False,  requires=IS_IN_DB(db, 'auth_user.id', '%(first_name)s %(last_name)s'), represent=lambda id, row: '%(first_name)s %(last_name)s' % db.auth_user[id] if id else ''),
    SQLField('ec_id_convention', db.ec_convention, label=T('Convention'), readable=False, writable=False,  requires=IS_IN_DB(db, 'ec_convention.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_convention[id] if id else ''),
)

def represent_publish_date(value, row):
    if not value:
        if hasattr(row,'ec_earliest_publish_date') and row.ec_earliest_publish_date:
            return '>= %s' % row.ec_earliest_publish_date.strftime(dateformatstring)
        else:
            return T('not yet', lazy=False)
    else:
        return value.strftime(dateformatstring)

db.define_table('ec_news',
    SQLField('ec_subject', type='string', required=True, notnull=True, label=T('Subject'), readable=True, writable=True,  ),
    SQLField('ec_text', type='text', label=T('Text'), readable=True, writable=True,  ),
    SQLField('ec_comment', type='text', label=T('Comment'), readable=True, writable=False, comment=T('Will be used to explain, e.g., why a entry cannot be published'), represent=lambda comment, row: comment if comment else ''),
    SQLField('ec_request_date', type='datetime', label=T('Request date'), readable=False, writable=False, represent = lambda value, row: value.strftime(datetimeformatstring) if value else str(T('not yet'))),
    SQLField('ec_approve_date', type='datetime', label=T('Approval date'), readable=False, writable=False, represent = lambda value, row: value.strftime(datetimeformatstring) if value else str(T('not yet'))),
    SQLField('ec_earliest_publish_date', type='date', label=T('Earliest publishing date'), readable=False, writable=True, comment=EX('publish_date_news'), represent = lambda value, row: value.strftime(dateformatstring) if value else str(T('not yet'))),
    SQLField('ec_publish_date', type='date', label=T('Publishing date'), readable=True, writable=False, represent = represent_publish_date),
    SQLField('ec_email_count', type='integer', default=0, label=T('Receipients (e-mail)'), readable=True, writable=False, represent = lambda value, row: value if value != None else str(T('none yet')) ),
    SQLField('ec_public', type='boolean', required=True, notnull=True, default=False, label=T('Should this entry be public?'), readable=True, writable=True, comment=T('i.e., without being logged-in'), ),
    SQLField('ec_status', type='string', required=True, notnull=True, default= 'draft', label=T('Status'), requires=IS_IN_SET([(x, MV(x)) for x in NEWS_STATE_CHOICES], zero=None), readable=True, writable=False, represent = lambda value, row: str(T(value))  ),
    SQLField('ec_created_on', type='datetime', required=True, notnull=True, default=request.now, label=T('Created on'), readable=False, writable=False,  ),
    SQLField('ec_created_by', db.auth_user, label=T('Created by'), readable=True, writable=False,  requires=IS_IN_DB(db, 'auth_user.id', '%(first_name)s %(last_name)s'), represent=lambda id, row: '%(first_name)s %(last_name)s' % db.auth_user[id] if id else ''),
    SQLField('ec_id_convention', db.ec_convention, label=T('Convention'), readable=True, writable=True,  requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_convention.id', '%(ec_name)s')), represent=lambda id, row: '%(ec_name)s' % db.ec_convention[id] if id else '', comment=EX('news_for_conventions')),
)


# definitions for attachments
def _has_image_extension(filename):
    # if it is really an image is checked in _validate_upload
    result = False
    for ext in IMAGE_EXTENSIONS:
        if filename.endswith(ext):
            return True

def _validate_image_upload(form):
    convention = db.ec_convention[session.conventionid]
    if not _has_image_extension(form.vars.ec_file.filename): # hack since IS_IMAGE does not understand my extensions parameter
        form.errors.ec_file = T('Invalid image')

def _get_file_url(row):
    # get the url of a given file entry
    convention=db.ec_convention(row.ec_id_convention)
    url = URL('convention','file',args=[convention.ec_short_name, row.ec_name], scheme=True, host=True)
    return url

def _has_file_permission(row):
    # checks whether someone can read a file based on ec_hidden and ec_public
    if row.ec_public and not row.ec_hidden:
        return True
    if auth.is_logged_in():
        if row.ec_hidden:
            return auth.has_permission('organize', db.ec_convention, row.ec_id_convention)
        else:
            return True
    return False

db.define_table('ec_file',
    SQLField('ec_name', type='string', label=T('Filename'), requires=IS_SLUG_FILENAME(), readable=True, writable=False),
    SQLField('ec_file', type='upload', required=True, notnull=True, label=T('File'), requires=IS_UPLOAD_FILENAME(extension='pdf|jpg|png|zip|bmp'), readable=True, writable=True, autodelete=True ),
    SQLField('ec_is_image', type='boolean', required=True, notnull=True, default=False, label=T('Is this an image?')),
    SQLField('ec_size', type='integer', label=T('Size'), readable=True, writable=False, represent=lambda size, row: '%.1f kB' % (float(size) / 1024)),
    SQLField('ec_description', type='text', label=T('Description'), readable=True, writable=True,  ),
    SQLField('ec_uploaded_on', type='datetime', required=True, notnull=True, default=request.now, label=T('Uploaded on'), readable=True, writable=False,  ),
    SQLField('ec_uploaded_by', db.auth_user, label=T('Uploaded by account'), readable=True, writable=True,  requires=IS_IN_DB(db, 'auth_user.id', '%(email)s'), represent=lambda id, row: '%(email)s' % db.auth_user[id] if id else ''),
    SQLField('ec_hidden', type='boolean', required=True, notnull=True, default=True, label=T('Should this file be only for organizers?'), readable=True, writable=True, comment=T('Will override the public setting'), ),
    SQLField('ec_public', type='boolean', required=True, notnull=True, default=False, label=T('Should this file be public?'), readable=True, writable=True, comment=T('i.e., without being logged-in'), ),
    SQLField('ec_id_convention', db.ec_convention, label=T('Convention'), readable=False, writable=False,  requires=IS_IN_DB(db, 'ec_convention.id', '%(ec_name)s'), represent=lambda id, row: '%(ec_name)s' % db.ec_convention[id] if id else ''),
    SQLField('ec_url', type='string', label=T('URL'), readable=True, writable=False, compute=_get_file_url, represent=lambda x, row: A(x, _href=x) if x else '' ),
)



db.ec_file.ec_file.authorize = _has_file_permission

