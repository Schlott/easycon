import os
from string import printable

class flc(float):
# behaves like float but prints so that German Excel can use it
    def __str__(self):
        return self.__repr__().replace(".",",")

def _lower_emails():
    db(db.auth_user.email != db.auth_user.email.lower()).update(email=db.auth_user.email.lower())
    db(db.ec_person.ec_email != db.ec_person.ec_email.lower()).update(ec_email=db.ec_person.ec_email.lower())
    return

def _get_convention_conname(conname):
    convention = db(db.ec_convention.ec_short_name==conname).select().first()
    if convention:
        return convention
        session.conventionid = convention.id
    else:
        return None

def _get_or_create_first_person_auth(is_me=False):
    if is_me:
        persons = db(auth.accessible_query('is_me', db.ec_person)).select(db.ec_person.ALL,orderby=['id']) or []
        if len(persons) == 1:
            auth.user.active_person = persons[0].id
            db.commit()
            return persons[0]
    else:
        persons = db(auth.accessible_query('edit', db.ec_person)).select(db.ec_person.ALL,orderby=['id']) or []
    if len(persons)==0: # no person entry for that user yet? Create one!
        person_id = db.ec_person.insert(ec_firstname=auth.user.first_name,
                                    ec_nickname=auth.user.nick_name,
                                    ec_lastname=auth.user.last_name,
                                    ec_email=auth.user.email)
        auth.add_permission(auth.user_group(), 'is_me', db.ec_person, person_id)
        auth.add_permission(auth.user_group(), 'edit', db.ec_person, person_id)
        auth.user.active_person = person_id
        db.commit()
        return db.ec_person[person_id]
    if auth.user.active_person != -1: # there is a person stored in user information? Return it!
        person = db.ec_person[auth.user.active_person]
        if person:
            db.commit()
            return person
    # else: return first person
    auth.user.active_person = persons[0].id
    db.commit()
    return persons[0]

def _check_page_permission(page, convention):
    if page.ec_hidden and not auth.has_permission('configure', db.ec_convention, convention.id):
        return False
    if not auth.is_logged_in() and not page.ec_public:
        return False
    return True

def _check_person_auth():
    persons = db((db.ec_person.ec_email==auth.user.email)& ~(auth.accessible_query('edit', db.ec_person))).select(db.ec_person.ALL)
    for p in persons: # A person exist with same e-mail as the user!
        # you may edit the person
        auth.add_permission(auth.user_group(), 'edit', db.ec_person, p.id)
        if p.ec_firstname==auth.user.first_name and p.ec_lastname==auth.user.last_name:
            auth.add_permission(auth.user_group(), 'is_me', db.ec_person, p.id)
        # you may edit all events that are organized by that person
        hostevents=db((db.ec_organizes_event.ec_id_person==p.id)).select()
        for hostevent in hostevents:
            auth.add_permission(auth.user_group(), 'edit', db.ec_event, hostevent.ec_id_event)
    return persons

def _check_warnings(convention,what):
    config=db.ec_configuration(convention.ec_id_configuration)
    warnings = ''
    if what=='events':
        if auth.is_logged_in:
            person = db.ec_person(auth.user.active_person) or _get_or_create_first_person_auth()
            # check SL min-max event registrations
            event_registration_warnings = ''
            if _is_eventorga(convention.id, person.id):
                if config.ec_event_registration_limit_eventorganizer == 0:
                    event_registration_warnings += '%s! %s.\n' % (T('Warning'), T('As an event organizer, you may not register online for events'))
                elif config.ec_event_registration_limit_eventorganizer > 0:
                    attended_events = _get_attended_events(convention.id,person.id)
                    if attended_events > config.ec_event_registration_limit_eventorganizer:
                        event_registration_warnings += '%s! %s.\n' % (T('Warning'), T('As an event organizer, you may only register online for %s event(s)') % config.ec_event_registration_limit_eventorganizer)
            else:
                if config.ec_event_registration_limit_normalattendee > -1:
                    if config.ec_event_registration_limit_normalattendee == 0:
                        event_registration_warnings += '%s! %s.' % (T('Warning'), T('If you do not organize an event, you may currently not register online for events'))
                    attended_events = _get_attended_events(convention.id, person.id)
                    if attended_events > config.ec_event_registration_limit_normalattendee:
                        event_registration_warnings += '%s! %s.' % (T('Warning'), T('If you do not organize an event, you may currently only register online for %s event(s)') % config.ec_event_registration_limit_normalattendee)
            if event_registration_warnings:
                warnings += event_registration_warnings + '\n' + T('Please deregister from some events') + '.'
    return warnings

def _select_convention_form(conventions, cid=None):
    # returns selection form for conventions
    if not conventions or len(conventions)==0:
        return
    else:
        options = [OPTION('%(ec_name)s' % conventions[i], _value=str(conventions[i].id)) for i in range(len(conventions))]
        conventionform = FORM(
              SELECT(_name='conventionid', *options, value=cid),
              INPUT(_type='submit', _value=T('Choose')))
    return conventionform

def _get_person_form(persons):
    # create selection form for person
    options = [OPTION('%(ec_firstname)s %(ec_lastname)s' % persons[i], _value=str(persons[i].id)) for i in range(len(persons))]
    options.append(OPTION(T('Register new person'), _value=-1))
    personform = FORM(TR(T('You can register the following people')+':',
              SELECT(_name='personid', *options)),
              TR(INPUT(_type='submit', _value=T('Choose'))))
    return personform

def _get_item_select(convention):
    items=db(db.ec_buyitem.ec_id_convention==convention.id).select()
    options = [OPTION('%(ec_name)s' % items[i], _value=str(items[i].id)) for i in range(len(items))]
    return TR(TD(T('Buyable items')),TD(SELECT(_name='itemid', *options, _multiple='multiple')))

def _get_registration_fields(convention, attendance=None, person=None):
    # creates optionally prefilled registration list of fields; if attendance is given, person is ignored
    config = db.ec_configuration[convention.ec_id_configuration]
    if attendance:
        person = db.ec_person[attendance.ec_id_person]
    fields = [] # all fields for form
    # create person fields for form based on config
    person_fields = ['ec_firstname','ec_nickname','ec_lastname','ec_email']
    for f in person_fields:
        if not getattr(db.ec_person, f).label.endswith('*'):
            setattr(getattr(db.ec_person, f), 'label', '%s*'% getattr(db.ec_person, f).label)
    if config:
        for key in db.ec_configuration.fields:
            if key.startswith('ec_registration_data_person_'):
                pkey='ec_'+key[len('ec_registration_data_person_'):]
                if getattr(config, key) in ['required','optional']:
                    person_fields.append(pkey)
                if getattr(config, key)=='required':
                    if not getattr(db.ec_person, pkey).label.endswith('*'):
                        setattr(getattr(db.ec_person, pkey), 'label', '%s*'% getattr(db.ec_person, pkey).label)
    for fieldstring in person_fields:
        f = getattr(db.ec_person,fieldstring)
        if person:
            f.set_attributes(default=person[fieldstring])
        fields.append(f)
    # append additional person fields from config
    p_extra = db((db.ec_extrafield.ec_id_convention==convention.id)&(db.ec_extrafield.ec_entity=='person')).select(orderby=['ec_order'])
    for p in p_extra:
        f = Field('extra_%s'%p.id, p.ec_datatype, required=(not p.ec_is_optional), comment=p.ec_comment, label=p.ec_name)
        fields.append(f)
    # create arrival and departure field for form based on config
    field=db.ec_attendance.ec_attendance_comment
    if attendance:
        field.default=attendance.ec_attendance_comment
    fields.append(field)
    # append additional person fields from config
    a_extra = db((db.ec_extrafield.ec_id_convention==convention.id)&(db.ec_extrafield.ec_entity=='attendance')).select(orderby=['ec_order'])
    for a in a_extra:
        f = Field('extra_%s'%a.id, a.ec_datatype, required=(not a.ec_is_optional), comment=a.ec_comment, label=a.ec_name)
        fields.append(f)
    if config and convention.ec_start_date and convention.ec_end_date:
            from datetime import timedelta
            if config.ec_registration_perday or config.ec_registration_data_attendance_arrival in ['required','optional']:
                arrivals=[]
                for day in config.ec_registration_perday_arrival_days.split(','):
                    aday=convention.ec_start_date+timedelta(days=int(day)-1)
                    arrivals.append(aday)
                field=db.ec_attendance.ec_arrival
                zero = T("Don't know yet") if (config.ec_registration_data_attendance_arrival=='optional' and not config.ec_registration_perday) else None
                field.requires = IS_IN_SET(arrivals,zero=zero)
                if attendance:
                    field.default=attendance.ec_arrival
                else:
                    field.default=arrivals[0]
                fields.append(field)

            if config.ec_registration_perday or config.ec_registration_data_attendance_departure in ['required','optional']:
                departures=[]
                for day in config.ec_registration_perday_departure_days.split(','):
                    dday=convention.ec_start_date+timedelta(days=int(day)-1)
                    departures.append(dday)
                field=db.ec_attendance.ec_departure
                zero = T("Don't know yet") if (config.ec_registration_data_attendance_departure=='optional' and not config.ec_registration_perday) else None
                field.requires = IS_IN_SET(departures,zero=zero)
                if attendance:
                    field.default=attendance.ec_departure
                else:
                    field.default=departures[0]
                fields.append(field)

    # create item fields based on config
    items = db(db.ec_buyitem.ec_id_convention==convention.id).select(orderby=['ec_order'])
    # create one field as heading to start the item section
    if items:
        f = Field('buyitems_heading', 'boolean') # not really rendered, just to display errors
        fields.append(f)
    for item in items:
        crules = db((db.ec_has_rule.ec_id_buyitem==item.id)&(db.ec_has_rule.ec_id_rule==db.ec_buyitemrule.id)&(db.ec_buyitemrule.ec_type=='one_of_these_items')).select(db.ec_buyitemrule.ALL)
        if crules: # there are choice rules for this , build rule field
            for r in crules:
                default = None
                citems = db((db.ec_buyitem.id==db.ec_has_rule.ec_id_buyitem)&(db.ec_has_rule.ec_id_rule==r.id)).select(db.ec_buyitem.ALL, db.ec_has_rule.ALL, orderby=['ec_list_order']) # the other items
                for citem in citems: # fill with value, generate default
                    if attendance and db((db.ec_buys_item.ec_id_person==person.id)&(db.ec_buys_item.ec_buy_state=='booked')&(db.ec_buys_item.ec_id_buyitem==citem.ec_buyitem.id)).select():
                        default=citem.ec_buyitem.id
                    elif citem.ec_buyitem.ec_is_preselected:
                        default=citem.ec_buyitem.id
                if citems[0].ec_buyitem.id == item.id: # we are the first, build the group
                    query = ((db.ec_buyitem.id==db.ec_has_rule.ec_id_buyitem)&(db.ec_has_rule.ec_id_rule==r.id))
                    ritems = db(query).select(db.ec_buyitem.ALL, orderby=['ec_buyitem.ec_order'])
                    idict = {}
                    commentlist = []
                    for ritem in ritems:
                        availability=_free_items(ritem)
                        if availability==-1:
                            idict[ritem.id] = '%s: %.2f %s'%(ritem.ec_name, ritem.ec_price, config.ec_registration_currency_symbol)
                            commentlist.append('%s: %s' % (ritem.ec_name, str(T('unlimited'))))
                        elif availability==0:
                            idict[ritem.id] = '%s %s %s: %.2f %s' % (T('waitlist'), T('for'), ritem.ec_name, ritem.ec_price, config.ec_registration_currency_symbol)
                            commentlist.append('%s: %s' % (ritem.ec_name, str(T('currently sold out'))))
                        else:
                            idict[ritem.id] = '%s: %.2f %s'%(ritem.ec_name, ritem.ec_price, config.ec_registration_currency_symbol)
                            commentlist.append('%s: %s' % (ritem.ec_name, str(T('%s left') % availability)))

                    comment=', '.join(commentlist)
                    f = Field('rule_%s'%r.id,
                                'integer',
                                default=default,
                                label=r.ec_name,
                                requires=IS_IN_SET(idict, zero=None),
                                widget=SQLFORM.widgets.radio.widget,
                                comment=comment,
                                readable=False,)
                    fields.append(f)
        elif item.ec_is_heading: # add a field, but with different name
            f = Field('heading_item_%s'%item.id, 'boolean', label=item.ec_name)
            fields.append(f)
        else: # no choice rules, just the item
            if attendance:
                default = db((db.ec_buys_item.ec_id_person==person.id)&(db.ec_buys_item.ec_buy_state=='booked')&(db.ec_buys_item.ec_id_buyitem==item.id)).select()
            else:
                default = item.ec_is_preselected
            label = '%s: %.2f %s' % (item.ec_name,item.ec_price,config.ec_registration_currency_symbol)
            availability = _free_items(item)
            if availability==-1:
                comment = T('unlimited')
            elif availability==0:
                comment = T('currently sold out')
                label = '%s %s %s: %.2f %s' % (T('waitlist'), T('for'), item.ec_name, item.ec_price, config.ec_registration_currency_symbol)
            else:
                comment = T('%s left') % availability
            f = Field('item_%s'%item.id, 'boolean', default=default, label=label, comment=comment)
            fields.append(f)
    # append global waitlist field
    f = Field('waitlist_only', 'boolean', label=T('Register for waitlist only'), comment=T('If you register for waitlist only, you will not be registered for the convention. However, you might be asked later by the convention organization if you want to move up to a full registration'))
    fields.append(f)
    return fields

def _get_event_fields(config,eventtype,mode='single'):
    # determines which fields are relevant for a given config and an eventtype
    fields = ['ec_name']
    if mode=='list':
        fields.append('hosts')
    if eventtype=='all':
        fields.append('ec_type')
    if mode=='single':
        for f in ['ec_min_attendees', 'ec_max_attendees']:
            fields.append(f)
        if config.ec_event_attendance:
            fields.append('ec_apply_online')
    if config.ec_event_time_slots:
        if mode=='list': fields.append('nosort_slot')
        fields.append('ec_id_timeslot')
    if config.ec_event_free_time or mode=='list':
        fields.append('ec_start')
        fields.append('ec_end')
    elif auth.has_membership('easycon_admin'):
        for field in ['ec_start','ec_end']:
            fields.append(field)
            db.ec_event[field].label = db.ec_event[field].label + ' ' + T('(Easy-Con Admin only)')
    if eventtype == 'rpggame':
        fields.append('ec_id_rpggame')
        if mode=='single':
            for f in ['ec_prepared_chars', 'ec_introduction', 'ec_rpg_newbees']:
                fields.append(f)
    if mode=='list':
        fields.append('ec_registertime')
    if mode=='list' and config.ec_event_attendance:
        fields.append('vacancies')
    if mode=='single':
        for f in ['ec_url', 'ec_description']:
            fields.append(f)
    return fields

def _get_event_types(config):
    eventtypes = []
    for et in EVENT_TYPE_CHOICES:
        if et !='misc' and config['ec_event_type_%s' % et]:
            eventtypes.append(et)
    eventtypes.append('misc')
    return eventtypes

def _get_timeslots(convention):
    # build explicit set to enforce time order
    timeslots_db = db(db.ec_timeslot.ec_id_convention==convention.id).select(orderby=[db.ec_timeslot.ec_start_time])
    timeslots = []
    for t in timeslots_db:
        tname = t.ec_name
        if t.ec_start_time:
            tname += ' ('
            if t.ec_end_time:
                if t.ec_end_time.date()==t.ec_start_time.date():
                    tname += '%s - %s)' % (t.ec_start_time.strftime(datetimeformatstring), t.ec_end_time.strftime(timeformatstring))
                else:
                    tname += '%s - %s)' % (t.ec_start_time.strftime(datetimeformatstring), t.ec_end_time.strftime(datetimeformatstring))
            else:
                tname += '%s)' % t.ec_start_time.strftime(datetimeformatstring)
        timeslots.append((t.id, tname))
    return timeslots

def _event_registration_open(convention):
    config = db.ec_configuration[convention.ec_id_configuration]
    if not config.ec_events:
        return False
    else:
        return _check_valid_dates(config.ec_event_registration_period_start, config.ec_event_registration_period_stop)

def _get_item_list(ruleid):
    itemsdict=db((db.ec_buyitem.id==db.ec_has_rule.ec_id_buyitem)&(db.ec_has_rule.ec_id_rule==ruleid)).select(db.ec_buyitem.ec_name).as_list()
    return ', '.join([i['ec_name'] for i in itemsdict])

def _get_rule_list(id):
    rulesdict=db((db.ec_buyitemrule.id==db.ec_has_rule.ec_id_rule)&(db.ec_has_rule.ec_id_buyitem==id)).select(db.ec_buyitemrule.ALL).as_list()
    #return ', '.join(['%(ec_name)s (%(ec_type)s)' % r for r in rulesdict])
    return ', '.join(['%(ec_type)s (%(id)s)' % r for r in rulesdict])

def _check_convention_permission(what):
    if not what:
        return False
    if not request.args(0):
        return False
    convention=_get_convention_conname(request.args(0))
    if not convention:
        return False
    else:
        return auth.has_permission(what, db.ec_convention, convention.id)

def _check_convention_person_permission(what):
    if not what:
        return False
    if not (request.args(0) and request.args(1)):
        return False
    convention=_get_convention_conname(request.args(0))
    person=db.ec_person[request.args(1) or auth.user.active_person]
    if not (convention and person):
        return False
    else:
        return auth.has_permission(what, db.ec_person, person.id)

def _check_auth_convention_attendee(regstates):
    if not auth.is_logged_in(): return False
    convention=_get_convention_conname(request.args(0))
    if not convention: return False
    people = db((auth.accessible_query('edit', db.ec_person)) &\
                (db.ec_attendance.ec_id_person==db.ec_person.id) &\
                (db.ec_attendance.ec_registration_state.belongs(regstates)) &\
                (db.ec_attendance.ec_id_convention==convention.id)).select()
    return True if people else False

def _check_auth_event_attendee(event):
    if not auth.is_logged_in(): return False
    if not event: return False
    attendee = db((auth.accessible_query('edit', db.ec_person)) &\
                (db.ec_attends_event.ec_id_person==db.ec_person.id) &\
                (db.ec_attends_event.ec_id_event==event.id)).select()
    return True if attendee else False

def _check_multi_email_permission(people):
    for person in people:
        if not _check_email_permission(person):
            return False
    return True

def _check_email_permission(person): # check permission for e-mail
    if not person: return False
    if not auth.is_logged_in(): return False
    # check if someone organizes the convention that the person attends
    convention=_get_convention_conname(request.args(0))
    if _check_convention_permission('organize') and Registration.get_attendee_status(convention.id, person.id): return True
    # check if someone writes to herself or an user that she manages
    ownperson = db((auth.accessible_query('edit', db.ec_person) &\
                   (db.ec_person.id==person.id))).select(db.ec_person.id)
    if ownperson: return True
    # check common convention attendance
    receiver = db.ec_attendance.with_alias('receiver')
    common_attendance = db((auth.accessible_query('edit', db.ec_person) &\
                           (db.ec_attendance.ec_id_person==db.ec_person.id) &\
                           (db.ec_attendance.ec_id_convention==receiver.ec_id_convention))).select(receiver.ec_id_person, receiver.ec_id_convention, db.ec_attendance.ec_id_person)
    if common_attendance: return True
    # check if someone is attendee of an event of the receiver
    is_event_attendee = db((auth.accessible_query('edit', db.ec_person) &\
                     (db.ec_attends_event.ec_id_person==db.ec_person.id) &\
                     (db.ec_organizes_event.ec_id_person==person.id) &\
                     (db.ec_attends_event.ec_id_event==db.ec_organizes_event.ec_id_event))).select(db.ec_attends_event.ec_id_person, db.ec_attends_event.ec_id_event, db.ec_organizes_event.ec_id_person)
    if is_event_attendee: return True
    # check if someone is host of an event that the receiver attends
    is_host = db((auth.accessible_query('edit', db.ec_person) &\
                     (db.ec_organizes_event.ec_id_person==db.ec_person.id) &\
                     (db.ec_attends_event.ec_id_person==person.id) &\
                     (db.ec_attends_event.ec_id_event==db.ec_organizes_event.ec_id_event))).select(db.ec_attends_event.ec_id_person, db.ec_attends_event.ec_id_event, db.ec_organizes_event.ec_id_person)
    if is_host: return True
    # check if both attend the same event
    receiver = db.ec_attends_event.with_alias('receiver')
    common_event_attendance = db((auth.accessible_query('edit', db.ec_person) &\
                           (db.ec_attends_event.ec_id_person==db.ec_person.id) &\
                           (db.ec_attends_event.ec_id_event==receiver.ec_id_event))).select(receiver.ec_id_person, receiver.ec_id_event, db.ec_attends_event.ec_id_person)
    if common_event_attendance: return True
    return False

def _attends_other_event(timeslotid, personid, eventid=None):
    if not timeslotid: return False
    query = (db.ec_attends_event.ec_id_person==personid)&(db.ec_attends_event.ec_id_event==db.ec_event.id)&(db.ec_event.ec_id_timeslot==timeslotid)
    if eventid:
        query = query & (db.ec_event.id != eventid)
    return len(db(query).select()) > 0

def _get_attended_events(conventionid, personid):
    query = (db.ec_attends_event.ec_id_person==personid)&(db.ec_attends_event.ec_id_event==db.ec_event.id)&(db.ec_event.ec_id_convention==conventionid)
    result = db(query).select()
    return len(result)

def _hosts_other_event(timeslotid, personid, eventid=None):
    if not timeslotid: return False
    query = (db.ec_organizes_event.ec_id_person==personid)&(db.ec_organizes_event.ec_id_event==db.ec_event.id)&(db.ec_event.ec_id_timeslot==timeslotid)
    if eventid:
        query = query & (db.ec_event.id != eventid)
    return len(db(query).select()) > 0

def _validate_event(form):
    convention = db.ec_convention(session.conventionid)
    config = db.ec_configuration(convention.ec_id_configuration)
    person = db.ec_person(auth.user.active_person) or _get_or_create_first_person_auth()
    # check for double registrations
    if person and config.ec_event_time_slots and not config.ec_event_double_registration_timeslot:
        if _attends_other_event(form.vars.ec_id_timeslot, person.id, eventid=request.vars.id):
            form.errors.ec_id_timeslot = T('You already registered for an event at this time')
        elif _hosts_other_event(form.vars.ec_id_timeslot, person.id, eventid=request.vars.id):
            form.errors.ec_id_timeslot = T('You already host an event at this time')
    # check for end after begin
    if config.ec_event_free_time:
        if form.vars.ec_end and form.vars.ec_start and form.vars.ec_end < form.vars.ec_start:
            form.errors.ec_end = T('Without a time machine, you cannot begin after you ended')
    # check for begin, end, and timeslot
    if config.ec_event_time_slots and config.ec_event_free_time:
        if form.vars.ec_id_timeslot:
            timeslot = db.ec_timeslot(form.vars.ec_id_timeslot)
            if form.vars.ec_start and timeslot.ec_start_time and form.vars.ec_start < timeslot.ec_start_time:
                form.errors.ec_start = T('If you define a timeslot and event specific times, these times must be within the timeslot (i.e., start time after %s)') % timeslot.ec_start_time.strftime(datetimeformatstring)
            if form.vars.ec_end and timeslot.ec_end_time and form.vars.ec_end > timeslot.ec_end_time:
                form.errors.ec_end = T('If you define a timeslot and event specific times, these times must be within the timeslot (i.e., end time before %s)') % timeslot.ec_end_time.strftime(datetimeformatstring)
    # check if timeslot has changed
    event = db.ec_event(request.vars.id)
    newtimeslot = form.vars.ec_id_timeslot
    if event and newtimeslot:
        oldtimeslot = event.ec_id_timeslot
        if oldtimeslot is None or int(oldtimeslot) != int(newtimeslot): # overwrite event times with timeslot times
            timeslot = db.ec_timeslot(form.vars.ec_id_timeslot)
            form.vars.ec_start = timeslot.ec_start_time
            form.vars.ec_end = timeslot.ec_end_time
    # check if there is a description and that it contains only printable characters
    if not form.vars.ec_description:
        form.errors.ec_description = T('Please give a description of your event')
    else:
        allowed=set(printable).union(['Ä','Ö','Ü','ä','ö','ü','ß','§'])
        description=set(form.vars.ec_description)
        specials=description-allowed
        if specials:
            form.errors.ec_description = T('Do not use the following characters: %s') % specials

def _validate_convention(form):
    # check for end after begin
    if form.vars.ec_start_date and form.vars.ec_end_date and form.vars.ec_end_date < form.vars.ec_start_date:
        form.errors.ec_end_date = T('Without a time machine, you cannot begin after you ended')

def _validate_buyitem(form):
    # check if timeslot has changed
    buyitem = db.ec_buyitem(request.vars.id)
    newtimeslot = form.vars.ec_id_timeslot
    if buyitem and newtimeslot:
        oldtimeslot = buyitem.ec_id_timeslot
        if oldtimeslot is None or int(oldtimeslot) != int(newtimeslot): # overwrite buyitem time with timeslot times
            timeslot = db.ec_timeslot(form.vars.ec_id_timeslot)
            form.vars.ec_at_time = timeslot.ec_start_time

def _validate_service(form):
    # check if timeslot has changed
    service = db.ec_service(request.vars.id)
    newtimeslot = form.vars.ec_id_timeslot
    if service and newtimeslot:
        oldtimeslot = service.ec_id_timeslot
        if oldtimeslot is None or int(oldtimeslot) != int(newtimeslot): # overwrite service times with timeslot times
            timeslot = db.ec_timeslot(form.vars.ec_id_timeslot)
            form.vars.ec_start_time = timeslot.ec_start_time
            form.vars.ec_end_time = timeslot.ec_end_time

def _validate_page(form):
    convention = db.ec_convention(session.conventionid)
    # check for double page names
    pageid = request.vars.id
    if pageid:
        pages = db((db.ec_page.ec_id_convention == session.conventionid) & (db.ec_page.ec_name == form.vars.ec_name) & (db.ec_page.id != pageid)).select()
    else:
        pages = db((db.ec_page.ec_id_convention == session.conventionid) & (db.ec_page.ec_name == form.vars.ec_name)).select()
    if pages:
        form.errors.ec_name = T('This page already exists')

def _validate_registration(form):
    convention = db.ec_convention[session.conventionid]
    config = db.ec_configuration[convention.ec_id_configuration]

    for field in form.fields:
        # check required fields
        for thing in ['person', 'attendance']:
            cfield = 'ec_registration_data_%s_%s' % (thing, field[3:])
            if hasattr(config,cfield) and getattr(config,cfield)=='required':
                value, error = IS_NOT_EMPTY()(getattr(form.vars,field))
                if error:
                    setattr(form.errors, field, error)
        # check extra attributes
        if field.startswith('extra'):
            eid = _get_extraid(field)
            if eid:
                extrafield = db.ec_extrafield[eid]
                if extrafield.ec_is_optional==False:
                    value, error = IS_NOT_EMPTY()(getattr(form.vars,field))
                    if error:
                        setattr(form.errors, field, error)
        # check individual capacities
        itemid = _get_item_id(field)
        if itemid and not form.vars.waitlist_only:
            item = db.ec_buyitem[itemid]
            if not getattr(form.vars, field):
                error = _check_item_min(item)
                if error:
                    setattr(form.errors, field, error)

    combined_capacity_rules = db((db.ec_buyitemrule.ec_id_convention==convention.id)&(db.ec_buyitemrule.ec_type=='combined_capacity')).select()

    # check combined capacity rules
    messages = []
    for cr in combined_capacity_rules:
        bought = db((db.ec_has_rule.ec_id_rule==cr.id)&(db.ec_has_rule.ec_id_buyitem==db.ec_buys_item.ec_id_buyitem)&(db.ec_buys_item.ec_buy_state=='booked')).count()
        if bought > cr.ec_number:
            messages.append(cr.ec_message)
    if messages:
        form.errors.buyitems_heading = '; '.join(messages)

def _check_item_capacity(item):
    if item.ec_capacity!=-1 and _free_items(item)==0:
        return T('Item "%(ec_name)s" is sold out') % item.as_dict()

def _check_item_min(item):
    if item.ec_min > 0:
        #return T('Item "%(ec_name)s" needs to be bought at least once') % .as_dict()
        #TODO: replace when multiple items are allowed
        return T('You need to buy "%(ec_name)s"') % item.as_dict()

def _free_items(item):
    query = (db.ec_buyitemrule.ec_type=='combined_capacity') &\
            (db.ec_buyitemrule.id==db.ec_has_rule.ec_id_rule) &\
            (db.ec_has_rule.ec_id_buyitem==item.id)
    combined_capacity_rules = db(query).select(db.ec_buyitemrule.ALL)
    available = None
    if combined_capacity_rules:
        for cr in combined_capacity_rules:
            bought = db((db.ec_has_rule.ec_id_rule==cr.id)&(db.ec_has_rule.ec_id_buyitem==db.ec_buys_item.ec_id_buyitem)&(db.ec_buys_item.ec_buy_state=='booked')).count()
            if available is None:
                available = cr.ec_number - bought
            else:
                available = min(available, cr.ec_number - bought)
        available = max(0, available)
    else:
        if item.ec_capacity==-1:
            available = -1
        else:
            bought = db((db.ec_buys_item.ec_id_buyitem==item.id)&(db.ec_buys_item.ec_buy_state=='booked')).count()
            available = max(0, (item.ec_capacity or 0) - bought)
    return available

def _get_item_id(field):
    if field.startswith('item_'):
        itemid=field[len('item_'):]
        if db.ec_buyitem[itemid]:
            return itemid

def _get_ruleid(field):
    if field.startswith('rule_'):
        ruleid=field[len('rule_'):]
        if db.ec_buyitemrule[ruleid]:
            return ruleid

def _get_extraid(field):
    if field.startswith('extra_'):
        extraid=field[len('extra_'):]
        if db.ec_extrafield[extraid]:
            return extraid

def _get_item_sum(convention,personid):
    config = db.ec_configuration[convention.ec_id_configuration]
    attendance = _get_attendance(convention, personid)
    itemsum = db.ec_buyitem.ec_price.sum()
    if config.ec_registration_perday and attendance and attendance.ec_departure and attendance.ec_arrival:
        itemsum_single = db((db.ec_buyitem.id==db.ec_buys_item.ec_id_buyitem)&\
                    (db.ec_buyitem.ec_is_perday==False)&\
                    (db.ec_buys_item.ec_id_person==personid)&\
                    (db.ec_buys_item.ec_buy_state=='booked')&\
                    (db.ec_buyitem.ec_id_convention==convention.id)).select(itemsum).first()[itemsum] or 0
        itemsum_perday = db((db.ec_buyitem.id==db.ec_buys_item.ec_id_buyitem)&\
                    (db.ec_buyitem.ec_is_perday==True)&\
                    (db.ec_buys_item.ec_id_person==personid)&\
                    (db.ec_buys_item.ec_buy_state=='booked')&\
                    (db.ec_buyitem.ec_id_convention==convention.id)).select(itemsum).first()[itemsum] or 0
        days = (attendance.ec_departure - attendance.ec_arrival).days - 1
        days += config.ec_registration_perday_arrival_factor * 1
        days += config.ec_registration_perday_departure_factor * 1
        allitemsum = itemsum_single + days * itemsum_perday
    else:
        allitemsum = db((db.ec_buyitem.id==db.ec_buys_item.ec_id_buyitem)&\
        (db.ec_buys_item.ec_id_person==personid)&\
        (db.ec_buys_item.ec_buy_state=='booked')&\
        (db.ec_buyitem.ec_id_convention==convention.id)).select(itemsum).first()[itemsum]
    return allitemsum or 0

def _get_to_pay(itemid, attendance):
    item = db.ec_buyitem(itemid)
    if not (item and attendance): return 0
    if not item.ec_is_perday: return item.ec_price
    convention = db.ec_convention(item.ec_id_convention)
    config = db.ec_configuration(convention.ec_id_configuration)
    if config and config.ec_registration_perday and attendance:
        days = (attendance.ec_departure - attendance.ec_arrival).days - 1
        days += config.ec_registration_perday_arrival_factor * 1
        days += config.ec_registration_perday_departure_factor * 1
        return days * item.ec_price
    else:
        return 99 #item.ec_price

def _get_regdata(attendance):
    # returns registration data
    # was used for e-mails - not used anymore because of privacy
    # was replaced by _get_regdataurl - the url for registration
    convention=db.ec_convention[attendance.ec_id_convention]
    config = db.ec_configuration[convention.ec_id_configuration]

    person=db.ec_person[attendance.ec_id_person]
    regfields = _get_registration_fields(convention, person=person)

    regdata = ''
    for field in regfields:
        if hasattr(person, field.name) and getattr(person, field.name):
            value = getattr(person, field.name)
        elif hasattr(attendance, field.name) and getattr(attendance, field.name):
            value = getattr(attendance, field.name)
        else:
            continue
        if isinstance(value,datetime.date):
            regdata += '%s:  %s\n' % (field.label, value.strftime(dateformatstring))
        elif isinstance(value,datetime.datetime):
            regdata += '%s:  %s\n' % (field.label, value.strftime(datetimeformatstring))
        elif isinstance(value,bool):
            regdata += '%s:  %s\n' % (field.label, MV(value))
        else:
            regdata += '%s:  %s\n' % (field.label, value)
    regdata += '\n'
    items = db((db.ec_buys_item.ec_id_person==person.id)&\
               (db.ec_buys_item.ec_id_buyitem==db.ec_buyitem.id)&\
               (db.ec_buyitem.ec_id_convention==convention.id)&\
               (db.ec_buys_item.ec_buy_state=='booked')).select(db.ec_buyitem.ALL,orderby=db.ec_buyitem.ec_order)
    for item in items:
        regdata += '%s: %.2f %s\n' % (item.ec_name,item.ec_price,config.ec_registration_currency_symbol)
    if items:
        regdata += '%s: %.2f %s\n' % (T('Sum'), _get_item_sum(convention, person.id), config.ec_registration_currency_symbol)
    items = db((db.ec_buys_item.ec_id_person==person.id)&\
               (db.ec_buys_item.ec_id_buyitem==db.ec_buyitem.id)&\
               (db.ec_buyitem.ec_id_convention==convention.id)&\
               (db.ec_buys_item.ec_buy_state=='waitlist')).select(db.ec_buyitem.ALL,orderby=db.ec_buyitem.ec_order)
    if items:
        regdata += '%s:' % T('Waitlist')
    for item in items:
        regdata += '%s: %.2f %s\n' % (item.ec_name,item.ec_price,config.ec_registration_currency_symbol)
    return regdata

def _get_regdata_url(attendance):
    # returns some registration url
    convention=db.ec_convention[attendance.ec_id_convention]
    config = db.ec_configuration[convention.ec_id_configuration]
    person=db.ec_person[attendance.ec_id_person]
    regdataurl = URL('registration',args=[convention.ec_short_name,person.id],user_signature=False,scheme=True, host=True)
    return regdataurl

def _get_eventdata(event):
    eventdata='%s: %s\n' % (db.ec_event.ec_type.label, T(event.ec_type))
    for entry in _get_event_fields(event.ec_id_convention,event.ec_type):
        if getattr(db.ec_event,entry).represent:
            eventdata += '%s: %s\n' % (getattr(db.ec_event,entry).label, getattr(db.ec_event,entry).represent(getattr(event,entry),None))
        else:
            eventdata += '%s: %s\n' % (getattr(db.ec_event,entry).label, getattr(event,entry))
    return eventdata

def _get_attendance(convention, personid):
    # deprecated - use Registration objects
    attendance = db((db.ec_attendance.ec_id_person==personid)&(db.ec_attendance.ec_id_convention==convention.id)).select()
    if attendance:
        return attendance[0]

def _book_item(itemid, personid, bookingstate):
    buys = db((db.ec_buys_item.ec_id_buyitem==itemid)&(db.ec_buys_item.ec_id_person==personid)).select()
    if len(buys)==0:
        buyid = db.ec_buys_item.insert(ec_id_buyitem=itemid, ec_id_person=personid, ec_buy_state=bookingstate)
    elif len(buys)>1:
        db((db.ec_buys_item.ec_id_buyitem==itemid)&(db.ec_buys_item.ec_id_person==personid)).delete()
        buyid = db.ec_buys_item.insert(ec_id_buyitem=itemid, ec_id_person=personid, ec_buy_state=bookingstate)
    else: # len(buys)==1:
        buyid=buys[0].id
        db.ec_buys_item[buyid].update(ec_id_buyitem=itemid, ec_id_person=personid,ec_buy_state=bookingstate)
    return buyid

def _book_items(bookitems, waitlist_only):
    can_attend = False
    for bitem in bookitems['req']:
        if _free_items(bitem['item']) > 0 or _free_items(bitem['item']) == -1:
            can_attend=True
    for bitem in bookitems['req']+bookitems['opt']:
        bookingstate='booked' if _free_items(bitem['item']) != 0 and can_attend and not waitlist_only else 'waitlist'
        _book_item(bitem['item'].id, bitem['person'].id, bookingstate)
    return can_attend

def _set_extra_value(extraid,value,entityid):
    extra=db.ec_extrafield[extraid]
    if not extra:
        return None
    eid = db.ec_has_value.update_or_insert(ec_id_extrafield=extraid)
    updict = {'ec_id_%s'%extra.ec_entity: entityid,
              'ec_%svalue'%extra.ec_datatype: value}
    db(db[db.ec_has_value]._id==eid).update(**updict)
    return eid

def _should_be_on_waitlist(convention, person):
    # returns if someone should be on waitlist
    # is True if a person booked no items that are required for that convention
    conitems = db((db.ec_buyitem.ec_id_convention==convention.id)&\
        (db.ec_buyitem.ec_min>0)&\
        (db.ec_buyitem.id==db.ec_buys_item.ec_id_buyitem)&\
        (db.ec_buys_item.ec_id_person==person.id)&\
        (db.ec_buys_item.ec_buy_state=='booked')).select()
    return False if conitems else True

def _attendee_action(convention, person, action):
    # Deprecated; should be done by registration object
    config = db.ec_configuration(convention.ec_id_configuration)
    messages=[]
    itemsum = _get_item_sum(convention,person.id)
    attendance = _get_attendance(convention, person)
    if action=='hasallpaid':
        attendance.update_record(ec_payment=itemsum)
    if action=='hasallpaid' or action=='activate':
        attendance.update_record(ec_registration_state='attendee')
        mailresult = _send_auto_email('registration_got_money',convention=convention,person=person)
        messages.append((T('%s %s has paid %.2f %s', lazy=False) % (person.ec_firstname, person.ec_lastname, attendance.ec_payment, config.ec_registration_currency_symbol)))
        messages.append('%s %s: %s (%s)' % (T('State of registration'),T('was changed to'),'attendee',MV('attendee')))
        messages.append('%s: %s' % (T('Email'), ','.join(mailresult)))
        return '; '.join(messages)
    elif action=='decline':
        # change status of attendance in database
        db((db.ec_attendance.ec_id_person==person.id)&(db.ec_attendance.ec_id_convention==convention.id)).update(ec_registration_state='deregistered')
        attendance = _get_attendance(convention,person)
        # change status of all booked items of that person
        buyitems=db((db.ec_buys_item.ec_id_person==person.id)&(db.ec_buys_item.ec_id_buyitem==db.ec_buyitem.id)&(db.ec_buyitem.ec_id_convention==convention.id)).select(db.ec_buys_item.ALL)
        for bitem in buyitems:
            db(db.ec_buys_item.id==bitem.id).update(ec_buy_state='declined')
        # check for services of that person
        helping=db((db.ec_helps.ec_id_person==person.id)&(db.ec_helps.ec_id_service==db.ec_service.id)&(db.ec_service.ec_id_convention==convention.id)).select(db.ec_helps.id)
        for help in helping:
            db(db.ec_helps.id==help.id).delete()
        # create feedback message for orga
        mailresult = _send_auto_email('registration_decline',convention=convention,person=person)
        messages.append(str(T('%s %s declined') % (person.ec_firstname, person.ec_lastname)))
        if buyitems:
            messages.append(str(T('All buyable items have been set to "declined"')))
        if helping:
            messages.append(str(T('All help services of this person are removed')))
        if attendance.ec_payment > 0:
            messages.append(str(T('You might want to refund up to %.2f %s') % (attendance.ec_payment,config.ec_registration_currency_symbol)))
        messages.append('%s: %s' % (T('Email'), ','.join(mailresult)))
        return '; '.join(messages)

def _get_task_reminder(task):
    taskdict = db.ec_task(task.id).as_dict()
    for t in taskdict:
        if not taskdict[t]:
            taskdict[t] = T('n/a')
        elif t in ['ec_status','ec_priority']:
            taskdict[t] = db.ec_task[t].represent(taskdict[t], taskdict)
    taskdict['line'] = '-' * len(taskdict['ec_name'])
    taskdict['state'] = T('Status')
    taskdict['prio'] = T('Priority')
    taskdict['start'] = T('Start')
    taskdict['end'] = T('Due')
    taskdict['description'] = T('Description')
    taskdict['created_by'] = T('Created by')
    taskdict['convention'] = T('Convention')
    taskdict['conname'] = db.ec_convention(task.ec_id_convention).ec_name
    taskdict['organame'] = '%(first_name)s %(last_name)s' % db.auth_user(task.ec_created_by).as_dict()
    if task['ec_assigned_to']:
        text = T('The following task is assigned to you and might need your attention')
    else:
        text = T('The following task needs someone who does it')
    text += """:

    %(line)s
    %(ec_name)s
    %(line)s

    %(state)s: %(ec_status)s
    %(prio)s: %(ec_priority)s
    %(start)s: %(ec_start_date)s
    %(end)s: %(ec_due_date)s
    %(description)s: %(ec_description)s
    %(created_by)s: %(organame)s
    %(convention)s: %(conname)s

""" % taskdict
    return text

def _redirect_message(errormessage,url):
    session.flash = errormessage
    redirect(url)
def _get_helpers(serviceid):
    helpers = db((db.ec_helps.ec_id_service==serviceid)&(db.ec_person.id==db.ec_helps.ec_id_person)).select(db.ec_person.id)
    service = db(db.ec_service.id==serviceid).select(db.ec_service.ec_id_convention).first()
    plist = [Registration.get_nickname_with_status(service.ec_id_convention, p.id) for p in helpers]
    if plist:
        return ', '.join(plist)
    else:
        return T('not yet')

def _get_hosts(eventid):
    hosts = db((db.ec_organizes_event.ec_id_event==eventid)&(db.ec_organizes_event.ec_id_person==db.ec_person.id)).select(db.ec_organizes_event.ec_id_person)
    event = db(db.ec_event.id==eventid).select(db.ec_event.ec_id_convention).first()
    return ', '.join([Registration.get_nickname_with_status(event.ec_id_convention, h.ec_id_person) for h in hosts])

def _get_attendees(eventid):
    attendees = db((db.ec_attends_event.ec_id_event==eventid)&(db.ec_attends_event.ec_id_person==db.ec_person.id)).select(db.ec_person.ec_nickname).as_list()
    return ', '.join([a['ec_nickname'] for a in attendees])

def _get_people_in_room(roomid):
    people = db((db.ec_stays_in_room.ec_id_room==roomid)&(db.ec_stays_in_room.ec_id_person==db.ec_person.id)).select(db.ec_person.ALL)
    return ', '.join(['%(ec_firstname)s %(ec_lastname)s ("%(ec_nickname)s")' % p.as_dict() for p in people])

def _get_events_at_table(tableid):
    events = db((db.ec_event_at_table.ec_id_table==tableid)&(db.ec_event_at_table.ec_id_event==db.ec_event.id)).select(db.ec_event.ALL)
    return ', '.join(['%(ec_name)s' % e.as_dict() for e in events])

def _get_events_at_table_with_time(tableid):
    events = db((db.ec_event_at_table.ec_id_table==tableid)&
            (db.ec_event_at_table.ec_id_event==db.ec_event.id)
            ).select(db.ec_event.ALL,orderby='ec_event.ec_start')
    return ', '.join(['%s (%s)' % (e.ec_name, _begin_end_datetime_string(begin=e.ec_start, end=e.ec_end)) for e in events])

def _validate_table(tableid, events=None):
    table = db.ec_table(tableid)
    if not tableid:
        return dict(error=T('Invalid table'))
    if not events:
        events = db((db.ec_event_at_table.ec_id_table==tableid)&(db.ec_event_at_table.ec_id_event==db.ec_event.id)).select(db.ec_event.ALL)
    result = {}
    no_max_attendees = events.exclude(lambda row: row.ec_max_attendees == None)
    if no_max_attendees: result['table_validation_no_max_attendees'] = no_max_attendees
    too_many_attendees = events.find(lambda row: row.ec_max_attendees > table.ec_max_attendees)
    if too_many_attendees: result['table_validation_too_many_attendees'] = too_many_attendees

    no_start = events.exclude(lambda row: row.ec_start == None)
    if no_start: result['table_validation_no_start'] = no_start
    no_end = events.exclude(lambda row: row.ec_end == None)
    if no_end: result['table_validation_no_end'] = no_end
    too_early = events.find(lambda row: (row.ec_start < table.ec_available_begin))
    if too_early: result['table_validation_too_early'] = too_early
    too_late = events.find(lambda row: row.ec_end > table.ec_available_end)
    if too_late: result['table_validation_too_late'] = too_late
    for e in events:
        e_overlaps = events.find(lambda row: row.ec_start <= e.ec_start and row.ec_end >= e.ec_end and row.id > e.id)
        e_ident = events.find(lambda row: row.id == e.id) # hack because i cannot append row e to rows
        if e_overlaps:
            result['table_validation_overlap_%s'%e.id] = e_ident | e_overlaps
    return result

def _get_conventions_for_series(seriesid):
    conventions = db((db.ec_in_series.ec_id_series==seriesid) & (db.ec_in_series.ec_id_convention==db.ec_convention.id)).select(db.ec_convention.ALL)
    return ', '.join(['%(ec_name)s' % c.as_dict() for c in conventions])

def _get_convention_resultset_for_person(personid):
    conventions = db((db.ec_attendance.ec_id_person==personid) &\
        (db.ec_attendance.ec_id_convention==db.ec_convention.id)).select(db.ec_convention.ALL, distinct=True)
    attendevents = db((db.ec_attends_event.ec_id_person==personid) &\
        (db.ec_event.id == db.ec_attends_event.ec_id_event) &\
        (db.ec_event.ec_id_convention == db.ec_convention.id)).select(db.ec_convention.ALL, distinct=True)
    orgaevents = db((db.ec_organizes_event.ec_id_person==personid) &\
        (db.ec_event.id == db.ec_organizes_event.ec_id_event) &\
        (db.ec_event.ec_id_convention == db.ec_convention.id)).select(db.ec_convention.ALL, distinct=True)
    return conventions | attendevents | orgaevents

def _get_conventions_for_person(personid):
    result = []
    for c in _get_convention_resultset_for_person(personid):
        result.append(A(c.ec_short_name, _href=str(URL('convention','convention', args=c.ec_short_name))))
    return result

def _get_number_of_people(accountid):
    people = db((db.ec_person.id == db.auth_permission.record_id) &
                    (db.auth_permission.table_name == 'ec_person') & \
                    (db.auth_permission.name == 'edit') &
                    (db.auth_group.id == db.auth_permission.group_id) & \
                    (db.auth_group.role == 'user_%s' % accountid)).select(db.ec_person.id)
    return len(people)

def _get_room_dates(roomid, minD=None, maxD=None):
    people = _get_people_dates_in_room(roomid)    
    if not people: return ''
    arrivals = [att.ec_attendance.ec_arrival for att in people]
    try:
        min_date = min(arrivals)
    except TypeError:
        min_date = minD
    if not min_date: min_date = minD
    min_date_string = min_date.strftime(dateformatstring) if min_date else T('n/a', lazy=False)
    departures = [att.ec_attendance.ec_departure for att in people]
    try:
        max_date = max(departures)
    except TypeError:
        max_date = maxD
    if not max_date: max_date = maxD
    max_date_string = max_date.strftime(dateformatstring) if max_date else T('n/a', lazy=False)
    return '%s - %s' % (min_date_string, max_date_string)

def _get_people_dates_in_room(roomid):
    people = db((db.ec_stays_in_room.ec_id_room==roomid)&\
                 (db.ec_stays_in_room.ec_id_person==db.ec_person.id)&\
                 (db.ec_attendance.ec_id_person==db.ec_person.id) &\
                 (db.ec_attendance.ec_id_convention==db.ec_bedroom.ec_id_convention) &\
                 (db.ec_bedroom.id==roomid)).select(db.ec_person.ec_nickname, db.ec_attendance.ec_arrival, db.ec_attendance.ec_departure, orderby = [db.ec_attendance.ec_arrival])
    # todo here: if no ec_arrival or ec_departure is set, use convention dates
    return people

def _get_event_vacancy(eventid, seats):
    return seats - db(db.ec_attends_event.ec_id_event==eventid).count()

def _get_extra_value(extraid, attid=0, personid=0):
    extra = db.ec_extrafield(extraid)
    if extra.ec_entity=='attendance':
        hv = db((db.ec_has_value.ec_id_extrafield==extraid)&(db.ec_has_value.ec_id_attendance==attid)).select().first()
    elif extra.ec_entity=='person':
        hv = db((db.ec_has_value.ec_id_extrafield==extraid)&(db.ec_has_value.ec_id_person==personid)).select().first()
    if hv:
        return hv['ec_%svalue' % extra.ec_datatype]
    else:
        return ''

def _get_bought_item_list(convention, personid):
    itemdb = db((db.ec_buys_item.ec_id_person==personid)&\
               (db.ec_buys_item.ec_id_buyitem==db.ec_buyitem.id)&\
               (db.ec_buyitem.ec_id_convention==convention.id)&\
               (db.ec_buys_item.ec_buy_state=='booked')).select(db.ec_buyitem.ALL)
    items = [CAT(item.ec_name,BR()) for item in itemdb]
    return items

def _extend_row(rid, entity, extra, convention, html=True):
    # returns the value for a given entity and extra field (export data)
    if entity=='event':
        if extra=='organized_by':
            return _get_hosts(rid)
        if extra=='attendees':
            return _get_attendees(rid)
        if extra=='table':
            tables = db((db.ec_event_at_table.ec_id_event==rid)&
                        (db.ec_event_at_table.ec_id_table==db.ec_table.id)
                        ).select(db.ec_table.ec_name).as_list()
            return ', '.join([a['ec_name'] for a in tables])
        if extra=='registration_lines':
            max_attendees = db.ec_event[rid]['ec_max_attendees']
            if max_attendees:
                return max_attendees
            else:
                return 0
    elif entity=='attendee':
        attendance = db.ec_attendance(rid)
        config = db.ec_configuration(convention.ec_id_configuration)
        person = db.ec_person(attendance.ec_id_person)
        if extra=='buyitems':
            items = db((db.ec_buys_item.ec_id_person==person.id)&\
                       (db.ec_buys_item.ec_id_buyitem==db.ec_buyitem.id)&\
                       (db.ec_buyitem.ec_id_convention==db.ec_attendance.ec_id_convention)&\
                       (db.ec_buys_item.ec_buy_state=='booked')&\
                       (db.ec_attendance.id==rid)).select(db.ec_buyitem.ec_name).as_list()
            return ', '.join([i['ec_name'] for i in items])
        if extra=='buyitems_as_columns':
            allitem_query = (db.ec_buyitem.ec_id_convention==convention.id) &\
                    (db.ec_buyitem.ec_is_heading==False)
            allitems = db(allitem_query).select().sort(lambda row: row.ec_order or 0)
            items = db((db.ec_buys_item.ec_id_person==person.id)&\
                       (db.ec_buys_item.ec_id_buyitem==db.ec_buyitem.id)&\
                       (db.ec_buyitem.ec_id_convention==db.ec_attendance.ec_id_convention)&\
                       (db.ec_buys_item.ec_buy_state=='booked')&\
                       (db.ec_attendance.id==rid)).select(db.ec_buyitem.id).as_list()
            itemlist = [i['id'] for i in items]
            for item in allitems:
                if item['id'] in itemlist:
                    item['ec_price'] = '1' # flc(item['ec_price']) if we need the price in German Excel format
                else:
                    item['ec_price'] = ''
            header = 'ec_name'
            return header, allitems
        if extra=='needs_to_pay':
            return '%.2f %s' % (_get_item_sum(convention, attendance.ec_id_person), config.ec_registration_currency_symbol)
        if extra=='days':
            if attendance and attendance.ec_departure and attendance.ec_arrival:
                days = (attendance.ec_departure - attendance.ec_arrival).days
                return '%s' % days
            else:
                return ''
        if extra.startswith('extra_'):
            exfield = db.ec_extrafield(extra[len('extra_'):])
            if exfield:
                if exfield.ec_entity=='attendance':
                    ex = db((db.ec_has_value.ec_id_extrafield==exfield.id)&(db.ec_has_value.ec_id_attendance==attendance.id)).select(db.ec_has_value.ALL).first()
                    if ex:
                        return getattr(ex,'ec_%svalue' % exfield.ec_datatype)
        if extra=='services':
            services = db((db.ec_helps.ec_id_person==person.id)&(db.ec_helps.ec_id_service==db.ec_service.id)&(db.ec_service.ec_id_convention==convention.id)).select()
            return ', '.join([service.ec_service.ec_name for service in services])
        if extra=='bedrooms':
            room = db((person.id==db.ec_stays_in_room.ec_id_person)&(db.ec_stays_in_room.ec_id_room==db.ec_bedroom.id)&(db.ec_bedroom.ec_id_convention==convention.id)).select(db.ec_bedroom.ALL).first()
            return '%(ec_name)s (%(ec_category)s)' % room.as_dict() if room else ''
    elif entity=='host':
        if extra=='events':
            events = db((db.ec_event.id==db.ec_organizes_event.ec_id_event)&\
                        (db.ec_organizes_event.ec_id_person==rid)&\
                        (db.ec_event.ec_id_convention==convention.id)).select(db.ec_event.ALL)
            return ', '.join(['%s%s' % (e['ec_name'], ' (%s)' % db.ec_event.ec_id_timeslot.represent(e['ec_id_timeslot'], e) if e['ec_id_timeslot'] else '') for e in events])
    elif entity=='boughtitem':
        basequery = (db.ec_buys_item.ec_id_buyitem==rid) &\
        (db.ec_buys_item.ec_id_person==db.ec_person.id) &\
        (db.ec_attendance.ec_id_convention==convention.id) &\
        (db.ec_person.id==db.ec_attendance.ec_id_person)

        for prefix in [
            'item_booked_attendee',
            'item_booked_registered',
            'item_waitlist_attendee',
            'item_waitlist_registered',
            'item_waitlist_waitlist',
            ]:
            (first, item_status, attendee_status) = prefix.split('_')
            if extra=='%s_count' % prefix:
                return db(basequery &\
                (db.ec_buys_item.ec_buy_state==item_status) &\
                (db.ec_attendance.ec_registration_state==attendee_status)).count()
            if extra=='%s_names' % prefix:
                dbnames = db(basequery &\
                (db.ec_buys_item.ec_buy_state==item_status) &\
                (db.ec_attendance.ec_registration_state==attendee_status)).select(db.ec_attendance.id, db.ec_person.ec_nickname,orderby=db.ec_person.ec_nickname)
                if html:
                    return CAT(*[CAT(Registration.get_manage_attendee_url(a.ec_person.ec_nickname,a.ec_attendance.id),"  ") for a in dbnames])
                else:
                    return ', '.join([a.ec_person.ec_nickname for a in dbnames])
        if extra=='item_not_buy_count':
            all_buy = db(basequery &\
                ((db.ec_buys_item.ec_buy_state=='booked') | \
                 (db.ec_buys_item.ec_buy_state=='waitlist')) &\
                ((db.ec_attendance.ec_registration_state=='attendee') | \
                 (db.ec_attendance.ec_registration_state=='registered') | \
                 (db.ec_attendance.ec_registration_state=='waitlist'))).count()
            all_attendees = db((db.ec_attendance.ec_id_convention==convention.id) &\
                ((db.ec_attendance.ec_registration_state=='attendee')  |\
                 (db.ec_attendance.ec_registration_state=='registered')|\
                 (db.ec_attendance.ec_registration_state=='waitlist')) &\
                (db.ec_person.id==db.ec_attendance.ec_id_person)).count()
            return all_attendees - all_buy
        if extra=='item_not_buy_names':
            all_buy = db(basequery &\
                ((db.ec_buys_item.ec_buy_state=='booked') | \
                 (db.ec_buys_item.ec_buy_state=='waitlist')) &\
                ((db.ec_attendance.ec_registration_state=='attendee') | \
                 (db.ec_attendance.ec_registration_state=='registered') | \
                 (db.ec_attendance.ec_registration_state=='waitlist'))).select(db.ec_attendance.id, db.ec_person.ec_nickname,orderby=db.ec_person.ec_nickname)
            all_attendees = db((db.ec_attendance.ec_id_convention==convention.id) &\
                ((db.ec_attendance.ec_registration_state=='attendee')  |\
                 (db.ec_attendance.ec_registration_state=='registered')|\
                 (db.ec_attendance.ec_registration_state=='waitlist')) &\
                (db.ec_person.id==db.ec_attendance.ec_id_person)).select(db.ec_attendance.id, db.ec_person.ec_nickname,orderby=db.ec_person.ec_nickname)
            return ', '.join([a.ec_person.ec_nickname for a in all_attendees if a not in all_buy])

    elif entity=='special_needs_per_item':
        attendees = db((db.ec_buys_item.ec_id_buyitem==rid) &\
                       (db.ec_buys_item.ec_buy_state=='booked') &\
                       (db.ec_buys_item.ec_id_person==db.ec_person.id) &\
                       (db.ec_attendance.ec_id_person==db.ec_person.id) &\
                       (db.ec_attendance.ec_id_convention==convention.id)).select(db.ec_person.ALL, db.ec_attendance.ec_registration_state)
        include_states=['attendee']
        if extra.endswith('_all'):
            include_states.append('registered')
        if extra.startswith('ec_special_needs'):
            lfind = lambda row: (row.ec_attendance['ec_registration_state'] in include_states) and (row.ec_person.ec_special_needs != '')
            filtered_attendees = attendees.find(lfind)
            if extra in ['ec_special_needs', 'ec_special_needs_all']:
                return '; '.join([a['ec_person.ec_special_needs'] for a in filtered_attendees if 'ec_person.ec_special_needs' in a])
            if extra.startswith('ec_special_needs_count'):
                oneperperson = set([a.ec_person.id for a in filtered_attendees])
                return max(0,len(oneperperson))
            if extra.startswith('ec_special_needs_names'):
                return '; '.join(['%s %s' % (a.ec_person.ec_firstname, a.ec_person.ec_lastname) for a in filtered_attendees])
        if extra.startswith('ec_vegetarians'):
            lfind = lambda row: (row.ec_attendance['ec_registration_state'] in include_states) and row.ec_person.ec_vegeterian
            filtered_attendees = attendees.find(lfind)
            if extra.startswith('ec_vegetarians_count'):
                oneperperson = set([a.ec_person.id for a in filtered_attendees])
                return max(0, len(oneperperson))
            if extra.startswith('ec_vegetarians_names'):
                return '; '.join(['%s %s' % (a.ec_person.ec_firstname, a.ec_person.ec_lastname) for a in filtered_attendees])

    elif entity == 'bedroom':
        occupied_string = _get_room_dates(rid, convention.ec_start_date, convention.ec_end_date)
        if extra=='attendees':
            return db.ec_bedroom(rid).v_people
        if extra=='occupied_from':
            return occupied_string[:occupied_string.find(' -')]
        if extra=='occupied_until':
            return occupied_string[occupied_string.find(' -')+3:]
    elif entity == 'service':
        if extra=='helpers':
            return db.ec_service(rid).v_helpers
    elif entity == 'table':
        if extra=='events':
            return _get_events_at_table(rid)
        elif extra=='events_with_time':
            return _get_events_at_table_with_time(rid)

def _get_accounts(personid):
    accounts = db((db.auth_permission.table_name=='ec_person') &\
                  (db.auth_permission.record_id==personid) &\
                  (db.auth_permission.name=='edit')).select()
    return accounts

def _get_systempage(convention, page):
    pages = db((db.ec_page.ec_id_convention == convention.id)&(db.ec_page.ec_systempage==page)).select(orderby=['ec_page.ec_order'])
    bodies = []
    for p in pages:
        if _check_page_permission(p,convention):
            bodies.append(p.ec_body)
    if bodies:
        return '\n\n'.join(bodies)

def _update_invitations():
    db((db.ec_invitation.ec_expiry_date!=None)&(db.ec_invitation.ec_expiry_date < request.now)).update(ec_status='expired')
    # not used anymore
    # registered = db((db.ec_invitation.ec_id_convention==db.ec_attendance.ec_id_convention)& \
        # (db.ec_invitation.ec_status.belongs(['invited','accepted but not registered'])) & \
        # (db.ec_attendance.ec_id_person==db.ec_person.id) & \
        # (db.ec_invitation.ec_email==db.ec_person.ec_email)  \
        # ).select(db.ec_invitation.ALL)
    # for entry in registered:
        # entry.update_record(ec_status='accepted')

def _open_user_invitations(convention, ec_for='convention', email=None):
    if not email: return None
    return db((db.ec_invitation.ec_id_convention==convention.id)&\
            (db.ec_invitation.ec_email.lower()==email.lower())&\
            (db.ec_invitation.ec_for==ec_for) &\
            (db.ec_invitation.ec_status.belongs(['invited', 'accepted but not registered']))).count()

def _begin_end_datetime_string(begin, end=None, dtfs=datetimeformatstring, tfs=timeformatstring, brackets=False):
    result = ''
    if not begin:
        return ''
    elif not end:
        result = begin.strftime(dtfs)
    elif begin.date() == end.date():
        result = '%s - %s' % (begin.strftime(dtfs), end.strftime(tfs))
    else:
        result = '%s - %s' % (begin.strftime(dtfs), end.strftime(dtfs))
    if brackets:
        return '(%s)' % result
    return result


def _get_event_html(line, result):
    html = ''
    fields = result[0]
    headers = result[1]
    # add a title
    title = ''
    event = db.ec_event(line[fields.index('id')])
    html += '<h2>'
    title = ''
    if 'ec_type' in fields:
        title += '%s: ' % MV(line[fields.index('ec_type')])
    if 'ec_name' in fields:
        title += '%s' % line[fields.index('ec_name')]
    if len(title) > 60 and request.extension=='pdf': # hack: only one linebrake
        lastspace = title[:60].rfind(' ')
        title = '%s<br/><br/>%s' % (title[0:lastspace],title[lastspace+1:])
    html += title
    html += '</h2>\n'
    # add the time (start, end, and/or timeslot)
    time = ''
    if 'ec_start' and 'ec_end' in fields:
        if event.ec_start:
            time += _begin_end_datetime_string(event.ec_start, event.ec_end)
        elif event.ec_end:
            time += '%s: %s' % (headers[fields.index('ec_end')], line[fields.index('ec_end')])
    if 'ec_id_timeslot' in fields:
        timeslot = db.ec_timeslot(event.ec_id_timeslot)
        if time and timeslot:
            time += ', '
        if timeslot:
            time += '%s (%s)' % (timeslot.ec_name, _begin_end_datetime_string(timeslot.ec_start_time, timeslot.ec_end_time))
    html+=str(H3(time))
    # add other fields as a list
    html+= '\n<ul>'
    # add the event ID
    if 'ec_id_pdf' in fields:
        html+=str(LI('%s: #%s' % (T('ID'), event.id)))
        html+='\n'
    # add the table
    if 'table' in fields:
        html+=str(LI('%s: %s' % (headers[fields.index('table')], line[fields.index('table')])))
        html+='\n'
    # add the roleplay specific fields
    if event.ec_type=='rpggame':
        for field in ['organized_by', 'ec_id_rpggame', 'ec_prepared_chars', 'ec_introduction', 'ec_rpg_newbees']:
            if field in fields:
                value = line[fields.index(field)]
                value = T('Yes') if value==True else value
                value = T('No') if value==False else value
                key = headers[fields.index(field)]
                key = T('Game master') if field == 'organized_by' else key
                html+=str(LI('%s: %s' % (key, value)))
                html+='\n'
    else:
        if 'organized_by' in fields:
            html+=str(LI('%s: %s' % (headers[fields.index('organized_by')], line[fields.index('organized_by')])))
            html+='\n'
    if 'ec_url' in fields and event.ec_url:
        html+='<li>' + '%s: %s' % (headers[fields.index('ec_url')], str(A(event.ec_url, _href=event.ec_url)))
        html+='\n'
    html += '</ul>\n'
    html += '<br />'
    # add the description
    if 'ec_description' in fields and event.ec_description:
        html += '<br />\n'
        html += str(H3('%s:' % T('Description')))
        html+=str(MARKMIN(event.ec_description))
        html += '<br/>\n'

    # add attendees
    if 'attendees' in fields and event.ec_max_attendees > 0:
        attendees = line[fields.index('attendees')].split(', ')
        atttitle = T('Attendees')
        mins = (('ec_min_attendees' in fields) and (event.ec_min_attendees > 0))
        maxes = (('ec_max_attendees' in fields) and (event.ec_max_attendees > 0))
        if mins or maxes: atttitle += ': ('
        if mins:
            atttitle += '%s %s' % (T('min.'), event.ec_min_attendees)
            if maxes: atttitle += ', '
        if maxes:
            atttitle += '%s %s' % (T('max.'), event.ec_max_attendees)
        if mins or maxes: atttitle += ')'
        html += str(H3('%s' % atttitle))
        html += '\n<pre><h2>'
        linelength = 40
        if 'registration_lines' in fields:
            if event.ec_max_attendees < 6:
                for i in range(event.ec_max_attendees):
                    if i < len(attendees):
                        html += str(P('%s. %s' % (i+1, attendees[i] if attendees[i] else '_' * linelength)))
                    else:
                        html +=str(P('%s. %s' % (i+1, '_' * linelength)))
                    if request.extension=='pdf': html += '<br />'
                    html += '\n'
            else:
                halfline = int(round(linelength / 2))
                for i in range(event.ec_max_attendees):
                    if i == 9:   # i+1 == 10
                       halfline = halfline - 1
                    if i == 99:   # i+1 == 100
                       halfline = halfline - 1
                    if i < len(attendees):
                       if i%2 == 0:  # even
                           html += '<p>'
                       else:
                           html += ' '
                       if attendees[i]:
                           attendee = attendees[i].strip()
                           html += '%s. %s' % (i+1, attendee)
                           if len(attendee) < halfline:
                               html += ' ' + '.' * (halfline - len(attendee) - 1 + 0*len(attendee)%2)
                       else:
                           html += '%s. %s' % (i+1, '_' * halfline)
                       if i%2 != 0:  # odd
                           html += '</p>\n'
                           if request.extension=='pdf': html += '<br />'
                    else:
                       if i%2 == 0: # even
                           html += '<p>'
                       else:
                           html += ' '
                       html += '%s. %s' % (i+1, '_' * halfline)
                       if i%2 != 0:   # odd
                           html += '</p>\n'
                           if request.extension=='pdf': html += '<br />'
                if i%2 == 0:  # final: odd
                    html += '</p>\n'
                    if request.extension=='pdf': html += '<br />'
        else:
            html += ', '.join(attendees)
        html += '</h2>'
        if request.extension!='pdf': html += '</pre>'

    return html

def _get_empty_event_html(convention, fields=[], attendee_count=0):
    config = db.ec_configuration(convention.ec_id_configuration)
    html = str(H2(T('Title')+':'))
    if not fields: fields=[]
    if not attendee_count: attendee_count=0
    if 'ec_type' in fields:
        html += '\n<small>'
        for t in EVENT_TYPE_CHOICES:
            if t != 'misc' and config['ec_event_type_%s' % t]:
                html += '[  ] %s ' % MV(t)
        html += '[  ] %s ' % MV('misc-short')
        html += '</small>\n'
    if 'organized_by' in fields:
        if 'ec_id_rpggame' in fields:
            html += '<br/>' + str(H2(T('Game master')+':'))
        else:
            html += '<br/>' + str(H2(T('Organized by')+':'))
    html += '<br/>'
    html += '<h3>' if [f for f in ['ec_start', 'ec_end', 'ec_id_timeslot'] if f in fields] else ''
    for f in ['ec_start', 'ec_end', 'ec_id_timeslot']:
        if f in fields:
            html += '%s:' % MV(f)
            if f != 'ec_id_timeslot':
                html += '<br />'
    html += '</h3>\n'  if [f for f in ['ec_start', 'ec_end', 'ec_id_timeslot'] if f in fields] else ''
    html += '<ul>\n'
    # add the event ID
    if 'ec_id_pdf' in fields:
        html+=str(LI('%s: #' % T('ID')))
        html+='\n'
    for field in ['ec_min_attendees', 'ec_max_attendees', 'ec_id_rpggame', 'ec_prepared_chars', 'ec_introduction',
    'ec_rpg_newbees',]:
        if field in fields:
            html += '  <li>%s' % MV(field)
            if html[-1] != '?':
                html += ':'
            html += '</li>\n'
    html += '</ul>\n'
    # add attendees
    if 'attendees' in fields:
        html += '<br />'
        atttitle = T('Attendees')
        html += str(H3('%s:' % atttitle))
    if attendee_count > 0:
        html += '<pre><h2>'
        linelength = 40
        if attendee_count < 6:
            for i in range(attendee_count):
                html += str(P('%s. %s' % (i+1, '_' * linelength)))
                html += '<br />'
                html += '\n'
        else:
            halfline = int(round(linelength / 2))
            for i in range(attendee_count):
                if i == 9:   # i+1 == 10
                   halfline = halfline - 1
                if i == 99:   # i+1 == 100
                   halfline = halfline - 1
                if i%2 == 0: # even
                   html += '<p>'
                else:
                   html += ' '
                html += '%s. %s' % (i+1, '_' * halfline)
                if i%2 != 0:   # odd
                   html += '</p>\n'
                   html += '<br />'
            if i%2 == 0:  # final: odd
                html += '</p>\n'
                html += '<br />'
        html += '</h2>'
    if 'ec_description' in fields:
        html += '<br />'
        html += str(H3(T('Description')+':'))
    return html

def _get_boughtitem_html(line, result,content={}):
    html = ''
    fields = result[0]
    headers = result[1]
    # add a title
    title = ''
    item = db.ec_buyitem(line[fields.index('id')])
    html += '<h2>'
    title = T('Buyable item')
    if 'ec_name' in fields:
        title += ': %s' % line[fields.index('ec_name')]
    if len(title) > 60 and request.extension=='pdf': # hack: only one linebreak
        lastspace = title[:60].rfind(' ')
        title = '%s<br/><br/>%s' % (title[0:lastspace],title[lastspace+1:])
    html += title
    html += '</h2>\n'
    # add item information
    html += '\n<ul>'
    for field in ['ec_is_perday', 'ec_price', 'ec_capacity',
                    'item_booked_attendee_count',
                    'item_booked_registered_count',
                    'item_waitlist_attendee_count',
                    'item_waitlist_registered_count',
                    'item_waitlist_waitlist_count',
                    'item_not_buy_count',]:
        if field in fields:
            key = headers[fields.index(field)]
            value = line[fields.index(field)]
            if field=='ec_is_perday':
                value = T('Yes') if value=='True' else T('No')
            if field=='ec_capacity':
                value = T('unlimited') if value==-1 else value
            html += str(LI('%s: %s' % (key, value)))
    html += '\n</ul>'
    # add extra_text_pdf_only
    if 'extra_text_pdf_only' in content and content['extra_text_pdf_only'] != 'None':
        html += '<p />'
        html += str(MARKMIN(content['extra_text_pdf_only']))
    # add participant list
    for field in [  'item_booked_attendee_names',
                    'item_booked_registered_names',
                    'item_waitlist_attendee_names',
                    'item_waitlist_registered_names',
                    'item_waitlist_waitlist_names',
                    'item_not_buy_names',]:
        if field in fields and len(line[fields.index(field)])>0:
            html += '<p />'
            html += str(H3("%s:" % MV(field)))
            html += '\n<ol>'
            attendees = sorted(line[fields.index(field)].split(', '))
            for attendee in attendees:
                html += str(LI(attendee))
            html += '\n</ol>'
    return html

def _check_valid_dates(start, end=None, what=request.now): # checks whether date "what" is after start and before end (if end is given)
    return start and what >= start and (end == None or what < end)

def _check_is_open(conventionid):
    convention = db.ec_convention(conventionid)
    config = db.ec_configuration(convention.ec_id_configuration)
    return  config and \
            (not convention.ec_end_date or convention.ec_end_date >= request.now.date()) and \
            (_check_valid_dates(config.ec_registration_period_start, config.ec_registration_period_stop) or \
             _event_registration_open(convention))

def _check_is_closed(conventionid):
    convention = db.ec_convention(conventionid)
    config = db.ec_configuration(convention.ec_id_configuration)
    return  config and \
            convention.ec_convention_state == 'active' and \
            (not convention.ec_end_date or convention.ec_end_date >= request.now.date()) and \
            not (_check_valid_dates(config.ec_registration_period_start, config.ec_registration_period_stop) or \
             _event_registration_open(convention))

def _check_is_over(conventionid):
    convention = db.ec_convention(conventionid)
    config = db.ec_configuration(convention.ec_id_configuration)
    return  config and \
            convention.ec_convention_state == 'active' and \
            convention.ec_end_date < request.now.date()

def _export_pdf(convention, result, what,content={}):
    pdftitle = request.title
    if what in ['event', 'boughtitem']:
        pdftitle = convention.ec_name
    pdf = Ec_pdftemplate(title=pdftitle)
    if not result:
        pdf.add_page()
        pdf.write_html(str(P(T('Error: nothing to export'))))
        return pdf.output(dest='S')
    fields = result[0]
    headers = result[1]
    for line in result[2:]:
        pdf.add_page()
        html=''
        if what == 'event':
            pdf.page_numbers = False
            html = _get_event_html(line, result)
            event = db.ec_event(line[fields.index('id')])
        elif what == 'boughtitem':
            pdf.page_numbers = False
            html = _get_boughtitem_html(line, result, content)
            item = db.ec_buyitem(line[fields.index('id')])
        else: # just export all fields
            html += '<ul>'
            for cnt, value in enumerate(line):
                html += str(LI('%s: %s' % (headers[cnt], value)))
            html += '</ul>'
        try:
            pdf.write_html(html)
        except:
            if what=='event':
                pdf.write_html(str(P(T('Error with event "#%s"') % event.id)))
            elif what=='boughtitem':
                pdf.write.html(str(P(T('Error with item "#%s"') % item.ec_name)))
            else:
                pdf.write_html(str(P(T('Error with object "%s"') % line[fields.index('id')])))
    return pdf.output(dest='S')

def _export_empty_pdf(convention, fields, attendee_count):
    pdftitle = convention.ec_name
    pdf = Ec_pdftemplate(title=pdftitle, convention=convention)
    pdf.page_numbers = False
    pdf.add_page()
    html = _get_empty_event_html(convention, fields, attendee_count)
    pdf.write_html(html)
    return pdf.output(dest='S')

def _save_metadata(form):
    # used after uploading a file
    fileentry = db.ec_file(form.vars.id)
    filename, error = IS_SLUG_FILENAME()(request.vars.ec_file.filename) # should always work
    fileentry.update_record(ec_name=filename)
    # set size
    import os
    path_list = [request.folder, 'uploads', fileentry.ec_file]
    fileentry.update_record(ec_size=os.path.getsize(os.path.join(*path_list)))
    fileentry.update_record(ec_url=_get_file_url(fileentry)) # hack; should be computed field but isn't

def _get_export_result(convention, what, fields, ids, html=True):
    config=db.ec_configuration(convention.ec_id_configuration)
    result = []
    extras = []
    dbresult = []
    if not fields: fields=[]
    if what=='empty_event':
        table = 'ec_event'
        return [fields, [short_label(key) if short_label(key)!=key else db[table][key].label for key in fields],['' for key in fields]]
    if what=='event':
        table = idtable = 'ec_event'
        projection = [db[table].id,]
        if ids:
            query = (db.ec_event.ec_id_convention==convention.id) & (db.ec_event.id.belongs(ids))
        else:
            query = db.ec_event.ec_id_convention==convention.id
        for f in fields:
            if f not in ['organized_by', 'attendees', 'table', 'registration_lines', 'ec_id_pdf']:
                projection.append(db.ec_event[f])
            else:
                extras.append(f)
        dbresults = db(query).select(*projection).as_list()
        dbresult = [dict(ec_event=res) for res in dbresults]
    elif what=='attendee':
        idtable = 'ec_attendance'
        projection = [db[idtable].id,db['ec_person'].id]
        query = db.ec_attendance.ec_id_convention==convention.id
        for f in fields:
            if f in db.ec_person.fields:
                projection.append(db.ec_person[f])
            elif f in db.ec_attendance.fields:
                projection.append(db.ec_attendance[f])
            else:
                extras.append(f)
        dbresult = db(query).select(*projection, left=db.ec_person.on(db.ec_person.id==db.ec_attendance.ec_id_person)).as_list()
    elif what=='host':
        idtable = 'ec_person'
        projection = [db.ec_person.id,]
        for f in fields:
            if f not in ['events']:
                projection.append(db.ec_person[f])
            else:
                extras.append(f)
        dbresults = db((db.ec_person.id==db.ec_organizes_event.ec_id_person)&\
                       (db.ec_organizes_event.ec_id_event==db.ec_event.id)&\
                       (db.ec_event.ec_id_convention==convention.id)).select(*projection, distinct=True).as_list()
        dbresult = [dict(ec_person=res) for res in dbresults]
    elif what=='boughtitem':
        idtable = 'ec_buyitem'
        projection = [db.ec_buyitem.id,]
        for f in fields:
            if f in db.ec_buyitem.fields:
                projection.append(db.ec_buyitem[f])
            else:
                extras.append(f)
        if ids:
            dbresults = db((db.ec_buyitem.ec_id_convention==convention.id) &\
                           (db.ec_buyitem.id.belongs(ids))).select(*projection, distinct=True).as_list()
        else:
            dbresults = db(db.ec_buyitem.ec_id_convention==convention.id).select(*projection, distinct=True).as_list()
        dbresult = [dict(ec_buyitem=res) for res in dbresults]
    elif what in ['bedroom', 'service']:
        idtable = 'ec_%s' % what
        projection = [db[idtable].id,]
        for f in fields:
            if f in db[idtable].fields:
                projection.append(db[idtable][f])
            else:
                extras.append(f)
        dbresults = db(db[idtable].ec_id_convention==convention.id).select(*projection, distinct=True).as_list()
        dbresult = [{idtable: res} for res in dbresults]
    elif what=='table':
        idtable = 'ec_table'
        projection = [db.ec_table.id,]
        for f in fields:
            if f in db.ec_table.fields:
                projection.append(db.ec_table[f])
            else:
                extras.append(f)
        dbresults = db((db.ec_table.id==db.ec_event_at_table.ec_id_table)&\
                       (db.ec_event_at_table.ec_id_event==db.ec_event.id)&\
                       (db.ec_event.ec_id_convention==convention.id)).select(*projection, distinct=True).as_list()
        dbresult = [dict(ec_table=res) for res in dbresults]
    elif what=='special_needs_per_item':
        # get attendees for items
        idtable = 'ec_buyitem'
        projection = [db.ec_buyitem.id,db.ec_buyitem.ec_name]
        itemids=[]
        for f in fields:
            if f.startswith('item_'):
                try:
                    itemids.append(int(f[len('item_'):]))
                except:
                    pass
            else:
                if f=='ec_special_needs' in fields:
                    nf = f
                    if 'include_registered' in fields:
                        nf += "_all"
                    extras.append(nf)
                if (f in ['ec_special_needs', 'ec_vegetarians']):
                    if ('ec_attendee_count' in fields):
                        nf = "%s_%s" % (f,'count')
                        if 'include_registered' in fields:
                            nf += "_all"
                        extras.append(nf)
                    if ('ec_attendee_names' in fields):
                        nf = "%s_%s" % (f,'names')
                        if 'include_registered' in fields:
                            nf += "_all"
                        extras.append(nf)
                else:
                    extras.append(f)
        for f in ['ec_attendee_count', 'ec_attendee_names', 'include_registered']:
            if f in extras:
                extras.remove(f)
        fields=['ec_name'] + extras
        dbresults = db((db[idtable].ec_id_convention==convention.id)).select(*projection, distinct=True)
        restrict = lambda row: row['id'] in itemids
        dbresult = [{idtable: res} for res in dbresults.find(restrict)]

    first = ['id', ]
    for dbres in dbresult:
        if what=='attendee': # Use ec_person.id instead of attendance id for better readability
            entry=[dbres['ec_person']['id']]
        else:
            entry=[dbres[idtable]['id'],]
        for key in fields:
            if key not in extras:
                if what=='attendee': table='ec_person' if key in db.ec_person.fields else 'ec_attendance'
                if what=='host': table='ec_person' if key in db.ec_person.fields else 'ec_organizes_event'
                if what=='boughtitem': table='ec_buyitem'
                if what=='table': table='ec_table' if key in db.ec_table.fields else 'ec_event'
                if what in ['bedroom', 'service','special_needs_per_item']: table=idtable
                if first: first.append(short_label(key) if short_label(key)!=key else db[table][key].label)
                if key == 'ec_payment':
                    entry.append('%.2f %s' % (dbres[table][key], config.ec_registration_currency_symbol))
                elif key in ['ec_registration_state']:
                    entry.append(MV(dbres[table][key]))
                elif key in ['ec_description']:
                    entry.append(dbres[table][key].replace('\n', '\\n').replace('\r',''))
                else:
                    entry.append(db[table][key].represent(dbres[table][key], dbres[table]) if db[table][key].represent else dbres[table][key])
            elif key.endswith('_as_columns'): # add not only one, but multiple columns
                header, columns = _extend_row(dbres[idtable]['id'], what, key, convention, html=html)
                for c in columns:
                    if key == 'buyitems_as_columns':
                        if first: first.append(c[header])
                        entry.append(c.ec_price)
            else:
                if first: first.append(short_label(key))
                entry.append(_extend_row(dbres[idtable]['id'], what, key, convention, html=html))
        if first:
            result = [['id'] + fields, first, ]
            first = None
        result.append(entry)
    return result

def _get_menu_if_valid(convention, page):
    config = db.ec_configuration(convention.ec_id_configuration)
    if page == 'convention':
        return ['%s: %s' % (T('Convention'), convention.ec_name),False,URL('convention','convention',args=convention.ec_short_name)]
    if page == 'register':
        if config and config.ec_convention_requires_registration and \
           (_check_is_open(convention.id) or auth.has_permission('organize', db.ec_convention, convention.id)):
            return [T('Registration'),False,URL('convention','register',args=convention.ec_short_name)]
        else:
            return None
    if page == 'participants':
        if config and auth.is_logged_in() and config.ec_convention_requires_registration:
            return [T('Participants'),False,URL('convention','participants',args=convention.ec_short_name)]
        else:
            return None
    if page == 'manage_services':
        if config and auth.is_logged_in() and config.ec_convention_requires_services:
            return [MV(page),False,URL('convention',page,args=convention.ec_short_name)]
        else:
            return None
    if page == 'events':
        if config and config.ec_events and (auth.is_logged_in() or config.ec_event_public):
            return [MV(page),False,URL('convention',page,args=convention.ec_short_name)]
        else:
            return None
    if page == 'organize':
        if config and auth.has_permission('organize', db.ec_convention, convention.id):
            return [MV(page),False,URL('convention',page,args=convention.ec_short_name)]
        else:
            return None
    if page == 'configure_main':
        if convention and auth.has_permission('configure', db.ec_convention, convention.id):
            return [MV(page),False,URL('convention',page,args=convention.ec_short_name)]
        else:
            return None
    return [MV(page),False,URL('convention',page,args=convention.ec_short_name)]

def _build_response_menu(c):
    response.menu = []
    config = db.ec_configuration(c.ec_id_configuration)
    menu_con_pages = db((db.ec_page.ec_id_convention==c.id)& \
                        (db.ec_page.ec_menu==True)).select(orderby=['ec_page.ec_order'])
    menu_con_pages.exclude(lambda row: (row.ec_hidden == True and not auth.has_permission('organize', db.ec_convention, c.id)))
    if not auth.is_logged_in():
        menu_con_pages.exclude(lambda row: (row.ec_public == False))
    for page in MENU_PAGES:
        con_pages_here = menu_con_pages.exclude(lambda row: row.ec_before_menu_entry==page)
        value = _get_menu_if_valid(c, page)
        if value:
            for con_page in con_pages_here:
                if _check_page_permission(con_page,c):
                    response.menu.append((con_page.ec_title if con_page.ec_title else con_page.ec_name.capitalize(),False,URL('convention','page', args=[c.ec_short_name,con_page.ec_name])))
            response.menu.append(value)
    for page in menu_con_pages:
        if _check_page_permission(page,c):
            response.menu.append((page.ec_title if page.ec_title else page.ec_name.capitalize(),False,URL('convention','page', args=[c.ec_short_name,page.ec_name])))

def _is_conadmin():
    return db(auth.accessible_query('configure', db.ec_convention) & (db.ec_convention.ec_convention_state != 'requested')).count() > 0

def _is_conorga():
    return _is_conadmin() or db(auth.accessible_query('organize', db.ec_convention) & (db.ec_convention.ec_convention_state != 'requested')).count() > 0

def _is_eventorga(conventionid,personid):
    return db((db.ec_organizes_event.ec_id_person==personid) & (db.ec_organizes_event.ec_id_event==db.ec_event.id) & (db.ec_event.ec_id_convention==conventionid)).select()

def _logged_in_and_privacy_consent():
    if auth.is_logged_in:
        if auth.user.privacy_consent:
            return True
        else:
            _redirect_message(T('Please accept first our privacy notice'),URL('user',args=['profile']))
    else:
        return False

def _if_logged_in_then_privacy_consent():
    if auth.is_logged_in:
        if auth.user and auth.user.privacy_consent:
            return True
        else:
            _redirect_message(T('Please accept first our privacy notice'),URL('user',args=['profile']))

def _create_series(form):
    admingroup = auth.add_group('conseries_admin_%s' % form.vars.id, 'Administrators of convention series %s' % form.vars.id)
    auth.add_permission(admingroup, 'configure', db.ec_series, form.vars.id)
    auth.add_membership(admingroup) # adds current user to that series

def _create_news(form):
    auth.add_permission(0, 'edit', db.ec_news, form.vars.id)
    easy_admingroup = auth.id_group('easycon_admin')
    auth.add_permission(easy_admingroup, 'edit', db.ec_news, form.vars.id)
    if form.vars.ec_id_convention:
        orgagroup = auth.id_group('con_orga_%s' % form.vars.ec_id_convention)
        auth.add_permission(orgagroup, 'edit', db.ec_news, form.vars.id)
    if form.vars.ec_status == 'requested':
        news = db.ec_news(form.vars.id)
        news.update_record(ec_request_date=request.now)

def _update_news(form):
    oldnews = db.ec_news(form.vars.id)
    if oldnews.ec_id_convention != form.vars.ec_id_convention:
        oldorgagroup = auth.id_group('con_orga_%s' % oldnews.ec_id_convention) if oldnews.ec_id_convention else None
        neworgagroup = auth.id_group('con_orga_%s' % form.vars.ec_id_convention) if form.vars.ec_id_convention else None
        if oldorgagroup:
            auth.del_permission(oldorgagroup, 'edit', db.ec_news, form.vars.id)
        if neworgagroup:
            auth.add_permission(neworgagroup, 'edit', db.ec_news, form.vars.id)

def _get_news_text(entry, html=True):
    result = ''
    if entry.ec_publish_date:
        result+=entry.ec_publish_date.strftime(dateformatstring)
    else:
        result+=T('Not published yet')
    result+=': '
    result+=str(B(entry.ec_subject)) if html else entry.ec_subject + '\n'
    if entry.ec_id_convention:
        convention = db.ec_convention(entry.ec_id_convention)
        result += ' ('
        result += str(A(convention.ec_name, _href=URL('convention', 'convention', args=convention.ec_short_name))) if html else convention.ec_name
        result += ')'
    result += '\n'
    result += str(MARKMIN(entry.ec_text)) if html else entry.ec_text
    result += '\n\n-- '
    result += db.auth_user(entry.ec_created_by).nick_name
    return result

def _merge_person(keepid, mergeid):
    keep = db.ec_person(keepid)
    merge = db.ec_person(mergeid)
    if not keep and merge: return T('Error when selecting the two persons to merge')
    # check attendance of conventions and events
    keep_cons = _get_convention_resultset_for_person(keepid)
    merge_cons = _get_convention_resultset_for_person(mergeid)
    if set([row.id for row in keep_cons]) & set([row.id for row in merge_cons]):
        return T('Error: these two persons attend the same convention and cannot be merged')
    # else: merge!
    for tablename in ['ec_attendance', 'ec_is_member', 'ec_profile','ec_attends_event', 'ec_buys_item', 'ec_helps', 'ec_organizes_event', 'ec_email', 'ec_has_value', 'ec_invitation', 'ec_stays_in_room', 'ec_personal_event', 'ec_systemevent']:
        db(db[tablename].ec_id_person == mergeid).update(ec_id_person = keep.id)
    for tablename in ['ec_page', 'ec_invitation']:
        db(db[tablename].ec_created_by == mergeid).update(ec_created_by = keep.id)
    db((db.auth_permission.record_id==mergeid) & (db.auth_permission.table_name=='ec_person')).update(record_id = keep.id)
    db(db.ec_person.id == mergeid).delete()
    return T('I merged %s and %s') % (keep.id, merge.id)

def _update_event_times(form): # when updating a timeslot, update events
    old_timeslot = db.ec_timeslot(request.vars.id)
    if not old_timeslot:
        return
    events = db(db.ec_event.ec_id_timeslot == old_timeslot.id).select()
    for e in events:
        if old_timeslot.ec_start_time != form.vars.ec_start_time:
            if old_timeslot.ec_start_time and e.ec_start == old_timeslot.ec_start_time:
                if form.vars.ec_start_time: e.update_record(ec_start = form.vars.ec_start_time)
                else: e.update_record(ec_start = None)
        if old_timeslot.ec_end_time != form.vars.ec_end_time:
            if old_timeslot.ec_end_time and e.ec_end == old_timeslot.ec_end_time:
                if form.vars.ec_end_time: e.update_record(ec_end = form.vars.ec_end_time)
                else: e.update_record(ec_end = form.vars.ec_end_time)

def _get_systemevent_text(ec_type='newsletter',id=None,separator='<br />'):
    if ec_type == 'newsletter':
        news = db.ec_news(id)
        if not news:
            return T('Invalid news')
        entries = db(db.ec_systemevent.ec_what.startswith('News Entry %s' % news.id)).select()
        if not entries:
            return T('No system events for news %s') % news.id
        else:
            return separator.join(['%s: %s' % (e.ec_when, e.ec_what) for e in entries])
    else:
        return T('Invalid action')

def _conhour2hour(hour, con_starttime=18):
    day = (hour+con_starttime-1) % 24
    return day

def _get_table_table(convention,tableid=None,printview=False): # returns table-table; if tableid is given, only for this table, else all
    # calculate con hours based on tables
    query = db.ec_table.ec_id_convention==convention.id
    begin_table = db(query).select(orderby = db.ec_table.ec_available_begin).first()
    end_table = db(query).select(orderby = ~db.ec_table.ec_available_end).first()
    if not begin_table:
        return
    con_begin = begin_table['ec_available_begin']
    con_end = end_table['ec_available_end']
    con_days = (end_table.ec_available_end.date() - begin_table.ec_available_begin.date()).days +1
    con_starttime = begin_table.ec_available_begin.hour
    con_endday = end_table.ec_available_end.day
    con_endtime = end_table.ec_available_end.hour
    hours = con_days*24 - (24-con_endtime) - con_starttime + 1

    # get tables
    if tableid:
        tables = db(db.ec_table.id==tableid).select()
        if tables[0].ec_id_convention != convention.id:
            return T('Invalid convention')
    else:
        tables = db(query).select(orderby = db.ec_table.ec_order)

    # create matrix
    table_matrix = {}

    for table in tables:
        table_matrix[table['id']] = []
        # check for unavailable times at the beginning
        if (table['ec_available_begin'] - con_begin).total_seconds() > 0:
            # table not available from the beginning
            entry = dict(
                etype='table_not_available',
                begin=0,
                end=int((table['ec_available_begin']-con_begin).total_seconds() // 3600)
                )
            entry['duration'] = entry['end'] - entry['begin']
            table_matrix[table['id']].append(entry)
        # check for unavailable times at the end
        if (con_end - table['ec_available_end']).total_seconds() > 0:
            # table not available until the end
            entry = dict(
                etype='table_not_available',
                begin=hours -(int((con_end - table['ec_available_end']).total_seconds() // 3600)) - 1,
                end=hours
                )
            entry['duration'] = entry['end'] - entry['begin']
            table_matrix[table['id']].append(entry)
        # create entries for events
        events = db((db.ec_table.id==table['id']) &
                    (db.ec_table.id==db.ec_event_at_table.ec_id_table) &
                    (db.ec_event_at_table.ec_id_event==db.ec_event.id)).select(db.ec_event.ALL).as_list()
        for event in events:
            timeslot = db.ec_timeslot[event['ec_id_timeslot']]
            if timeslot:
                for what in ['ec_start', 'ec_end']:
                    if not event[what]: event[what] = timeslot['%s_time' % what]
            event['time'] = _begin_end_datetime_string(begin=event['ec_start'], end=event['ec_end'])
            event['type'] = MV(event['ec_type'])
            entry = dict(
                etype='event',
                warnings = [],
                title='%(ec_name)s (%(type)s)\n%(time)s' % event
                )
            if event['ec_start'] and event['ec_start'] >= table['ec_available_begin']:
                begin = int((event['ec_start'] - con_begin).total_seconds() // 3600)
                entry['begin']=begin
            elif event['ec_start'] and event['ec_start'] < table['ec_available_begin']:
                entry['warnings'].append(T('this event starts earlier than the table is available',lazy=False))
                begin = int((table['ec_available_begin']-con_begin).total_seconds() // 3600)
                entry['begin']=begin
            else:
                entry['warnings'].append(T('this event does not have a start time',lazy=False))
                begin = int((table['ec_available_begin']-con_begin).total_seconds() // 3600)
                entry['begin']=begin
            if event['ec_end'] and event['ec_end'] <= table['ec_available_end']:
                end=hours-int((con_end-event['ec_end']).total_seconds() // 3600)-1
                entry['end']=end
            elif event['ec_end'] and event['ec_end'] > table['ec_available_end']:
                entry['warnings'].append(T('this event ends later than the table is available',lazy=False))
                end=hours-int((con_end-table['ec_available_end']).total_seconds() // 3600)
                entry['end']=end
            else:
                entry['warnings'].append(T('this event does not have an end time',lazy=False))
                end=hours-int((con_end-table['ec_available_end']).total_seconds() // 3600)-1
                entry['end']=end
            entry['duration'] = entry['end'] - entry['begin']
            removestring = CAT(' ',A(IMG(_id="iconhere", _src=URL('static','images/remove.png'),_alt=T('remove')),
                            _href=URL('convention','events_at_table',args=[convention.ec_short_name,table['id']],
                            vars=dict(remove=event['id'],back=request.env.request_uri)),_title=T('Remove from table')))
            if printview: removestring = ''
            if entry['warnings']:
                warnings = ', '.join(entry['warnings'])
                warningstring = CAT(' ',A(IMG(_src=URL('static','images/warning.png'),_alt=T('warning')),
                            _href=URL('convention','events_at_table',args=[convention.ec_short_name,table['id']]),
                            _title=warnings))
            else:
                warningstring = ''
            if printview or (len(event['ec_name']) < (entry['end'] - entry['begin'])*2):
                entry['cellcontent']= A(event['ec_name'],
                _href=URL('convention','event',args=[convention.ec_short_name, event['id']]),
                _title=entry['title']) + removestring + warningstring
            else:
                entry['cellcontent'] = A('...',
                _href=URL('convention','event',args=[convention.ec_short_name, event['id']]),
                _title=entry['title']) + removestring
            table_matrix[table['id']].append(entry)

    # sort matrix
    for key, entries in table_matrix.items():
        table_matrix[key]=sorted(entries,key=lambda entry: entry['begin'])

    # create HTML table heading
    htmlrows = []
    day_heading = [TD()]
    for day in range(con_days):
        thedate = begin_table.ec_available_begin.date() + timedelta(day)
        if day == 0:
            day_heading.append(TD(thedate.strftime(dateformatstring), _colspan=24-con_starttime, _class='timetable_date'))
        else:
            day_heading.append(TD(thedate.strftime(dateformatstring), _colspan=24, _class='timetable_date'))
    htmlrows.append(day_heading)
    htmlrows.append([TD()] + [TD(_conhour2hour(hour, con_starttime=con_starttime),_class='timetable_hours') for hour in range(1,hours)])

    # create HTML table rows
    for tableid, entries in table_matrix.items():
        # create first column (contains table name)
        table = db.ec_table(tableid)
        table_url = A(table.ec_name, _href=URL('events_at_table',args=[convention.ec_short_name,table.id],user_signature=True))
        table_title = """%s: %s
%s: %s
%s""" % (T('Location'), table.get('ec_location'),
            T('Available'), _begin_end_datetime_string(begin=table.get('ec_available_begin'), end=table.get('ec_available_end')),
            MV('click_to_events_at_table'))
        tableclass='timetable_firstrow'
        warning_url=''
        while entries:
            if tableclass == 'timetable_overflowrow' and not warning_url:
                warning_url = CAT(' ',A(IMG(_src=URL('static','images/warning.png'),_alt=T('warning')),
                    _href=URL('convention','events_at_table',args=[convention.ec_short_name,table['id']]),
                    _title=T('There are overlapping entries at this table')))
                table_url = CAT(table_url,warning_url)
            row = [TD(table_url, _title=table_title, _class=tableclass),]
            tableclass='timetable_overflowrow'
            overflow_entries = []
            last_entry = dict(begin=0,end=0)
            for entry in entries:
                if last_entry['end'] > entry['begin']: # overlap!
                    overflow_entries.append(entry)
                    continue
                if last_entry['end'] < entry['begin']: # create gap TD
                    row.append(TD('',_colspan=entry['begin']-last_entry['end'],_class='timetable_gap'))
                if entry['etype']=='table_not_available':
                    row.append(TD('',_colspan=entry['duration'],_class='timetable_not_available',_title=T('not available')))
                if entry['etype']=='event':
                    row.append(TD(entry['cellcontent'],_colspan=entry['duration'],_class='timetable_event',_title=entry['title']))
                last_entry = entry
            htmlrows.append(row)
            entries = overflow_entries

    result = TABLE(*[TR(*row) for row in htmlrows],_class='timetable')
    return result

def _parse_configuration(configfile): # read an uploaded json, parse it, put contents to DB and delete the file
    try:
        import json
        json_file = request.folder + '/uploads/' + configfile
        with open(json_file) as json_data:
            data = json.load(json_data)
            # create basic con data and make sure that it is unique
            imported_con = data['ec_convention'] # the imported con
            # get similar convention from local DB and create unique entries
            import_string = "_imported_" # to be attached to imported convention names
            for field in ['ec_short_name', 'ec_name']:
                if not imported_con[field]:
                    return T('Error while importing: No %s given') % field
                other_con = db(db.ec_convention[field]==imported_con[field]).select().first()
                if other_con:
                    counter = 1
                while other_con: # we found a Con with this (short_)name
                    ilocation = other_con[field].find(import_string)
                    if ilocation == -1: # import string is not there
                        imported_con[field] = '%s%s%s' % (imported_con[field], import_string, counter)
                    else: # don't attach the import_string again (for multiple imports)
                        imported_con[field] = '%s%s%s' % (imported_con[field][:ilocation], import_string, counter)
                    counter += 1
                    other_con = db(db.ec_convention[field]==imported_con[field]).select().first()
            # insert into DB
            imported_con.pop('id', None) # never use old DB IDs for imported data
            imported_conid = db.ec_convention.insert(**imported_con)
            db.commit()
            # create configuration entry
            imported_config = data['ec_configuration']
            imported_config['ec_name'] = 'Imported configuration for %s' % imported_con['ec_name']
            imported_config.pop('id', None) # never use old DB IDs for imported data
            imported_config['ec_convention_public'] = False # imported cons should not be public
            imported_config['ec_event_public'] = False # events of imported cons should not be public
            imported_configid = db.ec_configuration.insert(**imported_config)
            db.ec_convention[imported_conid] = dict(ec_id_configuration=imported_configid)
            # insert other tables that belong to the configuration
            import_errors = ''
            for table in CONFIG_TABLES:
                if table in data:
                    # print 'Import %ss' % table
                    for imported_table in data[table].values():
                        if table in ['ec_series', 'ec_in_series']:
                            continue # do not import series
                        if table in ['ec_page', ]:
                            imported_table.pop('ec_created_by', None) # don't use person ID here
                        imported_table.pop('id', None) # never use old DB IDs for imported data
                        imported_table['ec_id_convention'] = imported_conid
                        try:
                            db[table].insert(**imported_table)
                        except Exception as e:
                            import_errors = '%s, %s: %s (%s)' % (import_errors, T('Import error'), table, e)
                        db.commit()
            # add access rights for current user
            orgagroup = auth.add_group('con_orga_%s' % imported_conid, 'Organizers of convention %s' % imported_conid)
            admingroup = auth.add_group('con_admin_%s' % imported_conid, 'Administrators of convention %s' % imported_conid)
            auth.add_permission(orgagroup, 'organize', db.ec_convention, imported_conid)
            auth.add_permission(admingroup, 'configure', db.ec_convention, imported_conid)
            auth.add_membership(orgagroup)
            auth.add_membership(admingroup)
            db.commit()
            result = 'Convention inserted with id %s' % imported_conid
    except KeyError as e:
        result = '%s: %s, %s' % (T('Import error'), T('Could not import %s') % e, import_errors)
    except Exception as e:
        result = '%s: %s, %s' % (T('Import error'), e, import_errors)
    from os import remove
    remove(json_file) # delete the uploaded file
    if import_errors:
        result = '%s; %s' % (result, import_errors)
    return result

def _get_access_token(id, force_update):
    person = db.ec_person(id)
    token = person.ec_access_token
    if force_update or not person.ec_access_token:
        token = os.urandom(16).hex()
        db(db.ec_person.id == person.id).update(ec_access_token=token)
    return token
