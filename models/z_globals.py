# set globals and remember things

# set last language of user
if auth.is_logged_in():
    try:
        language = request.env.HTTP_ACCEPT_LANGUAGE.split(',')[0]
        if language:
            db(db.auth_user.id==auth.user.id).update(last_language=language)
            db.commit()
    except:
        pass


# Text for imprint
import os
private_path = os.path.normpath(os.path.join(os.path.dirname(__file__), '../private'))
with open(private_path+'/imprint_text.md', encoding='utf-8') as f:
    imprint_text = f.read()

# Text for privacy notice
with open(private_path+'/privacy_text.md', encoding='utf-8') as f:
    privacy_text = f.read()

