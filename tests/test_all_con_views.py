#!/usr/local/bin/python3
import unittest
from TestBaseClasses import TestBaseClass
from bs4 import BeautifulSoup
from testdata import Testdata
import time
import sys, os

class AllConViews(TestBaseClass):

    def _01_createCon(self):
        # Login Ruth and create convention
        client = self._get_client(self.Testdata.app_url, self.Testdata.ruth)
        path = 'default/request_installation'
        data = self.Testdata.thecon
        data['_formname'] = 'requestform'
        client.post(path,data)
        path = 'default/my_conventions'
        client.get(path)
        # Test if the convention appears in his "my_convention" list
        self.assertIn(self.Testdata.thecon['ec_name'], client.text, "Convention was not created")
        # Grant convention
        path = 'default/grant_conventions'
        client.get(path)
        soup = BeautifulSoup(client.text, 'html.parser')
        thecon_name = self.Testdata.thecon['ec_name']
        tds = soup.find_all('td')
        tr = None
        for td in tds:
            if td.contents[0]==thecon_name:
                tr = td.parent
        if tr:
            url = tr.find('td').a['href'][1:]
            client.get(url)
        path = "convention/configure_main/%s" % self.Testdata.thecon['ec_short_name']
        client.get(path)
        self.assertIn("to_be_configured", client.text, "Convention was not granted")

    def _02_configureCon(self):
        # Login Ruth and confiure convention
        client = self._get_client(self.Testdata.app_url, self.Testdata.ruth)
        path = "convention/configure_main/%s" % self.Testdata.thecon['ec_short_name']
        formdata = dict(_formname='copyconfigform',conid='-1')
        client.post(path,formdata)
        self.assertIn("New configuration created",client.text, "Convention was not configured")

        # Basic Config
        path = "convention/configure/%s/basic_config" % self.Testdata.thecon['ec_short_name']
        client.get(path)
        formdata = dict()
        # set _formname and the hidden record id
        for f in client.forms:
            if f.startswith('ec_configuration'):
                formdata=dict(_formname=f,id=f[len('ec_configuration/'):])
        formdata.update(self.Testdata.thecon_basic_configuration)
        client.post(path,formdata)
        soup = BeautifulSoup(client.text, 'html.parser')
        search = 'Configuration for "%s" changed' % self.Testdata.thecon['ec_name']
        teststring=str(soup.find('div', class_='flash'))
        # Test if there is the change message
        self.assertIn(search, teststring, "No message that configuration was changed")

        # adds a time slot for events
        path = 'convention/timeslots/%s' % self.Testdata.thecon['ec_short_name']
        client.get(path)
        # retrieve path with signature
        soup = BeautifulSoup(client.text, 'html.parser')
        link = soup.find(string='Add Record').find_parent('a')['href']
        path = link[len('/'):]
        # fill out form
        client.get(path)
        formdata = dict(_formname='_web2py_grid')
        soup = BeautifulSoup(client.text, 'html.parser')
        for i in soup.find_all('input'):
            if i.get('name', None):
                formdata[i['name']]=i['value']
        formdata.update(self.Testdata.timeslot_1)
        client.post(path,formdata)
        search = 'Thursday evening'
        teststring = client.text
        # Test if the new timeslot appears in list
        self.assertIn(search, client.text, "New timeslot not found")

        # create a buyable item
        path = 'convention/items/%s' % self.Testdata.thecon['ec_short_name']
        client.get(path)
        # retrieve path with signature
        soup = BeautifulSoup(client.text, 'html.parser')
        link = soup.find(string='Add Record').find_parent('a')['href']
        path = link[len('/'):]
        # fill out form
        client.get(path)
        formdata = dict(_formname='_web2py_grid')
        soup = BeautifulSoup(client.text, 'html.parser')
        for i in soup.find_all('input'):
            if i.get('name', None):
                formdata[i['name']]=i['value']
        formdata.update(self.Testdata.buyable_item_con_registration)
        dellist = []
        for k in formdata:
            if formdata[k]=='off':
                dellist.append(k)
        for k in dellist: del formdata[k]
        client.post(path,formdata)
        search = self.Testdata.buyable_item_con_registration['ec_name']
        teststring = client.text
        # Test if the new timeslot appears in list
        self.assertIn(search, client.text, "Item was not created")

        # activate event registration by entering a start date
        path = 'convention/configure/%s/events' % self.Testdata.thecon['ec_short_name']
        client.get(path)
        formdata = dict()
        # set _formname and the hidden record id
        for f in client.forms:
            if f.startswith('ec_configuration'):
                formdata=dict(_formname=f,id=f[len('ec_configuration/'):])
        soup = BeautifulSoup(client.text, 'html.parser')
        for i in soup.find_all('input'):
            if i.get('name', None):
                formdata[i['name']]=i['value']
        for i in soup.find_all('input'):
            if i.get('name', None):
                formdata[i['name']]=i['value']
        for i in soup.find_all('select'):
            selected=i.find('option', attrs={"selected": "selected"})
            if selected: formdata[i['name']]=selected['value']
        formdata.update(self.Testdata.event_config_1)
        dellist = []
        for k in formdata:
            if formdata[k]=='off':
                dellist.append(k)
        for k in dellist:
            del formdata[k]
        formdata['ec_event_registration_period_start']=time.strftime('%Y-%m-%d %H:%M:%S')
        formdata['ec_event_attendee_registration_period_start']=time.strftime('%Y-%m-%d %H:%M:%S')
        client.post(path,formdata)
        soup = BeautifulSoup(client.text, 'html.parser')
        search = 'Configuration for "%s" changed' % self.Testdata.thecon['ec_name']
        teststring=str(soup.find('div', class_='flash'))
        # Test if there is the change message
        self.assertIn(search, teststring, "No message that configuration of registration was changed")
        # open the registration by starting the registration now
        path = 'convention/configure/%s/registration' % self.Testdata.thecon['ec_short_name']
        client.get(path)
        formdata = dict()
        # set _formname and the hidden record id
        for f in client.forms:
            if f.startswith('ec_configuration'):
                formdata=dict(_formname=f,id=f[len('ec_configuration/'):])
        soup = BeautifulSoup(client.text, 'html.parser')
        for i in soup.find_all('input'):
            if i.get('name', None):
                formdata[i['name']]=i['value']
        for i in soup.find_all('input'):
            if i.get('name', None):
                formdata[i['name']]=i['value']
        for i in soup.find_all('select'):
            selected=i.find('option', attrs={"selected": "selected"})
            if selected: formdata[i['name']]=selected['value']
        formdata.update(self.Testdata.registration_config_1)
        formdata['ec_registration_period_start']=time.strftime('%Y-%m-%d %H:%M:%S')
        client.post(path,formdata)
        soup = BeautifulSoup(client.text, 'html.parser')
        search = 'Configuration for "%s" changed' % self.Testdata.thecon['ec_name']
        teststring=str(soup.find('div', class_='flash'))
        # Test if there is the change message
        self.assertIn(search, teststring, "No message that configuration of registration was changed")

        # make the convention active
        path = "convention/configure_main/%s/edit" % self.Testdata.thecon['ec_short_name']
        client.get(path)
        # get existing form data
        formdata = dict(_formname='conventionform')
        self._w(client.text)
        soup=BeautifulSoup(client.text,'html.parser')
        for i in soup.find_all('input'):
            if i.get('id', None):
                if i['id'].startswith('ec_convention_'):
                    key = i['id'][len('ec_convention_'):]
                    formdata[key]=i['value']
            if i.get('name', None) and i['name']=='id':
                formdata['id']=i['value']
        # change con status
        formdata['ec_convention_state']='active'
        client.post(path,formdata)
        self.assertIn("This convention is active", client.text, "Convention was not configured")

    def _03_test_views_nologin(self):
        """ Test views without login """
        # get the views from the directory
        views = sorted(os.listdir("../views/convention/"))
        views = [x for x in views if x.endswith('.html')]
        views = [x[:x.find('.html')] for x in views]
        no_test=[]
        # Test all views
        print("Without Login:")
        for view in views:
            if view in no_test: continue
            path = '%s/%s/%s' % ('convention', view, self.Testdata.thecon['ec_short_name'])
            client = self._get_client(self.Testdata.app_url)
            try:
                client.get(path)
                result=client.status
            except Exception as e:
                print(e)
                result=500
            self.assertEqual(result,200,"Error at: %s (without login)" % path)

    def _04_test_views_login(self):
        """ Test views with login """
        # get the views from the directory
        views = sorted(os.listdir("../views/convention/"))
        views = [x for x in views if x.endswith('.html')]
        views = [x[:x.find('.html')] for x in views]
        no_test=['orgatasks'] # see https://gitlab.com/fermate35/easycon/-/issues/95
        # Test all views, now with login
        print("With Login:")
        for view in views:
            if view in no_test: continue
            path = '%s/%s/%s' % ('convention', view, self.Testdata.thecon['ec_short_name'])
            client = self._get_client(self.Testdata.app_url, self.Testdata.ruth)
            try:
                client.get(path)
                result=client.status
            except Exception as e:
                print(e)
                result=500
            finally:
                self.assertEqual(result,200,"Error at: %s (with login)" % path)

    def _99_deleteConData(self):
        """Delete the data created by the scenario"""
        # Delete Convention
        # login Ruth
        client = self._get_client(self.Testdata.app_url, self.Testdata.ruth)
        path = 'default/grant_conventions?showall=1'
        client.get(path)
        # Find 'Edit' link
        try:
            soup=BeautifulSoup(client.text, 'html.parser')
            tr = soup.find(string=Testdata.thecon['ec_name']).find_parent('tr')
            link = tr.find(string='Edit').find_parent('a')['href']
            path = link[len('/'):]
        except:
            print('This can fail if you have too many undeleted TestCons and the Con we search for is not on first page')
        client.get(path)
        # Get formdata
        formdata = dict(_formname='_web2py_grid')
        soup = BeautifulSoup(client.text, 'html.parser')
        for i in soup.find_all('input'):
            if i.get('name', None):
                formdata[i['name']]=i['value']
        client.post(path,formdata)

    def tearDown(self):
        print("Tear Down!")
        self._99_deleteConData()
        pass

    def test_con_views(self):
        """Test all con views"""
        self._01_createCon()
        self._02_configureCon()
        self._03_test_views_nologin()
        self._04_test_views_login()

suite = unittest.TestLoader().loadTestsFromTestCase(AllConViews)
unittest.TextTestRunner(verbosity=2).run(suite)
