#!/usr/local/bin/python3
"""Somehow deprecated - does not use TestBaseClasses yet"""
import sys, os
sys.path.append('/opt/web2py')
from gluon.contrib.webclient import WebClient
import unittest
from testdata import Testdata
from contextlib import contextmanager

@contextmanager
def suppress_stdout():
    """We do not want to see the prints of WebClient"""
    with open(os.devnull, "w") as devnull:
        old_stdout = sys.stdout
        sys.stdout = devnull
        try:
            yield
        finally:
            sys.stdout = old_stdout

class TestContainer(unittest.TestCase):
    """Containts the testcases; most dynamically created and added"""
    longMessage = True
    def _get_client(self, app_url, user=None):
        """Creates a client object; if user is given, she will be logged in"""
        if hasattr(self,'_client'): return self._client
        baseurl = "%s/" % (app_url)
        client = WebClient(baseurl, postbacks=True)   
        with suppress_stdout():
            client.get('default/index') # get index first to set _formkey
        self._client = client
        if user:
            with suppress_stdout():
                client.get('default/user/login') # needs a get before we can post for _formkey
            data = dict(email=user['email'], password=user['password'], _formname='login')
            with suppress_stdout():
                client.post('default/user/login', data=data)
        return client
        
def make_testfunction_HTTP_200(app_url, path, user=None):
    """Creates a testfunction for a given path; if user is given, she is logged in"""
    def exec_test(self):
        # print('Testing %s' % (path))
        client = self._get_client(app_url, user)
        result=None
        message="Path: %s" % path
        try: 
            with suppress_stdout():
                client.get(path)
            result = client.status
        except Exception as e:
            message="%s; %s" % (message, e)
        self.assertEqual(result, 200, message)
    return exec_test
 
def test_AllViews_HTTP_200():
    controllers = ['default', 'export', 'mydata']
    print("Test: Do all views return a valid web page?")
    print("Controllers: %s" % controllers)
    for controller in controllers:
        # get the views from the directory
        views = sorted(os.listdir("../views/%s/" % controller))   #, '[\w/\-]+(\.\w+)+$'))
        views = [x for x in views if x.endswith('.html')]
        views = [x[:x.find('.html')] for x in views]
        # create the test functions
        for view in views:
            path = '%s/%s' % (controller, view)
            # create test without login
            setattr(TestContainer, 'test_view_{0}_{1}'.format(controller,view), 
                    make_testfunction_HTTP_200(Testdata.app_url,path))
            # create test with login
            user = Testdata.musdama
            setattr(TestContainer, 'test_view_withlogin_{0}_{1}'.format(controller,view), 
                    make_testfunction_HTTP_200(Testdata.app_url,path,user))
          
if __name__ == '__main__':
    test_AllViews_HTTP_200()
    unittest.main()

