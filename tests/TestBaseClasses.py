#!/usr/local/bin/python3
import unittest
import sys, os, time
from copy import deepcopy
from testdata import Testdata
sys.path.append('/opt/web2py')
from gluon.contrib.webclient import WebClient
from contextlib import contextmanager

# Fix the to_native function to not crash with PDF
import gluon.contrib.webclient
def fixed_to_native(obj, charset='utf8', errors='strict'):
    try:
        if obj is None or isinstance(obj, str):
            return obj
        return obj.decode(charset, errors)
    except:
        return ''
gluon.contrib.webclient.to_native = fixed_to_native

@contextmanager
def suppress_stdout():
    """When we do not want to see the print outs of WebClient"""
    with open(os.devnull, "w") as devnull:
        old_stdout = sys.stdout
        sys.stdout = devnull
        try:
            yield
        finally:
            sys.stdout = old_stdout

class TestBaseClass(unittest.TestCase):
    longMessage = True

    def setUp(self):
        self.suffix = '_' + str(int(time.time()))
        self.Testdata = deepcopy(Testdata)
        self.Testdata.thecon['ec_name'] += self.suffix
        self.Testdata.thecon['ec_short_name'] += self.suffix
        # todo: Create users

    def _get_client(self, app_url, user=None):
        """Helper: creates a client object; if user is given, she will be logged in"""
        baseurl = "%s/" % (app_url)
        client = PathPrintingWebClient(baseurl, postbacks=True)
        with suppress_stdout():
            client.get('default/index') # get index first to set _formkey
        if user:
            with suppress_stdout():
                client.get('default/user/login') # needs a get before we can post for _formkey
            data = dict(email=user['email'], password=user['password'], _formname='login')
            with suppress_stdout():
                client.post('default/user/login', data)
        return client

    def _w(self, text, filename='output.html'):
        """ write output to a file """
        f = open(filename, "w")
        f.write(text)
        f.close()

class PathPrintingWebClient(WebClient):
    """Extends WebClient functions to print out the called path"""
    def __init__(self, baseurl, postbacks=True, *args, **kwargs):
        WebClient.__init__(self, baseurl, postbacks, *args, **kwargs)

    def get(self, path, cookies=None, headers=None, auth=None):
        print("GET  %s" % path)
        with suppress_stdout():
            WebClient.get(super(), path, cookies, headers, auth)

    def post(self, path, data=None, cookies=None, headers=None, auth=None, method=None):
        print("POST %s" % path)
        if False:
            if data:
                print("  with data: ")
                for k in data:
                    print("      %s: %s" % (k, data[k]))

        super().post(path, data, cookies, headers, auth)
        



