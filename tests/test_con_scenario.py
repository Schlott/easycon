#!/usr/local/bin/python3
import sys, os, time
sys.path.append('/opt/web2py')
from TestBaseClasses import *
import unittest
from testdata import Testdata
from contextlib import contextmanager
from bs4 import BeautifulSoup
from testdata import Testdata
from copy import deepcopy
from gluon._compat import to_bytes

class TestConScenario(TestBaseClass):
    longMessage = True

    def setUp(self):
        self.suffix = '_' + str(int(time.time()))
        self.Testdata = deepcopy(Testdata)
        self.Testdata.thecon['ec_name'] += self.suffix
        self.Testdata.thecon['ec_short_name'] += self.suffix

    def _01_requestCon(self):
        """Alvin creates a Con request"""
        client = self._get_client(self.Testdata.app_url, self.Testdata.alvin)
        path = 'default/request_installation'
        data = self.Testdata.thecon
        data['_formname'] = 'requestform'
        client.post(path,data)
        path = 'default/my_conventions'
        client.get(path)
        # Test if the convention appears in his "my_convention" list
        self.assertIn(self.Testdata.thecon['ec_name'], client.text, "Convention was not created")
    def _02_grantCon(self):
        """Ruth grants Alvin's request"""
        client = self._get_client(self.Testdata.app_url, self.Testdata.ruth)
        path = 'default/grant_conventions'
        client.get(path)
        soup = BeautifulSoup(client.text, 'html.parser')
        thecon_name = self.Testdata.thecon['ec_name']
        tds = soup.find_all('td')
        tr = None
        for td in tds:
            if td.contents[0]==thecon_name:
                tr = td.parent
        if tr:
            url = tr.find('td').a['href']
            path=url[1:]
            client.get(path)
        # now logout Ruth and login Alvin; check if convention was granted
        with suppress_stdout():
            client.get('default/user/logout')
            client = self._get_client(self.Testdata.app_url, self.Testdata.alvin)
        path = "convention/configure_main/%s" % self.Testdata.thecon['ec_short_name']
        client.get(path)
        self.assertIn("to_be_configured", client.text, "Convention was not granted")
    def _03_configureCon(self):
        """Alvin configures the convention"""
        # Alvin creates a new configuration for the convention
        client = self._get_client(self.Testdata.app_url, self.Testdata.alvin)
        path = "convention/configure_main/%s" % self.Testdata.thecon['ec_short_name']
        formdata = dict(_formname='copyconfigform',conid='-1')
        client.post(path,formdata)
        self.assertIn("New configuration created",client.text, "Convention was not configured")

        # Basic Config
        path = "convention/configure/%s/basic_config" % self.Testdata.thecon['ec_short_name']
        client.get(path)
        formdata = dict()
        # set _formname and the hidden record id
        for f in client.forms:
            if f.startswith('ec_configuration'):
                formdata=dict(_formname=f,id=f[len('ec_configuration/'):])
        formdata.update(self.Testdata.thecon_basic_configuration)
        client.post(path,formdata)
        soup = BeautifulSoup(client.text, 'html.parser')
        search = 'Configuration for "%s" changed' % self.Testdata.thecon['ec_name']
        teststring=str(soup.find('div', class_='flash'))
        # Test if there is the change message
        self.assertIn(search, teststring, "No message that configuration was changed")

        # Alvin adds a time slot for events
        path = 'convention/timeslots/%s' % self.Testdata.thecon['ec_short_name']
        client.get(path)
        # retrieve path with signature
        soup = BeautifulSoup(client.text, 'html.parser')
        link = soup.find(string='Add Record').find_parent('a')['href']
        path = link[len('/'):]
        # fill out form
        client.get(path)
        formdata = dict(_formname='_web2py_grid')
        soup = BeautifulSoup(client.text, 'html.parser')
        for i in soup.find_all('input'):
            if i.get('name', None):
                formdata[i['name']]=i['value']
        formdata.update(self.Testdata.timeslot_1)
        client.post(path,formdata)
        search = 'Thursday evening'
        teststring = client.text
        # Test if the new timeslot appears in list
        self.assertIn(search, client.text, "New timeslot not found")

        # Alvin creates a buyable items
        path = 'convention/items/%s' % self.Testdata.thecon['ec_short_name']
        client.get(path)
        # retrieve path with signature
        soup = BeautifulSoup(client.text, 'html.parser')
        link = soup.find(string='Add Record').find_parent('a')['href']
        path = link[len('/'):]
        # fill out form
        client.get(path)
        formdata = dict(_formname='_web2py_grid')
        soup = BeautifulSoup(client.text, 'html.parser')
        for i in soup.find_all('input'):
            if i.get('name', None):
                formdata[i['name']]=i['value']
        formdata.update(self.Testdata.buyable_item_con_registration)
        dellist = []
        for k in formdata:
            if formdata[k]=='off':
                dellist.append(k)
        for k in dellist: del formdata[k]
        client.post(path,formdata)
        search = self.Testdata.buyable_item_con_registration['ec_name']
        teststring = client.text
        # Test if the new timeslot appears in list
        self.assertIn(search, client.text, "Item was not created")

        # Alvin activates event registration by entering a start date
        path = 'convention/configure/%s/events' % self.Testdata.thecon['ec_short_name']
        client.get(path)
        formdata = dict()
        # set _formname and the hidden record id
        for f in client.forms:
            if f.startswith('ec_configuration'):
                formdata=dict(_formname=f,id=f[len('ec_configuration/'):])
        soup = BeautifulSoup(client.text, 'html.parser')
        for i in soup.find_all('input'):
            if i.get('name', None):
                formdata[i['name']]=i['value']
        for i in soup.find_all('input'):
            if i.get('name', None):
                formdata[i['name']]=i['value']
        for i in soup.find_all('select'):
            selected=i.find('option', attrs={"selected": "selected"})
            if selected: formdata[i['name']]=selected['value']
        formdata.update(self.Testdata.event_config_1)
        dellist = []
        for k in formdata:
            if formdata[k]=='off':
                dellist.append(k)
        for k in dellist:
            del formdata[k]
        formdata['ec_event_registration_period_start']=time.strftime('%Y-%m-%d %H:%M:%S')
        formdata['ec_event_attendee_registration_period_start']=time.strftime('%Y-%m-%d %H:%M:%S')
        client.post(path,formdata)
        soup = BeautifulSoup(client.text, 'html.parser')
        search = 'Configuration for "%s" changed' % self.Testdata.thecon['ec_name']
        teststring=str(soup.find('div', class_='flash'))
        # Test if there is the change message
        self.assertIn(search, teststring, "No message that configuration of registration was changed")
        # Alvin opens the registration by starting the registration now
        path = 'convention/configure/%s/registration' % self.Testdata.thecon['ec_short_name']
        client.get(path)
        formdata = dict()
        # set _formname and the hidden record id
        for f in client.forms:
            if f.startswith('ec_configuration'):
                formdata=dict(_formname=f,id=f[len('ec_configuration/'):])
        soup = BeautifulSoup(client.text, 'html.parser')
        for i in soup.find_all('input'):
            if i.get('name', None):
                formdata[i['name']]=i['value']
        for i in soup.find_all('input'):
            if i.get('name', None):
                formdata[i['name']]=i['value']
        for i in soup.find_all('select'):
            selected=i.find('option', attrs={"selected": "selected"})
            if selected: formdata[i['name']]=selected['value']
        formdata.update(self.Testdata.registration_config_1)
        formdata['ec_registration_period_start']=time.strftime('%Y-%m-%d %H:%M:%S')
        client.post(path,formdata)
        soup = BeautifulSoup(client.text, 'html.parser')
        search = 'Configuration for "%s" changed' % self.Testdata.thecon['ec_name']
        teststring=str(soup.find('div', class_='flash'))
        # Test if there is the change message
        self.assertIn(search, teststring, "No message that configuration of registration was changed")

        # Alvin makes the convention active
        client = self._get_client(self.Testdata.app_url, self.Testdata.alvin)
        path = "convention/configure_main/%s/edit" % self.Testdata.thecon['ec_short_name']
        client.get(path)
        # get existing form data
        formdata = dict(_formname='conventionform')
        soup=BeautifulSoup(client.text,'html.parser')
        for i in soup.find_all('input'):
            if i.get('id', None):
                key = i['id'][len('ec_convention_'):]
                formdata[key]=i['value']
            if i.get('name', None) and i['name']=='id':
                formdata['id']=i['value']
        # change con status
        formdata['ec_convention_state']='active'
        client.post(path,formdata)
        self.assertIn("This convention is active", client.text, "Convention was not configured")

        # Alvin adds Olga to the Orga Group
        client = self._get_client(self.Testdata.app_url, self.Testdata.alvin)
        path = "convention/promote/%s?what=orga" % self.Testdata.thecon['ec_short_name']
        formdata = dict(_formname='default',email=self.Testdata.olga['email'])
        client.post(path,formdata)
        # Login Olga
        with suppress_stdout():
            client.get('default/user/logout')
            client = self._get_client(self.Testdata.app_url, self.Testdata.olga)
        path = 'convention/organize/%s' % self.Testdata.thecon['ec_short_name']
        client.get(path)
        search = 'Easy-Con for %s: Organize' % self.Testdata.thecon['ec_name']
        soup = BeautifulSoup(client.text, 'html.parser')
        teststring=str(soup.find('title'))
        # Test if she can access the orga page
        self.assertIn(search, teststring, "Orga access was not granted")
    def _04_register4Con(self):
        """Musdama registeres for convention"""
        # Musdama looks for Cons - first without, then with login
        path = 'default/open_conventions'
        with suppress_stdout():
            client=self._get_client(self.Testdata.app_url)
            client.get(path)
        search = self.Testdata.thecon['ec_name']
        teststring = client.text
        # Test if TheCon is in the list of open cons
        self.assertIn(search, teststring, "Con not found")
        with suppress_stdout():
            client.get('default/user/logout')
            client = self._get_client(self.Testdata.app_url, self.Testdata.musdama)
        client.get(path)
        search = self.Testdata.thecon['ec_name']
        teststring = client.text
        # Test if TheCon is in the list of open cons
        self.assertIn(search, teststring, "Con not found")

        # Musdama goes to convention page
        path = "convention/convention/%s" % self.Testdata.thecon['ec_short_name']
        search = self.Testdata.thecon['ec_name']
        teststring = client.text
        # Test if TheCon is in the list of open cons
        self.assertIn(search, teststring, "Con not found")

        # Musdama registers for TheCon
        path = 'convention/register/%s' % self.Testdata.thecon['ec_short_name']
        client.get(path)
        # get existing form data
        formdata = dict(_formname='regform')
        soup=BeautifulSoup(client.text,'html.parser')
        for i in soup.find_all('input'):
            if i.get('id', None):
                key = i['id'][len('no_table_'):]
                formdata[key]=i['value']
        for i in soup.find_all('select'):
            selected=i.find('option')
            if selected and selected.get('value'):
                formdata[i['name']]=selected['value']
        formdata.update(self.Testdata.registration_avalon)
        del formdata['waitlist']
        client.post(path,formdata)
        search='Avalon'
        self.assertIn(search, client.text, "Registration did not work")
    def _05_grantAccess4Con(self):
        # Olga checks back account, found that Musdama paid and enters that in the system
        with suppress_stdout():
            client = self._get_client(self.Testdata.app_url, self.Testdata.olga)
        path = "convention/attendees/%s" % self.Testdata.thecon['ec_short_name']
        client.get(path)
        soup=BeautifulSoup(client.text,'html.parser')
        tr = soup.find(string='Musdama').find_parent('tr')
        link = tr.find(string='paid all').find_parent('a')['href']
        path = link[len('/'):]
        client.get(path)
        search = 'Musdama Testen has paid'
        self.assertIn(search, client.text, "Has paid did not work")
    def _06_register4ConPartialPayment(self):
        """Willma registers for Con, makes partial payment"""
        # Willma registers for TheCon
        with suppress_stdout():
            client = self._get_client(self.Testdata.app_url, self.Testdata.willma)
        path = 'convention/register/%s' % self.Testdata.thecon['ec_short_name']
        client.get(path)
        # get existing form data
        formdata = dict(_formname='regform')
        soup=BeautifulSoup(client.text,'html.parser')
        for i in soup.find_all('input'):
            if i.get('id', None):
                key = i['id'][len('no_table_'):]
                formdata[key]=i['value']
        for i in soup.find_all('select'):
            selected=i.find('option')
            if selected and selected.get('value'):
                formdata[i['name']]=selected['value']
        formdata.update(self.Testdata.registration_helheim)
        del formdata['waitlist']
        client.post(path,formdata)
        search='Helheim'
        self.assertIn(search, client.text, "Registration did not work")

        # Olga checks back account, found that Willma paid 20 Euro and enters that in the system
        with suppress_stdout():
            client.get('default/user/logout')
            client = self._get_client(self.Testdata.app_url, self.Testdata.olga)
        path = "convention/attendees/%s" % self.Testdata.thecon['ec_short_name']
        # Find link for Willma
        client.get(path)
        soup=BeautifulSoup(client.text,'html.parser')
        tr = soup.find(string='Willma').find_parent('tr')
        link = tr.find(string='Manage attendee ...').find_parent('a')['href']
        path = link[len('/'):]
        client.get(path)
        soup=BeautifulSoup(client.text,'html.parser')
        link = soup.find(string='paid something ...').find_parent('a')['href']
        path = link[len('/'):]
        client.get(path)
        # Find formdata
        formdata = {}
        soup=BeautifulSoup(client.text,'html.parser')
        formdata['_formname']=soup.find('input', attrs={'name' : '_formname'})['value']
        formdata['id']=soup.find('input', attrs={'name' : 'id'})['value']
        formdata['ec_payment'] = "21"
        path='convention/attendee_paid_something/%s#' % self.Testdata.thecon['ec_short_name']
        client.post(path, formdata)
        search='Payment changed for Willma Testen'
        self.assertIn(search, client.text, "Partial payment was not entered")

        # Because of the partial payment, Olga changes status of Willma to "attendee"
        path = "convention/attendees/%s" % self.Testdata.thecon['ec_short_name']
        # Find link for Willma
        client.get(path)
        soup=BeautifulSoup(client.text,'html.parser')
        tr = soup.find(string='Willma').find_parent('tr')
        link = tr.find(string='Manage attendee ...').find_parent('a')['href']
        path = link[len('/'):]
        client.get(path)
        soup=BeautifulSoup(client.text,'html.parser')
        link = soup.find(string='activate*').find_parent('a')['href']
        path = link[len('/'):]
        client.get(path)
        search='State of registration was changed to: attendee'
        self.assertIn(search, client.text, "Attendee status was not changed")
    def _07_registerEvent(self):
        """Willma registeres an rpg event"""
        with suppress_stdout():
            client = self._get_client(self.Testdata.app_url, self.Testdata.willma)
        path = "convention/event/%s/new/rpggame" % self.Testdata.thecon['ec_short_name']
        client.get(path)
        # get existing form data
        formdata = dict(_formname='ec_event/create')
        soup=BeautifulSoup(client.text,'html.parser')
        for i in soup.find_all('input'):
            if i.get('id', None):
                key = i['id'][len('ec_event_'):]
                formdata[key]=i['value']
        for i in soup.find_all('select'):
            selected=i.find('option')
            if selected and selected.get('value'):
                formdata[i['name']]=selected['value']
        formdata.update(self.Testdata.event_sprungwelt)
        client.post(path,formdata)
        search='Create new game for event %s' % formdata['ec_name']
        self.assertIn(search, client.text, "Event was not created")

        # Willma creates a new game for her event
        # find out event_id
        soup=BeautifulSoup(client.text,'html.parser')
        urlbegin = '/user/profile'
        for a in soup.find_all('a'):
            if a['href'].startswith(urlbegin):
                event_id = a['href'][a['href'].rindex('/')+1:]
        path = 'convention/newgame/%s/%s' %  (self.Testdata.thecon['ec_short_name'],event_id)
        formdata = dict(_formname='ec_rpggame/create')
        formdata.update(self.Testdata.rpggame_idee)
        formdata['ec_name'] = "%s_%s" % (formdata['ec_name'], self.suffix)
        client.post(path, formdata)
        search="Game %s created" % formdata['ec_name']
        self.assertIn(search, client.text, "Game was not created")
    def _07_registerEventNoTime(self):
        """Willma registeres an workshop event with no start and end time"""
        with suppress_stdout():
            client = self._get_client(self.Testdata.app_url, self.Testdata.willma)
        path = "convention/event/%s/new/workshop" % self.Testdata.thecon['ec_short_name']
        client.get(path)
        # get existing form data
        formdata = dict(_formname='ec_event/create')
        soup=BeautifulSoup(client.text,'html.parser')
        for i in soup.find_all('input'):
            if i.get('id', None):
                key = i['id'][len('ec_event_'):]
                formdata[key]=i['value']
        for i in soup.find_all('select'):
            selected=i.find('option')
            if selected and selected.get('value'):
                formdata[i['name']]=selected['value']
        formdata.update(self.Testdata.workshop_notime)
        client.post(path,formdata)
        search='Event "%(ec_name)s" registered' % formdata
        self.assertIn(search, client.text, "Workshop with no time was not created")

    def _08_register4Event(self):
        """Musdama registers for event"""
        # Musdama looks for events and finds Willmas offer
        with suppress_stdout():
            client = self._get_client(self.Testdata.app_url, self.Testdata.musdama)
        path = "convention/events/%s" % self.Testdata.thecon['ec_short_name']
        client.get(path)
        # find link to event
        soup=BeautifulSoup(client.text,'html.parser')
        link = soup.find(string='View').find_parent('a')['href']
        path = link[len('/'):]
        client.get(path)
        # find link to attend
        soup=BeautifulSoup(client.text,'html.parser')
        link = soup.find('a', string='attend')['href']
        path = link[len('/'):]
        client.get(path)
        search='Musdama Testen was registered for event &quot;%s&quot;' % self.Testdata.event_sprungwelt['ec_name']
        self.assertIn(search, client.text, "Musdama not registered for event")
    def _09_createPDF(self):
        """Olga creates the PDF for the events"""
        with suppress_stdout():
            client = self._get_client(self.Testdata.app_url, self.Testdata.olga)
        from gluon.contrib import webclient
        # Olga create empty event pages
        path = "convention/data/%s/empty_event" % self.Testdata.thecon['ec_short_name']
        client.get(path)
        soup=BeautifulSoup(client.text,'html.parser')
        formdata=dict(_formname='choiceform',attendee_count=5,export_format='pdf')
        for i in soup.find_all('input', class_="boolean"):
            if i.get('id', None):
                key = i['id'][len('no_table_'):]
                formdata[key]=i['value']
        client.post(path,formdata)
        # Test if the page returns a PDF
        self.assertEqual('application/pdf',client.headers['content-type'],"No PDF was returned")
        # Olga create event pages for the current events
        path = "convention/data/%s/event?checked=1" % self.Testdata.thecon['ec_short_name']
        client.get(path)
        soup=BeautifulSoup(client.text,'html.parser')
        formdata=dict(_formname='choiceform',attendee_count=5,export_format='pdf')
        for i in soup.find_all('input', class_="boolean"):
            if i.get('id', None):
                key = i['id'][len('no_table_'):]
                formdata[key]=i['value']
        client.post(path,formdata)
        # Test if the page returns a PDF
        self.assertEqual('application/pdf',client.headers['content-type'],"No PDF was returned")

    def _99_deleteConData(self):
        """Delete the data created by the scenario"""
        # Delete Convention
        # login Ruth
        client = self._get_client(self.Testdata.app_url, self.Testdata.ruth)
        path = 'default/grant_conventions?showall=1'
        client.get(path)
        # Find 'Edit' link
        soup=BeautifulSoup(client.text, 'html.parser')
        tr = soup.find(string=Testdata.thecon['ec_name']).find_parent('tr')
        link = tr.find(string='Edit').find_parent('a')['href']
        path = link[len('/'):]
        client.get(path)
        # Get formdata
        formdata = dict(_formname='_web2py_grid')
        soup = BeautifulSoup(client.text, 'html.parser')
        for i in soup.find_all('input'):
            if i.get('name', None):
                formdata[i['name']]=i['value']
        client.post(path,formdata)
        # Delete game
        path='default/games'
        client.get(path)
        soup=BeautifulSoup(client.text, 'html.parser')
        game = "%s_%s" % (self.Testdata.rpggame_idee['ec_name'], self.suffix)
        tr = soup.find(string=game).find_parent('tr')
        record_id = tr.find('input', attrs={'name':'records'})['value']
        path='default/games/delete_many?gid=%s' % record_id
        client.get(path)

    def test_con_scenario(self):
        """Complete convention scenario
        """
        self._01_requestCon()
        self._02_grantCon()
        self._03_configureCon()
        self._04_register4Con()
        self._05_grantAccess4Con()
        self._06_register4ConPartialPayment()
        self._07_registerEvent()
        self._07_registerEventNoTime        
        self._08_register4Event()
        self._09_createPDF()

    def tearDown(self):
        """Disable next line to keep testdata"""
        self._99_deleteConData()
        pass


suite = unittest.TestLoader().loadTestsFromTestCase(TestConScenario)
unittest.TextTestRunner(verbosity=2).run(suite)
