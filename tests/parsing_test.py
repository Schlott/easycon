from bs4 import BeautifulSoup
#!/usr/local/bin/python3


def _tmp_soup():
    """Reads output.html and parses"""
    with open('output.html','r') as html:            
        soup=BeautifulSoup(html, 'html.parser')

    if True: # just for indenting :-)
        formdata=dict(_formname='choiceform',attendee_count=5,export_format='pdf')
        for i in soup.find_all('input', class_="boolean"):
            if i.get('id', None):
                key = i['id'][len('no_table_'):]
                formdata[key]=i['value']
        print(formdata)

_tmp_soup()