class Testdata():
    """Contains dummy data (users, conventions, events, ...) for testing.
    Used by test-con-scenario.
    """

    app_url = 'http://127.0.0.1:8080/easycon'

    # The users.
    # Current assumption: These users exists with the following roles:

    # Musdama Nur Testen, Willma Auch Testen: normal users
    # Conorga Olga Testen: first normal user, then convention organisator
    # Conadmin Alvin Testen: first normal user, then convention admin
    # Easycon Ruth Testen: an EasyCon adminstrator

    # Scenario:
    # - Alvin requests TheCon
    # - Ruth grants TheCon
    # - Alvin configures TheCon
    # - Alvin adds Olga to the orga group
    # - Musdama looks for cons
    # - Musdama and Willma register for TheCon
    # - Olga grants access
    # - Musdama adds events
    # - Willma register to the events

    musdama = dict(first_name='Musdama',
            nick_name='Nur',
            last_name='Testen',
            email='musdama@example.com',
            password='test',
            newsletter='immediate',
            privacy_consent=True,
            )

    willma = dict(first_name='Willma',
            nick_name='Auch',
            last_name='Testen',
            email='willma@example.com',
            password='test',
            newsletter='never',
            privacy_consent=True,
            )


    olga = dict(first_name='Conorga',
            nick_name='Olga',
            last_name='Testen',
            email='olga@example.com',
            password='test',
            newsletter='immediate',
            privacy_consent=True,
            )

    alvin = dict(first_name='Conadmin',
            nick_name='Alvin',
            last_name='Testen',
            email='alvin@example.com',
            password='test',
            newsletter='immediate',
            privacy_consent=True,
            )

    ruth = dict(first_name='Easycon',
            nick_name='Ruth',
            last_name='Testen',
            email='ruth@example.com',
            password='test',
            newsletter='immediate',
            privacy_consent=True,
            )

    # The Con.
    thecon = dict(ec_name='TheCon',
            ec_short_name='TC01',
            ec_organizer='Testen e.V.',
            ec_start_date='2021-04-01',
            ec_end_date='2021-04-05',
            ec_website='http://www.example.com/TheCon',
            ec_contact_email='info@example.com',
            ec_hidden_email='internal@example.com',
            ec_request_comment='Please accept this installation!')

    thecon_notime = dict(ec_name='NoTime',
            ec_short_name='TCNT01',
            ec_organizer='Testen e.V.',
#            ec_start_date=None,
 #           ec_end_date=None,
            ec_website='http://www.example.com/TheCon',
            ec_contact_email='info@example.com',
            ec_hidden_email='internal@example.com',
            ec_request_comment='Please accept this installation!')

    # Configuration
    thecon_basic_configuration = dict(
            ec_convention_public='on',
            ec_convention_requires_registration='on',
            ec_events='on',
            ec_event_public='on',
            ec_convention_requires_services='on'
            )

    timeslot_1 = dict(   # dates and times are default
            ec_name = 'Thursday evening',
            ec_description = 'This is the first night.'
            )

    registration_config_1 = dict( # dates and times are set by test function
            ec_convention_public = 'on',
            ec_convention_requires_registration = 'on',
            ec_registration_orga = 'on',
            ec_registration_currency_symbol = '€',
            ec_registration_other = 'on',
            ec_registration_capacity = '-1',
            ec_registration_waitlist = 'on',
            ec_registration_invite_capacity = '0',
            ec_registration_show_realnames = 'on',
            ec_registration_perday = 'on',
            ec_registration_perday_arrival_days = '1',
            ec_registration_perday_arrival_factor = '0.50',
            ec_registration_perday_departure_days = '3',
            ec_registration_perday_departure_factor = '0.50',
            ec_registration_show_rules = 'on',
            ec_registration_data_person_address = 'optional',
            ec_registration_data_person_zip = 'optional',
            ec_registration_data_person_city = 'optional',
            ec_registration_data_person_country = 'required',
            ec_registration_data_person_phonehome = 'optional',
            ec_registration_data_person_phonemobil = 'optional',
            ec_registration_data_person_phonework = 'optional',
            ec_registration_data_person_birthday = 'optional',
            ec_registration_data_person_potential_gamemaster = 'optional',
            ec_registration_data_person_special_needs = 'optional',
            ec_registration_data_person_vegeterian = 'optional',
            ec_registration_data_attendance_arrival = 'required',
            ec_registration_data_attendance_departure = 'required',
            )

    event_config_1 = dict (
            ec_convention_public = "on",
            ec_events = "on",
            ec_event_public = "on",
            ec_event_show_statistics = "on",
            ec_event_type_rpggame = "on",
            ec_event_type_tabletop = "on",
            ec_event_type_boardgame = "on",
            ec_event_type_workshop = "on",
            ec_event_requires_registration = "on",
            ec_event_registration_period_stop = "",
            ec_event_registration_limit_eventorganizer = "-1",
            ec_event_registration_limit_normalattendee = "-1",
            ec_event_time_slots = "on",
            ec_event_double_registration_timeslot = "on",
            ec_event_free_time = "on",
            ec_event_free_time_outofcon = "on",
            ec_event_attendance = "on",
            ec_event_attendance_requires_registration = "on",
            ec_event_attendee_registration_period_start = "",
            ec_event_attendee_registration_period_stop = "",
            ec_event_tables = "on",
            ec_event_tables_assignment_orga = "on",
            )

    registration_avalon = dict( # other values are taken from defaults
            ec_zip = '555',
            ec_city = 'Camelot',
            ec_country = 'Avalon',
            )

    registration_helheim = dict( # other values are taken from defaults
            ec_zip = '666',
            ec_city = 'Helheim',
            ec_country = 'Underworld',
            )

    buyable_item_con_registration = dict(
            ec_name = 'Con registration',
            ec_is_heading = 'off',
            ec_is_perday = 'off',
            ec_is_preselected = 'on',
            ec_price = '42',
            ec_capacity = '-1',
            ec_order = '10',
            ec_has_waitlist = 'off',
            ec_min = '0',
            ec_from_date = '',
            ec_to_date = '',
            )

    event_sprungwelt = dict(
            ec_name = "Mission-I",
            ec_min_attendees = "2",
            ec_max_attendees = "5",
            ec_apply_online = "3",
            ec_start = "2021-04-01 18:00:00",
            ec_end = "2021-04-01 23:00:00",
            ec_prepared_chars = "on",
            ec_introduction = "on",
            ec_url = "http://shop.flyinggames.de/produkt/mission_i_und_9_karten/",
            ec_rpg_newbees = "on",
            ec_description = "Dass musst du ausprobieren!"
            )

    workshop_notime = dict(
            ec_name = "No no never",
            ec_min_attendees = "2",
            ec_max_attendees = "5",
            ec_apply_online = "3",
            ec_prepared_chars = "on",
            ec_introduction = "on",
            ec_rpg_newbees = "on",
            ec_description = "Es wird in Tränen enden."
            )

    rpggame_idee = dict(
            ec_name = "idee! Sprungwelt",
            ec_system = "kartenbasiert",
            ec_setting = "Space Opera")