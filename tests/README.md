# How to run the tests

0. Create users
(only once)
See testdata.py which users are needed

1. Start Docker
see ../docker/README

2. Log into docker container
sudo docker exec -it easycon /bin/sh

3. Change to test directory
cd applications/easycon/tests/

4. Run script in Docker container
./run-tests.sh

