FROM alpine:3.13
LABEL AUTHORS="Stefan Schlott <stefan@ploing.de>, Daniela Festi <daniela@festi.info>"

ENV WEB2PY_ROOT=/opt/web2py
ENV WEB2PY_VERSION=v2.20.4
ENV WEB2PY_PASSWORD=
ENV WEB2PY_ADMIN_SECURITY_BYPASS=
ENV UWSGI_OPTIONS=

RUN apk update && \
  apk add python3 && \
  apk add py3-pip && \
  apk add uwsgi && \
  apk add tzdata
RUN addgroup -g 1042 -S web2py && \
  adduser -u 1042 -S web2py -G web2py && \
  mkdir $WEB2PY_ROOT
RUN apk add git && \
  git clone --branch ${WEB2PY_VERSION} --recurse-submodules --depth 1 https://github.com/web2py/web2py.git $WEB2PY_ROOT && \
  rm -rf `find $WEB2PY_ROOT -name .git` && \
  mv $WEB2PY_ROOT/handlers/wsgihandler.py $WEB2PY_ROOT && \
  chown -R web2py:web2py $WEB2PY_ROOT
RUN rm -rf ${WEB2PY_ROOT}/applications/examples && \
  mkdir -p /data/uploads /data/patches $WEB2PY_ROOT/applications/easycon && \
  chown -R web2py:web2py /data $WEB2PY_ROOT/applications/easycon && \
  echo "routers = dict(BASE = dict(default_application='easycon'),)" > $WEB2PY_ROOT/routes.py
RUN apk add py3-psycopg2 && \
  pip install pytz && \
  pip install 'beautifulsoup4==4.12.3'
COPY . /opt/web2py/applications/easycon
RUN cd $WEB2PY_ROOT/applications/easycon && \
  chown -R web2py:web2py . && \
  rm -rf .git Dockerfile docker && \
  chmod -R og-w,u+w . && \
  rm -rf $WEB2PY_ROOT/applications/easycon/uploads && \
  ln -s /data/uploads $WEB2PY_ROOT/applications/easycon/uploads
RUN mkdir -p /logs/errors /logs/sessions && \
  chown -R web2py:web2py /logs && \
  rm -rf $WEB2PY_ROOT/applications/easycon/errors && \
  ln -s /logs/errors $WEB2PY_ROOT/applications/easycon/errors && \
  rm -rf $WEB2PY_ROOT/applications/easycon/sessions && \
  ln -s /logs/sessions $WEB2PY_ROOT/applications/easycon/sessions && \
  ln -sf /logs/httpserver.log $WEB2PY_ROOT/httpserver.log
COPY docker/docker-root/ /

VOLUME /data
WORKDIR $WEB2PY_ROOT
ENTRYPOINT [ "/entrypoint-production.sh" ]
CMD [ "rocket" ]
USER web2py
EXPOSE 8080 9090
