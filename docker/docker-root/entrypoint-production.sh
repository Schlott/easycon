#!/bin/sh

# Adapted from https://github.com/smithmicro/web2py

# Copyright (c) 2018, Smith Micro Software, Inc.
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

# First, some custom stuff
if [[ -n "$POSTGRES_USER" && -n "$POSTGRES_PASSWORD" && -n "$POSTGRES_DB" && -n "$POSTGRES_HOST" ]] ; then
  echo "Creating models/db.py from template"
  cp "$WEB2PY_ROOT/applications/easycon/models/db.py_template" "$WEB2PY_ROOT/applications/easycon/models/db.py"
  sed -i "s#db = DAL('postgres://.*\$#db = DAL('postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}/${POSTGRES_DB}',pool_size=1,check_reserved=['all'],migrate_enabled=False,fake_migrate_all=False)#" "$WEB2PY_ROOT/applications/easycon/models/db.py"
fi
if [[ -n "$(ls -A /data/patches)" ]]; then
  echo "Patches directory detected, copying to easycon installation"
  cp -r /data/patches/* $WEB2PY_ROOT/applications/easycon/
fi
if ! [[ -d "/data/uploads" ]]; then
  mkdir /data/uploads
fi

# users can overwrite UWSGI_OPTIONS
if [ "$UWSGI_OPTIONS" == '' ]; then
  UWSGI_OPTIONS='--master --thunder-lock --enable-threads'
fi

# Run uWSGI using the uwsgi protocol
if [ "$1" = 'uwsgi' ]; then
  # add an admin password if specified
  if [ "$WEB2PY_PASSWORD" != '' ]; then
    python -c "from gluon.main import save_password; save_password('$WEB2PY_PASSWORD',443)"
  fi
  # run uwsgi
  exec uwsgi --socket 0.0.0.0:9090 --protocol uwsgi --wsgi wsgihandler:application $UWSGI_OPTIONS
fi

# Run uWSGI using http
if [ "$1" = 'http' ]; then
  # disable administrator HTTP protection if requested
  if [ "$WEB2PY_ADMIN_SECURITY_BYPASS" = 'true' ]; then
    if [ "$WEB2PY_PASSWORD" == '' ]; then
      echo "ERROR - WEB2PY_PASSWORD not specified"
      exit 1
    fi
    echo "WARNING! - Admin Application Security over HTTP bypassed"
    python -c "from gluon.main import save_password; save_password('$WEB2PY_PASSWORD',8080)"
    sed -i "s/elif not request.is_local and not DEMO_MODE:/elif False:/" \
      $WEB2PY_ROOT/applications/admin/models/access.py
    sed -i "s/is_local=(env.remote_addr in local_hosts and client == env.remote_addr)/is_local=True/" \
      $WEB2PY_ROOT/gluon/main.py
  fi
  # run uwsgi
  exec uwsgi --http 0.0.0.0:8080 --wsgi wsgihandler:application $UWSGI_OPTIONS
fi

# Run using the builtin Rocket web server
if [ "$1" = 'rocket' ]; then
  # Use the -a switch to specify the password
  exec python3 web2py.py -a "$WEB2PY_PASSWORD" -i 0.0.0.0 -p 8080
fi

exec "$@"
