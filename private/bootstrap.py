## Bootstrap Easy-Con
# these settings are only needed if the database is empty
# please fill in meaningful data and change your password soon
# you can delete or move this file afterwards

# not clear when to perform this yet

admin_first_name='First name'
admin_last_name='Last name'
admin_nick_name='Nick name'
admin_email='your@email.com'
admin_password='2easy'

## create admin user
if not db(db.auth_user.id>0).count():
    admin_id = db.auth_user.insert(
    	first_name=admin_first_name,
    	last_name=admin_last_name,
    	nick_name=admin_nick_name,
    	email=admin_email,
        password=db.auth_user.password.requires[0](admin_password)[0]
        )
else:
	admin_id = None

## create admin group
if not auth.id_group(role='easycon_admin'):
	admingroup=auth.add_group('easycon_admin', 'Adminstrator of the EasyCon system')
	auth.add_permission(admingroup, 'edit', db.ec_event, 0)
	auth.add_permission(admingroup, 'configure', db.ec_convention, 0)

## put admin in admin group
if admin_id:
	auth.add_membership('easycon_admin', admin_id )
