# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
import pytz


@auth.requires(lambda: _logged_in_and_privacy_consent())
def index():
    response.title='Easy-Con: %s' % (T('Dashboard'))
    personas = []
    for data in db(auth.accessible_query('edit', db.ec_person, auth.user.id)).select(db.ec_person.id, db.ec_person.ec_firstname, db.ec_person.ec_lastname, db.ec_person.ec_access_token):
        p = {}
        p['id'] = data.id
        p['firstname'] = data.ec_firstname
        p['lastname'] = data.ec_lastname
        p['access_token'] = data.ec_access_token or _get_access_token(data.id, False)
        personas.append(p)
    return dict(personas=personas)

@auth.requires(lambda: _logged_in_and_privacy_consent())
def testselect(): # test the selection of events
    returnURL = URL(args=request.args, vars=request.get_vars, host=True)
    result=dict(link='http://127.0.0.1:8000/easycon/mydata/myevents/select?returnURL=%s' % returnURL)
    if request.vars.selected:
        result['event']=request.vars.selected
    else:
        result['event']='Nö.'
    return dict(result=result)

@auth.requires(lambda: _logged_in_and_privacy_consent())
def myevents(): #
    """View and search events - in all conventions.

    request.args:
    none -- Management overview
    select -- Select an event and return it to an URL

    request.vars:
    if select:
    controller -- the controller for the return URL
    function -- the function for the return URL
    args -- the request.args for the return URL
    selected -- the request.var to return the selected eventID
    """
    title = 'myevents'
    response.title='Easy-Con: %s' % MV(title)
    explanation=EX(title)

    # Show only events of current persona
    person = db.ec_person(auth.user.active_person) or _get_or_create_first_person_auth()
    query = (db.ec_event.id==db.ec_organizes_event.ec_id_event) & (db.ec_organizes_event.ec_id_person == person.id)

    # set grid column size (does not work properly)
    maxtextlengths = {'ec_event.ec_name' : 42, 'ec_event.ec_id_rpggame': 42, 'ec_event.attendees': 42, 'ec_event.description':20}

    # Create vitual field for attendees
    db.ec_event.attendees = Field.Virtual('attendees', lambda row: _get_attendees(row.ec_event.id), label=T('Attendees'))

    # set fields to be displayed in grid view
    fields = [db.ec_event.id,db.ec_event['ec_id_convention'],
        db.ec_event['ec_start'],db.ec_event['ec_name'],
        db.ec_event['ec_type'],
        db.ec_event['ec_id_rpggame'],
        db.ec_event['attendees'],
        db.ec_event['ec_description']
        ]

    # Set visibility for detailed view: if set to false, it will not occur there and will not be exported
    db.ec_event.id.readable, db.ec_event.id.writable=False, False
    db.ec_event.ec_id_timeslot.readable, db.ec_event.ec_id_timeslot.writable=False, False # we have 'nosort_slot'
    db.ec_event.ec_type.represent = lambda value, row: MV(value)

    # create link to convention for details
    links=[dict(header='', body=lambda row: A(T('Details'), _href=URL('convention','event',args=[db.ec_convention[row.ec_id_convention].ec_short_name, row.id]), _class='buttontext button'))]
    links_placement = 'right'

    # Normal behaviour: use all args for grid, export functions
    args=request.args
    csv=True

    # Check if we want to select an event. If yes: keep the args, remove the details link and change the title
    if request.args(0) == 'select':
        returnURL=URL(request.vars.controller,request.vars.function,args=request.vars.args,vars=request.vars.vars,host=True)
        links=[dict(header='', body=lambda row: A(T('Select'), _href=returnURL+'?selected=%s'%row.id, _class='buttontext button')),]
        links_placement = 'left'
        args=request.args[:1]
        title='selectevent'
        response.title='Easy-Con: %s' % MV(title)
        returnURLhtml = A(returnURL,_href=returnURL)
        explanation=XML("%s. %s" % (EX(title), T('Then you will return to %s') % returnURLhtml))
        csv=False

    form = SQLFORM.grid(
        query,
        args=args,
        fields=fields,
        field_id=db.ec_event.id,
        left=None,
        headers={},
        orderby=~db.ec_event.ec_start,
        groupby=None,
        searchable=True,
        sortable=True,
        paginate=20,
        deletable=False,
        editable=False,
        details=False,
        create=False,
        csv=csv,
        links=links,
        links_placement = links_placement,
        maxtextlengths=maxtextlengths,
        maxtextlength=20,
        advanced_search=True,
        cache_count=50,
        auto_pagination=True,
        use_cursor=False,
        )

    return dict(title=title, form=form, explanation=explanation)

def mycalendar():
    # Ensure required URL parameters are there
    if not request.vars['person'] or not request.vars['token']:
        response.status = 400
        return 'Missing parameter'
    # Lookup person
    person = db.ec_person(request.vars['person'])
    if not person:
        response.status = 404
        return 'Person not found'
    # Ensure person's token is identical to given token
    if not person.ec_access_token or request.vars['token'] != person.ec_access_token:
        response.status = 403
        return 'Access denied'
    # Generate list of events from several sources
    events = []
    tz = None
    dateformat = '%Y%m%dT%H%M%S'
    dtstamp = datetime.utcnow().strftime('%Y%m%dT%H%M%SZ')
    # Query personal events
    query_personal_event = (db.ec_personal_event.ec_id_person == person.id) & \
                           (db.ec_personal_event.ec_id_convention == db.ec_convention.id) & \
                           (db.ec_personal_event.ec_id_convention == db.ec_configuration.id) & \
                           (db.ec_personal_event.ec_start > (datetime.now() - timedelta(days=60)))
    for row in db(query_personal_event).select(db.ec_personal_event.id, db.ec_convention.ec_name,
                                               db.ec_configuration.ec_location_timezone,
                                               db.ec_personal_event.ec_name, db.ec_personal_event.ec_start,
                                               db.ec_personal_event.ec_end):
        event = {}
        tz = pytz.timezone(row.ec_configuration.ec_location_timezone if (row.ec_configuration.ec_location_timezone)
                           else db.ec_configuration.ec_location_timezone.default)
        event['id'] = "p%s@easycon" % row.ec_personal_event.id
        event['title'] = row.ec_personal_event.ec_name
        event['organizer'] = '-'
        event['location'] = row.ec_convention.ec_name
        event['tz'] = tz.zone
        event['dtstamp'] = dtstamp
        event['start'] = tz.localize(row.ec_personal_event.ec_start).strftime(dateformat) if event['start'] else None
        event['end'] = tz.localize(row.ec_personal_event.ec_end).strftime(dateformat) if event['end'] else None
        events.append(event)

    # Query attended events
    query_attends_events = (db.ec_attends_event.ec_id_person == person.id) & \
                           (db.ec_event.ec_id_convention == db.ec_convention.id) & \
                           (db.ec_event.ec_id_convention == db.ec_configuration.id) & \
                           (db.ec_attends_event.ec_id_event == db.ec_event.id) & \
                           (db.ec_event.ec_start > (datetime.now() - timedelta(days=60)))
    for row in db(query_attends_events).select(db.ec_event.id, db.ec_event.ec_name, db.ec_convention.ec_name,
                                               db.ec_configuration.ec_location_timezone,
                                               db.ec_event.ec_start, db.ec_event.ec_end):
        event = {}
        tz = pytz.timezone(row.ec_configuration.ec_location_timezone if (row.ec_configuration.ec_location_timezone)
                           else db.ec_configuration.ec_location_timezone.default)
        event['id'] = "e%s@easycon" % row.ec_event.id
        event['title'] = row.ec_event.ec_name
        event['organizer'] = '-'
        event['location'] = row.ec_convention.ec_name
        event['tz'] = tz.zone
        event['dtstamp'] = dtstamp
        event['start'] = tz.localize(row.ec_event.ec_start).strftime(dateformat) if row.ec_event.ec_start else None
        event['end'] = tz.localize(row.ec_event.ec_end).strftime(dateformat) if row.ec_event.ec_end else None
        events.append(event)

    # Query organized events
    query_organizes_events = (db.ec_organizes_event.ec_id_person == person.id) & \
                             (db.ec_event.ec_id_convention == db.ec_convention.id) & \
                             (db.ec_event.ec_id_convention == db.ec_configuration.id) & \
                             (db.ec_organizes_event.ec_id_event == db.ec_event.id) & \
                             (db.ec_event.ec_start > (datetime.now() - timedelta(days=60)))
    for row in db(query_organizes_events).select(db.ec_event.id, db.ec_event.ec_name, db.ec_convention.ec_name,
                                                 db.ec_configuration.ec_location_timezone,
                                                 db.ec_event.ec_start, db.ec_event.ec_end):
        event = {}
        tz = pytz.timezone(row.ec_configuration.ec_location_timezone if (row.ec_configuration.ec_location_timezone)
                           else db.ec_configuration.ec_location_timezone.default)
        event['id'] = "e%s@easycon" % row.ec_event.id
        event['title'] = row.ec_event.ec_name
        event['organizer'] = '-'
        event['location'] = row.ec_convention.ec_name
        event['tz'] = tz.zone
        event['dtstamp'] = dtstamp
        event['start'] = tz.localize(row.ec_event.ec_start).strftime(dateformat) if row.ec_event.ec_start else None
        event['end'] = tz.localize(row.ec_event.ec_end).strftime(dateformat) if row.ec_event.ec_end else None
        events.append(event)

    # TODO: Add info from ec_helps - but ec_service has no time information?

    # TODO: Add info from ec_buys_item - but ec_buyitem has only from/to date, no timestamp?

    # TODO: Add from ec_configuration:
    # Anmeldung zum Event - ec_registration_period_start, ec_registration_period_stop
    # Anlegen von Programmpunkten - ec_event_registration_period_start, ec_event_registration_period_stop
    # Anmeldung zu Programmpunkten - ec_event_attendee_registration_period_start, ec_event_attendee_registration_period_stop

    # response.headers['Content-Type'] = 'text/calendar'
    return dict(events=events)

@auth.requires(lambda: _logged_in_and_privacy_consent())
def reset_token():
    person = request.args(0)
    personas = db((db.auth_permission.table_name == 'ec_person') & \
       (db.auth_permission.group_id == auth.user.id) & \
       (db.auth_permission.name == 'edit') & \
       (db.auth_permission.record_id == person)) \
        .select(db.auth_permission.record_id)
    if not personas.first():
        raise HTTP(403)
    session.flash = T('Access token reset')
    _get_access_token(person, True)
    return redirect(URL('mydata', 'index'))
