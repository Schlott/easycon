# -*- coding: utf-8 -*-
### required - do no delete
def user():
    #_lower_emails()
    if auth.is_logged_in and auth.user:
        if auth.user.privacy_consent==True: # don't show it again
            db.auth_user.privacy_consent.readable, db.auth_user.privacy_consent.writable=False, False
    form=auth()
    return dict(form=form)

def download():
    filename = request.vars.filename
    if filename:
        return response.download(request,db, download_filename=filename)
    else: # try to get filename from table
        import re
        # some code copied from response.download
        name = request.args[-1]
        items = re.compile('(?P<table>.*?)\.(?P<field>.*?)\..*').match(name)
        if not items: raise HTTP(404)
        (t, f) = (items.group('table'), items.group('field'))
        field = db[t][f]
        # get filename from table
        if t=='ec_file':
            row = db(db[t][f] == name).select().first()
            if not row: raise HTTP(404)
            filename = row.ec_name
            return response.download(request, db, download_filename=filename)

    # else: normal behaviour
    return response.download(request,db)

def call(): return service()
### end requires

# public controllers
def error404():
    return dict()

def index():
    response.title='Easy-Con: %s' % (T('Welcome'))
    return dict()

def error():
    response.title='Easy-Con: %s' % (T('Error'))
    return dict()

def imprint():
    response.title='Easy-Con: %s' % (T('Imprint'))
    return dict()

def privacy():
    response.title='Easy-Con: %s' % (T('Privacy notice'))
    return dict()

def open_conventions():
    # shows a list of open conventions with registration links
    # considers config:
    #   ec_convention_public
    response.title='Easy-Con: %s' % (T('Open conventions'))
    active_conventions = db((db.ec_convention.ec_convention_state=='active')&
                     (db.ec_convention.ec_id_configuration==db.ec_configuration.id)&
                     (db.ec_configuration.ec_convention_public==True)).select(db.ec_convention.ALL,db.ec_configuration.ALL,orderby='ec_start_date')

    open_conventions = active_conventions.exclude(lambda row: _check_is_open(row.ec_convention.id))
    capacity = {}
    for c in open_conventions:
        cap = Registration.get_capacity(c.ec_convention)
        capacity[c.ec_convention.id] = T('unlimited') if cap==-1 else cap

    planned_conventions = active_conventions.exclude(lambda row: _check_is_closed(row.ec_convention.id))
    old_conventions = active_conventions
    return dict(conventions=open_conventions,planned_conventions=planned_conventions,old_conventions=old_conventions,capacity=capacity)

def help():
    response.title='Easy-Con: %s' % (T('Help'))
    return dict()

def markmin():
    title=T('Markmin markup language')
    response.title='Easy-Con: %s' % title
    return dict(title=title)


def series():
    if request.args(0):
        series = db(db.ec_series.ec_short_name==request.args(0)).select().first()
        if not series:
            _redirect_message(T('Invalid series'), URL('series'))
        else:
            title = series.ec_name
            conventions = db((db.ec_in_series.ec_id_series==series.id)&(db.ec_in_series.ec_id_convention==db.ec_convention.id)).select(db.ec_convention.ALL, orderby=[~db.ec_convention.ec_start_date])
            next = conventions.find(lambda c: c.ec_start_date >= request.now.date())
            if not next:
                next = conventions.find(lambda c: c.ec_end_date >= request.now.date())
            if next:
                next_convention = next[-1]
                _build_response_menu(next_convention)
            else:
                next_convention = None
    else:
        series = None
        next_convention = None
        conventions = None
        title = MV('series')
    if request.args(1):
        if next_convention and request.args(1) in MENU_PAGES:
            redirect(URL('convention',request.args(1), args=next_convention.ec_short_name))
        else:
            redirect(URL('series', args=series.ec_short_name))
    printview = 'display' in request.vars and request.vars.display=='print'
    layout = 'print_default.html' if printview else 'layout.html'
    all_series = db(db.ec_series.ec_series_public==True).select()
    return dict(title=title,series=series,all_series=all_series,next_convention=next_convention,conventions=conventions, layout=layout)

def news():
    title=T('News')
    response.title='Easy-Con: %s' % title
    approved_news = db((db.ec_news.ec_status == 'approved') & (db.ec_news.ec_publish_date <= request.now.date()))
    approved_news.update(ec_status = 'published')
    if auth.is_logged_in():
        news = db((db.ec_news.ec_status == 'published') & (db.ec_news.ec_publish_date <= request.now.date())).select(orderby=~db.ec_news.ec_publish_date)
    else:
        news = db((db.ec_news.ec_status == 'published') & (db.ec_news.ec_publish_date <= request.now.date()) & (db.ec_news.ec_public==True)).select(orderby=~db.ec_news.ec_publish_date)
    if len(news) > 5:
        news = news[:5]
    return dict(title=title,news=news)

def newsletter():
    import datetime
    pagesize = 10
    printview = 'display' in request.vars and request.vars.display=='print'
    layout = 'print_default.html' if printview else 'layout.html'
    approved_news = db((db.ec_news.ec_status == 'approved') & (db.ec_news.ec_publish_date <= request.now.date()))
    approved_news.update(ec_status = 'published')
    if auth.is_logged_in():
        news = db((db.ec_news.ec_status == 'published') & (db.ec_news.ec_publish_date <= request.now.date())).select(orderby=~db.ec_news.ec_publish_date)
    else:
        news = db((db.ec_news.ec_status == 'published') & (db.ec_news.ec_publish_date <= request.now.date()) & (db.ec_news.ec_public==True)).select(orderby=~db.ec_news.ec_publish_date)
    pagination = len(news) > pagesize
    nextpage = 0

    if 'page' in request.vars:
        if request.vars.page == 'last':
            currentpage = (len(news) / pagesize) + (0 if float(len(news)) % pagesize == 0 else 1)
        else:
            currentpage = int(request.vars.page)
        if not currentpage or currentpage < 1:
            _redirect_message(T('Invalid page'), URL('newsletter'))
    else:
        currentpage = 1

    if pagination:
        nextpage = currentpage + 1
        if len(news[(nextpage-1)*pagesize:nextpage*pagesize]) == 0:
            nextpage = 0
        news = news[(currentpage-1)*pagesize:currentpage*pagesize]
        if not news:
            _redirect_message(T(' are no more news'),URL('newsletter', vars=dict(page=currentpage-1)))


    title = T('Newsletter')
    response.title='Easy-Con: %s' % title
    return dict(title=title, layout=layout, news=news, pagination=pagination, nextpage=nextpage, page=currentpage)

def twitter():
    title=T('Twitter news')
    return dict(title=title)

@auth.requires(lambda: _logged_in_and_privacy_consent())
def my_conventions():
    # shows an overview on the conventions where the user is registered to
    response.title='Easy-Con: %s' % (T('My Conventions'))
    persons = _check_person_auth()
    if persons:
        response.flash = T('New person entry found with same email as this account - and connected')
    mycons = {}
    oldcons = {}
    orgacons = db(auth.accessible_query('organize', db.ec_convention)&(db.ec_convention.ec_id_configuration==db.ec_configuration.id)).select(db.ec_convention.ALL).as_dict()
    admincons =  db(auth.accessible_query('configure', db.ec_convention)).select(db.ec_convention.ALL).as_dict()
    attendcons = db((auth.accessible_query('edit', db.ec_person)) &
                    (db.ec_attendance.ec_id_convention==db.ec_convention.id) &
                    (db.ec_attendance.ec_id_person==db.ec_person.id)).select(db.ec_convention.ALL).as_dict()
    eventcons = db((auth.accessible_query('edit', db.ec_event)) & (db.ec_event.ec_id_convention==db.ec_convention.id)).select(db.ec_convention.ALL).as_dict()
    invitecons = db((db.ec_invitation.ec_email==auth.user.email) &
        ~db.ec_invitation.ec_status.belongs(['declined', 'expired']) &
        (db.ec_invitation.ec_id_convention==db.ec_convention.id)).select(db.ec_convention.ALL).as_dict()
    for mycon in orgacons, admincons, attendcons, eventcons, invitecons, []:
        mycons.update(mycon)
    return dict(mycons=mycons)


@auth.requires(lambda: _logged_in_and_privacy_consent())
def my_personae(): # create and manage personae - without any convention
    title = T('Manage persons')
    response.title='Easy-Con: %s' % title
    db.ec_person.id.readable, db.ec_person.id.writable=False, False
    db.ec_person.release = Field.Virtual('release', lambda row: A(T('release')+' ...', _href=URL('release_persona', args=[row.ec_person.id])) if session.auth_personid!=row.ec_person.id else '', label=title)
    fields = ['ec_firstname', 'ec_lastname', 'release']
    dbfields = [db.ec_person[f] for f in fields]
    form=SQLFORM.grid(auth.accessible_query('edit', db.ec_person),
        field_id = db.ec_person.id,
        args=request.args[:1],
        orderby=[db.ec_person.ec_firstname,db.ec_person.ec_lastname],
        fields=dbfields,
        oncreate=lambda form: auth.add_permission(auth.user_group(), 'edit', db.ec_person, form.vars.id),
        searchable=False, csv=False, deletable=False, create=False)
    return dict(form=form,title=title)

@auth.requires(lambda: _logged_in_and_privacy_consent())
def release_persona():
    person = db.ec_person(request.args(0))
    if not person: _redirect_message(T('Invalid person'), URL('my_personae'))
    title = T('Release person')
    response.title='Easy-Con: %s' % title
    accounts = _get_accounts(person.id)
    form = None
    message = None
    if person.ec_email==auth.user.email:
        message = CAT('%s. %s.' % (T('You cannot release this persona'), T('Your account has the same email address')),
            ' ',TAG.button(T('OK'),_type="button",_onClick = "parent.location='%s' " % URL('my_personae')))
    elif len(accounts)==1:
        message = CAT('%s. %s.' % (T('You cannot release this persona'), T('You have the only account that can manage it')),' ',TAG.button(T('OK'),_type="button",_onClick = "parent.location='%s' " % URL('my_personae')))
    else:
        form = FORM(T('Do you really want to release %(ec_firstname)s %(ec_lastname)s? You will not be able to manage that persona with this account anymore') % person.as_dict()+'. ',INPUT(_type='submit', _value=T('release')))
        form.add_button(T('Cancel'), URL('my_personae'))
    if form and form.process().accepted:
        auth.del_permission(auth.user, 'edit', 'ec_person', person.id)
        _redirect_message(T('%(ec_firstname)s %(ec_lastname)s released') % person.as_dict(), URL('my_personae'))
    return dict(title=title,form=form,message=message)

@auth.requires(lambda: _logged_in_and_privacy_consent())
def request_installation():
    response.title='Easy-Con: %s' % (T('Request installation'))
    requestform = SQLFORM(db.ec_convention,
                   fields = ['ec_name', 'ec_short_name', 'ec_organizer', 'ec_start_date',
                             'ec_end_date','ec_website', 'ec_contact_email', 'ec_hidden_email',
                             'ec_request_comment'],
                   comments=True, ignore_rw=True, submit_button = T('Request'))
    if requestform.process(formname='requestform', keepvalues=True, onvalidation=_validate_convention).accepted:
        orgagroup = auth.add_group('con_orga_%s' % requestform.vars.id, 'Organizers of convention %s' % requestform.vars.id)
        admingroup = auth.add_group('con_admin_%s' % requestform.vars.id, 'Administrators of convention %s' % requestform.vars.id)
        auth.add_permission(orgagroup, 'organize', db.ec_convention, requestform.vars.id)
        auth.add_permission(admingroup, 'configure', db.ec_convention, requestform.vars.id)
        auth.add_membership(orgagroup)
        auth.add_membership(admingroup)
        admin = _get_or_create_first_person_auth()
        db.ec_systemevent.insert(ec_when=request.now, ec_what='convention requested', ec_type='request_conventions', ec_id_convention=requestform.vars.id, ec_id_person=admin.id,  ec_id_auth_user=auth.user.id)
        _send_con_request_notification(db.ec_convention(requestform.vars.id))
        session.flash = T('Convention requested')
        redirect(URL('my_conventions'))
    return dict(requestform=requestform)


@auth.requires(lambda: _logged_in_and_privacy_consent() and auth.has_permission('edit', 'ec_person', request.args(0)))
def person_data():
    response.title='Easy-Con: %s' % (T('My data'))
    person=db.ec_person[request.args(0)]
    action='edit' if request.args(1) and request.args(1)=='edit' else 'view'
    db.ec_person.id.readable,db.ec_person.id.writable=False,False
    form=SQLFORM(db.ec_person,person.id,readonly=(action=='view'))
    if form.process().accepted:
        session.flash = T('Personal data changed')
        redirect(URL('person_data',args=person.id))
    return dict(form=form,action=action,person=person)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _is_conadmin())
def manage_series():
    if 'cid' in request.vars:
        convention = db.ec_convention(request.vars.cid)
    else:
        convention = None
    title = '%s %s %s' % (MV('manage_series'), T('for'), convention.ec_name) if convention else MV('manage_series')
    response.title='Easy-Con: %s' % title
    printview = 'display' in request.vars and request.vars.display=='print'
    layout = 'print.html' if printview else 'layout.html'
    query = auth.accessible_query('configure', db.ec_series)
    db.ec_series.v_in_series = Field.Virtual('v_in_series', lambda row: MV(db((db.ec_in_series.ec_id_series==row.ec_series.id) & (db.ec_in_series.ec_id_convention==convention.id)).count() > 0), label=T('Part of?'))
    fields = [db.ec_series.ec_short_name,db.ec_series.ec_name, db.ec_series.ec_series_public]
    db.ec_series.ec_series_public.label = T('Public?')
    db.ec_series.ec_short_name.readable = False
    selectable = None
    if convention:
        fields.append(db.ec_series.v_in_series)
        if not printview:
            selectable = [(T('Change participation of %s for selected convention series') % convention.ec_name,
                   lambda series_ids : redirect(URL('manage_series',vars=dict(add=series_ids,cid=convention.id)))),]
    fields.append(db.ec_series.v_conventions)
    links = [dict(header='', body=lambda row: CAT(A(T('Homepage'), _href=URL('series', args=row.ec_short_name), _class='buttontext button'), ' ',
                                                  A(T('Manage Admin group'), _href=URL('promote',vars=dict(series=row.id)), _class='buttontext button'), ' ',
                                                  A(T('Delete'), _href=URL('delete_series', args=row.id), _class='buttontext button'))),
             ] if not printview else None

    # create selection form for conventions (if  are series for this user)
    conventions = db(auth.accessible_query('configure', db.ec_convention)).select()
    series = db(query).select()
    convention_selection_form = _select_convention_form(conventions, cid = convention.id if convention else None) if series else None
    if convention_selection_form and convention_selection_form.process().accepted:
        redirect(URL('manage_series', vars=dict(cid=convention_selection_form.vars.conventionid)))

    # process
    if 'add' in request.vars:
        ids = request.vars.add
        if ids and not isinstance(ids, list): ids = [ids,]
        if not convention: _redirect_message(T('Invalid convention'), URL('manage_series'))
        admingroup = auth.id_group('con_admin_%s' % convention.id)
        additions=[]
        removals=[]
        for sid in ids:
            series = db.ec_series(sid)
            if not series: _redirect_message(T('Invalid convention series'), URL('manage_series', vars=dict(cid=convention.id)))
            participation = db((db.ec_in_series.ec_id_series==series.id)&(db.ec_in_series.ec_id_convention==convention.id)).select()
            if participation:
                db((db.ec_in_series.ec_id_series==series.id)&(db.ec_in_series.ec_id_convention==convention.id)).delete()
                removals.append(series.ec_name)
                auth.del_permission(admingroup, 'configure', 'ec_series', series.id)
            else:
                db.ec_in_series.insert(ec_id_series=series.id, ec_id_convention=convention.id)
                additions.append(series.ec_name)
                auth.add_permission(admingroup, 'configure', 'ec_series', series.id)
        result = ''
        if additions:
            result += '%s: %s. ' % (T('%s has been added to the following convention series') % convention.ec_name, ', '.join(additions))
        if removals:
            result += '%s: %s. ' % (T('%s has been removed from the following convention series') % convention.ec_name, ', '.join(removals))
        _redirect_message(result, URL('manage_series', vars=dict(cid=convention.id) if convention else None))

    db.ec_series.id.readable, db.ec_series.id.writable = False, False
    form = SQLFORM.grid(query,
                        fields = fields,
                        args=request.args[:1],
                        orderby=[db.ec_series.ec_name],
                        searchable=False,
                        details=not printview,
                        create=not printview,
                        editable=not printview,
                        deletable=False,
                        oncreate=_create_series,
                        maxtextlengths = {'db.ec_series.v_in_series': 150},
                        links = links,
                        selectable=selectable,
                        csv = None,
        )
    return dict(title=title,layout=layout,conventions=conventions,form=form,convention_selection_form=convention_selection_form)

@auth.requires(lambda: _logged_in_and_privacy_consent() and auth.has_permission('configure', 'ec_series', request.args(0)))
def delete_series():
    series = db.ec_series(request.args(0))
    if not series: _redirect_message(T('Invalid convention series'), URL('manage_series'))
    title = T('Delete %s?') % series.ec_name
    conventions = db((db.ec_in_series.ec_id_series==series.id)&(db.ec_in_series.ec_id_convention==db.ec_convention.id)).select(db.ec_convention.ALL)
    if 'yes' in request.vars or not conventions:
        db(db.ec_series.id==series.id).delete()
        db((db.auth_permission.table_name=='ec_series') & (db.auth_permission.record_id==series.id)).delete()
        admingroup = auth.id_group('conseries_admin_%s' % series.id)
        auth.del_group(admingroup)
        _redirect_message(T('%s deleted') % series.ec_name,  URL('manage_series'))
    else:
        return dict(title=title, conventions=conventions)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _is_conorga())
def manage_news():
    title = MV('manage_news')
    response.title='Easy-Con: %s' % title
    printview = 'display' in request.vars and request.vars.display=='print'
    adminview = 'display' in request.vars and request.vars.display=='admin' and auth.has_membership('easycon_admin')
    if 'action' in request.vars:
        news = db.ec_news(request.args(0))
        if not news:
            _redirect_message(T('Invalid news'), URL('manage_news'))
        if request.vars.action=='request':
            if not news.ec_earliest_publish_date:
                _redirect_message(T('Please enter a publishing date'), URL('manage_news'))
            news.update_record(ec_status='requested')
            news.update_record(ec_request_date=request.now)
            _send_news_request_notification(news)
            db.ec_systemevent.insert(ec_when=request.now, ec_what='News Entry %s (%s) requested' % (news.id, news.ec_subject), ec_type='newsletter_requested', ec_id_convention=news.ec_id_convention, ec_id_person=_get_or_create_first_person_auth(), ec_id_auth_user=auth.user.id)
            session.flash = T('News entry requested, email to Easy-Con was sent')
        if request.vars.action=='back2draft':
            news.update_record(ec_status='draft')
            news.update_record(ec_request_date=None)
            db.ec_systemevent.insert(ec_when=request.now, ec_what='News Entry %s (%s) de-requested' % (news.id, news.ec_subject), ec_type='newsletter_derequested', ec_id_convention=news.ec_id_convention, ec_id_person=_get_or_create_first_person_auth(), ec_id_auth_user=auth.user.id)
        if request.vars.action=='grant':
            if not auth.has_membership('easycon_admin'):
                _redirect_message(T('You are not allowed'), URL('manage_news'))
            news.update_record(ec_status='approved')
            news.update_record(ec_approve_date=request.now)
            news.update_record(ec_publish_date=max(news.ec_earliest_publish_date, request.now.date()))
            _send_news_grant_mail(news)
            db.ec_systemevent.insert(ec_when=request.now, ec_what='News Entry %s (%s) granted' % (news.id, news.ec_subject), ec_type='newsletter_granted', ec_id_convention=news.ec_id_convention, ec_id_person=_get_or_create_first_person_auth(), ec_id_auth_user=auth.user.id)
            if news.ec_publish_date == request.now.date():
                users = db(db.auth_user.newsletter=='immediate').select()
                if users:
                    result = _send_single_news_email(news, users, format='structure')
                    successes = len([(r,s) for (r,s) in result if r])
                    news.update_record(ec_email_count=successes)
                    db.ec_systemevent.insert(ec_when=request.now, ec_what='News Entry %s (%s) immediately sent to %s users' % (news.id, news.ec_subject, successes), ec_type='newsletter_immediate', ec_id_convention=news.ec_id_convention, ec_id_person=_get_or_create_first_person_auth(), ec_id_auth_user=auth.user.id)
            db.commit()
        if request.vars.action=='decline':
            if not auth.has_membership('easycon_admin'):
                _redirect_message(T('You are not allowed'), URL('manage_news'))
            if not news.ec_comment:
                _redirect_message(T('Please enter a comment in the news why you decline this news entry'),
                    URL('manage_news', vars=dict(display='admin'), user_signature=True))
            news.update_record(ec_status='declined')
            _send_news_decline_mail(news)
            db.ec_systemevent.insert(ec_when=request.now, ec_what='News Entry %s (%s) declined' % (news.id, news.ec_subject), ec_type='newsletter_declined', ec_id_convention=news.ec_id_convention, ec_id_person=_get_or_create_first_person_auth(), ec_id_auth_user=auth.user.id)
        if request.vars.action=='test_email':
            session.flash = _send_single_news_email(news, [auth.user,])
        if request.vars.action in ['daily_email','weekly_email'] and auth.has_membership('easycon_admin'):
            what = request.vars.action[:request.vars.action.find('_')]
            users = db(db.auth_user.newsletter==what).select()
            if users:
                result = _send_single_news_email(news, users, format='structure')
                session.flash = '; '.join([s for (r,s) in result])
                admin = _get_or_create_first_person_auth()
                db.ec_systemevent.insert(ec_when=request.now, ec_what='News Entry %s (%s) manually send to %s %s users' % (news.id, news.ec_subject, len(result), what), ec_type='newsletter_%s' % what, ec_id_convention=news.ec_id_convention, ec_id_person=admin.id, ec_id_auth_user=auth.user.id)
                successes = len([(r,s) for (r,s) in result if r])
                news.update_record(ec_email_count=news.ec_email_count+successes)
            else:
                session.flash = T('Nothing to do')
        #    _redirect_message(T('Invalid action'), URL('manage_news'))
        redirect(URL('manage_news'))
    if 'cid' in request.vars:
        convention = db.ec_convention(request.vars.cid)
        if convention:
            db.ec_news.ec_id_convention.default = convention.id
    db.ec_news.id.readable, db.ec_news.id.writable=False, False
    if auth.has_membership('easycon_admin') and adminview:
        db.ec_news.ec_status.writable = True
        db.ec_news.ec_comment.writable = True
        db.ec_news.ec_publish_date.writable = True
    else:
        db.ec_news.ec_earliest_publish_date.requires = IS_EMPTY_OR(IS_DATE_IN_RANGE(format=T('%Y-%m-%d'),
                                                           minimum=request.now.date(),
                                                           error_message='Must be in the future!'))
    if 'new' in request.args:
        for field in ['ec_request_date', 'ec_email_count', 'ec_comment', 'ec_created_on', 'ec_status', 'ec_publish_date']:
            db.ec_news[field].readable = False

    db.ec_news.ec_created_by.default = auth.user.id
    db.ec_news.ec_id_convention.requires=IS_EMPTY_OR(IS_IN_DB(db(auth.accessible_query('organize', db.ec_convention)), 'ec_convention.id', '%(ec_name)s'))
    requestlink = lambda row: A(T('Request'), _href=URL('manage_news',args=[row.id], vars=dict(action='request'), user_signature=True), _class='button')
    requestagainlink = lambda row: A(T('Request again'), _href=URL('manage_news',args=[row.id], vars=dict(action='request'), user_signature=True), _class='button')
    back2draftlink = lambda row: A(T('Back to draft'),_href=URL('manage_news',args=[row.id], vars=dict(action='back2draft'), user_signature=True), _class='button')
    grantlink = lambda row: A(T('grant'),_href=URL('manage_news',args=[row.id], vars=dict(action='grant'), user_signature=True), _class='button')
    declinelink = lambda row: A(T('reject'),_href=URL('manage_news',args=[row.id], vars=dict(action='decline'), user_signature=True), _class='button')
    testemaillink = lambda row: A(SPAN(IMG(_src=URL('static','images/email.png'), _width="20px"), SPAN(T('Send news email to yourself'), _class='popup'), _class='haspopup'),_href=URL('manage_news',args=[row.id], vars=dict(action='test_email'), user_signature=True))
    dailylink = lambda row: A(SPAN(IMG(_src=URL('static','images/email.png'), _width="20px"), SPAN('_d'), SPAN(T('Send news email to daily users'), _class='popup'), _class='haspopup'),_href=URL('manage_news',args=[row.id], vars=dict(action='daily_email'), user_signature=True))
    weeklylink = lambda row: A(SPAN(IMG(_src=URL('static','images/email.png'), _width="20px"), SPAN('_w'), SPAN(T('Send news email to weekly users'), _class='popup'), _class='haspopup'),_href=URL('manage_news',args=[row.id], vars=dict(action='weekly_email'), user_signature=True))
    infolink = lambda row: A(SPAN(IMG(_src=URL('static','images/info.png'), _width="20px"), SPAN(XML(_get_systemevent_text(ec_type='newsletter', id=row.id)), _class='popup'), _class='haspopup'))

    links=[dict(header=T('Action'), body=lambda row: CAT(requestlink(row) if row.ec_status == 'draft' else back2draftlink(row) if row.ec_status == 'requested' else '', ' ',
                                                         requestagainlink(row) if row.ec_status == 'declined' else '', ' ',
                                                         grantlink(row) if row.ec_status=='requested' and auth.has_membership('easycon_admin') else '', ' ',
                                                         declinelink(row) if row.ec_status=='requested' and auth.has_membership('easycon_admin') else '', ' ',
                                                         testemaillink(row), ' ',
                                                         infolink(row), ' ',
                                                         dailylink(row) if auth.has_membership('easycon_admin') else '', ' ',
                                                         weeklylink(row) if auth.has_membership('easycon_admin') else '')
                )] if not printview else []
    query = auth.accessible_query('edit', db.ec_news)
    news = db(query).select()
    fields = [
        'ec_subject',
        'ec_status',
        'ec_public',
        'ec_created_by',
        'ec_id_convention',
        'ec_created_on',
        'ec_request_date',
        'ec_earliest_publish_date',
        'ec_publish_date',
        'ec_email_count',
             ]
    dbfields=[getattr(db.ec_news,field) for field in fields]
    headers = {}
    for f in ['ec_request_date','ec_publish_date','ec_public']:
        headers['ec_news.%s' % f] = short_label(f)

    approved_news = db((db.ec_news.ec_status == 'approved') & (db.ec_news.ec_publish_date <= request.now.date()))
    approved_news.update(ec_status = 'published')

    result=SQLFORM.grid(query,
        fields=dbfields,
        args=request.args[:1],
        orderby=[db.ec_news.ec_status, db.ec_news.ec_publish_date, db.ec_news.ec_subject],
        links=links,
        maxtextlengths = {'db.ec_news.ec_subject': 200},
        csv=False,
        paginate=80,
        headers=headers,
        deletable= lambda row: not printview and (adminview or row.ec_status in ['draft', 'requested', 'declined']),
        editable = lambda row: not printview and (adminview or row.ec_status in ['draft', 'requested', 'declined']),
        oncreate=_create_news,
        onupdate=_update_news,
        ondelete = lambda table, record_id: db((db.auth_permission.table_name==table) & (db.auth_permission.record_id==record_id)).delete(),
        searchable = not printview, create = not printview, details=not printview,
        )
    layout = 'print_default.html' if printview else 'layout.html'
    return dict(title=title, result=result, layout=layout, news=news, adminview=adminview)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _is_conadmin())
def promote():
    series=db.ec_series(request.vars.series) if 'series' in request.vars else None
    if not series: _redirect_message(T('Invalid series'), URL('index'))
    group = 'admin'
    title=T('Manage %s group for series %s') % (group.capitalize(), series.ec_name)
    response.title='Easy-Con %s %s: %s' % (T('for'), series.ec_name, title)
    confirm_form = ''
    confirm_message = ''
    dbgroup = db(db.auth_group.role=='conseries_%s_%s' % (group, series.id)).select().first()
    if request.vars.action=='promote':
        user = db(db.auth_user.email.lower()==request.vars.email.lower()).select().first()
        if user:
            auth.add_membership(dbgroup, user.id)
            _redirect_message(T('User %(first_name)s %(last_name)s promoted to group') % user.as_dict(), URL('promote', vars=dict(what=group,series=series.id)))
        else:
            _redirect_message(T('No user with this e-mail registered'), URL('convention', 'promote', vars=dict(series=series.id,what=group)))
    elif request.vars.action=='depromote':
        user = db.auth_user(request.vars.user)
        if user:
            if user.id == auth.user.id:
                confirm_message = T('Do you really want to remove yourself from %s group?' % group)
                confirm_form = FORM.confirm(T('Yes'), {T('No') : URL('promote', vars=dict(series=series.id,what=group))})
                if confirm_form.accepted:
                    auth.del_membership(dbgroup, user.id)
                    message=T('You removed yourself from %s group' % group)
                    if group=='admin':
                        _redirect_message(message, URL('series', args=series.ec_short_name))
                    else:
                        _redirect_message(message, URL('promote', vars=dict(series=series.id, what=group)))
            else:
                auth.del_membership(dbgroup, user.id)
                _redirect_message(T('User %(first_name)s %(last_name)s was removed from group') % user.as_dict(), URL('promote', vars=dict(series=series.id,what=group)))
        else:
            response.flash=T('Invalid user')
    # elif request.vars.action=='email':
        # user = db.auth_user(request.vars.user)
        # if user:
            # _redirect_message('Send e-mail to %(first_name)s %(last_name)s' % user.as_dict(), URL('convention', 'emails', args=convention.ec_short_name, vars=dict(emailtos=user.email,autoemail='promote_%s_group' % group)))
        # else:
            # response.flash=T('Invalid user')
    query = (db.auth_user.id==db.auth_membership.user_id)&(db.auth_membership.group_id==db.auth_group.id)&(db.auth_group.role=='conseries_%s_%s'%(group,series.id))
    if len(db(query).select())>1:
        links=[dict(header=T('Action'),\
                    body=lambda row: CAT(A(T('Delete from group'), _class=('button'),
                                         _href=URL('promote', vars=dict(what=group, action='depromote', user=row.id, series=series.id), user_signature=True)),
                                         #' ',
                                         #A(T('Send e-mail'), _class=('button'),
                                         #_href=URL('promote',args=convention.ec_short_name, vars=dict(what=group, action='email', user=row.id), user_signature=True)))
                                        ))]
    else: links=None
    searchform = FORM(T('E-Mail of user to be promoted')+':  ', INPUT(_name='email', requires=IS_EMAIL()), INPUT(_type='submit', _value=T('Add to group')))
    if db.auth_user.nick_name.label.endswith('*'):
        db.auth_user.nick_name.label = db.auth_user.nick_name.label[:-1]
    groupform = SQLFORM.grid(query,
        fields=[db.auth_user.nick_name, db.auth_user.first_name, db.auth_user.last_name, db.auth_user.email],
        deletable=False, details=False, create=False, searchable=False, csv=False, editable=False,
        links=links,
        args=request.args[:1])
    if searchform.process().accepted:
        redirect(URL('promote', vars=dict(action='promote', email=request.vars.email, what=group, series=series.id), user_signature=True))
    return dict(title=title, confirm_message=confirm_message, confirm_form=confirm_form, searchform=searchform, groupform=groupform, group=group)

@auth.requires(lambda: _logged_in_and_privacy_consent() and auth.has_membership('easycon_admin'))
def grant_conventions():
    response.title='Easy-Con: %s' % (T('Grant installation'))
    fields = [db.ec_convention.ec_request_date,
                                db.ec_convention.ec_name,
                                db.ec_convention.ec_convention_state,
                                db.ec_convention.ec_start_date,
                                db.ec_convention.ec_end_date,
                                db.ec_convention.ec_organizer,
                                db.ec_convention.ec_request_comment,
                        ]
    if 'showall' in request.vars:
        query = db.ec_convention
    else:
        db.ec_convention.ec_convention_state.readable = False
        query = db.ec_convention.ec_convention_state=='requested'
    deletable = True if 'edit' in request.args else False
    if 'grant' in request.args and db.ec_convention[request.args[0]]:
        conid = request.args[0]
        db.ec_convention[conid]=dict(ec_convention_state='to_be_configured')
        _send_grant_mail(db.ec_convention[conid])
        admin = _get_or_create_first_person_auth()
        db.ec_systemevent.insert(ec_when=request.now, ec_what='convention granted', ec_type='grant_conventions', ec_id_convention=conid, ec_id_person=admin.id, ec_id_auth_user=auth.user.id)
        response.flash = T('Convention %s granted') % db.ec_convention[conid]['ec_name']
    db.ec_convention.ec_request_comment.readable=True
    form = SQLFORM.grid(query,
                        fields=fields,
                        orderby=[db.ec_convention.ec_convention_state, db.ec_convention.ec_start_date],
                        searchable=False,
                        create=False,
                        args=request.args[:1],
                        deletable=deletable,
                        links=[dict(header='',
                                body=lambda row: A(T('Grant'), _class=('button'),
                                    _href=URL('default','grant_conventions',args=[row.id,'grant'],user_signature=True))
                                    if row.ec_convention_state=='requested' else ''),],
                        links_placement = 'left'
                        )
    return dict(form=form)

@auth.requires(lambda: _logged_in_and_privacy_consent() and auth.has_membership('easycon_admin'))
def schedule_system_tasks(): # start and stop managed tasks
    title = T('Schedule System Tasks')
    response.title='Easy-Con: %s' % title
    system_tasks = VALID_SYSTEM_TASKS.copy()
    run_tasks = VALID_SYSTEM_TASKS.copy()
    for task in db(db.scheduler_task).select():
        if task.task_name in system_tasks:
            del system_tasks[task.task_name]
    if 'start' in request.vars:
        if request.vars.start in system_tasks:
            result = scheduler.queue_task(system_tasks[request.vars.start], period = 24*3600, repeats = 0)
            redirect(URL('schedule_system_tasks'))
    if 'stop' in request.vars:
        task = db.scheduler_task(request.vars.stop)
        if task:
            task.update_record(task_name='%s_old' % task.task_name)
            scheduler.stop_task(task.uuid)
            redirect(URL('schedule_system_tasks'))
    if 'run' in request.vars:
        taskname = request.vars.run
        if taskname in run_tasks:
            result = eval('%s()' % taskname)
            _redirect_message(result,URL('schedule_system_tasks'))

    fields = ['id', 'task_name', 'status', 'function_name', 'start_time', 'uuid', 'next_run_time', 'stop_time', 'repeats', 'retry_failed', 'period', 'times_run', 'times_failed']
    db.scheduler_task.id.readable = False
    dbfields = [db.scheduler_task[field] for field in fields]
    form = SQLFORM.grid(db.scheduler_task,
                        fields = dbfields,
                        create=False,
                        csv = None,
                        links=[dict(header='',
                                body=lambda row: A(T('Stop'), _class=('button'),
                                    _href=URL('schedule_system_tasks',vars=dict(stop=row.id),user_signature=True))),],
                        links_placement = 'left',
                        orderby = ~db.scheduler_task.next_run_time
                        )
    return dict(title=title, system_tasks=system_tasks, form=form, run_tasks=run_tasks)

@auth.requires(lambda: _logged_in_and_privacy_consent() and auth.has_membership('easycon_admin'))
def games():
    title = MV('games')
    response.title='Easy-Con: %s' % title
    printview = 'display' in request.vars and request.vars.display=='print'

    firstgame = db.ec_rpggame.with_alias('firstgame')
    duplicates = db((db.ec_rpggame.ec_name==firstgame.ec_name)&\
                    (db.ec_rpggame.ec_system==firstgame.ec_system)&\
                    (db.ec_rpggame.ec_setting==firstgame.ec_setting)&\
                    (db.ec_rpggame.id > firstgame.id)).select(db.ec_rpggame.ALL, firstgame.ALL, orderby=firstgame.id)
    merges = []

    for d in duplicates:
        game = '%(ec_name)s (%(ec_system)s): %(ec_setting)s' % d.ec_rpggame
        if game not in merges:
            merges.append(game)


    if 'cleanup' in request.args:
        for d in duplicates:
            db(db.ec_event.ec_id_rpggame == d.ec_rpggame.id).update(ec_id_rpggame = d.firstgame.id)
            if d.ec_rpggame.event_count==0:
                db(db.ec_rpggame.id == d.ec_rpggame.id).delete()
        if merges:
            _redirect_message(T('Merged %s') % ', '.join(merges), URL('games'))
        else:
            _redirect_message(T('Merged nothing'), URL('games'))

    if 'delete_many' in request.args:
        message = ''
        count = 0
        ids = request.vars.gid
        if ids and not isinstance(ids, list): ids = [ids,]
        for gid in ids:
            game = db.ec_rpggame(gid)
            if game.event_count != 0:
                message = T('You cannot delete games still in usage by an event') + '.'
            else:
                count += 1
                db(db.ec_rpggame.id==gid).delete()
        _redirect_message(message + T('%s entries deleted') % count, URL('games'))

    db.ec_rpggame.id.writable = False
    fields = [
            'id',
            'ec_name',
            'ec_system',
            'ec_setting',
            'event_count',
             ]
    links = []
    dbfields=[getattr(db.ec_rpggame,field) for field in fields]
    form=SQLFORM.grid(db.ec_rpggame,
        fields=dbfields,
        args=request.args[:1],
        orderby=[db.ec_rpggame.ec_name,db.ec_rpggame.ec_system,db.ec_rpggame.ec_setting,],
        links=links,
        maxtextlength = 42,
        csv=False,
        paginate=80,
        selectable=[(T('Delete selected games'), lambda ids : redirect(URL('games', args='delete_many', vars=dict(gid=ids)))),] if not printview else [],
        searchable = not printview, create = not printview, deletable = False, editable=not printview, details=not printview,
        )
    layout = 'print_default.html' if printview else 'layout.html'
    return dict(title=title,form=form,layout=layout,duplicates=merges)

@auth.requires(lambda: _logged_in_and_privacy_consent() and auth.has_membership('easycon_admin'))
def repair(): # repair empty nicknames
    users = db(db.auth_user).select(db.auth_user.id,db.auth_user.first_name,db.auth_user.nick_name,db.auth_user.email)
    for user in users:
        if not user.nick_name:
            person = db(auth.accessible_query('is_me','ec_person',user.id)).select(db.ec_person.ALL).first()
            if person:
                user.update_record(nick_name=person.ec_nickname)
            else:
                user.update_record(nick_name = user.first_name)
    return dict(users=users)

@auth.requires(lambda: _logged_in_and_privacy_consent() and auth.has_membership('easycon_admin'))
def merge_person(): # merge person entries in data base
    title = T('Merge persons')
    response.title='Easy-Con: %s' % title
    labels = {}
    no_labels = {}
    for field in db.ec_person.fields:
        labels[field] = short_label(field) if short_label(field) != field else db.ec_person[field].label
        no_labels[field] = ''
    if 'done' in request.vars:
        done_person = db.ec_person(request.vars.done)
        done = SQLFORM(db.ec_person, db.ec_person(done_person.id), labels=labels, comments=False, readonly=True, ignore_rw=True)
        return dict(title=title,keep=None,merge=None,done=done)
    if 'keep' in request.vars and 'merge' in request.vars:
        keep_person = db.ec_person(request.vars.keep)
        merge_person = db.ec_person(request.vars.merge)
    if not (keep_person and merge_person):
        response.flash = T('Error when selecting the two persons to merge')
        return dict(title=title, keep=None, merge=None, done=None)
    # check attendance of conventions and events
    keep_cons = _get_convention_resultset_for_person(keep_person.id)
    merge_cons = _get_convention_resultset_for_person(merge_person.id)
    if set([row.id for row in keep_cons]) & set([row.id for row in merge_cons]):
        response.flash = T('Error: these two persons attend the same convention and cannot be merged')
        return dict(title=title, keep=None, merge=None, done=None)

    if 'field' in request.vars and request.vars.field in db.ec_person.fields:
        keep_person[request.vars.field] = merge_person[request.vars.field]
        keep_person.update_record()
    else:
        value = False
    keep = SQLFORM(db.ec_person, db.ec_person(keep_person.id), labels=labels, comments=False, ignore_rw=True, submit_button=T('Save and merge'))
    merge = SQLFORM(db.ec_person, db.ec_person(merge_person.id), labels=no_labels, comments=False, readonly=True, ignore_rw=True)
    if keep.process(formname='keep').accepted:
        message = _merge_person(keep_person.id, merge_person.id)
        _redirect_message(message+'.',URL('merge_person', vars=dict(done=keep_person.id)))
    return dict(title=title,keep=keep,merge=merge,done=None)

@auth.requires(lambda: _logged_in_and_privacy_consent() and auth.has_membership('easycon_admin'))
def manage_accounts(): # manage accounts
    title = T('Manage accounts')
    response.title='Easy-Con: %s' % title
    links = [dict(header='', body=lambda row: A(T('Show people'), _href=URL('manage_people', vars=dict(account=row.id)), _class='buttontext button'))]
    if 'person' in request.vars:
        person = db.ec_person[request.vars.person]
        permissions = db( (db.auth_permission.table_name == 'ec_person') & \
                    (db.auth_permission.record_id == person.id) & \
                    (db.auth_permission.name == 'edit') &
                    (db.auth_group.id == db.auth_permission.group_id))
        auth_ids = []
        for entry in permissions.select(db.auth_group.ALL):
            if entry.role[:5] == 'user_':
                auth_ids.append(int(entry.role[5:]))
        query = db.auth_user.id.belongs(auth_ids)
    else:
        person = None
        query = db.auth_user
        db.auth_user.people = Field.Virtual('people', lambda row: _get_number_of_people(row.auth_user.id))

    form = SQLFORM.grid(query,
                        create=False,
                        csv=False,
                        links=links)
    return dict(title=title, form=form, person=person)

@auth.requires(lambda: _logged_in_and_privacy_consent() and auth.has_membership('easycon_admin'))
def manage_people(): # manage people
    title = T('Manage people')
    response.title='Easy-Con: %s' % title
    links = [dict(header='', body=lambda row: A(T('Show accounts'), _href=URL('manage_accounts', vars=dict(person=row.id)), _class='buttontext button'))]
    if 'account' in request.vars:
        account = db.auth_user[request.vars.account]
        people = db((db.ec_person.id == db.auth_permission.record_id) &
                    (db.auth_permission.table_name == 'ec_person') & \
                    (db.auth_permission.name == 'edit') &
                    (db.auth_group.id == db.auth_permission.group_id) & \
                    (db.auth_group.role == 'user_%s' % account.id))
        people_ids = []
        for entry in people.select(db.ec_person.ALL):
            people_ids.append(entry.id)
        query = db.ec_person.id.belongs(people_ids)
        if len(people_ids) > 1:
            selectable = [(T('Select (only) two people to merge'), lambda people_ids : redirect(URL('merge_person',vars=dict(keep=people_ids[0], merge=people_ids[1]))))]
        else:
            selectable = None
    else:
        account = None
        selectable = None
        query = db.ec_person
    db.ec_person.cons = Field.Virtual('cons', lambda row: [CAT(XML(a),'... ') for a in _get_conventions_for_person(row.ec_person.id)])
    fields = [db.ec_person.id, db.ec_person.ec_firstname, db.ec_person.ec_lastname, db.ec_person.ec_email, db.ec_person.cons]
    form = SQLFORM.grid(query,
                        field_id = db.ec_person.id,
                        create=False,
                        csv=False,
                        fields=fields,
                        links=links,
                        selectable=selectable,
                        orderby=db.ec_person.id)

    return dict(title=title, form=form, account=account)

@auth.requires(lambda: _logged_in_and_privacy_consent() and auth.has_membership('easycon_admin'))
def admin(): # EasyCon Admin Tasks
    title = T('EasyCon Admin Tasks')
    response.title='Easy-Con: %s' % title
    return dict(title=title)
