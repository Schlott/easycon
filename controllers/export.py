def call():
    session.forget()
    return service()

@service.xml
@service.csv
@service.json

def events():
    message=''
    formtitle=''
    convention = _get_convention_conname(request.args(0))
    if not convention:
        message=T('Invalid convention')
        _redirect_message(message, URL('default', 'error404'))
    config=db.ec_configuration[convention.ec_id_configuration]
    if not config: return dict(message=T('This convention needs to be configured'))
    eventtype = request.args(1) if request.args(1) in EVENT_TYPE_CHOICES + ['all'] else 'all'
    timeslot = db.ec_timeslot(request.args(2)) if request.args(2) else None
    title = T('Events')
    if eventtype!='all':
        title+=' (%s)' % MV(eventtype)
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    db.ec_event.id.readable, db.ec_event.id.writable=False, False
    db.ec_event.ec_id_convention.readable, db.ec_event.ec_id_convention.writable = False, False
    db.ec_event.hosts = Field.Virtual('hosts', lambda row: _get_hosts(row.ec_event.id))
    fields=['ec_name',
            'hosts',
    'ec_type',]
    if config.ec_event_time_slots: fields.append('ec_id_timeslot')
    if config.ec_event_free_time:
        fields.append('ec_start')
        fields.append('ec_end')
    if config.ec_event_type_rpggame:
        fields.append('ec_id_rpggame')
    db.ec_event.ec_apply_online.readable, db.ec_event.ec_apply_online.writable=False, False # but we do not want to see it
    db.ec_event.vacancies = Field.Virtual('vacancies', lambda row: '%s/%s' % (_get_event_vacancy(row.ec_event.id, row.ec_event.ec_apply_online), row.ec_event.ec_apply_online))
    db.ec_event.ec_type.represent = lambda value, row: MV(value)
    headers = {}
    for f in fields:
        headers['ec_event.%s' % f] = short_label(f)
    headers['ec_event.vacancies'] = T('Vacancies (online)')
    if eventtype == 'rpggame':
        headers['ec_event.hosts'] = T('Game master')
    else:
        headers['ec_event.hosts']
    links=[dict(header='',
                body=lambda row: A(T('View'), _href=URL('convention','event',args=[convention.ec_short_name, row.id]), _class='buttontext button', _target="_blank"))]
    query = (db.ec_event.ec_id_convention==convention.id)
    if eventtype !='all':
        query = query&(db.ec_event.ec_type==eventtype)
    if timeslot:
        query = query & (db.ec_event.ec_id_timeslot==timeslot.id)
        formtitle = '%s %s %s' % (T('Events'),T('for'), timeslot.ec_name)
        fields.remove('ec_id_timeslot')
    if request.args(2)=='none':
        query = query & (db.ec_event.ec_id_timeslot==None)
        formtitle = T('Events without time slot')
        fields.remove('ec_id_timeslot')
    headers['ec_event.ec_name'] = T('Title')
    dbfields = [db.ec_event[f] for f in fields]
    form=SQLFORM.grid(query,
        field_id = db.ec_event.id,
        args=request.args[:3],
        orderby=[db.ec_event.ec_type],
        fields=dbfields,
        maxtextlength=300,
        user_signature=False,
        headers=headers,
        links=links,
        searchable=False, csv=False, create=False, editable=False, deletable=False, details=False)
    form.element('.web2py_counter', replace=None)
    if request.vars.display=='pdf':
        return _export_pdf(convention, db(query).select(db.ec_event.ALL))
    else:
        return dict(form=form, title=title, eventtype=eventtype, message=message, formtitle=formtitle)

def event():
    event=db.ec_event(request.args(0)) # only eventid
    if event: # first argument is eventid? --> redirect
        convention=db.ec_convention(event.ec_id_convention)
        if convention: redirect(URL('export','event',args=[convention.ec_short_name] + request.args))
        _redirect_message(T('Invalid convention'), URL('index'))
    else: # set event
        convention=_get_convention_conname(request.args(0))
        if not convention: _redirect_message(T('Invalid convention'),URL('index'))
        event=db.ec_event(request.args(1))
        if not event: _redirect_message(T('Invalid event'), URL('convention','events',args=convention.ec_short_name))
    config = db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('This convention needs to be configured'), URL('convention',args=[convention.ec_short_name]))
    # check if events can be viewed
    if not (config.ec_event_public or auth.is_logged_in()): _redirect_message(T('The events for this convention are not public (yet)'), URL('convention',args=[convention.ec_short_name]))
    # get fields
    fields = _get_event_fields(config,event.ec_type,'single')
    eventform = SQLFORM(db.ec_event, event.id, fields=fields, readonly=True,showid=False)

    if request.vars.display=='pdf':
        from gluon.contrib.pyfpdf import Template
        event_template = db((db.pdf_template.ec_id_convention==convention.id)&(db.pdf_template.ec_type=='event')).select().first()
        if not event_template:
            _redirect_message(T('No template specified'), URL('convention', 'configure_templates', extension=False, args=[convention.ec_short_name]))
        ec_elements = db(db.pdf_element.ec_id_template==event_template.id).select(orderby=db.pdf_element.ec_priority)
        elements = []
        for ec_element in ec_elements: # remove ec_ from name
            element = {}
            for ec_key in ec_element:
                if ec_key.startswith('ec_'):
                    key = ec_key[3:]
                    element[key] = ec_element[ec_key]
                else:
                    element[ec_key] = ec_element[ec_key]
            elements.append(element)
        f = Template(format=event_template.ec_format,
            elements=elements,
            title=T('Event export for %s (powered by Easy-Con)') % convention.ec_name,
            author='Easy-Con-System',)
        f.add_page()
        for field in event:
            if field in ['ec_type']:
                f[field]=MV(event[field])
            elif field in ['ec_min_attendees','ec_max_attendees']:
                f[field]='%s: %s' % (db.ec_event[field].label, event[field])
            elif field in ['ec_start', 'ec_end']:
                f[field]='%s: %s' % (db.ec_event[field].label, event[field].strftime(datetimeformatstring))
            else:
                f[field]=event[field]
        response.headers['Content-Type']='application/pdf'
        return f.render('event.pdf', dest='S')
    return dict(eventform=eventform)

