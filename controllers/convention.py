# coding: utf8
import datetime
from collections import OrderedDict
import re

## public controller functions

def index():
    # if args(0) is a valid convention, redirects to convention
    # else: selects convention and redirects to session.back
    convention=_get_convention_conname(request.args(0))
    if convention:
        redirect(URL('convention',args=request.args(0)))
    else:
        _redirect_message(T('Invalid convention'),URL('default','index'))
    return dict(message=message,form=form)

def imprint():
    convention = _get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('This convention needs to be configured'), URL('convention',args=[convention.ec_short_name]))

    response.title='Easy-Con: %s' % (T('Imprint'))
    return dict(convention=convention)

def privacy():
    convention = _get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('This convention needs to be configured'), URL('convention',args=[convention.ec_short_name]))

    response.title='Easy-Con: %s' % (T('Privacy notice'))
    return dict(convention=convention)

def convention():
    # shows basic information about a convention
    session.back=None if session.back=='convention' else session.back
    message=''
    convention=_get_convention_conname(request.args(0))
    systempage = _get_systempage(convention, 'convention_page')
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: redirect(URL(configure_main, args=convention.ec_short_name))
    response.title='Easy-Con %s %s' % (T('for'), convention.ec_name)
    #capacity = Registration.get_capacity(convention)
    free_items = Registration.get_item_counter(convention)
    #capacity = T('unlimited') if capacity==-1 else capacity
    is_attendee=False
    if auth.is_logged_in():
        person = db.ec_person(auth.user.active_person) or _get_or_create_first_person_auth()
        if person and Registration.get_attendee_status(convention.id, person.id) == 'attendee':
            is_attendee=True
    series = db((db.ec_in_series.ec_id_convention==convention.id)&(db.ec_in_series.ec_id_series==db.ec_series.id)).select(db.ec_series.ALL)
    return dict(message=message, convention=convention, config=config, free_items=free_items, is_attendee=is_attendee, systempage=systempage, series=series)

def events():
    convention = _get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('This convention needs to be configured'), URL('convention',args=[convention.ec_short_name]))
    systempage = _get_systempage(convention, 'Events')
    title = T('Events')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    try:
        pagination = int(request.vars.pagination)
    except:
        if request.vars.pagination != 'all': pagination = 40
        else: pagination = None
    filters = request.vars.filters or []
    if filters == 'rpggame':
        eventtype = 'rpggame'
    else:
        eventtype = 'all'

    if isinstance(filters,str):
        filters = [filters,]
        for f in filters:
            if f not in EVENT_FILTERS:
                _redirect_message(T('Invalid filter'), URL('convention','events',args=[convention.ec_short_name]))

    # data grooming: if events have not ec_start and ec_end but a timeslot, set values
    nostartevents = db((db.ec_event.ec_id_convention==convention.id) & (db.ec_event.ec_start == None) & \
        (db.ec_event.ec_id_timeslot == db.ec_timeslot.id) & (db.ec_timeslot.ec_start_time != None)).select(db.ec_event.ALL)
    for e in nostartevents:
        timeslot = db.ec_timeslot(e.ec_id_timeslot)
        e.update_record(ec_start = timeslot.ec_start_time)
    noendevents = db((db.ec_event.ec_id_convention==convention.id) & (db.ec_event.ec_end == None) & \
        (db.ec_event.ec_id_timeslot == db.ec_timeslot.id) & (db.ec_timeslot.ec_end_time != None)).select(db.ec_event.ALL)
    for e in noendevents:
        timeslot = db.ec_timeslot(e.ec_id_timeslot)
        e.update_record(ec_end = timeslot.ec_end_time)

    # create list of people if config.ec_registration_other for family_program
    people = []
    if config.ec_registration_other:
        people_db = db(auth.accessible_query('edit', db.ec_person) &\
                        (db.ec_attendance.ec_id_person==db.ec_person.id)&\
                        (db.ec_attendance.ec_id_convention==convention.id)&\
                        (db.ec_attendance.ec_registration_state=='attendee')).select(db.ec_person.id)
        if len(people_db) > 1:
            for p in people_db: people.append(p.id)

    # set fields for grid
    db.ec_event.id.readable, db.ec_event.id.writable=False, False
    db.ec_event.ec_id_convention.readable, db.ec_event.ec_id_convention.writable = False, False
    db.ec_event.hosts = Field.Virtual('hosts', lambda row: _get_hosts(row.ec_event.id))
    fields=_get_event_fields(config,eventtype,'list')
    fields.append('ec_apply_online') # needs to be part of grid, or cannot be used in virtual field
    db.ec_event.ec_apply_online.readable, db.ec_event.ec_apply_online.writable=False, False # but we do not want to see it
    db.ec_event.vacancies = Field.Virtual('vacancies', lambda row: '%s/%s' % (_get_event_vacancy(row.ec_event.id, row.ec_event.ec_apply_online), row.ec_event.ec_apply_online))

    db.ec_event.ec_id_timeslot.readable, db.ec_event.ec_id_timeslot.writable=False, False # we have 'nosort_slot'

    db.ec_event.ec_type.represent = lambda value, row: MV(value)
    headers = {}
    for f in fields:
        headers['ec_event.%s' % f] = short_label(f)
    headers['ec_event.vacancies'] = T('Vacancies (online)')
    headers['ec_event.hosts'] = T('Game master') if eventtype == 'rpggame' else  headers['ec_event.hosts']
    dbfields = [db.ec_event[f] for f in fields]
    if auth.is_logged_in():
        links=[dict(header='', body=lambda row: A(T('View'), _href=URL('convention','event',args=[convention.ec_short_name, row.id]), _class='buttontext button'))]
    else:
        links=[dict(header='', body=lambda row: A(T('View'), _href=URL('convention','viewevent',args=[convention.ec_short_name, row.id]), _class='buttontext button'))]

    query = db.ec_event.ec_id_convention==convention.id
    if filters:
        et_filters = [et for et in filters if et in EVENT_TYPE_CHOICES]
        if et_filters:
            query = query & (db.ec_event.ec_type.belongs(et_filters))
        if 'free_online' in filters:
            free_event_ids = []
            for row in db(query).select():
                if _get_event_vacancy(row.id, row.ec_apply_online) > 0:
                    free_event_ids.append(row.id)
            query = query & (db.ec_event.id.belongs(free_event_ids))

    form=SQLFORM.grid(query,
        field_id = db.ec_event.id,
        args=request.args[:2],
        orderby=[~db.ec_event.ec_registertime],
        fields=dbfields,
        maxtextlength=300,
        paginate=pagination,
        headers=headers,
        links=links,
        searchable=True, csv=False, create=False, editable=False, deletable=False, details=False)
    warnings = _check_warnings(convention, 'events') if auth.is_logged_in() else ''
    if not auth.is_logged_in: session.forget(response)
    return dict(form=form, title=title, eventtype=eventtype, systempage=systempage, warnings=warnings, people=people)

def viewevent():
    # for public usage
    # usage: id/args --> redirects to conname/id/args
    # check args and set event, config, convention
    event=db.ec_event(request.args(0)) # only eventid
    if event: # first argument is eventid? --> redirect
        convention=db.ec_convention(event.ec_id_convention)
        if convention: redirect(URL('convention','event',args=[convention.ec_short_name] + request.args))
        _redirect_message(T('Invalid convention'), URL('index'))
    else: # set event
        convention=_get_convention_conname(request.args(0))
        if not convention: _redirect_message(T('Invalid convention'),URL('index'))
        event=db.ec_event(request.args(1))
        if not event: _redirect_message(T('Invalid event'), URL('convention','events',args=convention.ec_short_name))
    config = db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('This convention needs to be configured'), URL('convention',args=[convention.ec_short_name]))
    # check if events can be viewed
    if not (config.ec_event_public or auth.is_logged_in()): _redirect_message(T('The events for this convention are not public (yet)'), URL('convention',args=[convention.ec_short_name]))
    # get fields
    fields = _get_event_fields(config,event.ec_type,'single')
    eventform = SQLFORM(db.ec_event, event.id, fields=fields, readonly=True,showid=False)
    hosts = _get_hosts(event.id)
    attendees = db((db.ec_attends_event.ec_id_event==event.id)&(db.ec_attends_event.ec_id_person==db.ec_person.id)).select(db.ec_person.ec_nickname).as_list() if event else None
    systempage = _get_systempage(convention, 'event')
    return dict(event=event, eventtype=event.ec_type, convention=convention, eventform=eventform, hosts=hosts, attendees=attendees, systempage=systempage)

def contact():
    session.back=None if session.back=='contact' else session.back
    convention=_get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    systempage = _get_systempage(convention, 'Contact')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, T('Contact'))
    config=db.ec_configuration[convention.ec_id_configuration]
    return dict(convention=convention,config=config,systempage=systempage)

def error():
    convention=_get_convention_conname(request.args(0))
    #if not convention: redirect(URL('default','error404'))
    return dict()

def markmin():
    convention=_get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    title=T('Markmin markup language')
    return dict(title=title)

def page():
    convention=_get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    page=db((db.ec_page.ec_id_convention==convention.id)&(db.ec_page.ec_name==request.args(1))).select().first()
    if not page: _redirect_message(T('Invalide page'), URL('convention',args=request.args(0)))
    if not _check_page_permission(page, convention):
        _redirect_message(T('invalid request'), URL('convention',args=request.args(0)))
    title=page.ec_title
    body=page.ec_body
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title or page.ec_name)
    return dict(title=title, body=body)

def file():
    # transforms a filename for a given convention into a file ID and calls download link
    convention=_get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    efile = db((db.ec_file.ec_name==request.args(1)) & (db.ec_file.ec_id_convention==convention.id)).select().first()
    if not efile: _redirect_message(T('Invalid file'), URL('convention',args=convention.ec_short_name))
    redirect(URL('default','download', args=efile.ec_file, vars=dict(filename=efile.ec_name)))

def event_statistics():
    import collections
    convention=_get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    config = db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('This convention needs to be configured'), URL('convention',args=[convention.ec_short_name]))
    if not config.ec_event_show_statistics: redirect(URL('error', args=convention.ec_short_name))
    title=MV('event_statistics')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title or page.ec_name)
    systempage = _get_systempage(convention, 'event_statistics')
    overview = collections.OrderedDict()
    events = db(db.ec_event.ec_id_convention==convention.id).select()
    eventtypes = _get_event_types(config)
    # all events
    overview[T('Total number', lazy=False)] = len(events)
    # by type
    overview[T('Per type')] = ', '.join(['%s: %s' % (MV(eventtype),len(events.find(lambda row: row.ec_type==eventtype))) for eventtype in eventtypes])
    # capacity
    overview[T('Total capacity')] = sum([e.ec_max_attendees for e in events])
    # capacity per type
    cap_per_type = ['%s: %s' % (MV(eventtype), sum([e.ec_max_attendees for e in events.find(lambda row: row.ec_type==eventtype)])) for eventtype in eventtypes]
    overview[T('Capacity per type')] = ', '.join(cap_per_type)

    # capacity per timeslot
    pertimeslot = []    # list of lists (one per line of result table)
    timeslots = db(db.ec_timeslot.ec_id_convention==convention.id).select(orderby=[db.ec_timeslot.ec_start_time])
    ## events without timeslot
    none_entry = [T('n/a'),
        len(events.find(lambda row: row.ec_id_timeslot == None)),
        sum([e.ec_max_attendees for e in events.find(lambda row: row.ec_id_timeslot == None)]),
        ]
    for eventtype in eventtypes:
        cap_sum = sum([e.ec_max_attendees for e in events.find(lambda row: (row.ec_type==eventtype) & (row.ec_id_timeslot==None))])
        none_entry.append(A(cap_sum, _href=URL('event', args=[convention.ec_short_name, 'new', eventtype])))
    pertimeslot.append(none_entry)
    ## other events
    for timeslot in timeslots:
        entry = [CAT(_begin_end_datetime_string(timeslot.ec_start_time, timeslot.ec_end_time), BR(), timeslot.ec_name),
            len(events.find(lambda row: row.ec_id_timeslot == timeslot.id)),
            sum([e.ec_max_attendees for e in events.find(lambda row: row.ec_id_timeslot == timeslot.id)]),
            ]
        for eventtype in eventtypes:
            cap_sum = sum([e.ec_max_attendees for e in events.find(lambda row: (row.ec_type==eventtype) & (row.ec_id_timeslot==timeslot.id))])
            entry.append(A(cap_sum, _href=URL('event', args=[convention.ec_short_name, 'new', eventtype], vars=dict(timeslot=timeslot.id))))
        pertimeslot.append(entry)
    # attendees
    if config.ec_convention_requires_registration:
        attendees = db((db.ec_attendance.ec_id_convention == convention.id) & \
                       (db.ec_attendance.ec_registration_state=='attendee')).select()
        overview[''] = XML('&nbsp;') # whitespace
        overview[T('Preregistered convention attendees')] = len(attendees)
    # hosts
    host_pids = db((db.ec_organizes_event.ec_id_event == db.ec_event.id) &\
                   (db.ec_event.ec_id_convention == convention.id)).select(db.ec_organizes_event.ec_id_person)
    host_string = T('of these,') + ' ' + T('Event organizers', lazy=False)
    if 'rpggame' in eventtypes:
        host_string = T('of these,') + ' ' + T('Game masters', lazy=False)
    overview[host_string] = len(set([h.ec_id_person for h in host_pids]))
    potential_gms = None
    if config.ec_registration_data_person_potential_gamemaster in ['required','optional']:
        potential_gm_ids = db((db.ec_attendance.ec_id_convention == convention.id) &\
                              (db.ec_attendance.ec_id_person == db.ec_person.id) & \
                              (db.ec_person.ec_potential_gamemaster == True)).select(db.ec_person.id)
        potential_gms = len(set([gm.id for gm in potential_gm_ids]))
    layout = 'print.html' if 'display' in request.vars and request.vars.display=='print' else 'layout.html'
    return dict(title=title, systempage=systempage, layout=layout, overview=overview, potential_gms=potential_gms, pertimeslot=pertimeslot, eventtypes=eventtypes)




## login controller functions
@auth.requires(lambda: _logged_in_and_privacy_consent())
def event():
    # usage:
    # id/args --> redirects to conname/id/args
    # conname/id/[edit/view] --> edit, view event id
    # conname/new --> new event misc
    # conname/new/ectype --> new event of type ectype
    # conname/new?selected=ID --> new event based on the selected ID
    action = None
    fillevent = None
    # parse args and set event, convention, action, person, config, fields, redirect if something is missing
    event=db.ec_event(request.args(0)) # only eventid
    if event: # first argument is eventid? --> redirect
        convention=db.ec_convention(event.ec_id_convention)
        if convention: redirect(URL('convention','event',args=[convention.ec_short_name] + request.args))
        _redirect_message(T('Invalid convention'), URL('index'))
    else: # set event or set action to 'new' or redirect
        convention=_get_convention_conname(request.args(0))
        if not convention: _redirect_message(T('Invalid convention'),URL('index'))
        if request.args(1):
            if request.args(1).isdigit():
                event=db.ec_event(request.args(1)) # try to parse event id
                if not event: _redirect_message(T('Invalid event'), URL('convention','events',args=convention.ec_short_name))
            elif request.args(1) == 'new':
                action = 'new'
            else: _redirect_message(T('Invalid event'), URL('convention','events',args=convention.ec_short_name))
        else: _redirect_message(T('Invalid event'), URL('convention','events',args=convention.ec_short_name))
    person = db.ec_person(auth.user.active_person) or _get_or_create_first_person_auth()
    if auth.has_permission('organize', db.ec_convention, convention.id) and request.vars.hostid:
        person = db.ec_person(request.vars.hostid)
        if not person: _redirect_message(T('Invalid person'), URL('organize', args=convention.ec_short_name))
    if action=='new':
        eventtype = request.args(2) if request.args(2) in EVENT_TYPE_CHOICES else 'misc'
        title = MV('new_%s' % eventtype)
        systempage = _get_systempage(convention, 'newevent')
        timeslot = db.ec_timeslot(request.vars.timeslot)
        if request.vars.selected:
            fillevent=db(auth.accessible_query('edit', db.ec_event)&(db.ec_event.id==request.vars.selected)).select().first()
    else:
        action = request.args(2) if request.args(2) in ['view','edit'] else 'view'
        eventtype = event.ec_type
        title = MV(eventtype)
        systempage = _get_systempage(convention, 'event')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    config = db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('This convention needs to be configured'), URL('convention',args=[convention.ec_short_name]))
    if action in ['new', 'edit']:
        db.ec_event.ec_description.comment = T('Please do not use any special characters (like "–", a long dash) since they may hinder the PDF export')

    # check if events can be created or edited
    if action in ['new', 'edit'] and not auth.has_permission('organize', db.ec_convention, convention.id):
        person = db.ec_person(auth.user.active_person) or _get_or_create_first_person_auth()
        if action=='edit' and not auth.has_permission('edit', db.ec_event, event.id):
            _redirect_message(T('You are not allowed to %s this event' % action), URL('convention','events',args=convention.ec_short_name))
        if not _event_registration_open(convention):
            _redirect_message(T('Event registration is not open for %(ec_name)s') % convention.as_dict(),  URL('convention','events',args=convention.ec_short_name))
        if config.ec_event_requires_registration:
            att_status = Registration.get_attendee_status(convention.id, person.id)
            if att_status!='attendee':
                _redirect_message(T('You have to be attendee for %(ec_name)s to register events') % convention.as_dict(),URL('convention','register',args=convention.ec_short_name))

    # get fields
    fields = _get_event_fields(config,eventtype,'single')

    # test only: remove description
    # fields.remove('ec_description')

    # check timeslot
    if config.ec_event_time_slots:
        db.ec_event.ec_id_timeslot.requires = IS_EMPTY_OR(IS_IN_SET(_get_timeslots(convention)))
        if action != 'view': db.ec_event.ec_start.comment = T('Leave blank to use times of timeslot')
        if action != 'view': db.ec_event.ec_end.comment = T('Leave blank to use times of timeslot')

    # check freetime
    if config.ec_event_free_time and not config.ec_event_time_slots:
        db.ec_event.ec_start.default=datetime.datetime(convention.ec_start_date.year, convention.ec_start_date.month, convention.ec_start_date.day, 18, 0, 0) if convention.ec_start_date else None
        db.ec_event.ec_end.default=datetime.datetime(convention.ec_start_date.year, convention.ec_start_date.month, convention.ec_start_date.day, 23, 0, 0) if convention.ec_start_date else None
    if config.ec_event_free_time:
        if not config.ec_event_free_time_outofcon:
            db.ec_event.ec_start.requires = IS_EMPTY_OR(IS_DATETIME_IN_RANGE(format=T('%Y-%m-%d %H:%M:%S'),
                                                                 minimum=datetime.datetime(convention.ec_start_date.year, convention.ec_start_date.month, convention.ec_start_date.day, 0, 0, 0) if convention.ec_start_date else None,
                                                                 maximum=datetime.datetime(convention.ec_end_date.year, convention.ec_end_date.month, convention.ec_end_date.day, 23, 59, 59) if convention.ec_end_date else None))
            db.ec_event.ec_end.requires = IS_EMPTY_OR(IS_DATETIME_IN_RANGE(format=T('%Y-%m-%d %H:%M:%S'),
                                                                 minimum=datetime.datetime(convention.ec_start_date.year, convention.ec_start_date.month, convention.ec_start_date.day, 0, 0, 0) if convention.ec_start_date else None,
                                                                 maximum=datetime.datetime(convention.ec_end_date.year, convention.ec_end_date.month, convention.ec_end_date.day, 23, 59, 59) if convention.ec_end_date else None))

    # validators for roleplaygames:
    col3 = {}
    if action in ['edit', 'new'] and eventtype == 'rpggame':
        db.ec_event.ec_id_rpggame.requires=IS_EMPTY_OR(IS_IN_DB(db, 'ec_rpggame.id', '%(ec_name)s (%(ec_system)s): %(ec_setting)s', zero=T('Create new game or choose one')))
        col3 = {'ec_id_rpggame': T('You can create a new game for this event after registering it.')}

    # create form
    if action=='view':
        eventform = SQLFORM(db.ec_event, event.id, fields=fields, readonly=True, showid=False,comments=False)
    elif action=='edit':
        eventform = SQLFORM(db.ec_event, event.id, fields=fields, col3=col3, deletable=True, showid=False, submit_button=T('Save'))
    else:
        if timeslot:
            db.ec_event.ec_id_timeslot.default = timeslot.id
        eventform = SQLFORM(db.ec_event, fields=fields, col3=col3, deletable=True, showid=False, submit_button=T('Create'))
        eventform.vars.ec_registertime=request.now
        eventform.vars.ec_id_convention=convention.id
        eventform.vars.ec_type=eventtype
        if fillevent:
            for field in ['ec_name','ec_min_attendees', 'ec_max_attendees', 'ec_apply_online','ec_url', 'ec_description', 'ec_id_rpggame', 'ec_prepared_chars', 'ec_introduction', 'ec_rpg_newbees']:
                eventform.vars[field]=fillevent[field]

    # process form
    if eventform and eventform.process(onvalidation=_validate_event, keepvalues=True).accepted:
        event = db.ec_event(eventform.vars.id)
        if not event: _redirect_message(T('Event "%(ec_name)s" deleted') % {'ec_name' : eventform.vars.ec_name}, URL('events',args=convention.ec_short_name))
        if action=='new':
            session.flash = T('Event "%(ec_name)s" registered') % event.as_dict()
            orgaid = db.ec_organizes_event.insert(ec_id_event=eventform.vars.id,ec_id_person=person.id)
            auth.add_permission(0, 'edit', db.ec_event, event.id)
            auth.add_permission(auth.id_group('con_orga_%s' % convention.id), 'edit', db.ec_event, event.id)
            auth.add_permission(auth.id_group('con_admin_%s' % convention.id), 'edit', db.ec_event, event.id)
            db.ec_systemevent.insert(ec_when=request.now, ec_what='new event %s (no. %s) registered' % (event.ec_name, event.id), ec_type='event_registration', ec_id_convention=convention.id, ec_id_person=person.id, ec_id_auth_user=auth.user.id)
        else:
            session.flash = T('Event "%(ec_name)s" changed') % event.as_dict()
        if eventtype=='rpggame' and not event.ec_id_rpggame:
            session.back='event'
            session.flash = '%s. %s' % (T('Event "%(ec_name)s" registered') % event.as_dict(),T('Now enter a role play game'))
            redirect(URL('newgame',args=[convention.ec_short_name,event.id]))
        else:
            redirect(URL('event',args=[convention.ec_short_name,event.id]))

    if event:
        hosts = db((db.ec_organizes_event.ec_id_event==event.id)&(db.ec_organizes_event.ec_id_person==db.ec_person.id)).select(db.ec_person.ALL)
        hoststring = _get_hosts(event.id)
        attendees = db((db.ec_attends_event.ec_id_event==event.id)&(db.ec_attends_event.ec_id_person==db.ec_person.id)).select(db.ec_person.ALL)
        is_attendee = db((db.ec_attends_event.ec_id_event==event.id)&(db.ec_attends_event.ec_id_person==person.id)).select()
    else: # new event
        is_attendee=False
        hosts = db(db.ec_person.id == person.id).select(db.ec_person.ALL).first()
        hoststring = hosts['ec_nickname']
        attendees = None

    # redirect if pdf
    if request.extension=="pdf":
        exportfields = fields + ['ec_type','organized_by','attendees','ec_id_pdf']
        result = _get_export_result(convention, 'event', exportfields, [event.id])
        return _export_pdf(convention, result, 'event')
    if 'display' in request.vars and request.vars.display=='print':
        exportfields = fields + ['ec_type','organized_by','attendees']
        layout = 'print.html'
        result = _get_export_result(convention, 'event', exportfields, [event.id])
        eventform = XML(_get_event_html(result[2], result))
    else:
        layout = 'layout.html'
    return dict(event=event, eventtype=eventtype, convention=convention, title=title, eventform=eventform,
                hosts=hosts, hoststring=hoststring, action=action, attendees=attendees, is_attendee=is_attendee,
                systempage=systempage, layout=layout, config=config)

@auth.requires(lambda: _logged_in_and_privacy_consent())
def my_invitations():
    session.invite = None
    _update_invitations()
    convention = _get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('Invalid configuration'), URL('convention', args=convention.ec_short_name))
    invite = db.ec_invitation(request.vars.iid)
    if not invite: db.ec_invitation(session.inviteid)
    email = auth.user.email
    if invite and invite.ec_email.lower()!=email.lower(): _redirect_message(T('Invalid invitation'), URL('convention', args=convention.ec_short_name))
    if invite: session.inviteid = invite.id
    title = T('My invitations')
    if config.ec_registration_other:
        person = db.ec_person(request.args(1)) if request.args(1) else db.ec_person(auth.user.active_person) or _get_or_create_first_person_auth()
    else:
        person = _get_or_create_first_person_auth()
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    coninvitations = db((db.ec_invitation.ec_email.lower()==auth.user.email.lower())&\
        (db.ec_invitation.ec_for=='convention')&\
        (db.ec_invitation.ec_id_convention==convention.id)).select(orderby=['ec_invitation.ec_status'])
    coninvites_for = {}
    if config.ec_registration_other:
        for cinv in coninvitations:
            coninvites_for[cinv.id] = db((db.ec_attendance.id==cinv.ec_id_attendance)&(db.ec_attendance.ec_id_person==db.ec_person.id)).select(db.ec_person.ALL).first()
    else:
        if len(coninvitations) > 0:
            coninvitations = [coninvitations[0],]
    if coninvitations and not invite: redirect(URL('my_invitations', args=convention.ec_short_name, vars=dict(iid=coninvitations[0].id)))
    return dict(title=title,person=person,config=config,invite=invite,coninvitations=coninvitations,coninvites_for=coninvites_for)

@auth.requires(lambda: _logged_in_and_privacy_consent())
def decline_invitation():
    _update_invitations()
    convention = _get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('Invalid configuration'), URL('convention', args=convention.ec_short_name))
    invitation = db.ec_invitation(request.vars.iid)
    if not invitation: _redirect_message(T('Invalid invitation'), URL('my_invitations', args=[convention.ec_short_name]))
    title = T('Decline invitation')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    if config.ec_registration_other:
        person = db.ec_person(request.args(1)) if request.args(1) else db.ec_person(auth.user.active_person) or _get_or_create_first_person_auth()
    else:
        person = _get_or_create_first_person_auth(me=True)
    form = FORM(T('Do you really want to decline this invitation?'),INPUT(_type='submit', _value=T('Decline invitation')))
    form.add_button(T('Cancel'), URL('my_invitations', args=[convention.ec_short_name], vars=dict(iid=invitation.id)))
    if form.process().accepted:
        invitation.update_record(ec_status='declined', ec_answer_date=request.now)
        db.ec_systemevent.insert(ec_when=request.now, ec_what='invitation no. %s for %s was declined' % (invitation.id, invitation.ec_email), ec_type='invitation_decline', ec_id_convention=convention.id, ec_id_auth_user=auth.user.id)
        _send_invitation_answer(convention,invitation)
        _redirect_message(T('Invitation declined'), URL('my_invitations', args=[convention.ec_short_name],vars=dict(iid=invitation.id)))
    return dict(title=title, invite=invitation, form=form, convention=convention)

@auth.requires(lambda: _logged_in_and_privacy_consent())
def decline_all_invitations():
    _update_invitations()
    convention = _get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('Invalid configuration'), URL('convention', args=convention.ec_short_name))
    what = request.vars.what if request.vars.what in INVITATION_CHOICES else None
    title = T('Decline invitations')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    if what:
        invitations = db((db.ec_invitation.ec_email.lower()==auth.user.email.lower())& (db.ec_invitation.ec_for==what)& (db.ec_invitation.ec_status.belongs(['invited','accepted but not registered'])) & (db.ec_invitation.ec_id_convention==convention.id)).select(orderby=['ec_invitation.ec_status'])
    else:
        _redirect_message(T("Don't know what to do"), URL('my_invitations', args=[convention.ec_short_name]))
    form = FORM(T('Do you really want to decline all these invitations?'),INPUT(_type='submit', _value=T('Decline invitations')))
    form.add_button(T('Cancel'), URL('my_invitations', args=[convention.ec_short_name]))
    if form.process().accepted:
        for invitation in invitations:
            invitation.update_record(ec_status='declined', ec_answer_date=request.now)
            _send_invitation_answer(convention,invitation)
        _redirect_message(T('Invitations declined'), URL('my_invitations', args=[convention.ec_short_name]))
    return dict(title=title, invitations=invitations, form=form, convention=convention)

@auth.requires(lambda: _logged_in_and_privacy_consent())
def register_one(): # register only the login person ("is_me")
    convention = _get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('This convention needs to be configured'), URL('convention',args=[convention.ec_short_name]))
    if config.ec_registration_other: redirect(URL('register', args=[convention.ec_short_name], vars=request.vars if request.vars else None ))
    systempage = _get_systempage(convention, 'Registration')
    title = T('Registration')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)

    # check for person with same email than account
    _check_person_auth()

    # invitation?
    _update_invitations()
    canregister = Registration.check_registration_capacity(convention, auth.user.email)
    use_invitation = None
    invite = None
    if config.ec_registration_invite_capacity!=0:
        if request.vars.iid:
            use_invitation = db.ec_invitation(request.vars.iid)
            if not use_invitation or use_invitation.ec_email.lower()!=auth.user.email: _redirect_message(T('Invalid invitation'), URL('register_one', args=[convention.ec_short_name]))
        else:
            invite = db((db.ec_invitation.ec_email.lower()==auth.user.email.lower())&\
                            (db.ec_invitation.ec_for=='convention')&\
                            (db.ec_invitation.ec_id_convention==convention.id)).select().first()

    # get person
    person = _get_or_create_first_person_auth(is_me=True)

    # create registration
    registration = Registration(convention, person=person, use_invitation=use_invitation)

    # check if already registered --> redirect
    attendeestatus = Registration.get_attendee_status(convention.id, person.id)
    if attendeestatus and attendeestatus!='deregistered':
        redirect(URL('registration', args=[convention.ec_short_name, person.id]))

    # process form
    regform = registration.get_regform(waitlist_only=(canregister=='waitlist_only'))
    session.conventionid = convention.id
    if regform.process(formname='regform', onvalidation=Registration.validate, keepvalues=True).accepted:
        registration.update(regform, person=person, attendanceid=None)
        message = registration.save()
        session.flash = message
        redirect(URL('registration', args=[convention.ec_short_name,registration.persondata['id']]))

    free_items = Registration.get_item_counter(convention)
    return dict(title=title, config=config, invite=invite, systempage=systempage, regform=regform, canregister=canregister, use_invitation=use_invitation, free_items=free_items)

@auth.requires(lambda: _logged_in_and_privacy_consent())
def register():  # registration if multiple registrations are allowed
    convention = _get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('This convention needs to be configured'), URL('convention',args=[convention.ec_short_name]))
    if not config.ec_registration_other: redirect(URL('register_one', args=[convention.ec_short_name], vars=dict(iid=request.vars.iid) if request.vars.iid else {} ))
    systempage = _get_systempage(convention, 'Registration')
    title = T('Registration')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    canregister = None      # if registration form could be shown; see Registration.check_registration_capacity for values
    invitations = None      # a list of invitations
    use_invitation = None   # an invitation to be used
    coninvites_for = {}     # a dict who the invitation was used for
    people = None           # a list of users who could be registered with their status
    regform = None          # the registration form
    person = None           # the person to be registered

    newperson = False       # if a new person should be registered

    # check for person with same email than account
    _check_person_auth()

    # registration with invitation system
    _update_invitations()
    canregister = Registration.check_registration_capacity(convention, auth.user.email)
    if config.ec_registration_invite_capacity!=0:
        if request.vars.iid:
            use_invitation = db.ec_invitation(request.vars.iid)
            if not use_invitation or use_invitation.ec_email.lower()!=auth.user.email: _redirect_message(T('Invalid invitation'), URL('register_one', args=[convention.ec_short_name]))
        else:
            invitations = db((db.ec_invitation.ec_email.lower()==auth.user.email.lower())&\
                            (db.ec_invitation.ec_for=='convention')&\
                            (db.ec_invitation.ec_id_convention==convention.id)).select()
            for cinv in invitations:
                coninvites_for[cinv.id] = db((db.ec_attendance.id==cinv.ec_id_attendance)&(db.ec_attendance.ec_id_person==db.ec_person.id)).select(db.ec_person.ALL).first()

    # check for orga registration
    if canregister != 'invite_ok' and _check_convention_permission('organize') and config.ec_registration_orga: canregister = 'register_ok'

    # create list of people if config.ec_registration_other
    people = db(auth.accessible_query('edit', db.ec_person)).select(db.ec_person.ALL,db.ec_attendance.ALL, \
                    left=db.ec_attendance.on((db.ec_attendance.ec_id_person==db.ec_person.id)&(db.ec_attendance.ec_id_convention==convention.id)),
                    orderby=[db.ec_person.ec_firstname])

    # try to set person from pid (person id)
    person = None
    newperson = False
    oldperson = {}
    if request.vars.pid:
        if request.vars.pid != "-1":
            person =  db.ec_person(int(request.vars.pid))
            if not person: _redirect_message(T('Invalid person'), URL('convention', args=convention.ec_short_name))
            if person and not auth.has_permission('edit', db.ec_person, person.id): _redirect_message(T('You are not allowed to register that person'), URL('my_personae', args=convention.ec_short_name))
        else:
            newperson = True
            oldperson = db.ec_person(auth.user.active_person) or _get_or_create_first_person_auth()
    elif auth.user.active_person != -1:
        person = db.ec_person(auth.user.active_person)
    else:
        person = _get_or_create_first_person_auth()

    # create registration
    registration = Registration(convention, person=person, use_invitation=use_invitation, oldperson=oldperson)
    attendeestatus = Registration.get_attendee_status(convention.id, person.id) if person else None
    if attendeestatus=='no_status': attendeestatus=None
    if not (config.ec_registration_orga and _check_convention_permission('organize')):
        if not _check_is_open(convention.id): _redirect_message(MV('register_nok'), URL('convention', args=convention.ec_short_name))
    if (canregister=='register_ok' or canregister=='waitlist_only' or use_invitation) and (not attendeestatus or attendeestatus=='deregistered'):
        regform = registration.get_regform(waitlist_only=(canregister=='waitlist_only'))
    session.conventionid = convention.id
    if regform and regform.process(formname='regform', onvalidation=Registration.validate, keepvalues=True).accepted:
        if request.vars.waitlist: regform.vars.waitlist = request.vars.waitlist # move hidden fields
        registration.update(regform, person=person, attendanceid=None)
        message = registration.save()
        session.flash = message
        redirect(URL('convention','registration', args=[convention.ec_short_name,registration.persondata['id']]))
    free_items = Registration.get_item_counter(convention)
    return dict(title=title, config=config, systempage=systempage, invitations=invitations, use_invitation=use_invitation, coninvites_for=coninvites_for, people=people, regform=regform, attperson=person, canregister=canregister, free_items=free_items)

@auth.requires(lambda: _logged_in_and_privacy_consent())
def my_personae(): # create and manage personae
    convention=_get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    systempage = _get_systempage(convention, 'my_personae')
    title = T('Manage persons')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    config=db.ec_configuration[convention.ec_id_configuration]
    if not config: redirect(URL(configure_main, args=convention.ec_short_name))
    db.ec_person.id.readable, db.ec_person.id.writable=False, False
    db.ec_person.state = Field.Virtual('state', lambda row: A(T(Registration.get_attendee_status(convention.id, row.ec_person.id)), _href=URL('registration', args=[convention.ec_short_name,row.ec_person.id])) if Registration.get_attendee_status(convention.id, row.ec_person.id) else '', label=T('State of registration'))
    db.ec_person.acting_person = Field.Virtual('acting_person', lambda row: A(T('Change acting person'), _href=URL('convention', 'change_acting_person', args=[convention.ec_short_name, row.ec_person.id])) if auth.user.active_person!=row.ec_person.id else T('Acting person'), label=T('State'))
    db.ec_person.release = Field.Virtual('release', lambda row: A(T('release')+' ...', _href=URL('convention', 'release_persona', args=[convention.ec_short_name, row.ec_person.id])) if auth.user.active_person!=row.ec_person.id else '', label=title)
    fields = ['acting_person', 'ec_firstname', 'ec_lastname', 'state', 'release']
    dbfields = [db.ec_person[f] for f in fields]
    form=SQLFORM.grid(auth.accessible_query('edit', db.ec_person),
        field_id = db.ec_person.id,
        args=request.args[:1],
        orderby=[db.ec_person.ec_firstname,db.ec_person.ec_lastname],
        fields=dbfields,
        oncreate=lambda form: auth.add_permission(auth.user_group(), 'edit', db.ec_person, form.vars.id),
        searchable=False, csv=False, deletable=False)
    return dict(form=form,title=title,systempage=systempage,registration_other=config.ec_registration_other)

@auth.requires(lambda: _logged_in_and_privacy_consent())
def change_acting_person():
    convention = _get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    person = db.ec_person(request.args(1))
    if not person: _redirect_message(T('Invalid person'), URL('my_personae', args=convention.ec_short_name))
    auth.user.active_person = person.id
    redirect(URL('my_personae', args=convention.ec_short_name))

@auth.requires(lambda: _logged_in_and_privacy_consent())
def change_registration():
    convention=_get_convention_conname(request.args(0))
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, T('Change registration'))
    return dict(message='Change registrations',convention=convention)

@auth.requires(lambda: _logged_in_and_privacy_consent())
def newgame():
    # creates a new game for an event, given as args(1)
    convention = _get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    if request.args(1):
        event = db.ec_event(request.args(1))
        if not event:
            session.flash = T('Invalid event')
            redirect(URL('convention','index'))
        elif not (auth.has_permission('edit', db.ec_event, event.id) and _event_registration_open(db.ec_convention[event.ec_id_convention])) \
             and not auth.has_permission('organize', db.ec_convention, convention.id):
            session.flash = T('You are not allowed to edit this event')
            redirect(URL('convention','event',args=[convention.ec_short_name,event.id]))
        else:
            eventconvention=db.ec_convention[event.ec_id_convention]
            if eventconvention.id != convention.id:
                session.flash = T('This event belongs to another convention')
                redirect(URL('events'),args=convention.ec_short_name)
    else:
        _redirect_message(T('Invalid event'), URL('default','index'))
    title = MV('newgame')
    systempage = _get_systempage(convention, 'newgame')
    title += ' %s %s' % (T('for'), event.ec_name) if event else ''
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)

    gameform=SQLFORM(db.ec_rpggame, submit_button=T('Create'))
    if gameform.process().accepted:
        gameid=gameform.vars.id
        if event:
            db.ec_event[event.id] = dict(ec_id_rpggame = gameid)
            person=db((event.id==db.ec_organizes_event.ec_id_event)&(db.ec_organizes_event.ec_id_person==db.ec_person.id)).select(db.ec_person.ALL)[0]
            _send_auto_email('event_registered',event=db.ec_event[event.id],person=person,convention=convention)

        session.flash=T('Game %(ec_name)s created') % {'ec_name': gameform.vars.ec_name}
        if session.back:
            redirect(URL(session.back,args=[convention.ec_short_name,event.id]))
    return dict(gameform=gameform,event=event,systempage=systempage)

@auth.requires(lambda: _logged_in_and_privacy_consent())
def orga_email():
    # allows a user to send an email to a convention orga
    session.back=None if session.back=='orga_email' else session.back
    message=''
    convention=_get_convention_conname(request.args(0))
    if not convention:
        session.back='orga_email'
        session.flash = T('Invalid convention')
        redirect(URL('index'))
    if not (convention.ec_contact_email or convention.ec_hidden_email):
        session.flash = T('This convention organization has not defined any e-mail address')
        redirect(URL('default','index'))
    title = T('Send e-mail to organizers of %(ec_name)s') % convention.as_dict()
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, T('Email to organizers'))
    config = db.ec_configuration[convention.ec_id_configuration]
    # create form
    db.ec_email.ec_from.default = mail.settings.sender
    db.ec_email.ec_id_convention.default=convention.id
    db.ec_email.ec_to.readable, db.ec_email.ec_to.writable = False, False
    db.ec_email.ec_text.default = config.ec_email_generic_text % _get_email_dict(convention=convention) if config.ec_email_generic_text else ''
    db.ec_email.ec_text.requires = IS_NOT_EMPTY()
    db.ec_email.ec_subject.default = config.ec_email_generic_subject % _get_email_dict(convention=convention) if config.ec_email_generic_subject else ''
    db.ec_email.ec_replyto.default=auth.user.email
    col3 = {'ec_replyto': T('You need one of your valid email addresses here or the organisators will not be able to answer your message')}
    emailform = SQLFORM(db.ec_email, submit_button=T('Send'), col3=col3)
    message=''
    result=''
    if emailform.validate(keepvalues=True):
        orga_email = convention.ec_contact_email or convention.ec_hidden_email
        emailform.vars.ec_to = orga_email
        todict = {str(orga_email) : T('organizers of %(ec_name)s') % convention.as_dict()}
        if emailform.vars.ec_showonly:
            response.flash = T('Message not sent') +' !'
            message=_show_email(emailform,config,convention=convention,todict=todict,expandVariables=False)
        else:
            emailid = _save_email(emailform,expandVariables=False)
            result = _send_email(emailid,todict=todict)
            response.flash = T('Emails were sent, please check the result list below')
            message=_show_email(emailform,config,convention,todict=todict,expandVariables=False,fakesignature=False)
            emailform = A(T('write another email'), _class=('button'),_href=URL('orga_email',args=[convention.ec_short_name]))
    return dict(message=message,emailform=emailform,result=result,title=title)

@auth.requires(lambda: _logged_in_and_privacy_consent())
def attend_event():
    # check parameters
    conid, eventid, personid = request.args(0), request.args(1), request.args(2)
    convention = _get_convention_conname(conid)
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    event = db.ec_event(eventid)
    if not event or event.ec_id_convention!=convention.id: _redirect_message(T('Invalid person'), URL('convention','convention', args=convention.ec_short_name))
    person = db.ec_person(personid) or db.ec_person(auth.user.active_person) or _get_or_create_first_person_auth()
    if not person: _redirect_message(T('Invalid person'), URL('convention', args=[convention.ec_short_name]))
    config=db.ec_configuration(convention.ec_id_configuration)
    # check if registration for events is allowed
    if not config.ec_event_attendance: _redirect_message(T('You cannot register for events at this convention'),URL('convention', 'event', args=[convention.ec_short_name,event.id]))
    if config.ec_event_attendance_requires_registration:
        attendance = _get_attendance(convention, person)
        if not attendance or attendance.ec_registration_state!='attendee':
            _redirect_message(T('You have to be attendee for %(ec_name)s to register for events') % convention.as_dict(),URL('convention', 'event', args=[convention.ec_short_name,event.id]))
    if not config.ec_event_attendee_registration_period_start or config.ec_event_attendee_registration_period_start > request.now:
        _redirect_message(T('The registration for events at this convention has not started yet'),URL('event', args=[convention.ec_short_name,event.id]))
    if config.ec_event_attendee_registration_period_stop and config.ec_event_attendee_registration_period_stop < request.now:
        _redirect_message(T('The registration for events at this convention has stopped'),URL('event', args=[convention.ec_short_name,event.id]))
    bought = db(db.ec_attends_event.ec_id_event==event.id).count()
    if max(0, event.ec_apply_online - bought)==0: _redirect_message(T('There are no online seats left for this event'),URL('event', args=[convention.ec_short_name,event.id]))
    if db((db.ec_attends_event.ec_id_event==event.id)&(db.ec_attends_event.ec_id_person==person.id)).select():
        _redirect_message(T('You are already registered for this event'),URL('event', args=[convention.ec_short_name,event.id]))

    if config.ec_event_time_slots and not config.ec_event_double_registration_timeslot:
        if _attends_other_event(event.ec_id_timeslot, person.id):
            _redirect_message('%s. %s' % (T('Sorry'), T('You already registered for an event at this time')), URL('convention', 'event', args=[convention.ec_short_name,event.id]))
        if _hosts_other_event(event.ec_id_timeslot, person.id):
            _redirect_message('%s. %s' % (T('Sorry'), T('You already host an event at this time')), URL('convention', 'event', args=[convention.ec_short_name,event.id]))

    if _is_eventorga(convention.id, person.id):
        if config.ec_event_registration_limit_eventorganizer > -1:
            if config.ec_event_registration_limit_eventorganizer == 0:
                _redirect_message('%s. %s' % (T('Sorry'), T('As an event organizer, you may not register online for events')), URL('convention', 'event', args=[convention.ec_short_name,event.id]))
            attended_events = _get_attended_events(convention.id,person.id)
            if attended_events >= config.ec_event_registration_limit_eventorganizer:
                _redirect_message('%s. %s' % (T('Sorry'), T('As an event organizer, you may only register online for %s event(s)') % config.ec_event_registration_limit_eventorganizer), URL('convention', 'event', args=[convention.ec_short_name,event.id]))
    else:
        if config.ec_event_registration_limit_normalattendee > -1:
            if config.ec_event_registration_limit_normalattendee == 0:
                _redirect_message('%s. %s' % (T('Sorry'), T('If you do not organize an event, you may currently not register online for events')), URL('convention', 'event', args=[convention.ec_short_name,event.id]))
            attended_events = _get_attended_events(convention.id,person.id)
            if attended_events >= config.ec_event_registration_limit_normalattendee:
                _redirect_message('%s. %s' % (T('Sorry'), T('If you do not organize an event, you may currently only register online for %s event(s)') % config.ec_event_registration_limit_normalattendee), URL('convention', 'event', args=[convention.ec_short_name,event.id]))

    # really do it
    messagedict = person.as_dict().copy()
    messagedict.update(event.as_dict())
    message=T('%(ec_firstname)s %(ec_lastname)s was registered for event "%(ec_name)s"') % messagedict
    title=T('Register for event')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)

    db.ec_systemevent.insert(ec_when=request.now, ec_what='new event attendance for event %s (no. %s)' % (event.ec_name, event.id), ec_type='event_attendance', ec_id_convention=convention.id, ec_id_person=person.id, ec_id_auth_user=auth.user.id)
    db.ec_attends_event.insert(ec_id_event=event.id, ec_id_person=person.id)

    # generate warnings
    if config.ec_event_time_slots and _attends_other_event(event.ec_id_timeslot, person.id, event.id):
        message+='. %s: %s' % (T('Warning'),T('You already registered for an event at this time'))
    if config.ec_event_time_slots and _hosts_other_event(event.ec_id_timeslot, person.id, event.id):
        message+='. %s: %s' % (T('Warning'),T('You already host an event at this time'))
    _redirect_message(message, URL('convention', 'event', args=[convention.ec_short_name,event.id]))
    return dict(title=title, message=message, event=event, person=person)

@auth.requires(lambda: _logged_in_and_privacy_consent())
def de_attend_event():
    # check parameters
    conname, eventid, personid = request.args(0), request.args(1), request.args(2)
    convention = _get_convention_conname(conname)
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    event = db.ec_event(eventid)
    if not event or event.ec_id_convention!=convention.id: _redirect_message(T('Invalid person'), URL('convention','convention', args=convention.ec_short_name))
    person = db.ec_person(personid) or db.ec_person(auth.user.active_person) or _get_or_create_first_person_auth()
    if not person: _redirect_message(T('Invalid person'), URL('convention', args=convention.ec_short_name))
    config=db.ec_configuration(convention.ec_id_configuration)
    attend = db((db.ec_attends_event.ec_id_event==event.id)&(db.ec_attends_event.ec_id_person==person.id)).select()
    if not attend:
        _redirect_message(T('Your are not an attendee for this event'),URL('event', args=[convention.ec_short_name,event.id]))
    # check permission
    if not (auth.has_permission('edit', db.ec_person, person.id) or auth.has_permission('organize', db.ec_convention, convention.id)):
        _redirect_message(T('You are not allowed to de-register this person from the event'), URL('event', args=[convention.ec_short_name,event.id]))
    # really do it
    db((db.ec_attends_event.ec_id_event==event.id)&(db.ec_attends_event.ec_id_person==person.id)).delete()
    messagedict = person.as_dict().copy()
    messagedict.update(event.as_dict())
    message=T('%(ec_firstname)s %(ec_lastname)s was de-registered from event "%(ec_name)s"') % messagedict
    _redirect_message(message, URL('convention', 'event', args=[convention.ec_short_name,event.id]))

@auth.requires(lambda: _logged_in_and_privacy_consent())
def manage_services():
    convention=_get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    systempage = _get_systempage(convention, 'manage_services')
    title=T('Help out')+'!'
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    config=db.ec_configuration[convention.ec_id_configuration]
    if not config: _redirect_message(T('There are no services for this convention yet'), URL('convention',args=[convention.ec_short_name]))
    person = db.ec_person(auth.user.active_person) or _get_or_create_first_person_auth()
    if not person: _redirect_message(T('Invalid person'), URL('convention', args=convention.ec_short_name))
    db.ec_service.id.readable, db.ec_service.id.writable=False, False
    db.ec_service.ec_id_convention.readable, db.ec_service.ec_id_convention.writable = False, False
    db.ec_service.ec_id_convention.default=convention.id
    db.ec_service.ec_id_timeslot.requires = IS_EMPTY_OR(IS_IN_SET(_get_timeslots(convention)))
    fields = ['ec_name', 'ec_description', 'ec_min_helpers', 'ec_max_helpers', 'v_helper_count', 'v_helpers', 'ec_start_time', 'ec_end_time']
    dbfields = [db.ec_service[f] for f in fields]
    headers={}
    for f in fields:
        headers['ec_service.%s' % f] = short_label(f)
    form=SQLFORM.grid(db.ec_service.ec_id_convention==convention.id,
        field_id = db.ec_service.id,
        args=request.args[:1],
        orderby=[db.ec_service.ec_order],
        fields=dbfields,
        maxtextlength=300,
        headers=headers,
        onvalidation=_validate_service,
        links=[dict(header='',body=lambda row: CAT(A(T('Help out'), _href=URL('help_with_service', args=[convention.ec_short_name, row.id, person.id]), _class='buttontext button') if db((db.ec_helps.ec_id_service==row.id)&(db.ec_helps.ec_id_person==person.id)).count()==0 and (row.v_helper_count==T('unlimited') or row.v_helper_count > 0) else '',\
                                                   ' ',\
                                                   A(T("Don't help out"), _href=URL('de_help_with_service', args=[convention.ec_short_name, row.id, person.id]), _class='buttontext button') if db((db.ec_helps.ec_id_service==row.id)&(db.ec_helps.ec_id_person==person.id)).select() else '')),],
        searchable=False, csv=False,
        create=auth.has_permission('organize', db.ec_convention, convention.id),
        editable=auth.has_permission('organize', db.ec_convention, convention.id),
        deletable=auth.has_permission('organize', db.ec_convention, convention.id),
        details=auth.has_permission('organize', db.ec_convention, convention.id),)
    services=db((db.ec_service.ec_id_convention==convention.id)&\
                (db.ec_service.id==db.ec_helps.ec_id_service)&\
                (db.ec_helps.ec_id_person==person.id)).select(db.ec_service.ALL)
    return dict(form=form,title=title,services=services,systempage=systempage)

@auth.requires(lambda: _logged_in_and_privacy_consent())
def help_with_service():
    convention = _get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    service = db.ec_service(request.args(1))
    if not service: _redirect_message(T('Invalid service'), URL('convention',args=[convention.ec_short_name]))
    person = db.ec_person(request.args(2)) or db.ec_person(auth.user.active_person) or _get_or_create_first_person_auth()
    if not person: _redirect_message(T('Invalid person'), URL('convention', args=[convention.ec_short_name]))
    needed_helpers = service.ec_max_helpers - db(db.ec_helps.ec_id_service==service.id).count()
    if service.ec_max_helpers==-1 or needed_helpers > 0:
        db.ec_helps.update_or_insert((db.ec_helps.ec_id_person==person.id)&(db.ec_helps.ec_id_service==service.id), ec_id_person=person.id, ec_id_service=service.id)
    else:
        _redirect_message(T('This service does not need any more helpers'), URL('manage_services', args=[convention.ec_short_name]))
    messagedict = person.as_dict().copy()
    messagedict.update(service.as_dict())
    message=T('%(ec_firstname)s %(ec_lastname)s helps with service "%(ec_name)s"') % messagedict
    _redirect_message(message, URL('manage_services', args=[convention.ec_short_name]))

@auth.requires(lambda: _logged_in_and_privacy_consent())
def de_help_with_service():
    convention = _get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    service = db.ec_service(request.args(1))
    if not convention: _redirect_message(T('Invalid service'), URL('convention',args=[convention.ec_short_name]))
    person = db.ec_person(request.args(2)) or db.ec_person(auth.user.active_person) or _get_or_create_first_person_auth()
    if not person: _redirect_message(T('Invalid person'), URL('convention', args=[convention.ec_short_name]))
    db((db.ec_helps.ec_id_person==person.id)&(db.ec_helps.ec_id_service==service.id)).delete()
    messagedict = person.as_dict().copy()
    messagedict.update(service.as_dict())
    message=T('%(ec_firstname)s %(ec_lastname)s does not helps with service "%(ec_name)s"') % messagedict
    _redirect_message(message, URL('manage_services', args=[convention.ec_short_name]))

@auth.requires(lambda: _logged_in_and_privacy_consent())
def participants():
    convention = _get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    systempage = _get_systempage(convention, 'Participants')
    config = db.ec_configuration[convention.ec_id_configuration]
    if not config: _redirect_message(T('This convention is not configured yet'), URL('default','index'))
    title = T('List of participants')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    is_participant = _check_auth_convention_attendee(['registered','attendee','deregistered','waitlist'])
    fields = ['ec_nickname']
    for f in ['zip', 'city', 'country']:
        if getattr(config, 'ec_registration_data_person_%s'%f):
            fields.append('ec_%s' %f)
    dbfields = [db.ec_person[f] for f in fields]
    if is_participant:
        db.ec_attendance.ec_registration_state.represent = lambda value, row: MV(value)
        if config.ec_registration_show_realnames:
            dbfields.insert(0,db.ec_person.ec_lastname)
            dbfields.insert(0,db.ec_person.ec_firstname)
        dbfields.insert(0,db.ec_attendance.ec_registration_state)
        selectable=[(T('Send e-mail to selected people'),
                 lambda ids : redirect(URL('convention', 'attendee_email', args=convention.ec_short_name,
                                           vars=dict(tos=ids))))]
        orderby=[db.ec_attendance.ec_registration_state]
        searchable=True
    else:
        selectable = []
        orderby = [db.ec_person.ec_zip]
        searchable=False
    query = (db.ec_person.id==db.ec_attendance.ec_id_person)&\
            (db.ec_attendance.ec_id_convention==convention.id)&\
            (db.ec_attendance.ec_registration_state.belongs(['registered','attendee'])) #,'waitlist']))
    form = SQLFORM.grid(query,
                        field_id = db.ec_person.id,
                        left = db.ec_attendance.on((db.ec_person.id==db.ec_attendance.ec_id_person)&(db.ec_attendance.ec_id_convention==convention.id)),
                        args=request.args[:1],
                        orderby=orderby,
                        fields=dbfields,
                        selectable=selectable,
                        paginate=100,
                        csv=False, create=False, editable=False, deletable=False, details=False, searchable=searchable,
                        )
    return dict(title=title, form=form, convention=convention, systempage=systempage)

@auth.requires(lambda: _logged_in_and_privacy_consent())
def personal_programme():
    convention = _get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    systempage = _get_systempage(convention, 'personal_program')
    config = db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('This convention is not configured yet'), URL('default','index'))
    person = db.ec_person(auth.user.active_person) or _get_or_create_first_person_auth()
    message = ''
    # create list of filters (and make sure that it's a list)
    filters = []
    if request.vars.filters:
        if isinstance(request.vars.filters, list):
            filters = request.vars.filters
        else:
            filters = [request.vars.filters, ]
    # create list of people: make sure that it's a list and remove all invalid ids
    people = [] # people who are selected
    peoplelist = [] # people who are selectable
    if config.ec_registration_other:
        peoplelist = db(auth.accessible_query('edit', db.ec_person) &\
                        (db.ec_attendance.ec_id_person==db.ec_person.id)&\
                        (db.ec_attendance.ec_id_convention==convention.id)&\
                        (db.ec_attendance.ec_registration_state=='attendee')).select(db.ec_person.ALL)
        allowedids = [p.id for p in peoplelist]
        if request.vars.people:
            if isinstance(request.vars.people, list):
                request_people = request.vars.people
            else:
                request_people = [request.vars.people, ]
            for p in request_people:
                try:
                    if int(p) in allowedids:
                        people.append(p)
                    else:
                        _redirect_message(T('Invalid person'), URL('personal_programme', args=convention.ec_short_name))
                except:
                    _redirect_message(T('Invalid person'), URL('personal_programme',args=convention.ec_short_name))
    if not people:
        people = [person.id,]
        title = T('Personal programme for %(ec_firstname)s %(ec_lastname)s ("%(ec_nickname)s")') % person.as_dict()
    else:
        nicknames = []
        for personid in people:
            nicknames.append(db.ec_person[personid].ec_nickname)
        title = T('Family Programme for %s') % ', '.join(nicknames)
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    entries = [] # contains entries for the programm, tuples with begintime, what, who
    all_services = [] # contains entries for all services of all people
    all_events = [] # ids of all events in the list (to avoid duplicates
    for personid in people:
        if filters and 'events' not in filters:
            events_orga = []
            events_attend = []
        else:
            events_orga = db((db.ec_event.ec_id_convention==convention.id) &\
                             (db.ec_event.id==db.ec_organizes_event.ec_id_event)&\
                             (db.ec_organizes_event.ec_id_person==personid)).select(db.ec_event.ALL)
            events_attend = db((db.ec_event.ec_id_convention==convention.id) &\
                               (db.ec_event.id==db.ec_attends_event.ec_id_event)&\
                               (db.ec_attends_event.ec_id_person==personid)).select(db.ec_event.ALL)
        if filters and 'boughtitems' not in filters:
            boughtitems = []
        else:
            boughtitems = db((db.ec_buyitem.ec_id_convention==convention.id) &\
                           (db.ec_buys_item.ec_id_buyitem==db.ec_buyitem.id) &\
                           (db.ec_buys_item.ec_id_person==personid) &\
                           (db.ec_buys_item.ec_buy_state=='booked') &\
                           (db.ec_buys_item.ec_id_person==db.ec_person.id) &\
                           (db.ec_buyitem.ec_at_time!=None)).select(db.ec_buyitem.ALL, db.ec_person.ALL)
        if filters and 'personal_events' not in filters:
            personal_events = []
        else:
            personal_events = db((db.ec_personal_event.ec_id_convention==convention.id) &\
                                 (db.ec_personal_event.ec_id_person==personid) &\
                                 (db.ec_personal_event.ec_id_person==db.ec_person.id)).select(db.ec_personal_event.ALL, db.ec_person.ALL)
        where = PRE('                       ') # will be replaced by rooms if there are some

        # create entries for regular events
        for event in events_attend + events_orga:
            # avoid duplicates
            if event.id in all_events:
                continue
            else:
                all_events.append(event.id)
            # fill entries
            entry = dict(when='', what='', who='', where=where)
            begin, end = None, None
            if event.ec_start:
                begin = event.ec_start
            else:
                if config.ec_event_time_slots and event.ec_id_timeslot:
                    begin = db.ec_timeslot(event.ec_id_timeslot)['ec_start_time']
            if event.ec_end:
                end = event.ec_end
            else:
                if config.ec_event_time_slots and event.ec_id_timeslot and db.ec_timeslot(event.ec_id_timeslot)['ec_end_time']:
                    end = db.ec_timeslot(event.ec_id_timeslot)['ec_end_time']
            entry['when'] = _begin_end_datetime_string(begin, end)
            if config.ec_event_time_slots and event.ec_id_timeslot:
                entry['when'] = CAT(entry['when'], BR(), db.ec_timeslot(event.ec_id_timeslot)['ec_name'])
            entry['what'] = A('%s (%s)' % (event.ec_name, MV(event.ec_type)), _href=URL('event', args=[convention.ec_short_name,event.id]))
            entry['who'] = '%s: %s' % (event.ec_hostname, event.ec_host) if event.ec_host else ''
            if event.ec_attendees: entry['who'] += '; %s: %s' % (T('Event attendees'), event.ec_attendees)
            entries.append((begin, entry))

        # create entries for personal events
        for event in personal_events:
            entry = dict(when='', what='', who='', where=where)
            begin, end = None, None
            if event.ec_personal_event.ec_start:
                begin = event.ec_personal_event.ec_start
            else:
                if config.ec_event_time_slots and event.ec_personal_event.ec_id_timeslot:
                    begin = db.ec_timeslot(event.ec_personal_event.ec_id_timeslot)['ec_start_time']
            if event.ec_personal_event.ec_end:
                end = event.ec_personal_event.ec_end
            else:
                if config.ec_event_time_slots and event.ec_personal_event.ec_id_timeslot and db.ec_timeslot(event.ec_personal_event.ec_id_timeslot)['ec_end_time']:
                    end = db.ec_timeslot(event.ec_personal_event.ec_id_timeslot)['ec_end_time']
            entry['when'] = _begin_end_datetime_string(begin, end)
            if config.ec_event_time_slots and event.ec_personal_event.ec_id_timeslot:
                entry['when'] = CAT(entry['when'], BR(), db.ec_timeslot(event.ec_personal_event.ec_id_timeslot)['ec_name'])
            if person.id==personid:
            # it's me, Babe!
                entry['what'] = A('%s (%s)' % (event.ec_personal_event.ec_name, T('Personal event')), _href=URL('manage_personal_events', args=[convention.ec_short_name,'edit','ec_personal_event',event.ec_personal_event.id], user_signature=True))
                entry['who'] = person.ec_nickname
            else:
                entry['what'] = '%s (%s)' % (event.ec_personal_event.ec_name, T('Personal event'))
                entry['who'] = db.ec_person[personid].ec_nickname
            entries.append((begin, entry))

        # create entries for buyable items with datetime
        for item in boughtitems:
            entry = dict(when='', what='', who='', where=where)
            begin, end = None, None
            if item.ec_buyitem.ec_at_time:
                begin = item.ec_buyitem.ec_at_time
                entry['when'] = _begin_end_datetime_string(item.ec_buyitem.ec_at_time, None)
                if config.ec_event_time_slots and item.ec_buyitem.ec_id_timeslot:
                    entry['when'] = CAT(entry['when'], BR(), db.ec_timeslot(item.ec_buyitem.ec_id_timeslot)['ec_name'])
            entry['what'] = A('%s (%s)' % (item.ec_buyitem.ec_name, T('Bought item')), _href=URL('registration', args=[convention.ec_short_name,personid], user_signature=True))
            entry['who'] = item.ec_person.ec_nickname
            entries.append((begin, entry))

        # create entries for services
        services = []
        if config.ec_convention_requires_services:
            services = db((db.ec_service.ec_id_convention==convention.id)&\
                            (db.ec_service.id==db.ec_helps.ec_id_service)&\
                            (db.ec_helps.ec_id_person==personid)&\
                            (db.ec_person.id==personid)).select(db.ec_service.ALL,db.ec_person.ALL)
        if request.vars.filters and 'services' not in request.vars.filters:
            services = []
        for service in services:
            entry = dict(when='', what='', who='', where=where)
            begin, end = None, None
            if service.ec_service.ec_start_time:
                begin = service.ec_service.ec_start_time
                if service.ec_service.ec_end_time:
                    end = service.ec_service.ec_end_time
                entry['when'] = _begin_end_datetime_string(service.ec_service.ec_start_time, service.ec_service.ec_end_time)
                if config.ec_event_time_slots and service.ec_service.ec_id_timeslot:
                    entry['when'] = CAT(entry['when'], BR(), db.ec_timeslot(service.ec_service.ec_id_timeslot)['ec_name'])
            entry['what'] = A('%s (%s)' % (service.ec_service.ec_name, T('Service')), _href=URL('manage_services', args=[convention.ec_short_name], user_signature=True))
            entry['who'] = service.ec_person.ec_nickname
            entries.append((begin, entry))
        all_services.append(services)

    # create alle entries and empty time slots
    if config.ec_event_time_slots:
        timeslots = db(db.ec_timeslot.ec_id_convention==convention.id).select(orderby=[db.ec_timeslot.ec_start_time])
    for timeslot in timeslots:
        if not [match for match in entries if timeslot.ec_start_time==match[0]]: # no event in timeslot yet
            entry = dict(when=CAT(_begin_end_datetime_string(timeslot.ec_start_time, timeslot.ec_end_time), BR(), timeslot.ec_name), what='', who='', where=where)
            entries.append((timeslot.ec_start_time, entry))
    entries.sort(key=lambda entry: entry[0] if entry[0] else request.now)
    layout = 'print.html' if 'display' in request.vars and request.vars.display=='print' else 'layout.html'

    return dict(title=title, entrytable=entries, config=config, convention=convention, systempage=systempage,
                layout=layout, all_services=all_services, peoplelist=peoplelist, me=person.id, message=message)

@auth.requires(lambda: _logged_in_and_privacy_consent())
def manage_personal_events():
    convention=_get_convention_conname(request.args(0))
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, T('Personal events'))
    config=db.ec_configuration[convention.ec_id_configuration]
    if not config: _redirect_message(T('You have to copy or create a configuration for your convention'),URL('configure_main',args=convention.ec_short_name))
    person = db.ec_person(auth.user.active_person) or _get_or_create_first_person_auth()
    title = T('Personal events for %(ec_firstname)s %(ec_lastname)s ("%(ec_nickname)s")') % person.as_dict()
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    db.ec_personal_event.id.readable, db.ec_personal_event.id.writable=False, False
    db.ec_personal_event.ec_id_convention.readable, db.ec_personal_event.ec_id_convention.writable=False, False
    db.ec_personal_event.ec_id_convention.default=convention.id
    db.ec_personal_event.ec_id_person.readable, db.ec_personal_event.ec_id_person.writable=False, False
    db.ec_personal_event.ec_id_person.default=person.id
    fields = ['ec_name']
    if config.ec_event_time_slots:
        fields.append('ec_id_timeslot')
        db.ec_personal_event.ec_id_timeslot.requires = IS_EMPTY_OR(IS_IN_SET(_get_timeslots(convention)))
    else:
        db.ec_personal_event.ec_id_timeslot.readable, db.ec_personal_event.ec_id_timeslot.writable = False, False
    fields = fields + ['ec_start', 'ec_end']
    dbfields = [db.ec_personal_event[f] for f in fields]
    form=SQLFORM.grid((db.ec_personal_event.ec_id_convention==convention.id)&(db.ec_personal_event.ec_id_person==person.id),
        field_id=db.ec_personal_event.id,
        args=request.args[:1],
        fields = dbfields,
        orderby=[db.ec_personal_event.ec_start],
        searchable=False, csv=False)
    return dict(title=title, form=form)

## person controller functions
@auth.requires(lambda: _logged_in_and_privacy_consent() and (_check_convention_person_permission('edit') or _check_convention_permission('easycon_admin')))
def person_data():
    convention = _get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    person = db.ec_person(request.args(1))
    if not person: _redirect_message(T('Invalid person'), URL('back', args=convention.ec_short_name))
    back = request.vars.back if request.vars.back else None
    backargs = [convention.ec_short_name,]
    if back == 'registration':
        backargs.append(person.id)
    title = T('Personal data')
    response.title='Easy-Con: %s' % title
    action='edit' if request.args(2) and request.args(2)=='edit' else 'view'
    db.ec_person.id.readable,db.ec_person.id.writable=False,False
    if False: #_check_convention_permission('organize') and not _check_convention_person_permission('edit'):
        fields = Registration.get_person_fields(convention)
    else:
        fields = db.ec_person.fields()
    form=SQLFORM(db.ec_person,person.id,fields=fields,readonly=(action=='view'),submit_button=T('Save'))
    if form.process().accepted:
        if back:
            _redirect_message(T('Personal data changed'), URL(back,args=backargs))
        else:
            _redirect_message(T('Personal data changed'), URL('person_data',args=[convention.ec_short_name, person.id, 'view']))
    return dict(form=form,action=action,person=person, title=title, back=back, backargs=backargs)

@auth.requires(lambda: _logged_in_and_privacy_consent() and auth.has_permission('edit', 'ec_person', request.args(1)))
def release_persona():
    convention = _get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    person = db.ec_person(request.args(1))
    if not person: _redirect_message(T('Invalid person'), URL('my_personae', args=convention.ec_short_name))
    title = T('Release person')
    response.title='Easy-Con: %s' % title
    accounts = _get_accounts(request.args(1))
    form = None
    message = None
    if person.ec_email==auth.user.email:
        message = CAT('%s. %s.' % (T('You cannot release this persona'), T('Your account has the same email address')),
            ' ',TAG.button(T('OK'),_type="button",_onClick = "parent.location='%s' " % URL('my_personae',args=convention.ec_short_name)))
    elif len(accounts)==1:
        message = CAT('%s. %s.' % (T('You cannot release this persona'), T('You have the only account that can manage it')),' ',TAG.button(T('OK'),_type="button",_onClick = "parent.location='%s' " % URL('my_personae',args=convention.ec_short_name)))
    else:
        form = FORM(T('Do you really want to release %(ec_firstname)s %(ec_lastname)s? You will not be able to manage that persona with this account anymore') % person.as_dict()+'. ',INPUT(_type='submit', _value=T('release')))
        form.add_button(T('Cancel'), URL('my_personae', args=[convention.ec_short_name]))
    if form and form.process().accepted:
        auth.del_permission(auth.user, 'edit', 'ec_person', person.id)
        _redirect_message(T('%(ec_firstname)s %(ec_lastname)s released') % person.as_dict(), URL('my_personae', args=[convention.ec_short_name]))
    return dict(title=title,form=form,message=message)

## attendee controller functions
@auth.requires(lambda: _logged_in_and_privacy_consent())
def attendee_email():
    convention=_get_convention_conname(request.args(0))
    config = db.ec_configuration[convention.ec_id_configuration]
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, T('Emails'))
    sendto=request.get_vars['tos'] or []
    message = ''
    back = request.vars.back or 'participants'
    backargs = request.vars.backargs or convention.ec_short_name
    # create form
    db.ec_email.ec_id_convention.default=convention.id
    todict = {}
    col3 = {}
    if not isinstance(sendto, list):
        sendto = [sendto,]
    noemails = ''
    for personid in sendto:
        person = db.ec_person(personid)
        if person:
            if person.ec_email:
                if not _check_email_permission(person): # check permission: sender (auth.user) and receiver are attendee of the con, or orga, or attendee/organizer of an event
                    noemails +=T('You are not allowed to write an e-mail to %s') % person.ec_nickname + '. '
                else:
                    if todict.get(person.ec_email):
                        todict[person.ec_email] += ', ' + person.ec_nickname
                    else:
                        todict[person.ec_email] = person.ec_nickname
            else:
                noemails += T('%s has no e-mail specified') % person.ec_nickname + '. '
        else:
            _redirect_message(T('Invalid person'), URL('convention', args=convention.ec_short_name))
    if not todict:
        _redirect_message(noemails, URL(back, args=backargs))
    db.ec_email.ec_from.default = mail.settings.sender
    db.ec_email.ec_to.writable = False
    db.ec_email.ec_to.default = ', '.join(list(todict.values()))
    db.ec_email.ec_to.comment = noemails
    db.ec_email.ec_replyto.default=auth.user.email
    db.ec_email.ec_subject.default = config.ec_email_tag % _get_email_dict(convention=convention) if config.ec_email_tag else ''
    if 'subject' in request.vars:
        db.ec_email.ec_subject.default += ' ' + request.vars.subject
    col3['ec_replyto'] = T('You need one of your valid email addresses here or the person will not be able to answer your message')
    db.ec_email.ec_subject.requires = IS_NOT_EMPTY()
    db.ec_email.ec_text.requires = IS_NOT_EMPTY()
    fields = ['ec_from', 'ec_to', 'ec_replyto', 'ec_subject', 'ec_text', 'ec_showonly']
    emailform = SQLFORM(db.ec_email,
                        fields=fields,
                        col3=col3,
                        buttons = [
                            TAG.button(T('Send'),_type="submit"),
                            TAG.button(T('Cancel'),_type="button",_onClick = "parent.location='%s' " % URL(back,args=backargs)),
                                   ])
    # process form
    preview = ''
    result= ''
    if emailform.validate(keepvalues=True):
        emailform.vars.ec_to = ', '.join(list(todict.keys()))
        if emailform.vars.ec_showonly:
            response.flash = T('Message not sent') +' !'
            preview=_show_email(emailform,config,convention,todict=todict,expandVariables=False)
        else:
            emailid = _save_email(emailform,expandVariables=False)
            result = _send_email(emailid, todict=todict)
            response.flash = T('Emails were sent, please check the result list below')
            preview=_show_email(emailform,config,convention,todict=todict,expandVariables=False,fakesignature=False)
    return dict(preview=preview, message=message, emailform=emailform,result=result,todict=todict)

@auth.requires(lambda: _logged_in_and_privacy_consent())
def registration():  # view individual registration
    convention=_get_convention_conname(request.args(0))
    config=db.ec_configuration[convention.ec_id_configuration]
    if not config: _redirect_message(T('This convention needs to be configured'), URL('convention',args=[convention.ec_short_name]))
    person=db.ec_person(request.args(1)) or db.ec_person(auth.user.active_person)
    if not person: _redirect_message(T('Invalid person'), URL('convention', args=[convention.ec_short_name]))
    if not (auth.has_permission('edit', db.ec_person, person.id) or _check_convention_permission('organize')):
        _redirect_message(T('You are not allowed to manage this person'), URL('convention', args=[convention.ec_short_name]))
    systempage = _get_systempage(convention, 'registration')
    title = T('Registration for %(ec_firstname)s %(ec_lastname)s ("%(ec_nickname)s")') % person.as_dict()
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    attendances=db((db.ec_attendance.ec_id_person==person.id)&(db.ec_attendance.ec_id_convention==convention.id)).select()
    if not attendances:
        session.flash=T('This person is not registered for %(ec_name)s')%convention.as_dict()
        if _check_is_open(convention.id):
            redirect(URL('convention','register',args=convention.ec_short_name, vars=request.vars))
        else:
            redirect(URL('convention','my_personae',args=convention.ec_short_name))
    if len(attendances)>1:
        session.flash=T('Error: This person has multiple registrations for %(ec_name)s. Please contact the organisators.')%convention.as_dict()
        redirect(URL('convention','orga_email',args=convention.ec_short_name, vars=request.vars))
    else:
        attendance=attendances[0]
    fields=_get_registration_fields(convention, attendance)
    form = SQLFORM.factory(*fields,readonly=True)
    message = 'View registration for %s at con %s' % (person.ec_nickname,convention.ec_name)
    query=(db.ec_buyitem.id==db.ec_buys_item.ec_id_buyitem)&(db.ec_buys_item.ec_id_person==person.id)&(db.ec_buyitem.ec_id_convention==convention.id)
    items=db(query).select(db.ec_buyitem.ALL,db.ec_buys_item.ALL,orderby=['ec_buys_item.ec_buy_state'])
    itemsum=_get_item_sum(convention,person.id)
    extraatts = db((db.ec_has_value.ec_id_attendance==attendance.id)&(db.ec_has_value.ec_id_extrafield==db.ec_extrafield.id)).select(orderby=[db.ec_extrafield.ec_order])

    ed = _get_email_dict(convention=convention,person=person)
    account = config.ec_registration_account % ed if config.ec_registration_account else ''
    services=db((db.ec_service.ec_id_convention==convention.id)&(db.ec_service.id==db.ec_helps.ec_id_service)&(db.ec_helps.ec_id_person==person.id)).select(db.ec_service.ALL)
    room=db((person.id==db.ec_stays_in_room.ec_id_person)&(db.ec_stays_in_room.ec_id_room==db.ec_bedroom.id)&(db.ec_bedroom.ec_id_convention==convention.id)).select(db.ec_bedroom.ALL).first()
    layout = 'print.html' if 'display' in request.vars and request.vars.display=='print' else 'layout.html'
    return dict(title=title,systempage=systempage,message=message,form=form,items=items,regperson=person,attendance=attendance,itemsum=itemsum,extraatts=extraatts,account=account,services=services,room=room,layout=layout)

@auth.requires(lambda: _logged_in_and_privacy_consent())
def registration_summary():  # show a summary of all registrations for an account
    convention=_get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    systempage = _get_systempage(convention, 'registration_summary')
    title = MV('registration_summary')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    config=db.ec_configuration[convention.ec_id_configuration]
    if not config: redirect(URL(configure_main, args=convention.ec_short_name))
    db.ec_person.id.readable, db.ec_person.id.writable=False, False
    db.ec_person.state = Field.Virtual('state', lambda row: A(T(Registration.get_attendee_status(convention.id, row.ec_person.id)),_href=URL('convention','registration',args=[convention.ec_short_name, row.ec_person.id])) if Registration.get_attendee_status(convention.id, row.ec_person.id) else '', label=T('Registration'))
    db.ec_person.boughtitems = Field.Virtual('boughtitems', lambda row: _get_bought_item_list(convention,row.ec_person.id), label=T('Bought items'))
    db.ec_person.topay = Field.Virtual('topay', lambda row: '%.2f %s' % (_get_item_sum(convention,row.ec_person.id), config.ec_registration_currency_symbol) if Registration.get_attendee_status(convention.id, row.ec_person.id) in REGISTRATION_STATE_CHOICES else '', label=T('Needs to pay'))
    db.ec_person.has_paid = Field.Virtual('has_paid', lambda row: '%.2f %s' % (db((db.ec_attendance.ec_id_person==row.ec_person.id)&(db.ec_attendance.ec_id_convention==convention.id)).select().first()['ec_payment'], config.ec_registration_currency_symbol) if Registration.get_attendee_status(convention.id, row.ec_person.id) in REGISTRATION_STATE_CHOICES else '', label=db.ec_attendance.ec_payment.label)
    fields = ['ec_firstname', 'ec_nickname', 'ec_lastname', 'state', 'boughtitems', 'topay', 'has_paid']
    dbfields = [db.ec_person[f] for f in fields]
    if config.ec_registration_data_attendance_departure in ['optional','required']: dbfields.insert(3, db.ec_attendance.ec_departure)
    if config.ec_registration_data_attendance_arrival in ['optional','required']: dbfields.insert(3, db.ec_attendance.ec_arrival)
    query=(db.ec_attendance.ec_id_person==db.ec_person.id)&(db.ec_attendance.ec_id_convention==convention.id)& auth.accessible_query('edit', db.ec_person)
    form=SQLFORM.grid(query,
        field_id = db.ec_person.id,
        args=request.args[:1],
        orderby=[db.ec_person.ec_firstname,db.ec_person.ec_lastname],
        fields=dbfields,
        searchable=False, csv=False, deletable=False, editable=False, create=False, details=False)
    total_topay = 0
    total_has_paid = 0
    for row in db(query).select():
        total_topay += _get_item_sum(convention,row.ec_person.id)
        total_has_paid += db((db.ec_attendance.ec_id_person==row.ec_person.id)&(db.ec_attendance.ec_id_convention==convention.id)).select().first()['ec_payment']
    layout = 'print.html' if 'display' in request.vars and request.vars.display=='print' else 'layout.html'
    return dict(form=form,title=title,systempage=systempage,total_topay=total_topay,total_has_paid=total_has_paid,config=config,layout=layout)

## organize controller functions
@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def organize():
    convention=_get_convention_conname(request.args(0))
    systempage = _get_systempage(convention, 'Organize')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, T('Organize'))
    config=db.ec_configuration[convention.ec_id_configuration]
    return dict(convention=convention,config=config,systempage=systempage)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def attendees():
    convention=_get_convention_conname(request.args(0))
    title=T('Manage attendees')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    config=db.ec_configuration(convention.ec_id_configuration)
    printview = 'display' in request.vars and request.vars.display=='print'
    for action in ['decline']:
        if action in request.args:
            person = db.ec_person[request.args(1)]
            registration = Registration(convention,person)
            result = registration.decline()
            session.flash = result
            redirect(URL('attendees', args=convention.ec_short_name))
    for action in ['hasallpaid', 'activate']: # TODO: replace with registration
        if action in request.args:
            person = db.ec_person[request.args(1)]
            result = _attendee_action(convention, person, action)
            session.flash = result
            redirect(URL('attendees', args=convention.ec_short_name))
    for action in ['attendee_paid_something']:
        if action in request.args:
            session.personid = request.args(1)
            person = db.ec_person[request.args(1)]
            redirect(URL(action, args=convention.ec_short_name))
    db.ec_attendance.id.readable,db.ec_attendance.id.writable=False,False
    db.ec_attendance.ec_id_person.readable,db.ec_attendance.ec_id_person.writable=False,False
    db.ec_attendance.ec_id_convention.readable,db.ec_attendance.ec_id_convention.writable=False,False
    # pre-generate needs_to_pay (otherwise grid does not work, error message:
    # "Query Not Supported: maximum recursion depth exceeded while calling a Python object"
    # vfield = Field.Virtual('v_needs_to_pay',
        # lambda row: '%.2f %s' % (_get_item_sum(convention,row.ec_attendance.ec_id_person)-row.ec_attendance.ec_payment, config.ec_registration_currency_symbol),
        # label=T('Needs to pay'),)
    pay_attendances = db(db.ec_attendance.ec_id_convention==convention.id).select(db.ec_attendance.ALL)
    pay = {}
    for p in pay_attendances:
        pay[p.id] = _get_item_sum(convention,p.ec_id_person)
    vfield = Field.Virtual('v_needs_to_pay', lambda row: '%.2f %s' % (pay[row.ec_attendance.id],config.ec_registration_currency_symbol), label=T('Needs to pay'),)
    db.ec_attendance.v_needs_to_pay = vfield
    db.ec_attendance.ec_payment.represent = lambda value, row: '%.2f %s' % (value, config.ec_registration_currency_symbol)
    db.ec_attendance.ec_id_person.represent = lambda value, row: '%s %s' % (db.ec_person[value].ec_firstname, db.ec_person[value].ec_lastname) if value else ''
    fields=['ec_id_person',
            'ec_registration_state',
            'ec_payment',
            'v_needs_to_pay',
            'ec_registertime',
           #'ec_arrival',
           #'ec_departure',
        ]
    dbfields = [getattr(db.ec_attendance, f) for f in fields]
    dbfields.insert(0,db.ec_person.ec_lastname)
    dbfields.insert(0,db.ec_person.ec_firstname)
    links=[
            dict(header='', body=lambda row: SPAN(A(T('paid all'), _class=('button'),
                                                   _href=URL('attendees',args=[convention.ec_short_name,row.ec_attendance.ec_id_person,'hasallpaid'], user_signature=True)),
                                                 SPAN(EX('action_paid_all'), _class="popup"),
                                                 _class="haspopup")),
            dict(header='',body=lambda row: SPAN(A(T('Manage attendee') + ' ...', _class=('button'),
                                                   _href=URL('manage_attendee',args=[convention.ec_short_name,row.ec_attendance.id])),
                                                 SPAN(EX('manage_attendee'), _class="popup"),
                                                 _class="haspopup")),
        ] if request.args(1) not in ['edit','view'] else None
    form=SQLFORM.grid(db.ec_attendance.ec_id_convention==convention.id,
        field_id = db.ec_attendance.id,
        left = db.ec_person.on(db.ec_person.id==db.ec_attendance.ec_id_person),
        args=request.args[:1],
        create=False, deletable=False, details=False, editable=False, searchable=not printview,
        fields=dbfields,
        orderby=[~db.ec_attendance.ec_registration_state,db.ec_person.ec_lastname],
        selectable=[(T('Send e-mail to selected people'), lambda ids : redirect(URL('convention', 'emails', args=convention.ec_short_name,
        vars=dict(tos=[db.ec_attendance[aid].ec_id_person for aid in ids])))),] if not printview else [],
        links=links if not printview else [],
        paginate=100,
        csv=False,
        )
    waitlist_people = db((db.ec_attendance.ec_id_convention==convention.id)&(db.ec_attendance.ec_registration_state=='waitlist')).count()
    waitlist_items = len(db((db.ec_buys_item.ec_id_buyitem==db.ec_buyitem.id)&\
                (db.ec_buys_item.ec_buy_state=='waitlist')&\
                (db.ec_buyitem.ec_has_waitlist==True)&\
                (db.ec_buys_item.ec_id_person==db.ec_attendance.ec_id_person)&\
                (db.ec_attendance.ec_registration_state.belongs(['registered','attendee']))&\
                (db.ec_attendance.ec_id_convention==db.ec_buyitem.ec_id_convention)).select(db.ec_buys_item.ec_id_person, distinct=True))
    there_are_waitlist_items = db((db.ec_buyitem.ec_has_waitlist==True)&(db.ec_buyitem.ec_id_convention==convention.id)).count() > 0
    layout = 'print.html' if printview else 'layout.html'
    return dict(form=form,title=title,waitlist_people=waitlist_people,waitlist_items=waitlist_items, there_are_waitlist_items=there_are_waitlist_items,layout=layout)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def manage_attendee():
    convention = _get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('Invalid configuration'), URL('convention', args=convention.ec_short_name))
    attendance = db.ec_attendance(request.args(1))
    registration = Registration(attendance=attendance)
    additionaldata = registration.show_additionaldata()
    readonly_att=True
    readonly_extra=True
    if 'edit_attendance' in request.args:
        readonly_att=False
    if 'edit_extra' in request.args:
        readonly_extra=False
    if not attendance or attendance.ec_id_convention!=convention.id: _redirect_message(T('Invalid registration'), URL('attendees', args=convention.ec_short_name))
    person = db.ec_person(attendance.ec_id_person)
    title = '%s: %s %s' % (T('Manage attendee'), person.ec_firstname, person.ec_lastname)
    registration = Registration(attendance=attendance)
    attendanceform = registration.get_attendance_form(readonly=readonly_att)
    items = registration.show_items()
    personform = registration.get_person_form(readonly=True)
    extras = registration.get_extras_form(readonly=readonly_extra)
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    if attendanceform.process().accepted:
        redirect(URL('manage_attendee',args=[convention.ec_short_name, attendance.id]))
    if extras.process().accepted:
        registration.save_extras(extras)
        redirect(URL('manage_attendee',args=[convention.ec_short_name, attendance.id]))
    return dict(title=title, attendanceform=attendanceform, items=items, attendance=attendance, extras=extras,  personform=personform, additionaldata=additionaldata)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def attendee_paid_something():
    convention=_get_convention_conname(request.args(0))
    config=db.ec_configuration(convention.ec_id_configuration)
    person=db.ec_person(session.personid)
    db.ec_attendance.ec_registration_state.writable=False
    db.ec_attendance.ec_id_person.represent = lambda value, row: '%s %s' % (db.ec_person[value].ec_firstname, db.ec_person[value].ec_lastname) if value else ''
    attendance=_get_attendance(convention, session.personid)
    if not person or not attendance:
        session.flash=T('Invalid person')
        session.personid=None
        redirect(URL('attendees',args=convention.ec_short_name))
    title='%s: %s %s %s' % (T('Manage attendees'),person.ec_firstname, person.ec_lastname, T('paid something'))
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    fields=['ec_id_person',
            'ec_registration_state',
            'ec_registertime',
            'ec_payment',
    ]
    form=SQLFORM(db.ec_attendance, record=attendance,
        fields=fields,
        showid=False,
        col3=dict(ec_payment=config.ec_registration_currency_symbol),
        buttons = [
            TAG.button(T('Save'),_type="submit"),
            TAG.button(T('Cancel'),_type="button",_onClick = "parent.location='%s' " % URL('attendees',args=convention.ec_short_name)),
            ]
        )
    if form.process().accepted:
        attendance = db.ec_attendance(form.vars.id)
        person = db.ec_person(attendance.ec_id_person)
        messages = [T('Payment changed for %(ec_firstname)s %(ec_lastname)s, no email was sent',lazy=False) % person.as_dict(),]
        if attendance.ec_registration_state=='registered':
            messages.append(T('Maybe you want to activate the registration now?',lazy=False))
        session.flash = '. '.join(messages)
        redirect(URL('attendees',args=convention.ec_short_name))
    return dict(form=form,title=title)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def attendee_items():
    convention=_get_convention_conname(request.args(0))
    config=db.ec_configuration(convention.ec_id_configuration)
    action = request.vars.action if 'action' in request.vars and request.vars.action in ATTENDEE_ITEM_ACTIONS else 'view'

    # check attendance and set title
    attendance = db.ec_attendance(request.vars.attid)
    if not attendance or (attendance.ec_id_convention!=convention.id):
        attid=request.vars.attid
        _redirect_message(T('Invalid registration'), URL('attendees', args=convention.ec_short_name))
    registration = Registration(attendance=attendance)
    attendee = db.ec_person(attendance.ec_id_person)

    title='%s: %s %s %s %s' % (T('Manage attendees'), T('Bought items'), T('of'), attendee.ec_firstname, attendee.ec_lastname)
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)

    # prepare standard form
    db.ec_attendance.ec_registration_state.writable=False
    db.ec_attendance.ec_id_person.represent = lambda value, row: '%s %s' % (db.ec_person[value].ec_firstname, db.ec_person[value].ec_lastname) if value else ''
    itemfields = ['id','ec_order','ec_name','ec_price','ec_is_perday','ec_has_waitlist']
    db.ec_buyitem.ec_is_perday.label = short_label('ec_is_perday')
    db.ec_buyitem.id.readable,db.ec_buyitem.id.writable=False,False
    db.ec_buyitem.ec_has_waitlist.readable,db.ec_buyitem.ec_has_waitlist.writable=False,False
    dbfields = [getattr(db.ec_buyitem, f) for f in itemfields]
    db.ec_buys_item.ec_buy_state.represent = lambda value, row: MV(value)
    db.ec_buyitem.ec_price.represent = lambda value, row: '%.2f %s' % (value, config.ec_registration_currency_symbol)
    dbfields.insert(3, db.ec_buys_item.ec_buy_state)
    # virtual field for total price (if bought):
    # vfield = Field.Virtual('v_to_pay', lambda row: '%.2f %s' % (_get_to_pay(row.ec_buyitem.id,attendance), config.ec_registration_currency_symbol) if row.ec_buys_item.ec_buy_state=='booked' else '')
    # otal = _get_item_sum(convention,attendee.id)
    # vfield.label = '%s (%.2f %s)' % (T('Needs to pay'), total, config.ec_registration_currency_symbol)
    # db.ec_buyitem.v_to_pay = vfield
    # dbfields.append(db.ec_buyitem.v_to_pay)
    links=[
            dict(header='-1: %s' % T('unlimited'),body=lambda row: A('%s (%s)' % (T('buy'),T('%s left') % _free_items(db.ec_buyitem(row.ec_buyitem.id))), _class=('button'),_href=URL('attendee_items',args=request.args, vars=dict(action='booked', item=row.ec_buyitem.id, attid=attendance.id), user_signature=True))),
            dict(header='',body=lambda row: A(T('unbuy'), _class=('button'),_href=URL('attendee_items',args=request.args, vars=dict(action='declined',item=row.ec_buyitem.id,attid=attendance.id), user_signature=True))),
            dict(header='',body=lambda row: A(T('on waitlist'), _class=('button'),_href=URL('attendee_items',args=request.args, vars=dict(action='waitlist',item=row.ec_buyitem.id,attid=attendance.id), user_signature=True)) if row.ec_buyitem.ec_has_waitlist else ''),
            dict(header='',body=lambda row: A(T('delete entry'), _class=('button'),_href=URL('attendee_items',args=request.args, vars=dict(action='delete',item=row.ec_buyitem.id,attid=attendance.id), user_signature=True))),
        ]

    # React on single item action
    if action in ['booked', 'declined', 'delete', 'waitlist']:
        requestvars = dict(attid=attendance.id)
        mode = request.vars.get('mode' or '') or ''
        if mode == 'moveup':
            requestvars['action'] = 'moveup_waitlist'
        try:
            item = db.ec_buyitem(request.vars.item)
        except:
            _redirect_message(T('Invalid item'), URL('convention','attendees',args=[convention.ec_short_name]))
        if action=='delete':
                db((db.ec_buys_item.ec_id_person==attendee.id) & (db.ec_buys_item.ec_id_buyitem==item.id)).delete()
                session.flash = T('Buy entry for item %s deleted') % item.ec_name
                redirect(URL('attendee_items', args=convention.ec_short_name, vars=requestvars))
        else:
            bookingstate = action
            db.ec_buys_item.update_or_insert((db.ec_buys_item.ec_id_person==attendee.id) & (db.ec_buys_item.ec_id_buyitem==item.id), ec_id_person=attendee.id, ec_id_buyitem=item.id, ec_buy_state=bookingstate)
            session.flash = T('State of item %s changed to %s') % (item.ec_name, MV(bookingstate))
            redirect(URL('attendee_items', args=convention.ec_short_name, vars=requestvars))
    # Handle moveup waitlist action
    if action=='moveup_notify':
        if attendance.ec_registration_state != 'registered':
            _redirect_message(T('Invalid registration'), URL('attendees', args=convention.ec_short_name))
        message = _send_auto_email('registration_waitlist_movedup_waitlist', convention=convention, person=attendee)
        _redirect_message(message, URL('convention','manage_waitlist',args=[convention.ec_short_name]))
    if action == 'moveup_waitlist':
        title = '%s: %s "%s" %s' % (T('Moveup and register'), attendee.ec_firstname, attendee.ec_nickname, attendee.ec_lastname)
        moveup_report = registration.moveup_from_waitlist()
        required_item_report = registration.check_required_items()
        attendance.ec_registration_state = registration.attendancedata['ec_registration_state']
        moveup_email = P(T('If you notify %s now, he*she will get the following email') % attendee.ec_firstname +':')
        mailstring='mailto:%s?subject=%s %s' % (attendee.ec_email, config.ec_email_tag % {'conname': convention.ec_name}, config.ec_email_registration_waitlist_movedup_waitlist_subject)
        if config.ec_email_registration_waitlist_movedup_waitlist_bcc:
            mailstring +='?bcc=%s' % config.ec_email_registration_waitlist_movedup_waitlist_bcc
        moveup_email += CAT(HR(),
            PRE(_show_auto_email_text('registration_waitlist_movedup_waitlist', convention=convention, person=attendee)),
            HR(),
            A(T('Notify attendee with this email'), _href=URL('attendee_items',args=[convention.ec_short_name], vars=dict(attid=attendance.id, action='moveup_notify')), _class='linkbutton'),
            A(T('Write your own email'), _href=mailstring, _class='linkbutton'),
            P(T('Before you do that, you might want to adapt the booked items')+':'))
        # redefine links for grid to stay in moveup_waitlist mode
        links=[
            dict(header='-1: %s' % T('unlimited'),body=lambda row: A('%s (%s)' % (T('buy'),T('%s left') % _free_items(db.ec_buyitem(row.ec_buyitem.id))), _class=('button'),_href=URL('attendee_items',args=request.args, vars=dict(action='booked', item=row.ec_buyitem.id, attid=attendance.id, mode='moveup'), user_signature=True))),
            dict(header='',body=lambda row: A(T('unbuy'), _class=('button'),_href=URL('attendee_items',args=request.args, vars=dict(action='declined',item=row.ec_buyitem.id,attid=attendance.id, mode='moveup'), user_signature=True))),
            dict(header='',body=lambda row: A(T('on waitlist'), _class=('button'),_href=URL('attendee_items',args=request.args, vars=dict(action='waitlist',item=row.ec_buyitem.id,attid=attendance.id, mode='moveup'), user_signature=True)) if row.ec_buyitem.ec_has_waitlist else ''),
            dict(header='',body=lambda row: A(T('delete entry'), _class=('button'),_href=URL('attendee_items',args=request.args, vars=dict(action='delete',item=row.ec_buyitem.id,attid=attendance.id, mode='moveup'), user_signature=True))),
        ]

    else:
        moveup_report = ''
        moveup_email = ''
        required_item_report = registration.check_required_items()
    # generate form
    form=SQLFORM.grid(db.ec_buyitem.ec_id_convention==convention.id,
        field_id = db.ec_buys_item.id,
        maxtextlengths = {'db.ec_buyitem.ec_name': 150},
        left = db.ec_buys_item.on((db.ec_buys_item.ec_id_buyitem==db.ec_buyitem.id)&(db.ec_buys_item.ec_id_person==attendee.id)),
        args=request.args[:1],
        create=False, deletable=False, details=False, editable=False,
        fields=dbfields,
        orderby=[db.ec_buys_item.ec_buy_state, db.ec_buyitem.ec_order],
        headers={'ec_buyitem.ec_order': '#'},
        links=links,
        paginate=100,
        csv=False,
        searchable=False,
        )
    total = _get_item_sum(convention,attendee.id)
    return dict(form=form,title=title,attendance=attendance,moveup_report=moveup_report,required_item_report=required_item_report,moveup_email=moveup_email)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def show_helpers():
    convention=_get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    config=db.ec_configuration[convention.ec_id_configuration]
    if not config: _redirect_message(T('You have to copy or create a configuration for your convention'), URL('configure_main',args=[convention.ec_short_name]))
    service = db.ec_service(request.args(1))
    if not service or service.ec_id_convention!=convention.id: _redirect_message(T('Invalid service'), URL('convention', 'convention', args=[convention.ec_short_name]))
    title=T('Show helpers of service %s') % service.ec_name
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    helpers = db((db.ec_helps.ec_id_service==service.id)&(db.ec_helps.ec_id_person==db.ec_person.id)).select(db.ec_person.ALL)
    return dict(title=title,service=service,helpers=helpers)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def services():
    convention=_get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    title=T('Services')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    config=db.ec_configuration[convention.ec_id_configuration]
    if not config: _redirect_message(T('You have to copy or create a configuration for your convention'), URL('configure_main',args=[convention.ec_short_name]))
    db.ec_service.id.readable, db.ec_service.id.writable=False, False
    db.ec_service.ec_id_convention.readable, db.ec_service.ec_id_convention.writable = False, False
    db.ec_service.ec_id_convention.default=convention.id
    db.ec_service.ec_id_timeslot.requires = IS_EMPTY_OR(IS_IN_SET(_get_timeslots(convention)))
    configform = SQLFORM(db.ec_configuration, config.id, fields=['ec_convention_requires_services'], showid=False,comments=True,submit_button=T('Save'))
    if configform and configform.process(formname='configform').accepted:
        session.flash = T('Configuration for "%(ec_name)s" changed') % {'ec_name' : convention.ec_name}
        redirect(URL('services',args=convention.ec_short_name))
    fields = ['ec_name', 'ec_description', 'ec_min_helpers', 'ec_max_helpers', 'v_helper_count', 'v_helpers', 'ec_start_time', 'ec_end_time']
    dbfields = [db.ec_service[f] for f in fields]
    headers={}
    for f in fields:
        headers['ec_service.%s' % f] = short_label(f)
    links=[dict(header='', body=lambda row: A(T('Register someone else for service'), _href=URL('choose_or_create_person',args=[convention.ec_short_name,'service'], vars=dict(sid=row.id)), _class='buttontext button')),
           dict(header='', body=lambda row: A(T('Show helpers'), _href=URL('show_helpers',args=[convention.ec_short_name,row.id]), _class='buttontext button'))]
    form=SQLFORM.grid(db.ec_service.ec_id_convention==convention.id,
        args=request.args[:1],
        fields=dbfields,
        headers=headers,
        maxtextlength=300,
        onvalidation=_validate_service,
        orderby=[db.ec_service.ec_order],
        searchable=False, csv=False,
        links=links)
    return dict(form=form,title=title,configform=configform)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def emails(): # sends e-mails to specified groups or users (orga only)
    convention=_get_convention_conname(request.args(0))
    config = db.ec_configuration[convention.ec_id_configuration]
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, T('Emails'))
    sendto=request.get_vars['tos'] or []
    if not isinstance(sendto, list): sendto = [sendto,]
    groupto = request.get_vars['grouptos'] or []
    if not isinstance(groupto, list): groupto = [groupto,]
    emailto=request.get_vars['emailtos'] or []
    if not isinstance(emailto, list): emailto = [emailto,]
    autoemail = request.get_vars['autoemail'] or []
    # create form
    db.ec_email.ec_id_convention.default=convention.id
    tos=[]
    for personid in sendto:
        person = db.ec_person[personid]
        if person:
            tos.append(person.ec_email)
    for email in emailto:
        if IS_EMAIL(email):
            tos.append(email)
    db.ec_email.ec_from.default = mail.settings.sender
    db.ec_email.ec_to.default = ', '.join(tos)
    db.ec_email.ec_to.requires = IS_EMPTY_OR(IS_EMAIL_CSV_LIST())
    person = db.ec_person(auth.user.active_person) or _get_or_create_first_person_auth()
    if autoemail in ['promote_orga_group','promote_admin_group']:
        user = db(db.auth_user.email==tos[0]).select().first()
        if getattr(config,'ec_email_%s_subject' % autoemail):
            db.ec_email.ec_subject.default = config.ec_email_tag % _get_email_dict(convention=convention) + ' ' + getattr(config,'ec_email_%s_subject' % autoemail)
        if getattr(config,'ec_email_%s_text' % autoemail):
            db.ec_email.ec_text.default =  getattr(config,'ec_email_%s_text' % autoemail) % _get_email_dict(convention=convention,user=user)
        db.ec_email.ec_cc.default = auth.user.email
    else:
        tag = config.ec_email_tag % _get_email_dict(convention=convention) if config.ec_email_tag else ''
        db.ec_email.ec_subject.default = tag + ' ' + request.vars.subject if request.vars.subject else ''
    db.ec_email.ec_subject.requires = IS_NOT_EMPTY()
    db.ec_email.ec_replyto.default = person.ec_email

    # find out which expand variables are defined
    explain_variables = "%s: %s" % (T('You can use the following variables'), ','.join(_get_valid_email_variables(convention=convention)))

    # Add explanations to form
    col3 = {
        'ec_to': T('Easy-Con will send an invididual email to each of these addresses (but only one)'),
        'ec_subject': explain_variables,
        'ec_text': explain_variables
    }

    emailform = SQLFORM(db.ec_email, submit_button=T('Send'), col3=col3)
    if autoemail not in ['promote_orga_group','promote_admin_group']:
        for group in AUTO_GROUPS:
            size = len(_get_email_autogroup(convention,group))
            group_element = TR(LABEL(T('To')+ ' "%s":'%T(group)), INPUT(_name='%s_group'%group,value=(group in groupto),_type='checkbox'),T('# of entries: %s') % size)
            emailform[0].insert(-7,group_element)
    # process form
    message=''
    result=''
    if emailform.validate(keepvalues=True):
        to_emails=set(emailform.vars.ec_to.split(',')) if emailform.vars.ec_to else set()
        for group in AUTO_GROUPS:
            if getattr(emailform.vars,'%s_group'%group):
                to_emails = to_emails | set(_get_email_autogroup(convention,group))
        emailform.vars.ec_to = ','.join(to_emails)
        if not emailform.vars.ec_to:
            response.flash = T('No receiver specified')
        elif emailform.vars.ec_showonly:
            response.flash = T('Message not sent') +' !'
            message=_show_email(emailform,config,convention,fakesignature=False,expandVariables=True)
        else:
            try:
                emailid = _save_email(emailform,convention=convention,expandVariables=True)
                result = _send_email(emailid)
                response.flash = T('Emails were sent, please check the result list below')
            except KeyError as e:
                response.flash = T('Message not sent')
            message=_show_email(emailform,config,convention,fakesignature=False,expandVariables=True)
            if result: emailform = A(T('write another email'), _class=('button'),_href=URL('emails',args=[convention.ec_short_name]))
    return dict(message=message,emailform=emailform,result=result)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def data():
    # export data
    convention=_get_convention_conname(request.args(0))
    config=db.ec_configuration[convention.ec_id_configuration]
    if not config: _redirect_message(T('You have to copy or create a configuration for your convention'), URL('configure_main',args=convention.ec_short_name))
    what=request.args(1) if request.args(1) in DATA_EXPORT_CHOICE else None
    title=MV('data')
    if what and what in DATA_EXPORT_CHOICE:
        title = '%s: %s' % (title, MV('%ss'%what))
    if what and what=='convention_config':
        redirect(URL('export_config', args=[convention.ec_short_name]))
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    fields, dbfields = [], []
    data_export_format = DATA_EXPORT_FORMAT_CHOICE # options for all things
    checked = 'checked' in request.vars
    if what == 'event':
        fields = [
                    'ec_name',
                    'ec_type',
                    'ec_min_attendees',
                    'ec_max_attendees',
                    'ec_apply_online',
                    'ec_prepared_chars',
                    'ec_introduction',
                    'ec_rpg_newbees',
                    'ec_registertime',
                    'ec_url',
                    'ec_description',
                    'ec_id_rpggame',
                    'registration_lines',
                    'ec_id_pdf',
                    ]
        if config.ec_event_free_time:
            fields.append('ec_start')
            fields.append('ec_end')
        if config.ec_event_time_slots:
            fields.append('ec_id_timeslot')
        dbfields = [Field(f, 'boolean', default=checked, label=short_label(f) if short_label(f)!=f else db.ec_event[f].label) for f in fields]
        if config.ec_event_tables:
            dbfields.append(Field('table', 'boolean', default=checked, label=short_label('table')))
        dbfields.append(Field('attendees', 'boolean', default=checked, label=short_label('attendees')))
        dbfields.insert(0,Field('organized_by', 'boolean', default=checked,  label=short_label('organized_by')))
        data_export_format.append('pdf')
    elif what == 'empty_event':
        fields = [
                    'ec_type',
                    'ec_id_rpggame',
                    'ec_start',
                    'ec_end',
                    'ec_min_attendees',
                    'ec_max_attendees',
                    'ec_prepared_chars',
                    'ec_introduction',
                    'ec_rpg_newbees',
                    'ec_description',
                    'ec_id_pdf',
                    ]
        if config.ec_event_time_slots:
            fields.append('ec_id_timeslot')
        dbfields = [Field(f, 'boolean', default=checked, label=short_label(f) if short_label(f)!=f else db.ec_event[f].label) for f in fields]
        dbfields.append(Field('attendees', 'boolean', default=checked, label=short_label('attendees')))
        dbfields.append(Field('attendee_count', 'integer', default=5, label=T('How many lines for attendees?')))
        dbfields.insert(0,Field('organized_by', 'boolean', default=checked,  label=short_label('organized_by')))
        data_export_format = ['pdf']
    elif what=='attendee':
        fields = [
                'ec_firstname',
                'ec_lastname',
                'ec_nickname',
                'ec_email',
                ]
        for cf in db.ec_configuration.fields:
            if cf.startswith('ec_registration_data_person_'):
                if config[cf] != 'do_not_ask':
                    fields.append('ec_%s' % cf[len('ec_registration_data_person_'):])
        dbfields = [Field(f, 'boolean', default=checked, label=short_label(f) if short_label(f)!=f else db.ec_person[f].label) for f in fields]
        extras = db((db.ec_extrafield.ec_id_convention==convention.id) &\
                   (db.ec_extrafield.ec_entity=='person')).select(db.ec_extrafield.ALL)
        for e in extras:
            dbfields.append(Field('extra_%s' % e.id, 'boolean', default=checked, label=e.ec_name))
        attfields = ['ec_registration_state',
                    'ec_registertime',
                    'ec_arrival',
                    'ec_departure',
                    'ec_attendance_comment',
                    'ec_payment',
                    ]
        for f in attfields:
            dbfields.append(Field(f, 'boolean', default=checked, label=short_label(f) if short_label(f)!=f else db.ec_attendance[f].label))
        extras = db((db.ec_extrafield.ec_id_convention==convention.id) &\
                   (db.ec_extrafield.ec_entity=='attendance')).select(db.ec_extrafield.ALL)
        for e in extras:
            dbfields.append(Field('extra_%s' % e.id, 'boolean', default=checked, label=e.ec_name))
        dbfields.append(Field('days', 'boolean', default=checked, label=short_label('days')))
        dbfields.append(Field('needs_to_pay', 'boolean', default=checked, label=short_label('needs_to_pay')))
        dbfields.append(Field('buyitems', 'boolean', default=checked, label=short_label('buyitems')))
        dbfields.append(Field('buyitems_as_columns', 'boolean', default=checked, label=short_label('buyitems_as_columns')))
        if config.ec_convention_requires_services:
            dbfields.append(Field('services', 'boolean', default=checked, label=MV('services')))
        dbfields.append(Field('bedrooms', 'boolean', default=checked, label=MV('bedrooms')))
    elif what=='host':
        fields = [
                'ec_firstname',
                'ec_lastname',
                'ec_nickname',
                'ec_email',
                ]
        dbfields = [Field(f, 'boolean', default=checked, label=short_label(f) if short_label(f)!=f else db.ec_person[f].label) for f in fields]
        dbfields.append(Field('events', 'boolean', default=checked, label=T('Events')))
    elif what=='boughtitem':
        dbfields=[]
        buyable_items_DB=db(db.ec_buyitem.ec_id_convention==convention.id).select('id', 'ec_name', 'ec_order')
        buyable_items=[]
        buyable_items.append(('-1',T('All')))
        for row in buyable_items_DB.sort(lambda row: row.ec_order):
            buyable_items.append((row['id'],row['ec_name']))
        items_dropdown=IS_IN_SET(buyable_items, zero=None, multiple=True)
        dbfields.append(Field('ids', 'string', label=T('Which items?'), requires=items_dropdown))
        fields = [
                'ec_name',
                'ec_is_perday',
                'ec_price',
                'ec_capacity',
                ]
        for f in fields:
            dbfields.append(Field(f, 'boolean', default=checked, label=db.ec_buyitem[f].label))
        dbfields.append(Field('item_booked_attendee_count', 'boolean', default=checked, label=MV('item_booked_attendee_count')))
        dbfields.append(Field('item_booked_registered_count', 'boolean', default=checked, label=MV('item_booked_registered_count')))
        dbfields.append(Field('item_waitlist_attendee_count', 'boolean', default=checked, label=MV('item_waitlist_attendee_count')))
        dbfields.append(Field('item_waitlist_registered_count', 'boolean', default=checked, label=MV('item_waitlist_registered_count')))
        dbfields.append(Field('item_waitlist_waitlist_count', 'boolean', default=checked, label=MV('item_waitlist_waitlist_count')))
        dbfields.append(Field('item_not_buy_count', 'boolean', default=checked, label=MV('item_not_buy_count')))
        dbfields.append(Field('item_booked_attendee_names', 'boolean', default=checked, label=MV('item_booked_attendee_names')))
        dbfields.append(Field('item_booked_registered_names', 'boolean', default=checked, label=MV('item_booked_registered_names')))
        dbfields.append(Field('item_waitlist_attendee_names', 'boolean', default=checked, label=MV('item_waitlist_attendee_names')))
        dbfields.append(Field('item_waitlist_registered_names', 'boolean', default=checked, label=MV('item_waitlist_registered_names')))
        dbfields.append(Field('item_waitlist_waitlist_names', 'boolean', default=checked, label=MV('item_waitlist_waitlist_names')))
        dbfields.append(Field('item_not_buy_names', 'boolean', default=checked, label=MV('item_not_buy_names')))

        dbfields.append(Field('extra_text_pdf_only', 'text', label=MV('extra_text_pdf_only')))
        data_export_format.append('pdf')
    elif what=='bedroom':
        fields = [ 'ec_name', 'ec_capacity', 'ec_category', 'ec_comment']
        dbfields = [Field(f, 'boolean', default=checked, label=db.ec_bedroom[f].label) for f in fields]
        dbfields.append(Field('attendees', 'boolean', default=checked, label=T('Attendees')))
        dbfields.append(Field('occupied_from', 'boolean', default=checked, label=T('Occupied from')))
        dbfields.append(Field('occupied_until', 'boolean', default=checked, label=T('Occupied until')))
    elif what=='service':
        fields = ['ec_name', 'ec_description', 'ec_min_helpers', 'ec_max_helpers']
        dbfields = [Field(f, 'boolean', default=checked, label=db.ec_service[f].label) for f in fields]
        dbfields.append(Field('helpers', 'boolean', default=checked, label=T('Helpers')))
    elif what=='table':
        fields = [  'ec_name',
                    'ec_location',
                    'ec_picture',
                    'ec_min_attendees',
                    'ec_max_attendees',
                    'ec_available_begin',
                    'ec_available_end',
                    'ec_setup',
                    'ec_comment',
        ]
        dbfields = [Field(f, 'boolean', default=checked, label=db.ec_table[f].label) for f in fields]
        dbfields.append(Field('events', 'boolean', default=checked, label=T('Events')))
        dbfields.append(Field('events_with_time', 'boolean', default=checked, label=T('Events with time')))
    elif what=='special_needs_per_item':
        fields = ['ec_special_needs','ec_vegetarians','ec_attendee_count',]
        dbfields = [Field(f, 'boolean', default=checked, label=T(MV(f))) for f in fields]
        dbfields.append(Field('ec_attendee_names', 'boolean',
            label=MV('ec_attendee_names'))) # unchecked for privacy by default
        fields = ['include_registered']
        for f in fields:
            dbfields.append(Field('include_registered', 'boolean', default=checked, label=T(MV(f))))
        buyitems = db(db.ec_buyitem.ec_id_convention==convention.id).select()
        for item in buyitems:
            dbfields.append(Field('item_%s' % item['id'], 'boolean', default=checked,
                label='%s: %s' % (T('Item'),item['ec_name'])))
    elif what and what in DATA_EXPORT_CHOICE:
        response.flash = '%s: %s' % (T('Not implemented yet'), MV('%ss'%what))
    form = None
    if fields:
        dbfields.append(Field('export_format', 'string', label=T('Export format'), requires=IS_IN_SET([(eo, MV(eo)) for eo in data_export_format], zero=None)))
        form = SQLFORM.factory(*dbfields, submit_button=MV('data'))
    if form and form.process(formname='choiceform', keepvalues=True).accepted:
        export=[]
        oid=[]
        if 'ids' in form.fields:
            oid=form.vars['ids']
        for f in form.fields:
            if form.vars.export_format!='pdf' and f.endswith('_pdf_only'):
                continue
            if getattr(form.vars, f) and f not in ['export_format', 'attendee_count', 'ids']:
                export.append(f)
        if form.vars.export_format=='tsv':
            redirect(URL('export.tsv', args=[convention.ec_short_name,what], vars=dict(export=export,oid=oid)))
        elif form.vars.export_format=='csv':
            redirect(URL('export.csv', args=[convention.ec_short_name,what], vars=dict(export=export,oid=oid)))
        elif form.vars.export_format=='pdf' and what == 'boughtitem':
            redirect(URL('export.pdf', args=[convention.ec_short_name,what],
                vars=dict(export=export,extra_text_pdf_only=form.vars.extra_text_pdf_only,oid=oid)))
        elif form.vars.export_format=='pdf' and what in ['event']:
            redirect(URL('export.pdf', args=[convention.ec_short_name,what], vars=dict(export=export,oid=oid)))
        elif form.vars.export_format=='pdf' and what == 'empty_event':
            if 'attendees' in export:
                redirect(URL('export.pdf', args=[convention.ec_short_name,what],
                    vars=dict(export=export,oid=oid,attendee_count=form.vars.attendee_count)))
            else:
                redirect(URL('export.pdf', args=[convention.ec_short_name,what],
                    vars=dict(export=export,oid=oid)))
        elif form.vars.export_format!='htmltable':
            session.flash=T('Unknown format, export as HTML table')
        redirect(URL('export', args=[convention.ec_short_name,what], vars=dict(export=export,oid=oid)))
    return dict(title=title, what=what, form=form)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def export():
    convention=_get_convention_conname(request.args(0))
    config=db.ec_configuration[convention.ec_id_configuration]
    if not config: _redirect_message(T('You have to copy or create a configuration for your convention'), URL('configure_main',args=convention.ec_short_name))
    what=request.args(1) if request.args(1) in DATA_EXPORT_CHOICE else None
    if not what: _redirect_message(T('Invalid parameters'), URL('data',args=convention.ec_short_name))
    title = '%s: %s %s' % (T('Result'), MV('data'), MV('%ss'%what))
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    fields = request.vars.export
    if fields and not isinstance(fields, list): fields = [fields,]
    if what == 'empty_event':
        attendee_count = int(request.vars.attendee_count) if request.vars.attendee_count else None
        return _export_empty_pdf(convention, fields, attendee_count)
    # match the IDs from the request vars
    ids=None
    if request.vars.oid:
        try:
            str_ids=request.vars.oid
            if not isinstance(str_ids, list): str_ids = [str_ids,]
            ids = [int(id) for id in str_ids]
            if -1 in ids: ids=None
        except:
            _redirect_message(T('Invalid parameters'), URL('data',args=convention.ec_short_name))
    # get the data
    layout = 'print.html' if 'display' in request.vars and request.vars.display=='print' else 'layout.html'
    if request.extension=="pdf":
        filename = '%s-export-%s-%s.pdf' % (convention.ec_short_name, what, request.now.strftime(dateformatstring))
        content = {}
        if what == 'boughtitem':
            content['extra_text_pdf_only'] = request.vars.extra_text_pdf_only
        result = _get_export_result(convention, what, fields, ids, html=False)
        return _export_pdf(convention, result, what, content)
    elif request.extension=="html":
        result = _get_export_result(convention, what, fields, ids, html=True)
    else:
        result = _get_export_result(convention, what, fields, ids, html=False)
    return dict(title=title, what=what, result=result, params=request.vars, layout=layout)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def pages():
    convention=_get_convention_conname(request.args(0))
    config=db.ec_configuration(convention.ec_id_configuration)
    title = MV('pages')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    db.ec_page.ec_id_convention.default = convention.id
    person = db.ec_person(auth.user.active_person) or _get_or_create_first_person_auth()
    db.ec_page.ec_created_by.default = person.id
    db.ec_page.id.readable, db.ec_page.id.writable=False, False
    db.ec_page.ec_systempage.represent = lambda value, row: '%s: %s' % (T('Yes'), MV(value)) if value!='No' else MV(value)
    db.ec_page.ec_name.represent=lambda x, row: A(x, _href=URL('convention','page', args=[convention.ec_short_name,x])) if x else ''
    db.ec_page.ec_name.label += '*'
    fields = [
            'ec_order',
            'ec_name',
            'ec_title',
            'ec_created_by',
            'ec_created_on',
            'ec_systempage',
            'ec_menu',
            'ec_before_menu_entry',
            'ec_hidden',
            'ec_public',
        ]
    dbfields=[getattr(db.ec_page,field) for field in fields]
    col3 = {'col3': {'ec_body': XML(T('You can use %s to layout your description') % A('Markmin', _target='new', _href=URL('convention','markmin', args=convention.ec_short_name))),}}
    result=SQLFORM.grid(db.ec_page.ec_id_convention==convention.id,
        fields=dbfields,
        args=request.args[:1],
        orderby=[db.ec_page.ec_order,db.ec_page.ec_title,],
        maxtextlengths = {'ec_page.ec_systempage': 150, 'ec_page.ec_title': 100},
        csv=False,
        onvalidation = _validate_page,
        editargs = col3,
        viewargs = col3,
        createargs = col3,
        )
    return dict(title=title,result=result)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def files(): # manages attachments; upload is separate to check for images
    convention=_get_convention_conname(request.args(0))
    config=db.ec_configuration(convention.ec_id_configuration)
    title = MV('files')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    db.ec_file.ec_id_convention.default = convention.id
    db.ec_file.ec_uploaded_by.default = auth.user.id
    db.ec_file.ec_uploaded_by.writable = False
    db.ec_file.id.readable, db.ec_file.id.writable=False, False
    db.ec_file.ec_is_image.readable = not(('edit' in request.args) or ('view' in request.args))
    db.ec_file.ec_is_image.writable = False
    db.ec_file.ec_file.writable=False
    db.ec_file.ec_file.readable=True
    db.ec_file.ec_name.writable=True
    fields = [
        'id',
        'ec_name',
        'ec_size',
        'ec_uploaded_on',
        'ec_uploaded_by',
        'ec_is_image',
        'ec_public',
        'ec_hidden',
        ]
    dbfields=[getattr(db.ec_file,field) for field in fields]
    headers = {}
    for field in ['ec_is_image','ec_public','ec_hidden']:
        headers['ec_file.%s' % field] = short_label(field)
    form=SQLFORM.grid(db.ec_file.ec_id_convention==convention.id,
        fields=dbfields,
        args=request.args[:1],
        headers = headers,
        orderby=[db.ec_file.ec_name,],
        csv=False, create=False,
        editargs = {'col3': {'ec_name': T('When you change the filename, do not change the extension')}},
        upload=URL('default','download'),
        links=[dict(header='', body=lambda row: A(T('Download'), _href=db.ec_file(row.id).ec_url, _class='buttontext button'))]
        )
    size_sum = db.ec_file.ec_size.sum()
    quota = db(db.ec_file.ec_id_convention==convention.id).select(size_sum).first()[size_sum]
    count = db.ec_file.ec_name.count()
    doubles = db(db.ec_file.ec_id_convention==convention.id).select(db.ec_file.ec_name, count, groupby=db.ec_file.ec_name, having=count>1)
    double=[]
    for d in doubles:
        double.append(d.ec_file.ec_name)
    return dict(title=title,form=form,quota=quota,double=double)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def upload():
    convention=_get_convention_conname(request.args(0))
    config=db.ec_configuration(convention.ec_id_configuration)
    is_image = ('image' in request.vars)
    title = T('Upload image') if is_image else T('Upload file')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    db.ec_file.ec_id_convention.default = convention.id
    db.ec_file.ec_uploaded_by.default = auth.user.id
    db.ec_file.id.readable, db.ec_file.id.writable=False, False
    db.ec_file.ec_uploaded_by.readable, db.ec_file.ec_uploaded_by.writable=False, False
    db.ec_file.ec_is_image.readable = True
    db.ec_file.ec_file.writable=True
    db.ec_file.ec_name.writable=False
    if is_image:
        #db.ec_file.ec_file.requires = IS_IMAGE(extensions=IMAGE_EXTENSIONS)  # does not work. Why??? Hack with _validate_image_upload
        db.ec_file.ec_file.requires = IS_IMAGE()
        db.ec_file.ec_is_image.default = True
        db.ec_file.ec_file.comment = '%s: %s' % (T('Allowed extensions'), ', '.join(IMAGE_EXTENSIONS))
        onvalidation = _validate_image_upload
    else:
        db.ec_file.ec_file.requires = IS_UPLOAD_FILENAME(extension='|'.join(FILE_EXTENSIONS))
        db.ec_file.ec_is_image.default = False
        db.ec_file.ec_file.comment = '%s: %s' % (T('Allowed extensions'), ', '.join(FILE_EXTENSIONS))
        onvalidation = None
    # check if quota is exceeded
    size_sum = db.ec_file.ec_size.sum()
    file_con_quota=float(settings.file_con_quota)
    quota = float(db(db.ec_file.ec_id_convention==convention.id).select(size_sum).first()[size_sum] or 0) / 1048576
    if quota > file_con_quota:
        _redirect_message(T('You have exceeded your quota of %s by %s') % ('%.1f MB' % file_con_quota, '%.1f MB' % (quota - file_con_quota)),URL('files',args=convention.ec_short_name))
    fields = [
        'ec_file',
        'ec_description',
        'ec_hidden',
        'ec_public',
        ]
    form = SQLFORM(db.ec_file, fields=fields, submit_button=title)
    if form.process(onvalidation=onvalidation).accepted:
        _save_metadata(form)
        redirect(URL('files',args=[convention.ec_short_name]))
    return dict(title=title,form=form)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def con_invitations():
    _update_invitations()
    convention=_get_convention_conname(request.args(0))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: redirect(URL(configure_main, args=convention.ec_short_name))
    title = MV('con_invitations')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    db.ec_invitation.ec_id_convention.default = convention.id
    person = db.ec_person(auth.user.active_person) or _get_or_create_first_person_auth()
    if 'notify' in request.vars:
        invite=db.ec_invitation(request.vars.notify)
        if invite:
            invite.update_record(ec_notify_date=request.now)
            result = _send_invitation(convention, tos=[invite.ec_email,], text=invite.ec_text, ec_for='convention', person=person, replyto=person.ec_email if person.ec_email else auth.user.email)
            _redirect_message(','.join(result), URL('con_invitations', args=[convention.ec_short_name]))
    if 'notifyall' in request.vars:
        invites = db((db.ec_invitation.ec_id_convention==convention.id)&(db.ec_invitation.ec_for=='convention')).select()
        if request.vars.notifyall != 'everyone':
            invites.exclude(lambda row: row.ec_notify_date!=None)
        allresult = []
        for invite in invites:
            invite.update_record(ec_notify_date=request.now)
            result = _send_invitation(convention, tos=[invite.ec_email,], text=invite.ec_text, ec_for='convention', person=person, replyto=person.ec_email if person.ec_email else auth.user.email)
            allresult.append(','.join(result))
        _redirect_message(','.join(allresult), URL('con_invitations', args=[convention.ec_short_name]))
    db.ec_invitation.ec_created_by.default = person.id
    db.ec_invitation.id.readable, db.ec_invitation.id.writable=False, False
    db.ec_invitation.ec_for.readable, db.ec_invitation.ec_for.writable=False, False
    db.ec_invitation.ec_for.default = 'convention'
    db.ec_invitation.ec_status.represent=lambda x, row: MV(x)
    db.ec_invitation.ec_expiry_date.represent=lambda x, row: '' if not x else x
    db.ec_invitation.ec_text.default=config.ec_registration_invite_text % _get_email_dict(convention=convention) if config.ec_registration_invite_text else ''
    fields = [
        'ec_status',
        'ec_email',
        'ec_invite_date',
        'ec_answer_date',
        'ec_expiry_date',
        'ec_notify_date',        ]
    dbfields=[getattr(db.ec_invitation,field) for field in fields]
    if config.ec_registration_period_stop and config.ec_registration_period_stop > request.now:
        db.ec_invitation.ec_expiry_date.default = config.ec_registration_period_stop
        db.ec_invitation.ec_expiry_date.comment = MV('ec_registration_period_stop')
    result=SQLFORM.grid(db.ec_invitation.ec_id_convention==convention.id,
        fields=dbfields,
        args=request.args[:1],
        orderby=[db.ec_invitation.ec_invite_date],
        maxtextlengths = {'ec_invitation.ec_email': 50, 'ec_page.ec_title': 100},
        createargs = {'fields' : ['ec_email', 'ec_text','ec_expiry_date',]},
        csv=False,
        links=[
                dict(header='',body=lambda row: A(T('Send invitation') if not row.ec_notify_date else T('Send invitation again'), _class=('button'),_href=URL('con_invitations',args=[convention.ec_short_name],vars=dict(notify=row.id),user_signature=True))),
            ] if request.args(1) not in ['edit','view'] else None,
        represent_none=T('not yet')
        )
    return dict(title=title,result=result,config=config)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def bulk_con_invitations():
    convention=_get_convention_conname(request.args(0))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: redirect(URL(configure_main, args=convention.ec_short_name))
    title = T('Bulk convention invitations')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    person = db.ec_person(auth.user.active_person) or _get_or_create_first_person_auth()
    result=None
    db.ec_invitation.ec_text.default=config.ec_registration_invite_text % _get_email_dict(convention=convention) if config.ec_registration_invite_text else ''
    form = SQLFORM.factory(
        db.ec_invitation.ec_text,
        db.ec_invitation.ec_expiry_date,
        Field('emails', 'text', label=T('Enter e-mails to invite'),
            requires=IS_EMAIL_CSV_LIST(),
            comment=T('Separated by commas'),))
    if form.process(keepvalues=True, onsuccess=None).accepted:
        emails = form.vars.emails.split(',')
        emails = [email.strip() for email in emails]
        for email in emails:
            x = db.ec_invitation.insert(ec_email=email,
                ec_expiry_date=form.vars.ec_expiry_date,
                ec_text=form.vars.ec_text,
                ec_id_convention=convention.id,
                ec_created_by=person.id)
        result = P('%s: %s'  % (T(db.ec_invitation.ec_text.label), form.vars.ec_text))
        result = CAT(result,P('%s: %s' % (T('Invitations'), '; '.join(emails))))
    return dict(title=title,form=form,result=result,convention=convention)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def timeslots():
    convention=_get_convention_conname(request.args(0))
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, T('Time slots'))
    config=db.ec_configuration[convention.ec_id_configuration]
    if not config:
        session.flash = T('You have to copy or create a configuration for your convention')
        redirect(URL('configure_main',args=convention.ec_short_name))
    db.ec_timeslot.id.writable=False
    db.ec_timeslot.ec_id_convention.readable=db.ec_timeslot.ec_id_convention.writable=False
    db.ec_timeslot.ec_id_convention.default=convention.id
    defaultdate= convention.ec_start_date or request.now
    db.ec_timeslot.ec_start_time.default=datetime.datetime.combine(defaultdate,datetime.time(18))
    db.ec_timeslot.ec_name.default=T(defaultdate.strftime('%A')+' night')
    form=SQLFORM.grid(db.ec_timeslot.ec_id_convention==convention.id,
        args=request.args[:1],
        orderby=[db.ec_timeslot.ec_start_time],
        searchable=False, csv=False,
        onvalidation = _update_event_times)
    return dict(form=form)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def preview_registration():
    convention = _get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    config=db.ec_configuration(convention.ec_id_configuration)
    systempage = _get_systempage(convention, 'Registration')
    title = MV('preview_registration')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    # check if new person exist with this e-mail
    persons = _check_person_auth()
    if persons:
        session.flash = T('New person entry found with same email as this account - and connected')
        redirect(URL('register',args=request.args, vars=request.args))
    person = _get_or_create_first_person_auth()
    # create registration
    registration = Registration(convention, person=person, use_invitation=None)
    regform = registration.get_regform()
    session.conventionid = convention.id
    return dict(title=title, regform=regform,  person=person, config=config, systempage=systempage)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def configure_emails():
    message=''
    convention=_get_convention_conname(request.args(0))
    action=request.args(1) if request.args(1) in ['view','edit'] else 'view'
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('You have to copy or create a configuration for your convention'), URL('configure_main',args=convention.ec_short_name))
    title='%s: %s' % (T('Configure'),MV('emails'))
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    configfields=[]
    fields = [
        'ec_email_signature',
        'ec_email_tag',
        'ec_email_registration_attendee_registered_subject',
        'ec_email_registration_attendee_registered_text',
        'ec_email_registration_attendee_registered_orga',
        'ec_email_registration_attendee_registered_bcc',
        'ec_email_registration_got_money_subject',
        'ec_email_registration_got_money_text',
        'ec_email_registration_got_money_orga',
        'ec_email_registration_got_money_bcc',
        'ec_email_registration_waitlist_subject',
        'ec_email_registration_waitlist_text',
        'ec_email_registration_waitlist_orga',
        'ec_email_registration_waitlist_bcc',
        'ec_email_registration_decline_subject',
        'ec_email_registration_decline_text',
        'ec_email_registration_decline_orga',
        'ec_email_registration_decline_bcc',
        'ec_email_event_registered_subject',
        'ec_email_event_registered_text',
        'ec_email_event_registered_orga',
        'ec_email_event_registered_bcc',
        'ec_email_event_new_attendee_subject',
        'ec_email_event_new_attendee_text',
        'ec_email_event_new_attendee_orga',
        'ec_email_event_new_attendee_bcc',
    ]
    for field in fields:
        if field.endswith('_subject'):
            db.ec_configuration[field].label = B(db.ec_configuration[field].label)
        if field.endswith('_text'):
            db.ec_configuration[field].represent = lambda value, row: TEXTAREA(value)
        else:
            db.ec_configuration[field].represent = lambda value, row: PRE(value)
        configfields.append(field)
    if not configfields: _redirect_message(T('Nothing to configure'), URL('configure_main',args=convention.ec_short_name))
    configform = SQLFORM(db.ec_configuration,
        config.id,
        fields=configfields,
        formstyle='table2cols',
        readonly=(action=='view'),
        showid=False,
        comments=True,
        submit_button=T('Save'))
    if configform and configform.process().accepted:
        _redirect_message(T('Configuration for "%(ec_name)s" changed') % convention.as_dict(), URL('configure_emails',args=convention.ec_short_name))
    return dict(title=title,convention=convention,message=message,configform=configform)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def tables():
    convention=_get_convention_conname(request.args(0))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('This convention needs to be configured'), URL('convention',args=[convention.ec_short_name]))
    title = MV('tables')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    printview = 'display' in request.vars and request.vars.display=='print'

    db.ec_table.ec_id_convention.default = convention.id
    defaultdate=convention.ec_start_date or request.now
    db.ec_table.ec_available_begin.default = datetime.datetime.combine(defaultdate,datetime.time(18))
    db.ec_table.ec_available_end.default = datetime.datetime.combine(defaultdate,datetime.time(18))
    db.ec_table.id.readable, db.ec_table.id.writable=False, False
    db.ec_table.ec_id_convention.readable, db.ec_table.ec_id_convention.writable=False, False
    links=[
           dict(header='',body=lambda row: A(MV('events_at_table'), _class=('button'),_href=URL('events_at_table',args=[convention.ec_short_name,row.id],user_signature=True))),
            ] if not printview else []
    fields = [
        'ec_name',
        'ec_location',
        'ec_order',
        'ec_min_attendees',
        'ec_max_attendees',
        'ec_available_begin',
        'ec_available_end',
        'ec_setup',
        'ec_comment',
        'ec_picture',
        'v_events'
                ]
    dbfields=[getattr(db.ec_table,field) for field in fields]
    query = db.ec_table.ec_id_convention==convention.id
    result=SQLFORM.grid(query,
        fields=dbfields,
        args=request.args[:1],
        orderby=[db.ec_table.ec_name],
        links=links,
        maxtextlengths = {'db.ec_table.v_events': 150},
        csv=False,
        paginate=80,
        searchable = not printview, create = not printview, deletable = not printview, editable=not printview, details=not printview,
        )
    events_with_table = db((db.ec_table.ec_id_convention==convention.id)&(db.ec_table.id==db.ec_event_at_table.ec_id_table)&(db.ec_event_at_table.ec_id_event==db.ec_event.id))._select(db.ec_event.id)
    events_no_table = db((db.ec_event.ec_id_convention==convention.id)&~db.ec_event.id.belongs(events_with_table)).select(orderby=[db.ec_event.ec_start])

    # create selection forms to put events at tables
    tables = db(query).select()
    options = [OPTION(t['ec_name'], _value=str(t['id'])) for t in tables]
    event_table_selections={}
    for event in events_no_table:
        formtitle = '%s (%s), max. %s %s ' % (event.ec_name, _begin_end_datetime_string(begin=event.ec_start, end=event.ec_end), event.ec_max_attendees, T('attendees'))
        event_table_selections[event.id] = FORM(formtitle, SELECT(_name='at_table',*options), INPUT(_type='submit', _value=T('Add')),formname='event_%s' % event['id'])
        if event_table_selections[event.id].process(formname='event_%s' % event['id']).accepted:
            redirect(URL('convention','events_at_table',args=[convention.ec_short_name,event_table_selections[event.id].vars.at_table],
                                vars=dict(add=event['id'],back=request.env.request_uri)))


    # get the big overview table
    if printview:
        table_table = _get_table_table(convention,printview=True)
    else:
        table_table = _get_table_table(convention,printview=False)

    layout = 'print.html' if printview else 'layout.html'
    return dict(title=title,result=result,config=config, events_no_table=events_no_table, table_table=table_table, layout=layout, tables=tables, event_table_selections=event_table_selections)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def bulk_tables():
    convention=_get_convention_conname(request.args(0))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: redirect(URL(configure_main, args=convention.ec_short_name))
    title = T('Bulk table creation')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    db.ec_table.ec_name.comment = T('The string %s will be replaced by a serial number')
    db.ec_table.ec_name.default = T('Table_%s')
    defaultdate=convention.ec_start_date or request.now
    db.ec_table.ec_available_begin.default = datetime.datetime.combine(defaultdate,datetime.time(18))
    db.ec_table.ec_available_end.default = datetime.datetime.combine(defaultdate,datetime.time(18))
    result=''
    form = SQLFORM.factory(
        db.ec_table.ec_name,
        db.ec_table.ec_location,
        db.ec_table.ec_min_attendees,
        db.ec_table.ec_max_attendees,
        db.ec_table.ec_available_begin,
        db.ec_table.ec_available_end,
        db.ec_table.ec_setup,
        db.ec_table.ec_comment,
        db.ec_table.ec_picture,
        Field('number', 'integer', label=T('How many tables of this type shall be created?'), requires=IS_NOT_EMPTY()),
        Field('start_number', 'integer', default=1, label=T('Where should the serial number start from?'), requires=IS_NOT_EMPTY()),
        submit_button='Create',
        )
    if form.process(keepvalues=True, onsuccess=None).accepted:
        start = int(form.vars.start_number)
        for cnt in range(int(form.vars.number)+start)[start:]:
            tablename = form.vars.ec_name % cnt
            db.ec_table.insert(
                ec_name=tablename,
                ec_location=form.vars.ec_location,
                ec_min_attendees=form.vars.ec_min_attendees,
                ec_max_attendees=form.vars.ec_max_attendees,
                ec_available_begin=form.vars.ec_available_begin,
                ec_available_end=form.vars.ec_available_end,
                ec_setup=form.vars.ec_setup,
                ec_comment=form.vars.ec_comment,
                ec_id_convention=convention.id)
        redirect(URL('convention','tables',args=[convention.ec_short_name]))
    return dict(title=title,form=form,result=result,convention=convention)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def events_at_table():
    convention=_get_convention_conname(request.args(0))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('This convention needs to be configured'), URL('convention','configure_main',args=[convention.ec_short_name]))
    table = db.ec_table(request.args(1))
    if not table or table.ec_id_convention != convention.id: _redirect_message(T('Invalid table'), URL('convention', 'tables', args=[convention.ec_short_name]))
    title = '%s: %s' % (MV('events_at_table'), table.ec_name)
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    if 'add' in request.vars:
        event=db.ec_event(request.vars.add)
        url = request.vars.back if 'back' in request.vars else URL('convention', 'events_at_table', args=[convention.ec_short_name, table.id])
        if event:
            db.ec_event_at_table.insert(ec_id_event=event.id, ec_id_table=table.id, ec_begindate=event.ec_start, ec_enddate=event.ec_end)
            _redirect_message(T('Event %(ec_name)s added to table') % event.as_dict(), url)
        else:
            _redirect_message(T('Invalid event'), url)
    if 'remove' in request.vars:
        event=db.ec_event(request.vars.remove)
        if event:
            db((db.ec_event_at_table.ec_id_event==event.id)&(db.ec_event_at_table.ec_id_table==table.id)).delete()
            url = request.vars.back if 'back' in request.vars else URL('convention', 'events_at_table', args=[convention.ec_short_name, table.id])
            _redirect_message(T('Event %(ec_name)s removed from table') % event.as_dict(), url)
        else:
            _redirect_message(T('Invalid event'), URL('convention', 'events_at_table', args=[convention.ec_short_name, table.id]))
    events_with_table = db((db.ec_table.ec_id_convention==convention.id)&(db.ec_table.id==db.ec_event_at_table.ec_id_table)&(db.ec_event_at_table.ec_id_event==db.ec_event.id))._select(db.ec_event.id)
    events_no_table = db((db.ec_event.ec_id_convention==convention.id)&~db.ec_event.id.belongs(events_with_table)).select(db.ec_event.ALL,orderby=[db.ec_event.ec_start])
    eventslist = [(e.id, T('%s (%s), max. %s attendees') % (e.ec_name, _begin_end_datetime_string(begin=e.ec_start, end=e.ec_end), e.ec_max_attendees)) for e in events_no_table]
    form=SQLFORM.factory(
            Field('event', 'integer', requires=IS_IN_SET(eventslist, zero=None), label=''),
            submit_button=T('Add'),
        )
    if form.process().accepted:
        redirect(URL('events_at_table',args=[convention.ec_short_name, table.id], vars=dict(add=form.vars.event)))
    tableevents=db((db.ec_event.id==db.ec_event_at_table.ec_id_event)&(db.ec_event_at_table.ec_id_table==table.id)).select(db.ec_event.ALL)
    validation = _validate_table(table.id)
    table_table = _get_table_table(convention,table.id)
    return dict(title=title,table=table,config=config, form=form, eventslist=eventslist, tableevents=tableevents,validation=validation, table_table=table_table)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def table_table():
    convention=_get_convention_conname(request.args(0))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('This convention needs to be configured'), URL('convention','configure_main',args=[convention.ec_short_name]))
    title = T('Table occupation plan')
    printview = 'display' in request.vars and request.vars.display=='print'

    if printview:
        result = _get_table_table(convention,printview=True)
        layout = 'print.html'
    else:
        result = _get_table_table(convention,printview=False)
        layout = 'layout.html'
    return dict(title=title,layout=layout,result=result)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def bedrooms():
    convention=_get_convention_conname(request.args(0))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('This convention needs to be configured'), URL('convention',args=[convention.ec_short_name]))
    title = MV('bedrooms')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    configform = SQLFORM(db.ec_configuration, config.id, fields=['ec_convention_show_bedrooms'], showid=False,comments=True,submit_button=T('Save'))
    if configform and configform.process(formname='configform').accepted:
        session.flash = T('Configuration for "%(ec_name)s" changed') % {'ec_name' : convention.ec_name}
        redirect(URL('bedrooms',args=convention.ec_short_name))

    printview = 'display' in request.vars and request.vars.display=='print'

    db.ec_bedroom.ec_id_convention.default = convention.id
    db.ec_bedroom.id.readable, db.ec_bedroom.id.writable=False, False
    db.ec_bedroom.ec_id_convention.readable, db.ec_bedroom.ec_id_convention.writable=False, False
    links=[
                dict(header='',body=lambda row: A(MV('people_in_room'), _class=('button'),_href=URL('people_in_room',args=[convention.ec_short_name,row.id],user_signature=True))),
            ] if not printview else []
    if printview:
        fields = ['ec_name', 'ec_category', 'v_people']
    elif (config.ec_registration_data_attendance_arrival=='required' and config.ec_registration_data_attendance_departure=='required'):
        db.ec_bedroom.v_dates = Field.Virtual('v_dates', lambda row: _get_room_dates(row.ec_bedroom.id, minD=convention.ec_start_date, maxD=convention.ec_end_date), label=T('Min/max dates (from registrations)'))
        fields = [
            'ec_name',
            'ec_category',
            'ec_capacity',
            'v_free_capacity',
            'v_dates',
            'v_people',
            'ec_comment',
             ]
    else:
        fields = [
            'ec_name',
            'ec_category',
            'ec_capacity',
            'v_free_capacity',
            'v_people',
            'ec_comment',
             ]
    dbfields=[getattr(db.ec_bedroom,field) for field in fields]
    query = db.ec_bedroom.ec_id_convention==convention.id
    x = db(query).select()
    result=SQLFORM.grid(query,
        fields=dbfields,
        args=request.args[:1],
        orderby=[db.ec_bedroom.ec_category,db.ec_bedroom.ec_capacity],
        links=links,
        maxtextlength=100,
        csv=False,
        paginate=80,
        searchable = not printview, create = not printview, deletable = not printview, editable=not printview, details=not printview,
        )
    attendees_with_room = db((db.ec_bedroom.ec_id_convention==convention.id)&(db.ec_bedroom.id==db.ec_stays_in_room.ec_id_room)&(db.ec_stays_in_room.ec_id_person==db.ec_person.id))._select(db.ec_person.id)
    attendees_no_room = db((db.ec_attendance.ec_id_convention==convention.id)&((db.ec_attendance.ec_id_person==db.ec_person.id)&db.ec_attendance.ec_registration_state.belongs(['attendee','registered']))&~db.ec_attendance.ec_id_person.belongs(attendees_with_room)).select(orderby=[db.ec_person.ec_lastname])
    layout = 'print.html' if printview else 'layout.html'
    return dict(title=title,result=result,config=config, configform=configform, attendees_no_room=attendees_no_room, layout=layout)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def bulk_bedrooms():
    convention=_get_convention_conname(request.args(0))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: redirect(URL(configure_main, args=convention.ec_short_name))
    title = T('Bulk bedroom creation')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    db.ec_bedroom.ec_name.comment = T('The string %s will be replaced by a serial number')
    db.ec_bedroom.ec_name.default = T('Room_%s')
    result=''
    form = SQLFORM.factory(
        db.ec_bedroom.ec_capacity,
        db.ec_bedroom.ec_category,
        db.ec_bedroom.ec_name,
        Field('number', 'integer', label=T('How many rooms of this type shall be created?'), requires=IS_NOT_EMPTY()),
        Field('start_number', 'integer', default=1, label=T('Where should the serial number start from?'), requires=IS_NOT_EMPTY()),
        submit_button='Create',
        )
    if form.process(keepvalues=True, onsuccess=None).accepted:
        start = int(form.vars.start_number)
        for cnt in range(int(form.vars.number)+start)[start:]:
            roomname = form.vars.ec_name % cnt
            db.ec_bedroom.insert(ec_capacity=form.vars.ec_capacity, ec_category=form.vars.ec_category, ec_name=roomname, ec_id_convention=convention.id)
        redirect(URL('convention','bedrooms',args=[convention.ec_short_name]))
    return dict(title=title,form=form,result=result,convention=convention)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def people_in_room():
    convention=_get_convention_conname(request.args(0))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('This convention needs to be configured'), URL('convention','configure_main',args=[convention.ec_short_name]))
    title = MV('people_in_room')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    db.ec_bedroom.v_dates = Field.Virtual('v_dates', lambda row: _get_room_dates(row.ec_bedroom.id, minD=convention.ec_start_date, maxD=convention.ec_end_date), label=T('Min/max dates (from registrations)'))
    room = db.ec_bedroom(request.args(1))
    if not room or room.ec_id_convention != convention.id: _redirect_message(T('Invalid room'), URL('convention', 'bedrooms', args=[convention.ec_short_name]))
    if 'add' in request.vars:
        person=db.ec_person(request.vars.add)
        if person:
            db.ec_stays_in_room.insert(ec_id_person=person.id, ec_id_room=room.id)
            _redirect_message(T('%(ec_firstname)s %(ec_lastname)s added to room') % person.as_dict(), URL('convention', 'people_in_room', args=[convention.ec_short_name, room.id]))
        else:
            _redirect_message(T('Invalid person'), URL('convention', 'people_in_room', args=[convention.ec_short_name, room.id]))
    if 'remove' in request.vars:
        person=db.ec_person(request.vars.remove)
        if person:
            db((db.ec_stays_in_room.ec_id_person==person.id)&(db.ec_stays_in_room.ec_id_room==room.id)).delete()
            _redirect_message(T('%(ec_firstname)s %(ec_lastname)s removed from room') % person.as_dict(), URL('convention', 'people_in_room', args=[convention.ec_short_name, room.id]))
        else:
            _redirect_message(T('Invalid person'), URL('convention', 'people_in_room', args=[convention.ec_short_name, room.id]))
    attendees_with_room = db((db.ec_bedroom.ec_id_convention==convention.id)&(db.ec_bedroom.id==db.ec_stays_in_room.ec_id_room)&(db.ec_stays_in_room.ec_id_person==db.ec_person.id))._select(db.ec_person.id)
    attendees = db((db.ec_attendance.ec_id_convention==convention.id)&(db.ec_attendance.ec_registration_state.belongs(['attendee','registered']))&~db.ec_attendance.ec_id_person.belongs(attendees_with_room)).select(db.ec_attendance.ALL, orderby=[db.ec_attendance.ec_registration_state])
    peoplelist = [(a.ec_id_person, '%s %s (%s)' % (db.ec_person(a.ec_id_person).ec_firstname, db.ec_person(a.ec_id_person).ec_lastname, MV(a.ec_registration_state))) for a in attendees]
    form=SQLFORM.factory(
            Field('person', 'integer', requires=IS_IN_SET(peoplelist, zero=None), label=''),
            submit_button=T('Add'),
        )
    if form.process().accepted:
        redirect(URL('people_in_room',args=[convention.ec_short_name, room.id], vars=dict(add=form.vars.person)))
    roompeople=db((db.ec_person.id==db.ec_stays_in_room.ec_id_person)& \
        (db.ec_stays_in_room.ec_id_room==room.id)).select(db.ec_person.ALL, db.ec_attendance.ALL, left=db.ec_person.on((db.ec_person.id==db.ec_attendance.ec_id_person)&(db.ec_attendance.ec_id_convention==convention.id)))
    return dict(title=title,room=room,config=config, form=form, peoplelist=peoplelist, roompeople=roompeople)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def manage_waitlist():
    session.back='manage_waitlist'
    convention=_get_convention_conname(request.args(0))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('This convention needs to be configured'), URL('convention','configure_main',args=[convention.ec_short_name]))
    title = MV('manage_waitlist')
    printview = 'display' in request.vars and request.vars.display=='print'
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    query = (db.ec_attendance.ec_id_convention==convention.id)&(db.ec_attendance.ec_registration_state=='waitlist')
    waitlist = db(query).select()
    db.ec_attendance.id.readable, db.ec_attendance.id.writable=False,False
    db.ec_attendance.ec_id_person.writable=False
    db.ec_attendance.ec_id_person.represent = lambda id, row: '%(ec_firstname)s %(ec_lastname)s' % db.ec_person[id] if id else ''
    db.ec_attendance.ec_registration_state.represent = lambda value, row: MV(value)
    db.ec_attendance.ec_id_convention.readable,db.ec_attendance.ec_id_convention.writable=False,False
    #if 'moveup' in request.vars:
     #   redirect(URL('attendee_items', args=convention.ec_short_name, vars=dict(attid=request.vars.moveup, action='moveup_waitlist')))
        # attendance = db.ec_attendance(request.vars.moveup)
        # if not attendance or attendance.ec_id_convention!=convention.id: _redirect_message(T('Invalid attendance'), URL('convention', args=convention.ec_short_name))
        # attendance.update_record(ec_registration_state='registered')
        # person = db.ec_person(attendance.ec_id_person)
        # _redirect_message('%s. %s.' % (T('%(ec_firstname)s %(ec_lastname)s registered') % person.as_dict(), T("Don't forget to check buyable items and notify user")), URL('attendees',args=[convention.ec_short_name]))
    links=[
            dict(header='',body=lambda row: SPAN(A(T('delete'), _class=('button'),
                                                   _href=URL('delete_attendance',args=[convention.ec_short_name,row.id],user_signature=True)),
                                                SPAN(EX('delete_registration'), _class="popup"),
                                                 _class="haspopup")),
            dict(header='',body=lambda row: SPAN(A(T('move up and register'), _class=('button'),
                                                   _href=URL('attendee_items',args=[convention.ec_short_name],vars=dict(attid=row.id, action='moveup_waitlist'),user_signature=True)),
                                                 SPAN(EX('move_up'), _class="popup"),
                                                 _class="haspopup")),
            dict(header='',body=lambda row: SPAN(A(T('Manage attendee') + ' ...', _class=('button'),
                                                   _href=URL('manage_attendee',args=[convention.ec_short_name,row.id])),
                                                 SPAN(EX('manage_attendee'), _class="popup"),
                                                 _class="haspopup")),
        ] if request.args(1) not in ['edit','view'] else None
    fields = [  'v_waitlist_position',
                'ec_id_person',
                #'ec_registration_state',
                'ec_registertime',
                'ec_arrival',
                'ec_departure',
                'ec_attendance_comment',
                'ec_id_convention']
    dbfields = [getattr(db.ec_attendance, f) for f in fields]
    form=SQLFORM.grid(query,
        field_id = db.ec_attendance.id,
        fields=dbfields,
        args=request.args[:1],
        maxtextlengths = {'ec_attendance.ec_attendance_comment': 100},
        orderby=[db.ec_attendance.ec_registertime],
        links=links if not printview else [],
        selectable=[(T('Send e-mail to selected people'), lambda ids : redirect(URL('convention', 'emails', args=convention.ec_short_name,
            vars=dict(tos=[db.ec_attendance[aid].ec_id_person for aid in ids])))),] if not printview else [],
        csv=False, create=False, editable=False, details=False, deletable=False, searchable=False) if waitlist else None
    message = T('There is nobody on the convention waitlist right now')+'.' if not waitlist else ''
    layout = 'print.html' if printview else 'layout.html'
    return dict(title=title,config=config,form=form,message=message,layout=layout)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def manage_item_waitlist():
    convention=_get_convention_conname(request.args(0))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('This convention needs to be configured'), URL('convention','configure_main',args=[convention.ec_short_name]))
    title = MV('manage_item_waitlist')
    printview = 'display' in request.vars and request.vars.display=='print'
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    query = (db.ec_buyitem.ec_id_convention==convention.id)&(db.ec_buyitem.ec_has_waitlist==True)
    waitlist_items = db(query).select()
    links=[
            dict(header='',body=lambda row: A(T('Manage waitlist'), _class=('button'),_href=URL('manage_item_waitlist',args=[convention.ec_short_name], vars=dict(iid=row.id),user_signature=True))),
        ]
    db.ec_buyitem.id.readable, db.ec_buyitem.id.writable=False,False
    db.ec_buyitem.ec_id_convention.readable,db.ec_buyitem.ec_id_convention.writable=False,False
    fields = [  'ec_name',
                'ec_price',
                'ec_capacity',
                'v_sold_items',
                'v_waitlist_items',
                'ec_id_convention',
     ]
    dbfields = [getattr(db.ec_buyitem, f) for f in fields]
    db.ec_buyitem.ec_capacity.label = short_label('ec_capacity')
    item = None
    if 'deleteentry' in request.vars:
        entry = db.ec_buys_item(request.vars.deleteentry)
        item = entry.ec_id_buyitem
        db(db.ec_buys_item.id==entry.id).delete()
    if 'moveup' in request.vars:
        entry = db.ec_buys_item(request.vars.moveup)
        item = entry.ec_id_buyitem
        entry.update_record(ec_buy_state='booked',ec_bookingdate=request.now)
    if 'iid' in request.vars:
        item = db.ec_buyitem(request.vars.iid)
        if not item: _redirect_message(T('Invalid item'), URL('convention', args=[convention.ec_short_name]))
    if item:
        query = (db.ec_buys_item.ec_id_buyitem==item.id)&\
                (db.ec_buys_item.ec_buy_state=='waitlist')&\
                (db.ec_buys_item.ec_id_person==db.ec_attendance.ec_id_person)&\
                (db.ec_attendance.ec_registration_state.belongs(['registered','attendee']))&\
                (db.ec_attendance.ec_id_convention==item.ec_id_convention)

        db.ec_buys_item.ec_id_person.represent = lambda id, row: '%(ec_firstname)s %(ec_lastname)s' % db.ec_person[id] if id else ''
        db.ec_buys_item.ec_id_person.label = T('Waiting attendees')
        db.ec_buys_item.id.readable, db.ec_buys_item.id.writable=False,False
        db.ec_buys_item.ec_bookingdate.readable, db.ec_buys_item.ec_bookingdate.writable = False, False
        db.ec_buys_item.ec_id_buyitem.readable, db.ec_buys_item.ec_id_buyitem.writable=False, False
        title = T('Waitlist for %s') % item.ec_name
        links=[
                dict(header='',body=lambda row: A(T('delete'), _class=('button'),_href=URL('manage_item_waitlist',args=[convention.ec_short_name],vars=dict(deleteentry=row.id),user_signature=True))),
                dict(header='',body=lambda row: A(T('move up and register'), _class=('button'),_href=URL('manage_item_waitlist',args=[convention.ec_short_name],vars=dict(moveup=row.id),user_signature=True))),
            ]
        form=SQLFORM.grid(query,
            field_id=db.ec_buys_item.id,
            args=request.args[:1],
            orderby=[db.ec_buys_item.ec_bookingdate],
            fields = [db.ec_buys_item.id, db.ec_buys_item.ec_id_buyitem, db.ec_buys_item.v_waitlist_position, db.ec_buys_item.ec_bookingdate, db.ec_buys_item.ec_id_person],
            selectable=[(T('Send e-mail to selected people'), lambda ids : redirect(URL('convention', 'emails', args=convention.ec_short_name,
                vars=dict(tos=[db.ec_buys_item[aid].ec_id_person for aid in ids])))),] if not printview else [],
            links=links if not printview else [],
            csv=False, create=False, editable=False, details=False, deletable=False, searchable=False)
        back=A(T('back'),_href=URL('manage_item_waitlist', args=[convention.ec_short_name]), _class='button')
    else:
        form=SQLFORM.grid(query,
            field_id = db.ec_buyitem.id,
            fields=dbfields,
            args=request.args[:1],
            orderby=[db.ec_buyitem.ec_order],
            maxtextlengths = {'db.ec_buyitem.ec_name': 150},
            links=links if not printview else [],
            csv=False, create=False, editable=False, details=False, deletable=False, searchable=False) if waitlist_items else None
        back=None

    layout = 'print.html' if printview else 'layout.html'
    return dict(title=title,config=config,form=form, back=back, layout=layout)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def delete_attendance():
    # init data
    convention = _get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('Invalid configuration'), URL('convention', args=convention.ec_short_name))
    attendance = db.ec_attendance(request.args(1))
    if not attendance or attendance.ec_id_convention!=convention.id: _redirect_message(T('Invalid registration'), URL('convention', args=convention.ec_short_name))
    registration = Registration(attendance=attendance)
    # handle modes
    mode = request.vars.get('mode' or 'decline') or 'decline'
    if mode == 'decline':
        title = T('Deregistration')
    elif mode == 'waitlist':
        title = T('Move to waitlist')
    elif mode == 'delete':
        title = T('Delete attendance')
    else:
        raise ValueError
        _redirect_message(T('Invalid mode'), URL('convention','manage_attendee',args=[convention.ec_short_name,attendance.id]))
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    back = session.back if session.back else 'attendees'

    # handle actions
    if 'action' in request.vars:
        if request.vars.action=='deregister':
            message = registration.decline()
            _redirect_message(message, URL('manage_attendee',args=[convention.ec_short_name,attendance.id]))
        if request.vars.action=='move2waitlist':
            message = registration.decline(waitlist=True, waitlisttime=request.now, orga='%(first_name)s %(last_name)s' % auth.user)
            _redirect_message(message, URL('manage_attendee',args=[convention.ec_short_name,attendance.id]))

    # build data for view
    attendeedata = registration.show_attendeedata()
    deregistration = A(T('Deregister'), _href=URL('delete_attendance', args=[convention.ec_short_name,attendance.id], vars=dict(action='deregister')), _class="linkbutton")
    move_to_waitlist = A(T('Move to waitlist'), _href=URL('delete_attendance', args=[convention.ec_short_name,attendance.id], vars=dict(action='move2waitlist')), _class="linkbutton")
    # build form for deletion
    event_host = registration.get_events(what='organizes')
    if event_host:
        form = B(T('You cannot delete this user unless you delete all events that he*she offers')+'.')
    else:
        form = FORM(T('Do you really want to delete this registration?'),INPUT(_type='submit', _value=T('Delete registration')))
        form.add_button(T('Cancel'), URL('manage_attendee', args=[convention.ec_short_name,attendance.id]))
        if form.process().accepted and not event_host:
            registration.delete()
            _redirect_message(T('Registration deleted'), URL('attendees', args=[convention.ec_short_name]))
    return dict(title=title, attendeedata=attendeedata, deregistration=deregistration, move_to_waitlist=move_to_waitlist, deleteform=form, mode=mode,attendanceid=attendance.id)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def edit_extra_fields():
    convention = _get_convention_conname(request.args(0))
    if not convention: _redirect_message(T('Invalid convention'), URL('default','index'))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('Invalid configuration'), URL('convention', args=convention.ec_short_name))
    attendance = db.ec_attendance(request.args(1))
    if not attendance or attendance.ec_id_convention!=convention.id: _redirect_message(T('Invalid registration'), URL('convention', args=convention.ec_short_name))
    registration = Registration(attendance=attendance)
    attendanceform = registration.get_attendanceform()
    title = T('Edit registration')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    return dict(title=title, attendanceform=attendanceform)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def new_event_orga():
    # can be called with event type to avoid the form for it
    convention=_get_convention_conname(request.args(0))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('This convention needs to be configured'), URL('convention','configure_main',args=[convention.ec_short_name]))
    title = T('New event for someone else')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    eventtypes = _get_event_types(config)
    eventtype = request.vars.eventtype
    eventtypelist = [(et, MV(et)) for et in eventtypes]
    if not eventtype or eventtype not in eventtypes:
        eventtypeform = SQLFORM.factory(Field('eventtype', 'string', requires=IS_IN_SET(eventtypelist, zero=None), label=T('Choose event type')), submit_button=T('Select'))
    else:
        eventtypeform = None
    if eventtype and request.vars.hostid and db.ec_person(request.vars.hostid):
        redirect(URL('event',args=[convention.ec_short_name, 'new', eventtype], vars=dict(hostid=request.vars.hostid)))

    # create form to choose attendee as host

    regstates = ['attendee','registered','waitlist']

    attendees = db((db.ec_attendance.ec_id_convention==convention.id)&\
                    (db.ec_attendance.ec_id_person==db.ec_person.id) &\
                    (db.ec_attendance.ec_registration_state.belongs(regstates))).select(db.ec_person.ALL, orderby=db.ec_person.ec_firstname)
    hosts = db((db.ec_person.id==db.ec_organizes_event.ec_id_person)&\
               (db.ec_organizes_event.ec_id_event==db.ec_event.id)&\
               (db.ec_event.ec_id_convention==convention.id)).select(db.ec_person.ALL, orderby=db.ec_person.ec_firstname, distinct=True)
    eventattendees = db((db.ec_person.id==db.ec_attends_event.ec_id_person)&\
               (db.ec_attends_event.ec_id_event==db.ec_event.id)&\
               (db.ec_event.ec_id_convention==convention.id)).select(db.ec_person.ALL, orderby=db.ec_person.ec_firstname, distinct=True)
    allattendees = attendees | hosts | eventattendees
    hostselection = [(p.id, '%(ec_firstname)s "%(ec_nickname)s" %(ec_lastname)s' % p) for p in allattendees.sort(lambda row: row.ec_firstname)]

    if hostselection:
        if eventtype=='rpggame':
            host = T('Choose')+' '+T('Game master')
        else:
            host = T('Choose')+' '+T('Event organizer')
        attendeeform = SQLFORM.factory(
            Field('hostid', 'integer', requires=IS_IN_SET(hostselection, zero=None), label=host), submit_button=T('Submit'))
    else:
        attendeeform = None

    # create form for new person
    db.ec_person.ec_firstname.comment = T('required')
    db.ec_person.ec_nickname.comment = T('optional, will be filled same as firstname if none is given')
    db.ec_person.ec_nickname.requires = IS_LENGTH(512)
    db.ec_person.ec_lastname.comment = T('optional')
    db.ec_person.ec_email.comment = '%s; %s' % (T('optional'), T('if you enter an e-mail, an account with that e-mail will be able to edit that person and their events'))
    db.ec_person.ec_admin_comment.default = T('Created by %s %s to host an event at %s') % (auth.user.first_name, auth.user.last_name, convention.ec_name)
    newpersonform = SQLFORM(db.ec_person,
        fields = ['ec_firstname','ec_nickname', 'ec_lastname', 'ec_email'],
        submit_button=T('Create person'),
        )
    # process eventtypeform
    if eventtypeform and eventtypeform.process().accepted:
        redirect(URL('new_event_orga',args=[convention.ec_short_name], vars=dict(eventtype=eventtypeform.vars.eventtype)))

    # process newpersonform
    if newpersonform.process(keepvalues=True).accepted:
        person = db.ec_person(newpersonform.vars.id)
        if not request.vars.ec_nickname:
            person.update_record(ec_nickname=person.ec_firstname)
        auth.add_permission(auth.user_group(), 'edit', db.ec_person, person.id)
        redirect(URL('event',args=[convention.ec_short_name, 'new', eventtype], vars=dict(hostid=newpersonform.vars.id)))

    return dict(title=title,eventtypeform=eventtypeform,attendeeform=attendeeform,newpersonform=newpersonform)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def attend_event_orga():
    convention=_get_convention_conname(request.args(0))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('This convention needs to be configured'), URL('convention','configure_main',args=[convention.ec_short_name]))
    event = db.ec_event(request.vars.eid)
    if not event: _redirect_message(T('Invalid event'), URL('events', args=[convention.ec_short_name]))
    title = T('Register someone else for event')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)

    # if everything is there: register attendee for event and redirect back to event
    person = db.ec_person(request.vars.pid)
    if person:
        db.ec_systemevent.insert(ec_when=request.now, ec_what='new event attendance for event %s (no. %s) by orga' % (event.ec_name, event.id), ec_type='event_attendance', ec_id_convention=convention.id, ec_id_person=person.id, ec_id_auth_user=auth.user.id)
        db.ec_attends_event.insert(ec_id_event=event.id, ec_id_person=person.id)
        redirect(URL('event',args=[convention.ec_short_name, event.id]))

    # create form to choose attendee
    show_attendees = 'all' if request.vars.attendees and request.vars.attendees=='all' else 'registered'

    if config.ec_convention_requires_registration:
        if show_attendees=='all':
            regstates = ['attendee','registered','waitlist']
            comment = XML('%s (%s), %s' % (T('Attendees of state "%s"') % ', '.join([str(MV(state)) for state in regstates]),
                                   A(T('less'), _href=URL('attend_event_orga', args=request.args, vars=dict(eid=event.id))),
                                   T('hosts and attendees of events')))
        else:
            regstates = ['attendee']
            comment = XML('%s (%s), %s' % (T('Attendees of state "%s"') % MV('attendee'),
                                   A(T('more'), _href=URL('attend_event_orga', args=request.args, vars=dict(eid=event.id, attendees='all'))),
                                   T('hosts and attendees of events')))
    else:
        regstates = []
        comment = T('hosts and attendees of events')
    attendees = db((db.ec_attendance.ec_id_convention==convention.id)&\
                    (db.ec_attendance.ec_id_person==db.ec_person.id) &\
                    (db.ec_attendance.ec_registration_state.belongs(regstates))).select(db.ec_person.ALL, orderby=db.ec_person.ec_firstname)
    hosts = db((db.ec_person.id==db.ec_organizes_event.ec_id_person)&\
               (db.ec_organizes_event.ec_id_event==db.ec_event.id)&\
               (db.ec_event.ec_id_convention==convention.id)).select(db.ec_person.ALL, orderby=db.ec_person.ec_firstname, distinct=True)
    eventattendees = db((db.ec_person.id==db.ec_attends_event.ec_id_person)&\
               (db.ec_attends_event.ec_id_event==db.ec_event.id)&\
               (db.ec_event.ec_id_convention==convention.id)).select(db.ec_person.ALL, orderby=db.ec_person.ec_firstname, distinct=True)
    allattendees = attendees | hosts | eventattendees
    attendeeselection = [(p.id, '%(ec_firstname)s "%(ec_nickname)s" %(ec_lastname)s' % p) for p in allattendees.sort(lambda row: row.ec_firstname)]

    if attendeeselection:
        attendeeform = SQLFORM.factory(
            Field('pid', 'integer', requires=IS_IN_SET(attendeeselection, zero=None), label=T('Choose attendee'), comment=comment), submit_button=T('Choose attendee'))
    else:
        attendeeform = None

    # create form for new person
    db.ec_person.ec_firstname.comment = T('required')
    db.ec_person.ec_nickname.comment = T('optional, will be filled same as firstname if none is given')
    db.ec_person.ec_nickname.requires = IS_LENGTH(512)
    db.ec_person.ec_lastname.comment = T('optional')
    db.ec_person.ec_email.comment = '%s; %s' % (T('optional'), T('if you enter an e-mail, an account with that e-mail will be able to edit that person and their events'))
    db.ec_person.ec_admin_comment.default = T('Created by %s %s to host an event at %s') % (auth.user.first_name, auth.user.last_name, convention.ec_name)
    newpersonform = SQLFORM(db.ec_person,
        fields = ['ec_firstname','ec_nickname', 'ec_lastname', 'ec_email'],
        submit_button=T('Create person'),
        )
    if newpersonform.process(keepvalues=True).accepted: # new person attends event
        person = db.ec_person(newpersonform.vars.id)
        if not request.vars.ec_nickname:
            person.update_record(ec_nickname=person.ec_firstname)
        auth.add_permission(auth.user_group(), 'edit', db.ec_person, person.id)
        db.ec_systemevent.insert(ec_when=request.now, ec_what='new event attendance for event %s (no. %s) by orga' % (event.ec_name, event.id), ec_type='event_attendance', ec_id_convention=convention.id, ec_id_person=person.id, ec_id_auth_user=auth.user.id)
        db.ec_attends_event.insert(ec_id_event=event.id, ec_id_person=person.id)
        redirect(URL('event',args=[convention.ec_short_name, event.id]))

    return dict(title=title,attendeeform=attendeeform,newpersonform=newpersonform)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def choose_or_create_person():
    """Provides a dialog to either choose a person or create a new person, implemented for events and services"""
    convention=_get_convention_conname(request.args(0))
    what = request.args(1)
    if not what:
        _redirect_message(T("Don't know for what you want to choose or create a person"), URL('convention',args=[convention.ec_short_name]))
    title = T('Register someone else for event')
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('This convention needs to be configured'), URL('convention','configure_main',args=[convention.ec_short_name]))
    what = request.args(1)
    person = db.ec_person(request.vars.pid)
    if what=='event':
        event = db.ec_event(request.vars.eid)
        if not event: _redirect_message(T('Invalid event'), URL('events', args=[convention.ec_short_name]))
        title = T('Register someone else for event')
        # if everything is there: register attendee for event and redirect back to event
        if person:
            db.ec_systemevent.insert(ec_when=request.now, ec_what='new event attendance for event %s (no. %s) by orga' % (event.ec_name, event.id), ec_type='event_attendance', ec_id_convention=convention.id, ec_id_person=person.id, ec_id_auth_user=auth.user.id)
            db.ec_attends_event.insert(ec_id_event=event.id, ec_id_person=person.id)
            redirect(URL('event',args=[convention.ec_short_name, event.id]))
    elif what=='service':
        service = db.ec_service(request.vars.sid)
        if not service: _redirect_message(T('Invalid service'), URL('services', args=[convention.ec_short_name]))
        title = T('Register someone else for service')
        # if everything is there: register attendee for service and redirect back to services
        person = db.ec_person(request.vars.pid)
        if person:
            db.ec_helps.insert(ec_id_service=service.id, ec_id_person=person.id)
            redirect(URL('services',args=[convention.ec_short_name]))
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)

    # create form to choose attendee
    show_attendees = 'all' if request.vars.attendees and request.vars.attendees=='all' else 'registered'

    if config.ec_convention_requires_registration:
        if show_attendees=='all':
            del request.vars['attendees']
            regstates = ['attendee','registered','waitlist']
            comment = XML('%s (%s), %s' % (T('Attendees of state "%s"') % ', '.join([MV(state) for state in regstates]),
                                   A(T('less'), _href=URL('choose_or_create_person', args=request.args, vars=request.vars)),
                                   T('hosts and attendees of events')))
        else:
            regstates = ['attendee']
            request.vars.attendees = 'all'
            comment = XML('%s (%s), %s' % (T('Attendees of state "%s"') % MV('attendee'),
                                   A(T('more'), _href=URL('choose_or_create_person', args=request.args, vars=request.vars)),
                                   T('hosts and attendees of events')))
    else:
        regstates = []
        comment = T('hosts and attendees of events')
    attendees = db((db.ec_attendance.ec_id_convention==convention.id)&\
                    (db.ec_attendance.ec_id_person==db.ec_person.id) &\
                    (db.ec_attendance.ec_registration_state.belongs(regstates))).select(db.ec_person.ALL, orderby=db.ec_person.ec_firstname)
    hosts = db((db.ec_person.id==db.ec_organizes_event.ec_id_person)&\
               (db.ec_organizes_event.ec_id_event==db.ec_event.id)&\
               (db.ec_event.ec_id_convention==convention.id)).select(db.ec_person.ALL, orderby=db.ec_person.ec_firstname, distinct=True)
    eventattendees = db((db.ec_person.id==db.ec_attends_event.ec_id_person)&\
               (db.ec_attends_event.ec_id_event==db.ec_event.id)&\
               (db.ec_event.ec_id_convention==convention.id)).select(db.ec_person.ALL, orderby=db.ec_person.ec_firstname, distinct=True)
    allattendees = attendees | hosts | eventattendees
    attendeeselection = [(p.id, '%(ec_firstname)s "%(ec_nickname)s" %(ec_lastname)s' % p) for p in allattendees.sort(lambda row: row.ec_firstname)]

    if attendeeselection:
        attendeeform = SQLFORM.factory(
            Field('pid', 'integer', requires=IS_IN_SET(attendeeselection, zero=None), label=T('Choose attendee'), comment=comment), submit_button=T('Choose attendee'))
    else:
        attendeeform = None

    # create form for new person
    db.ec_person.ec_firstname.comment = T('required')
    db.ec_person.ec_nickname.comment = T('optional, will be filled same as firstname if none is given')
    db.ec_person.ec_nickname.requires = IS_LENGTH(512)
    db.ec_person.ec_lastname.comment = T('optional')
    db.ec_person.ec_email.comment = '%s; %s' % (T('optional'), T('if you enter an e-mail, an account with that e-mail will be able to edit that person and their events'))
    db.ec_person.ec_admin_comment.default = T('Created by %s %s to host an event at %s') % (auth.user.first_name, auth.user.last_name, convention.ec_name)
    newpersonform = SQLFORM(db.ec_person,
        fields = ['ec_firstname','ec_nickname', 'ec_lastname', 'ec_email'],
        submit_button=T('Create person'),
        )
    if newpersonform.process(keepvalues=True).accepted: # new person attends event
        person = db.ec_person(newpersonform.vars.id)
        if not request.vars.ec_nickname:
            person.update_record(ec_nickname=person.ec_firstname)
        auth.add_permission(auth.user_group(), 'edit', db.ec_person, person.id)
        request.vars.pid=person.id
        redirect(URL('choose_or_create_person', args=[convention.ec_short_name,what], vars=request.vars))

    return dict(title=title,attendeeform=attendeeform,newpersonform=newpersonform)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def orgatasks():
    convention=_get_convention_conname(request.args(0))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('This convention needs to be configured'), URL('convention',args=[convention.ec_short_name]))
    title = MV('orgatasks')
    if 'only_me' in request.vars and request.vars.only_me=='1':
        title += ' %s %s' % (T('for'), auth.user.first_name)
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    printview = 'display' in request.vars and request.vars.display=='print'
    message = request.vars.message
    configform = SQLFORM(db.ec_configuration, config.id, fields=['ec_orgatask_reminder', 'ec_orgatask_reminder_start', 'ec_orgatask_reminder_days'], showid=False,comments=True,submit_button=T('Save'))
    if 'action' in request.vars:
        if request.vars.action=='remind_orgatask' and int(request.args(1))==0:
            message = _send_task_reminder(convention, [])
            _redirect_message(message,URL('orgatasks', args=[convention.ec_short_name]))
        task = db.ec_task(request.args(1))
        if not task:
            _redirect_message(T('Invalid task'), URL('orgatasks', args=[convention.ec_short_name]))
        if request.vars.action=='done':
            task.update_record(ec_status='3')
            if not task.ec_assigned_to:
                task.update_record(ec_assigned_to=auth.user.id)
        if request.vars.action=='idoit':
            task.update_record(ec_assigned_to=auth.user.id)
        if request.vars.action=='started':
            task.update_record(ec_status='1')
            if not task.ec_assigned_to:
                task.update_record(ec_assigned_to=auth.user.id)
            if not task.ec_start_date:
                task.update_record(ec_start_date=request.now)
        if request.vars.action=='remind_orgatask':
            message = _send_task_reminder(convention, [task,])
            _redirect_message(message,URL('orgatasks', args=[convention.ec_short_name]))
        redirect(URL('orgatasks', args=[convention.ec_short_name]))
    db.ec_task.ec_id_convention.default = convention.id
    db.ec_task.id.readable, db.ec_task.id.writable=False, False
    db.ec_task.ec_id_convention.readable, db.ec_task.ec_id_convention.writable=False, False
    db.ec_task.ec_created_by.default = auth.user.id
    query = ((db.auth_user.id == db.auth_membership.user_id) & (db.auth_group.role=='con_orga_%s' % convention.id) & (db.auth_group.id==db.auth_membership.group_id))
    orgas = db(query).select()
    orga_set=[(o.auth_user.id, "%s %s" % (o.auth_user.first_name, o.auth_user.last_name)) for o in orgas]
    #db.ec_task.ec_assigned_to.requires=IS_IN_DB(db(query), 'auth_user.id', '%(first_name)s %(last_name)s')
    # changed because of https://gitlab.com/fermate35/easycon/-/issues/95
    db.ec_task.ec_assigned_to.requires=IS_IN_SET(orga_set)
    links=[dict(header=T('Action'), body=lambda row: CAT(   A(T('I do it!'), _class=('button'),_href=URL('orgatasks',args=[convention.ec_short_name,row.id], vars=dict(action='idoit'), user_signature=True)) if row.ec_assigned_to==None else '',
                                                            A(T("I'm working on it"), _class=('button'), _href=URL('orgatasks',args=[convention.ec_short_name,row.id], vars=dict(action='started'), user_signature=True)) if row.ec_status=='2' else '',
                                                            A(T('done!'), _class=('button'),_href=URL('orgatasks',args=[convention.ec_short_name,row.id], vars=dict(action='done'), user_signature=True)) if row.ec_status!='3' else '',
                                                            SPAN(A(IMG(_src=URL('static','images/email.png'),_alt=T('Send reminder'), _width="20px"), _class=('button'), _href=URL('orgatasks',args=[convention.ec_short_name,row.id], vars=dict(action='remind_orgatask'), user_signature=True)),
                                                                SPAN(EX('remind_one_orgatask'), _class="popup"),  _class="haspopup") if row.ec_status!='done' else '',
                                                             )),
            ] if not printview else []
    fields = [
                'ec_name',
                'ec_status',
                'ec_priority',
                'ec_start_date',
                'ec_due_date',
                'ec_assigned_to',
             ]
    dbfields=[db.ec_task[field] for field in fields]
    headers = {}
    for f in ['ec_start_date', 'ec_due_date', 'ec_assigned_to']:
        headers['ec_task.%s' % f] = short_label(f)

    thequery = db.ec_task.ec_id_convention==convention.id
    if 'only_me' in request.vars and request.vars.only_me=='1':
        query = query & (db.ec_task.ec_assigned_to==auth.user.id)
    if 'only_open' in request.vars and request.vars.only_open=='1':
        query = query & db.ec_task.ec_status.belongs(['1','2'])

    result=SQLFORM.grid(thequery,
        fields=dbfields,
        field_id=db.ec_task.id,
        args=request.args[:1],
        orderby=[db.ec_task.ec_status, db.ec_task.ec_due_date, db.ec_task.ec_start_date, db.ec_task.ec_priority, db.ec_task.ec_name],
        links=links,
        maxtextlengths = {'db.ec_task.ec_name': 150},
        csv=False,
        paginate=80,
        headers=headers,
        searchable=not printview,
        create=not printview, deletable=not printview,
        editable=not printview, details=not printview,
        )
    layout = 'print.html' if printview else 'layout.html'
    if configform and configform.process(formname='configform').accepted:
        session.flash = T('Configuration for "%(ec_name)s" changed') % {'ec_name' : convention.ec_name}
        redirect(URL('orgatasks',args=convention.ec_short_name))
    tasks = db(db.ec_task.ec_id_convention==convention.id).select()
    return dict(title=title, message=message, result=result,config=config, layout=layout, configform=configform, tasks=tasks)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('organize'))
def orga_standard_tasks():
    convention=_get_convention_conname(request.args(0))
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('This convention needs to be configured'), URL('convention',args=[convention.ec_short_name]))
    title = MV('orga_standard_tasks')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    fields = []
    for task in ORGA_STANDARD_TASKS:
        new = (db((db.ec_task.ec_id_convention==convention.id) & (db.ec_task.ec_name==task['ec_name'])).count() == 0)
        fields.append(Field(task['key'], 'boolean', default=new, label=task['ec_name'], comment=task['ec_description']))
    form = SQLFORM.factory(*fields,showid=False, submit_button = T('Create selected tasks'))
    if form.process().accepted:
        for task in ORGA_STANDARD_TASKS:
            if form.vars[task['key']]:
                del task['key']
                task['ec_id_convention'] = convention.id
                task['ec_created_by'] = auth.user.id
                db.ec_task.bulk_insert([task])
        redirect(URL('orgatasks', args=[convention.ec_short_name]))
    return dict(title=title, form=form)

## configure controller functions
@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('configure'))
def configure_main():
    convention=_get_convention_conname(request.args(0))
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, T('Configure'))
    configfields = db.ec_configuration.fields
    action = request.args(1) if request.args(1) in ['view','edit'] else 'view'
    what = request.args(2) if request.args(2) in ['convention','config'] else 'convention'
    # build conventionform
    conventionfields=['ec_name',
            'ec_organizer',
            'ec_short_name',
            'ec_convention_state',
            'ec_request_date',
            'ec_start_date',
            'ec_end_date',
            'ec_website',
            'ec_contact_email',
            'ec_hidden_email',
            'ec_description',
        ]
    db.ec_convention.ec_description.comment = XML(T('You can use %s to layout your description') % A('Markmin', _href=URL('convention','markmin', args=convention.ec_short_name), _target="_blank"))
    if convention.ec_convention_state=='requested' or convention.ec_id_configuration == None:
        db.ec_convention.ec_convention_state.writable=False
    else:
        db.ec_convention.ec_convention_state.requires = IS_IN_SET(['to_be_configured','to_be_deleted','active',], zero=None)
    conventionform = SQLFORM(db.ec_convention, convention.id,
        fields=conventionfields,
        readonly=((action=='view' or what!='convention')),
        showid=False,
        buttons = [TAG.button(T('Save'),_type="submit"),
                   TAG.button(T('Cancel'),_type="button",_onClick = "parent.location='%s' " % URL('configure_main',args=convention.ec_short_name))],
        )

    # build copyconfigform
    copyconfigform = None
    copy_tables = CONFIG_TABLES
    if convention.ec_convention_state=='to_be_configured':
        configs=db((db.ec_configuration.id==db.ec_convention.ec_id_configuration) & (db.ec_convention.id != convention.id) &
            (auth.accessible_query('configure',db.ec_convention))).select(db.ec_convention.ALL,db.ec_configuration.ALL,orderby=db.ec_convention.ec_name)
        options = [OPTION('%(ec_name)s' % configs[i].ec_convention, _value=str(configs[i].ec_convention.id)) for i in range(len(configs))]
        options.insert(0,OPTION(T('Create new configuration'), _value=-1))
        if convention.ec_id_configuration:
            copyconfigform = FORM(SELECT(_name='conid', *options),INPUT(_type='submit', _value=T('copy config or create new')),BR(),
                INPUT(_name='delete',value=False,_type='checkbox'),
                B('%s, %s' % (T('Replace my current configuration'), T('delete and replace all entries for %s') % ', '.join([str(MV('%ss' % table[3:])) for table in copy_tables]))))
        else:
            copyconfigform = FORM(SELECT(_name='conid', *options),INPUT(_type='submit', _value=T('copy config')))
    # build configform
    configform = None
    if convention.ec_id_configuration:
        config = db.ec_configuration[convention.ec_id_configuration]
        #configfields.insert(0,'ec_name')
        # remove configfields that are not needed
        configfields = configfields if config.ec_registration_perday else [c for c in configfields if not c.startswith('ec_registration_perday_')]
        configfields = configfields if config.ec_convention_requires_registration else [c for c in configfields if not (c.startswith('ec_registration_') or c.startswith('ec_email_registration_'))]
        configfields = configfields if config.ec_events else [c for c in configfields if not c.startswith('ec_event_')]
        configfields = configfields if config.ec_event_attendance else [c for c in configfields if not c.startswith('ec_event_attendee_')]
        configform = SQLFORM(db.ec_configuration, config.id, fields=configfields, readonly=((action=='view' or what!='config')), showid=False,comments=False)
    # build form to upload configuration
    # TODO upload_convention: should be changed to access rights if bootstrapping is done
    has_access = auth.has_membership(user_id=auth.user.id,role='easycon_admin')
    if has_access:
        configuploadform = SQLFORM.factory(
            Field('configfile', 'upload', label=T('Configuration file (JSON)')))
    else:
        configuploadform = None
    # process forms
    if conventionform.process(formname='conventionform', onvalidation=_validate_convention, keepvalues=True).accepted:
        session.flash = T('Configuration for "%(ec_name)s" changed') % {'ec_name' : convention.ec_name}
        redirect(URL('configure_main',args=convention.ec_short_name))
    if configform and configform.process(formname='configform').accepted:
        session.flash = T('Configuration for "%(ec_name)s" changed') % {'ec_name' : convention.ec_name}
        redirect(URL('configure_main',args=convention.ec_short_name))
    if configuploadform and configuploadform.process(formname='configuploadform') and configuploadform.vars.configfile:
        _redirect_message(_parse_configuration(configuploadform.vars.configfile), URL('configure_main',args=convention.ec_short_name))
    # copy configurations!
    if copyconfigform and copyconfigform.process(formname='copyconfigform').accepted:
        if convention.ec_id_configuration and not copyconfigform.vars.delete:
            response.flash = T('You have confirm that this will replace your current configuration')
        else:
            oldconfig = db.ec_configuration(convention.ec_id_configuration)
            if oldconfig: oldconfig.update_record(ec_name='Old configuration for %s' % convention.ec_name)
            if copyconfigform.vars.conid=='-1': # new configuration
                configid = db.ec_configuration.insert(ec_name='%s configuration' % convention.ec_name)
                db(db.ec_convention.id==convention.id).update(ec_id_configuration=configid)
                session.flash = T('New configuration created')
                redirect(URL('configure_main',args=convention.ec_short_name))
            else: # copy configuration from another convention
                copyconfig = db((db.ec_configuration.id==db.ec_convention.ec_id_configuration)&(db.ec_convention.id==copyconfigform.vars.conid)).select(db.ec_configuration.ALL).first().as_dict()
                copycon = db.ec_convention(copyconfigform.vars.conid)
                messages = []
                itemmap = {}
                for table in copy_tables:
                    db(db[table]['ec_id_convention']==convention.id).delete() # delete all old things
                    things = db(db[table]['ec_id_convention']==copycon.id).select().as_list() # get things from other convention
                    if things: messages.append(MV('%ss' % table[3:]))
                    for thing in things:
                        thing['ec_id_convention']=convention.id
                        oldid = thing['id']
                        for field in list(thing.keys()):
                            if not field.startswith('ec_'):
                                del thing[field] # will delete id and all virtual fields
                        newid = db[table].insert(**thing)
                        if table=='ec_buyitem': itemmap[oldid]=newid
                        if table=='ec_buyitemrule':
                            hasruleentries = db(db.ec_has_rule.ec_id_rule==oldid).select()
                            for entry in hasruleentries:
                                if db.ec_buyitem(entry.ec_id_buyitem): # if item exists
                                    db.ec_has_rule.insert(ec_id_rule=newid, ec_id_buyitem=itemmap[entry.ec_id_buyitem], ec_list_order=entry.ec_list_order)
                        if table=='ec_in_series':
                            admingroup = auth.id_group('con_admin_%s' % convention.id)
                            auth.add_permission(admingroup, 'configure', 'ec_series', thing['ec_id_series'])
                        if table=='ec_task':   # set all tasks to "2do"
                            db(db.ec_task.id == newid).update(ec_status=2)

                # copy all dates and times
                datefields = ['ec_registration_period_start',
                              'ec_registration_period_stop',
                              'ec_event_registration_period_start',
                              'ec_event_registration_period_stop',
                              'ec_event_attendee_registration_period_start',
                              'ec_event_attendee_registration_period_stop',
                              'ec_orgatask_reminder_start']
                if convention.ec_start_date and copycon.ec_start_date:
                    for field in datefields:
                        if copyconfig[field]:
                            copyconfig[field]=copyconfig[field]+(convention.ec_start_date-copycon.ec_start_date)
                    for slot in db(db.ec_timeslot.ec_id_convention==convention.id).select():
                        if slot.ec_start_time:
                            slot.update_record(ec_start_time=slot.ec_start_time+(convention.ec_start_date-copycon.ec_start_date))
                        if slot.ec_end_time:
                            slot.update_record(ec_end_time=slot.ec_end_time+(convention.ec_start_date-copycon.ec_start_date))
                    for task in db(db.ec_task.ec_id_convention==convention.id).select():
                        if task.ec_start_date:
                            task.update_record(ec_start_date=task.ec_start_date+(convention.ec_start_date-copycon.ec_start_date))
                        if task.ec_due_date:
                            task.update_record(ec_due_date=task.ec_due_date+(convention.ec_start_date-copycon.ec_start_date))
                    for item in db(db.ec_buyitem.ec_id_convention==convention.id).select():
                        if item.ec_from_date:
                            item.update_record(ec_from_date=item.ec_from_date+(convention.ec_start_date-copycon.ec_start_date))
                        if item.ec_to_date:
                            item.update_record(ec_to_date=item.ec_to_date+(convention.ec_start_date-copycon.ec_start_date))
                    for table in db(db.ec_table.ec_id_convention==convention.id).select():
                        if table.ec_available_begin:
                            table.update_record(ec_available_begin=table.ec_available_begin+(convention.ec_start_date-copycon.ec_start_date))
                        if table.ec_available_end:
                            table.update_record(ec_available_end=table.ec_available_end+(convention.ec_start_date-copycon.ec_start_date))
                else:
                    messages.append(T('Did not copy dates and times, since one of the conventions (or both) had no start date configured', lazy=False))
                del copyconfig['id'] # we will create a new id
                copyconfig['ec_name'] = '%s: configuration (copied from %s)' % (convention.ec_name, copycon.ec_name)
                configid = db.ec_configuration.insert(**copyconfig)
                db(db.ec_convention.id==convention.id).update(ec_id_configuration=configid)
                message = T('Configuration copied', lazy=False)
                if messages: message += ' ' + T('and') + ' ' + ', '.join([str(m) for m in messages])
                session.flash = message
                redirect(URL('configure_main',args=convention.ec_short_name))
    otherconventions = db((db.ec_convention.ec_id_configuration==convention.ec_id_configuration)&(db.ec_convention.id!=convention.id)).select(db.ec_convention.ALL)
    items=db(db.ec_buyitem.ec_id_convention==convention.id).select()
    timeslots=db(db.ec_timeslot.ec_id_convention==convention.id).select()
    return dict(conventionform=conventionform,copyconfigform=copyconfigform,convention=convention,configform=configform,action=action,otherconventions=otherconventions,items=items,timeslots=timeslots,configuploadform=configuploadform)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('configure'))
def configure():
    message=''
    convention=_get_convention_conname(request.args(0))
    what=request.args(1) or None
    config=db.ec_configuration[convention.ec_id_configuration]
    if not config: _redirect_message(T('You have to copy or create a configuration for your convention'), URL('configure_main',args=convention.ec_short_name))
    if what not in ['basic_config','emails','registration','events','location','design']:
        _redirect_message(T('Not implemented yet'), URL('configure_main',args=convention.ec_short_name))
    title='%s: %s' % (T('Configure'),MV(what))
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    configfields=[]
    if what == 'basic_config':
        configfields = [
            'ec_convention_public',
            'ec_convention_requires_registration',
            'ec_events',
            'ec_event_public',
            'ec_event_time_slots',
            'ec_convention_requires_services',
        ]
    else:
        key = what[:-1] if what in ['emails','events'] else what
        for field in db.ec_configuration.fields:
            if field.startswith('ec_%s' % key):
                configfields.append(field)
            if what=='registration' and field in ['ec_convention_requires_registration','ec_convention_public']:
                configfields.append(field)
            if what=='events' and field in ['ec_convention_public']:
                configfields.append(field)
    if not configfields: _redirect_message(T('Nothing to configure'), URL('configure_main',args=convention.ec_short_name))
    configform = SQLFORM(db.ec_configuration, config.id, fields=configfields, showid=False,comments=True,submit_button=T('Save'), upload=URL('default','download'))
    if configform and configform.process().accepted:
        session.flash = T('Configuration for "%(ec_name)s" changed') % {'ec_name' : convention.ec_name}
        redirect(URL('configure_main',args=convention.ec_short_name))
    return dict(title=title,convention=convention,message=message,what=what,configform=configform)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('configure'))
def export_config(): # export configuration
    convention=_get_convention_conname(request.args(0))
    title=MV('export_config')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    config=db.ec_configuration[convention.ec_id_configuration]
    if not config:
        session.flash = T('You have to copy or create a configuration for your convention')
        redirect(URL('configure_main',args=convention.ec_short_name))
    result = OrderedDict()
    # start with header
    import subprocess
    try: # git branch --verbose | grep '\*' in application directory
        appversion = subprocess.check_output(r"git branch --verbose | grep \*", shell=True, cwd=request.folder).strip()[2:]
    except Exception as e:
        appversion = 'Unknown'
    try: # git branch --verbose | grep '\*' in web2py directory
        w2pversion = subprocess.check_output(r"git branch --verbose | grep \*", shell=True).strip()[2:]
    except Exception as e:
        w2pversion = 'Unknown'
    result['description']="Exported configuration for convention %s" % convention.ec_name
    result['created_by_user']="%s %s" % (auth.user.first_name, auth.user.last_name)
    result['created_at_date']=request.now
    result['created_by_application']=request.application
    result['created_by_application_version']=appversion
    result['created_by_w2p_version']=w2pversion
    # add convention and configuration data
    result['ec_convention']=convention.as_dict()
    del result['ec_convention']['id']
    del result['ec_convention']['ec_id_configuration']
    result['ec_configuration']=config.as_dict()
    del result['ec_configuration']['id']
    # add all tables that are associated with a convention (and would be copied, too)
    for table in CONFIG_TABLES:
        tableresult = db(db[table].ec_id_convention==convention.id).select().as_dict()
        if tableresult:
            for entry in tableresult:
                for key in ['id','ec_id_convention']:
                    if key in tableresult[entry]:
                        del tableresult[entry][key]
            result[table]=tableresult
    return response.json(result)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('configure'))
def items():
    convention=_get_convention_conname(request.args(0))
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, T('Buyable items'))
    config=db.ec_configuration[convention.ec_id_configuration]
    if not config:
        session.flash = T('You have to copy or create a configuration for your convention')
        redirect(URL('configure_main',args=convention.ec_short_name))
    fields=[
            'ec_order',
            'ec_name',
            #'ec_is_heading',
            'ec_is_preselected',
            'ec_is_perday',
            'ec_capacity',
            'ec_price',
            'ec_has_waitlist',
            # 'ec_max',
            'ec_min',
            ]
    headers={}
    for f in fields:
        headers['ec_buyitem.%s' % f] = short_label(f)
    db.ec_buyitem.id.readable,db.ec_buyitem.id.writable=False,False
    db.ec_buyitem.ec_id_convention.readable,db.ec_buyitem.ec_id_convention.writable=False,False
    db.ec_buyitem.ec_max.readable,db.ec_buyitem.ec_max.writable=False,False
    db.ec_buyitem.ec_id_convention.default=convention.id
    db.ec_buyitem.ec_id_timeslot.requires = IS_EMPTY_OR(IS_IN_SET(_get_timeslots(convention)))
    dbfields=[getattr(db.ec_buyitem,field) for field in fields]
    itemform=SQLFORM.grid(db.ec_buyitem.ec_id_convention==convention.id,
        fields=dbfields,
        args=request.args[:1],
        headers=headers,
        maxtextlengths = {'ec_buyitem.ec_name': 150},
        orderby=[db.ec_buyitem.ec_order,db.ec_buyitem.ec_is_heading,db.ec_buyitem.ec_name],
        searchable=False, csv=False,
        links=[dict(header=T('Registration rules'),body=lambda row: _get_rule_list(row.id)),],
        paginate=100,
        onvalidation=_validate_buyitem,
        selectable=[(T('Add rule for selected items'),lambda ids : redirect(URL('convention', 'newrule', args=convention.ec_short_name, vars=dict(items=ids)))),]
        )
    return dict(itemform=itemform)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('configure'))
def rules():
    session.back=None if session.back=='rules' else session.back
    convention=_get_convention_conname(request.args(0))
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, T('Rules'))

    config=db.ec_configuration[convention.ec_id_configuration]
    if not config:
        session.flash = T('You have to copy or create a configuration for your convention')
        redirect(URL('configure_main',args=convention.ec_short_name))
    db.ec_buyitemrule.id.readable,db.ec_buyitemrule.id.writable=False,False
    #db.ec_buyitemrule.ec_message.readable,db.ec_buyitemrule.ec_message.writable=False,False
    db.ec_buyitemrule.ec_id_convention.readable,db.ec_buyitemrule.ec_id_convention.writable=False,False
    db.ec_buyitemrule.ec_id_convention.default=convention.id
    form=SQLFORM.grid(db.ec_buyitemrule.ec_id_convention==convention.id,
        args=request.args[:1],searchable=False, csv=False, create=False, deletable=True,
        links=[
            dict(header=T('for')+' '+T('buyable items'),body=lambda row: _get_item_list(row.id)),
            #dict(header=T('Action'),body=lambda row: A(T('edit'),_href=URL('rule',args=[convention.ec_short_name,row.id]))),
        ])
    return dict(form=form)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('configure'))
def rule():
    convention=_get_convention_conname(request.args(0))
    rule=db.ec_buyitemrule[request.args(1)] if (request.args(0) and request.args(1) and request.args(1).isdigit()) else None
    if not rule and convention:
        session.flash=T('Invalid rule')
        redirect(URL('convention','rules',args=convention.ec_short_name))
    elif not (rule and convention):
        session.flash=T('Invalid rule')
        session.back='rules'
        redirect(URL('convention','index'))
    response.title='Easy-Con %s %s: %s %s' % (T('for'), convention.ec_name, T('Rule'),rule.ec_name)
    action = request.args(2) if request.args(2) in ['view','edit','delete'] else 'view'
    message=action+' rule no '+str(rule.id)
    db.ec_buyitemrule.id.readable=db.ec_buyitemrule.id.writable=False
    db.ec_buyitemrule.ec_id_convention.readable=db.ec_buyitemrule.ec_id_convention.writable=False
    db.ec_buyitemrule.ec_id_convention.default=convention.id
    if action=='view':
        form = SQLFORM(db.ec_buyitemrule, rule.id, readonly=(action=='view'),showid=False,_action='view')
        extra=TR(TD('Buyable s'),TD(_get_item_list(rule.id)),TD())
    elif action=='edit':
        form = SQLFORM(db.ec_buyitemrule, rule.id, deletable=True, showid=False,_action='edit')
        extra=_get_item_select(convention)
    form[0].insert(-1,extra)
    return dict(message=message,form=form)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('configure'))
def newrule():
    convention=_get_convention_conname(request.args(0))
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, T('New rule'))
    itemids=request.get_vars['items']
    if not isinstance(itemids, list): itemids = [itemids,]
    db.ec_buyitemrule.id.readable=db.ec_buyitemrule.id.writable=False
    db.ec_buyitemrule.ec_id_convention.readable=db.ec_buyitemrule.ec_id_convention.writable=False
    db.ec_buyitemrule.ec_id_convention.default=convention.id
    fields=[
            'ec_type',
            'ec_name',
            'ec_number',
            'ec_message',
        ]
    form = SQLFORM(db.ec_buyitemrule, fields=fields, deletable=True, showid=False)
    if form.process(keepvalues=True).accepted:
        ruleid = form.vars.id
        count=0
        for itemid in itemids:
            count=count+1
            db.ec_has_rule.insert(ec_id_buyitem=itemid, ec_id_rule=ruleid,ec_list_order=count)
        redirect(URL('convention','items',args=convention.ec_short_name))
    items=db(db.ec_buyitem.id.belongs(itemids)).select()
    return dict(items=items, form=form)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('configure'))
def extradata():
    convention=_get_convention_conname(request.args(0))
    title=MV('extradata')
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    config=db.ec_configuration[convention.ec_id_configuration]
    if not config:
        session.flash = T('You have to copy or create a configuration for your convention')
        redirect(URL('configure_main',args=convention.ec_short_name))
    message=''
    db.ec_extrafield.id.readable,db.ec_extrafield.id.writable=False,False
    db.ec_extrafield.ec_id_convention.readable,db.ec_extrafield.ec_id_convention.writable=False,False
    db.ec_extrafield.ec_comment.readable,db.ec_extrafield.ec_comment.writable=False,False
    db.ec_extrafield.ec_id_convention.default=convention.id

    form=SQLFORM.grid(db.ec_extrafield.ec_id_convention==convention.id, field_id=db.ec_extrafield.id,
        args=request.args[:1],
        searchable=False, csv=False,
        paginate=100,
        )

    return dict(title=title,convention=convention,message=message,form=form)

@auth.requires(lambda: _logged_in_and_privacy_consent() and _check_convention_permission('configure'))
def promote():
    session.back = ''
    convention=_get_convention_conname(request.args(0))
    group = request.vars.what if request.vars.what in ['admin','orga'] else 'orga'
    title=T('Manage %s group' % group.capitalize())
    response.title='Easy-Con %s %s: %s' % (T('for'), convention.ec_name, title)
    config=db.ec_configuration(convention.ec_id_configuration)
    if not config: _redirect_message(T('You have to copy or create a configuration for your convention'), URL('configure_main',args=[convention.ec_short_name]))
    confirm_form = ''
    confirm_message = ''
    dbgroup = db(db.auth_group.role=='con_%s_%s' % (group, convention.id)).select().first()
    if request.vars.action=='promote':
        user = db(db.auth_user.email.lower()==request.vars.email.lower()).select().first()
        if user:
            auth.add_membership(dbgroup, user.id)
            _redirect_message(T('User %(first_name)s %(last_name)s promoted to group') % user.as_dict(), URL('convention', 'promote', args=convention.ec_short_name, vars=dict(what=group)))
        else:
            _redirect_message(T('No user with this e-mail registered'), URL('convention', 'promote', args=convention.ec_short_name, vars=dict(what=group)))
    elif request.vars.action=='depromote':
        user = db.auth_user(request.vars.user)
        if user:
            if user.id == auth.user.id:
                confirm_message = T('Do you really want to remove yourself from %s group?' % group)
                confirm_form = FORM.confirm(T('Yes'), {T('No') : URL('convention', 'promote', args=convention.ec_short_name, vars=dict(what=group))})
                if confirm_form.accepted:
                    auth.del_membership(dbgroup, user.id)
                    message=T('You removed yourself from %s group' % group)
                    if group=='admin':
                        _redirect_message(message, URL('convention', args=convention.ec_short_name))
                    else:
                        _redirect_message(message, URL('convention', 'promote', args=convention.ec_short_name, vars=dict(what=group)))
            else:
                auth.del_membership(dbgroup, user.id)
                _redirect_message(T('User %(first_name)s %(last_name)s was removed from group') % user.as_dict(), URL('convention', 'promote', args=convention.ec_short_name, vars=dict(what=group)))
        else:
            response.flash=T('Invalid user')
    elif request.vars.action=='email':
        user = db.auth_user(request.vars.user)
        if user:
            _redirect_message('Send e-mail to %(first_name)s %(last_name)s' % user.as_dict(), URL('convention', 'emails', args=convention.ec_short_name, vars=dict(emailtos=user.email,autoemail='promote_%s_group' % group)))
        else:
            response.flash=T('Invalid user')
    query = (db.auth_user.id==db.auth_membership.user_id)&(db.auth_membership.group_id==db.auth_group.id)&(db.auth_group.role=='con_%s_%s'%(group,convention.id))
    if len(db(query).select())>0:
        links=[dict(header=T('Action'),\
                    body=lambda row: CAT(A(T('Delete from group'), _class=('button'),
                                         _href=URL('promote',args=convention.ec_short_name, vars=dict(what=group, action='depromote', user=row.auth_user.id), user_signature=True)),
                                         ' ',
                                         A(T('Send e-mail'), _class=('button'),
                                         _href=URL('promote',args=convention.ec_short_name, vars=dict(what=group, action='email', user=row.auth_user.id), user_signature=True)))),]
    else: links=None
    searchform = FORM(T('E-Mail of user to be promoted')+':  ', INPUT(_name='email', requires=IS_EMAIL()), INPUT(_type='submit', _value=T('Add to group')))
    if db.auth_user.nick_name.label.endswith('*'):
        db.auth_user.nick_name.label = db.auth_user.nick_name.label[:-1]
    db.auth_user.id.readable=db.auth_user.id.writable=False
    groupform = SQLFORM.grid(query,
        fields=[db.auth_user.nick_name, db.auth_user.first_name, db.auth_user.last_name, db.auth_user.email,db.auth_user.id],
        maxtextlength=300,
        deletable=False, details=False, create=False, searchable=False, csv=False, editable=False,
        links=links,
        args=request.args[:1])
    if searchform.process().accepted:
        redirect(URL('promote', args=convention.ec_short_name, vars=dict(action='promote', email=request.vars.email, what=group), user_signature=True))
    return dict(title=title, confirm_message=confirm_message, confirm_form=confirm_form, searchform=searchform, groupform=groupform, group=group)


## all _helper functions in models/helpers.py
